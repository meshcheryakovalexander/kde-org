KDE Security Advisory: start_kdeinit multiple vulnerabilities
Original Release Date: 2008-04-26
URL: http://www.kde.org/info/security/advisory-20080426-2.txt

0. References
        CVE-2008-1671


1. Systems affected:

	start_kdeinit of KDE 3.x as of KDE 3.5.5 or newer. KDE 4.0
	and newer is not affected. Only Linux platform is affected.


2. Overview:

	start_kdeinit is a wrapper to launch kdeinit with a lower OOM
	score on Linux. This helper is used to ensure that a
	single KDE application triggering the Linux kernel OOM killer
	does not kill the whole KDE session. By default,
	start_kdeinit is installed as setuid root. The start_kdeinit
	processing of user-influenceable input is faulty. We'd like
	to thank Helmut Grohne for reporting the issue initially.

3. Impact:

        If start_kdeinit is installed as setuid root, a local user
        might be able to send unix signals to other processes, cause
        a denial of service or even possibly execute arbitrary code.

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        A patch for KDE 3.5.5 - KDE 3.5.9 is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        9d99d5f02b696e7a493836f285a319da  post-kde-3.5.5-kinit.diff
