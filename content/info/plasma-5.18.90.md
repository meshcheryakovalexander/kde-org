---
version: "5.18.90"
title: "KDE Plasma 5.18.90, Beta Release"
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
---

This is a Beta release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the [Plasma 5.18.90 announcement](/announcements/plasma-5.18.90).
