---
aliases:
- /info/releases-20.08.2
announcement: /announcements/releases/2020-10-apps-update
date: 2020-10-09
title: "20.08.2 Releases Source Info Page"
layout: releases
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
---
