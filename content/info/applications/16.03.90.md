---
title: "KDE Applications 16.03.90 Info Page"
announcement: /announcements/announce-applications-16.04-rc
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
