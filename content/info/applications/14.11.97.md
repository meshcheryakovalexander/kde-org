---
title: "KDE Applications 14.11.97 Info Page"
announcement: /announcements/announce-applications-14.12-rc
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
