---
title: "KDE Applications 16.08.2 Info Page"
announcement: /announcements/announce-applications-16.08.2
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
