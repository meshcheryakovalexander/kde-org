---
title: "KDE Software Privacy Policy"
---


KDE is a Community which has been formed by its collective members for the purpose of the development and promotion of open source computer software. This policy governs how the software we produce will handle information on your personal devices.

### Legal Entity

The KDE Community is represented in legal and financial matters by the K Desktop Environment e.V. (known as the KDE e.V.), a registered non-profit in Germany.
More information on the KDE e.V. can be found on its <a href="https://ev.kde.org/whatiskdeev">website</a>.


### Scope

While this policy applies to all software produced by the KDE Community, it is possible that where our software is obtained from third party distributors that they have modified our software to no longer respect this privacy policy.
<br /><br />
We recommend that users check with their distributor prior to using our software to ensure that they have not made any modifications which have a privacy impact.


### General Principle

As a general rule, software produced by the KDE Community does not transmit or otherwise transfer information from end-users devices except as a result of an explicit user action. Examples of this would include visiting webpages, sending emails, and downloading new content (such as desktop wallpapers).
<br /><br />
In some instances, our software may also provide the option for periodic automated checks, such as to check for new email or system software updates. Where these periodic automatic checks do exist, the option will be provided to disable them and only the minimum information needed to perform the periodic check will be transmitted.
<br /><br />
Where information from other services is retrieved by our software, such as email messages or files from cloud storage services, this information is never transferred from the device except as the result of explicit user action. An example of this would be the user using Dolphin to copy files from one cloud storage provider to another.


### Get Hot New Stuff Functionality

Certain software produced by the KDE Community may offer functionality to retrieve new content and addons, through a system known as "Get Hot New Stuff". This functionality is only activated when accessed by the user, and does not conduct any activity in the background. As part of this functionality, details such as your search terms and sort order will be sent to the KDE Store, which will handle it in accordance with its <a href="https://store.kde.org/privacy">privacy policy</a>.


### Telemetry

Some software produced by the KDE Community may include telemetry components, which provides details on the device it's running on to us. Where this functionality exists, it will always operate on an opt-in basis and be disabled by default, with the ability to change your preferences at any time.
<br /><br />
With regards to information, only details about the device itself (such as the software versions installed and its hardware specifications, but not identifying details such as a serial number) along with details about how our software is used (such as whether certain features are enabled and what plugins have been installed) are transmitted.
<br /><br />
Details on the exact information being gathered will be provided by the application prior to the user being given the option to enable Telemetry functionality and any changes to this will be included in the release announcements for our software.
<br /><br />
At no point will any data which could be used to identify the device or its user be transmitted from the system, and we will not make use of any device, installation or user specific identifiers.
<br /><br />
We will cease the transmission of any information we no longer have any use for, and any such information already in our possession will be securely destroyed at that point.
<br /><br />
Following the transmission of the telemetry information to us, it will be stored and accessed in accordance with the KDE.org Privacy Policy. Telemetry information will not be sent to any other party by our software.
<br /><br />
More information on the requirements our applications are subject to with regards to Telemetry can be found in our <a href="https://community.kde.org/Policies/Telemetry_Policy">Telemetry Policy</a>.


### Changes in the Privacy Policy

We may change the privacy policy when we add new services, retire existing services and update existing ones. When this is done, except where the updates are minor (such as correcting typographical errors) we will post an announcement regarding the update on our website and detail the impact of the changes on you.


### Contact information

Should you have any queries or other matters which aren't addressed above, please feel free to contact us as described below.
<br /><br />
For queries relating to updating or removing information, or relating to how our services operate and publish data, please contact the Sysadmin team in the first instance.
This can be done by sending an email to <a href="mailto:sysadmin@kde.org">sysadmin@kde.org</a>, or alternatively by filing a <a href="https://go.kde.org/systickets">ticket</a> (KDE Identity account required)
<br /><br />
For all other queries, please contact the KDE e.V. Board. They can be reached by email at <a href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>.
<br /><br />
Alternatively, should you wish to send your query via physically addressed mail, it can be sent to our office who will bring it to the attention of the Board.
<br/>
Our current postal address can be found on the <a href="https://ev.kde.org/contact">Contact section of the KDE e.V. website</a>.



