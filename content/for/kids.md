---
title: KDE for Kids
description: KDE provides a wide array of applications for children covering both educational applications and games.
layout: kids
scssFiles:
- /scss/for/kids.scss
download: Download
kturberling: "KTuberling a simple construction game suitable for children and adults alike. The idea is simple and compelling: stick the features onto the potato and build your own character!"
klettres: KLettres helps you learn an alphabet in a new language and then read simple syllables. Appropriate for young children aged from two and up, and for adults wanting to learn the basics of a foreign language.
kolourpaint: Unleash your creativity with this easy to master paint program.
games: A collection of games for kids from 6 to 99 years old!
gamestitle: Games
---

{{< container class="text-center text-small" >}}

## GCompris

An educational software suite used by teachers and parents worldwide. GCompris includes a large number of activities for children aged 2 to 10, covering everything from basic numeracy and alphabetization activities, to math and logic gates used in electronics.

{{< for/app-links download="https://gcompris.net/downloads-en.html" learn="https://gcompris.net/" centered="true" >}}

![](https://cdn.kde.org/screenshots/gcompris/gcompris.png)

{{< /container >}}
