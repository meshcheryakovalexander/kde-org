---
aliases:
- ../../plasma-5.8.6
changelog: 5.8.5-5.8.6
date: 2017-02-21
layout: plasma
youtube: LgH1Clgr-uE
figure:
  src: /announcements/plasma/5/5.8.0/plasma-5.8.png
  class: text-center mt-4
asBugfix: true
---

- Avoid a crash on Kwin decoration KCM teardown. <a href="https://commits.kde.org/kwin/70d2fb2378d636ef6d052da08417b27c99182fb0">Commit.</a> See bug <a href="https://bugs.kde.org/373628">#373628</a>
- [Folder View] Fix right click erroneously opening files. <a href="https://commits.kde.org/plasma-desktop/d2fde361d3c8fb40fb6c1e53e4178042799b6691">Commit.</a> Fixes bug <a href="https://bugs.kde.org/360219">#360219</a>
- Fix regression in which the Save dialog appears as an Open dialog. <a href="https://commits.kde.org/plasma-integration/87b27476cc8a3865994da066ce06a3e836462719">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/129732">#129732</a>
