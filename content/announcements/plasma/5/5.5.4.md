---
aliases:
- ../../plasma-5.5.4
changelog: 5.5.3-5.5.4
date: 2016-01-26
layout: plasma
figure:
  src: /announcements/plasma/5/5.5.0/plasma-5.5.png
asBugfix: true
---

- Many improvements and refactoring to notification positioning making them appear in the right place for multi-screen use.
