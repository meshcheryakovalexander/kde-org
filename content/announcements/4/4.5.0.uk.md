---
aliases:
- ../4.5
date: '2010-08-10'
custom_about: true
custom_contact: true
title: KDE випущено Платформу розробки, набір програм та робочі простори Плазми версії
  4.5.0
---

<p align="justify">
Сьогодні було випущено нову версію (4.5.0) Платформи розробки KDE та робочих просторів Плазми для стаціонарних та портативних комп’ютерів, а також нові версії багатьох програм. У цьому випуску ви зможете скористатися роботою команди KDE з покращення зручності у користуванні, швидкодії та стабільності багатьох розроблених раніше можливостей та технологій. Нижче наведено три окремих оголошення для кожного з продуктів KDE: Платформи розробки, збірки програм та робочих просторів Плазми.

<h2>У новій Платформі розробки KDE 4.5.0 ви зможете скористатися покращеною швидкодією, стабільністю, новим високошвидкісним кешем та підтримкою WebKit</h2>

<a href="./platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="Платформа розробки KDE 4.5.0"/>
</a>

<p align="justify">
 Сьогодні командою KDE було випущено Платформу розробки KDE версії 4.5.0. Цей випуск характеризується покращеннями у швидкодії та стабільності. Нову версію <b>KSharedDataCache</b> оптимізовано для пришвидшення доступу до даних, що зберігаються на диску, зокрема піктограм. У новій версії бібліотеки <b>WebKit</b> передбачено інтеграцію з параметрами мережі, системою зберігання паролів та іншими можливостями Konqueror. <a href="./platform"><b>Ознайомитися з повним текстом повідомлення</b></a>
</p>

<h2>Плазма для стаціонарних та портативних комп’ютерів, версія 4.5.0: зручніше користування</h2>
<p align="justify">

<a href="./plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="Робочі простори Плазма KDE 4.5.0" />
</a>

Сьогодні командою KDE випущено робочі простори Плазми для стаціонарних та портативних комп’ютерів версії 4.5.0. Користування Плазмою для стаціонарних комп’ютерів стало набагато зручнішим. Спрощено прийоми роботи з сповіщеннями та завданнями. З області сповіщень було усунуто зайві візуальні елементи, обробка сповіщень від програм стала одноріднішою за рахунок ширшого використання протоколу сповіщення Freedesktop.org, вперше реалізованого у попередній версії Плазми. <a href="./plasma"><b>Ознайомитися з повним текстом повідомлення</b></a>

</p>

<h2>Програми KDE 4.5.0: зручність у користуванні</h2>
<p align="justify">

<a href="./applications">
<img src="/announcements/4/4.5.0/images/applications.png" class="app-icon float-left m-3" alt="Програми KDE 4.5.0"/>
</a>

Сьогодні командою KDE було випущено нову версію збірки програм KDE. У цій збірці покращено роботу освітніх програм, інструментів та графічних програм. У віртуальному глобусі Marble реалізовано пошук маршрутів за допомогою служби OpenRouteService. Konqueror, переглядач Інтернету KDE, тепер можна налаштувати на використання WebKit за допомогою компонента KWebKit, встановити який можна зі сховища додаткових програм KDE. <a href="./applications"><b>Ознайомитися з повним текстом повідомлення</b></a>

</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/kde-general45.png">
	<img src="/announcements/4/4.5.0/thumbs/kde-general45.png" class="img-fluid" alt="The KDE Plasma Netbook in 4.5 RC1">
	</a> <br/>
	<em>Платформа розробки KDE, програми та Плазма для стаціонарних та мобільних комп’ютерів версії 4.5</em>
</div>
<br/>

<h4>
    Розкажіть іншим і будьте свідком результатів: мітка «KDE»
</h4>
<p align="justify">
Спільнота KDE буде вдячна вам за <strong>поширення інформації</strong> у соціальних мережах. Надсилайте статті на сайти новин, використовуйте канали обміну повідомленнями, зокрема delicious, digg, reddit, twitter, identi.ca. Вивантажуйте знімки вікон на служби зберігання зображень, зокрема Facebook, FlickR, ipernity і Picasa, і надсилайте їх до відповідних груп. Створюйте відеосюжети і вивантажуйте їх на YouTube, Blip.tv, Vimeo та інші служби. Не забудьте позначити вивантажений матеріал <em>міткою <strong>kde</strong></em>, щоб зацікавленим особам було легше його знайти, а також щоб полегшити команді KDE створення звітів щодо поширення оголошення про випуск KDE SC 4.5
. <strong>Допоможіть нам поширити інформацію, станьте учасником цієї події!</strong></p>

<p align="justify">
Ви можете спостерігати за подіями навколо випуску 4.5 у режимі реального часу у соціальній мережі за допомогою
<a href="http://buzz.kde.org"><strong>подачі новин Спільноти KDE</strong></a>. На цьому сайті будуть збиратися всі записи з identi.ca, twitter, youtube, flickr, picasaweb, блогів та багатьох інших сайтів соціальних мереж у режимі реального часу. Подачу новин можна знайти на <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/search?s=kde45"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/search?q=kde45"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde45"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde45"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde45"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde45"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde45"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
