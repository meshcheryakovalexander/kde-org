---
aliases:
- ../4.8
- ../4.8.fa
custom_about: true
custom_contact: true
date: '2012-01-25'
title: محیطهای کاری پلاسمای کی‌دی‌ای، برنامه‌ها و سکوی ۴٫۸ تجربه‌ی کاربری را افزایش
  میدهند
---

<p>
تیم توسعه‌ی کی‌دی‌ای مفتخر است مجموعه‌ای از انتشارهای خود، شامل به‌روزرسانی‌های اساسی در محیط‌کاری پلاسمای کی‌دی‌ای، برنامه‌های کی‌دی‌ای و سکوی کی‌دی‌ای را اعلام کند. نسخه ۴٫۸ شامل تعداد بسیار زیادی قابلیت جدید و بهبود در پایداری و کارایی است.
</p>

<div class="text-center">
	<a href="/announcements/4/4.8.0/plasma-desktop-4.8.png">
	<img src="/announcements/4/4.8.0/thumbs/plasma-desktop-4.8.png" class="img-fluid" alt="پلاسما و برنامه‌های ۴٫۸">
	</a> <br/>
	<em>پلاسما و برنامه‌های ۴٫۸</em>
</div>
<br/>

<h3>
<a href="./plasma">
محیط‌های کاری کی‌دی‌ای ۴٫۸ مدیریت برق سازگاری پذیر را به ارمغان آورده
</a>
</h3>

<p>

<a href="./plasma">
<img src="/announcements/4/4.8.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.8" />
</a>

￼مهمترین چیزهایی که محیط کاری پلاسما دربردارند عبارتند از <a href="http://philipp.knechtges.com/?p=10">بهینه‌سازی «کویین»</a> (مدیر پنجره)، طراحی مجدد مدیریت برق و یکپارچه‌‌سازی میزکارهای پلاسما. اولین ویجت‌های برپایه‌ی QtQuick به‌صورت پیش‌فرض همراه میزکار پلاسما نصب می‌شوند. در آینده بیشتر از آن‌ها خواهید دید. <a href="./plasma">اعلان محیط‌های کاری پلاسما را کامل بخوانید</a>.

<br />
</p>

<h3>
<a href="./applications">
برنامه‌های کی‌دی‌ای ۴٫۸ مدیریت پرونده‌ی سریعتر و مقیاس‌پذیرتری را پیشکش میکنند
</a>
</h3>

<p>
<a href="./applications">
<img src="/announcements/4/4.8.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.8"/>
</a>
￼برنامه‌های کی‌دی‌ای که امروز منتشر شدند شامل <a href="http://userbase.kde.org/Dolphin">دلفین</a> با موتور جدید نمایشش،نسخه‌ی جدید <a href="http://userbase.kde.org/Kate">Kate</a> با قابلیتها و بهینه‌سازی‌ها، <a href="http://userbase.kde.org/Gwenview">Gwenview</a> با پیشرفت در نمایش و تواناییها میشوند. تلپاتی کی‌دی‌ای اولین مراحل بتلی خود را پشت‌سر میگذارد. قابلیت‌های جدید <a href="http://userbase.kde.org/Marble/en">Marble</a> همچنان در حال بیشتر شدن هستند که در میان آن‌ها میتوان به نمایه (پروفایل) ارتفاع، ردیابی ماهواره و پیوستگی با <a href="http://userbase.kde.org/Plasma/Krunner">Krunner</a> اشاره کرد. <a href="./applications">متن کامل اعلان برنامه‌های کی‌دی‌ای را بخوانید</a>.
<br />
</p>

<h3>
<a href="./platform">
سکوی کی‌دی‌ای ۴٫۸ همکاری اجزا را بهبود بخشیده و مولفه‌های لمسی (Touch-Friendly) را معرفی میکند
</a>
</h3>

<p>
<a href="./platform">
<img src="/announcements/4/4.8.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.8"/>
</a>
￼سکوی کی‌دی‌ای پایه‌ای را برای نرم‌افزارهای کی‌دی‌ای فراهم میکند. نرم‌افزارهای کی‌دی‌ای اکنون از هر زمان دیگری پایدارترند. علاوه بر بهبود در پایداری و رفع اشکالات، سکوی ۴٫۸ ابزارهای بهتری را برای ￼ساخت رابط کاربری سیال و سازگار با فن‌آوری لمسی، یکپارچگی با دیگر مکانیزم‌های ذخیره رمزعبور در سامانه و بستر پایه برای تعامل قدرتمند با افراد دیگری که از سکوی تلپاتی کی‌دی‌ای استفاده میکنند را فراهم آورده.برای اصلاعات بیشتر <a href="./platform">اعلان انتشار سکوی کی‌دی‌ای ۴٫۸ را بخوانید</a>.
<br />
</p>

برای جشن گرفتن انتشار ۴٫۸، اعضای اجتماعات کاربری <a href="http://community.kde.org/Promo/Events/Release_Parties/4.8">جشن‌های انتشار در نقاط مختلف دنیا</a> را برنامه‌ریزی کردند. به جشنی که به شما نزدیک‌تر است بپیوندید و مشارکت‌کنندگان و استفاده کنندگان باحال کی‌دی‌ای را ملاقات کنید.
<br><br>
شما میتوانید آنچه در سایت‌های اجتماعی اتفاق می‌افتد را در فید زنده‌ی کی‌دی‌ای دنبال کنید. این سایت فعالیتهای واقعی را در آیدنتیکا، توییتر، یوتیوب، فلیکر، پیکاساوب، وبلاگها و دیگر سایتهای شبکه‌های اجتماعی جمع میکنذ. این فید زنده را میتوانید در<a href="http://buzz.kde.org">buzz.kde.org</a> بیابید.

<h3>
از کی‌دی‌ای بگویید و پیام را منتشر کنید. به گفته‌هایتان برچسب «KDE» بزنید. 
</h3>
<p>
کی‌دی‌ای همه را ترغیب به پخش و گسترش پیام در شبکه‌های اجتماعی وب میکند.داستان‌های خود را در سایتهای خبری و کانال‌هایی از قبیل  delicious، digg، reddit، توییتر و آیدنتیکا ارسال کنید. تصاویر محیط کاری خودتان را در سرویسهایی مانند فیس‌بوک، فلیکر، ایپرنیتی و پیکاسا ارسال و آن‌ها را در گروههای مناسب به اشتراک بگذارید. از محیط کاری خود فیلم تهیه کرده (screencast) و آن‌ها را در یوتیوب، بلیپ‌تی‌وی، ویمئو و دیگر سایتها بارگذاری کنید. لطفاً کارهایی را که بارگذاری میکنید با برچسب (تگ) «KDE»  نمایش دهید تا پیدا کردن آن‌ها آسانتر باشد و همچنین تیم تبلیغات کی‌دی‌ای (KDE promo team) پوشش بهتری برای تحلیل انتشار نرم‌افزارهای کی‌دی‌ای ۴٫۸ داشته باشد.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.8/&amp;title=KDE%20releases%20version%204.8%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="/announcements/4.8/" data-text="#KDE releases version 4.8 of Plasma Workspaces, Applications and Platform → https://www.kde.org/announcements/4.8/ #kde48" data-count="horizontal" data-via="kdecommunity">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.8/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.8%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="/announcements/4.8/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde48"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde48"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde48"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde48"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h3>
درباره این اعلان:
</h3><p>
این اعلان توسط محمدرضا میردامادی به فارسی ترجمه شده است.
</p>

<h4>از کی‌دی‌ای پشتیبانی کنید</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.8.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>
<a
href="http://jointhegame.kde.org/">سیستم جدید دستورالعمل عضو پشتیبانی کننده‌ی .KDE e.V اکنون باز است.</a> با €25 در هر ۳ماه میتوانید اطمینان حاصل کنید که تیم اجتماع بین‌المللی کی‌دی‌ای برای ساختن نرم‌افزار آزاد در کلاس جهانی، به بزرگ شدن خود ادامه میدهد.

<p>&nbsp;</p>
</div>
