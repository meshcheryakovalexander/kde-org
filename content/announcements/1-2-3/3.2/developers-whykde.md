---
custom_about: true
custom_contact: true
hidden: true
title: 'The Developer''s Perspective: Why KDE?'
---

KDE offers software developers five compelling reasons to write software
using KDE: superior technology, documentation, quality development tools,
an active and open development community and a significant user base.

## Superior Technology

For the developer, KDE is first and foremost a technology infrastructure. It provides
file access, network communication, configuration tools and standard user interface components
such as file open and save dialogs that are both sophisticated and easy to use. At the foundation of KDE lies the open source <a href="http://www.trolltech.com/products/qt">Qt toolkit</a>, which is
widely regarded as one of the best cross-platform toolkits available. Its clear and pragmatic
design makes development quick and easy. <a href="http://developer.kde.org/documentation/library/libraryref.html">KDE's own libraries</a> build upon Qt to extend the possibilities even further while keeping the same spirit of pragmatism, clarity and quality.

On top of these base libraries are a wide array of advanced technologies that prove indispensable
while creating modern graphical applications, such as:

 <li><a href="http://developer.kde.org/documentation/tutorials/xmlui/preface.html">XMLUI</a> which allows, as the name implies, defining UI elements such as menus and toolbars via XML files</li>
 <li><a href="http://developer.kde.org/documentation/tutorials/dot/dcopiface/dcop-interface.html">DCOP</a> which is a light-weight interprocess communication system that allows programs and users to pass messages with and remotely control applications
 <li><a href="http://phil.freehackers.org/kde/kpart-techno/kpart-techno.html">KParts</a> which is a component model that turns applications into shared libraries that can then be easily and generically embedded by other KParts-aware applications
 <li><a href="http://www.heise.de/ct/english/01/05/242/">KIO</a> which provides extensible network-transparent file access for KDE applications
 <li><a href="http://developer.kde.org/documentation/tutorials/kconfigxt/kconfigxt.html">KConfigXT</a> which takes an XML file and produces source code to manage configuration options, including classes to glue the resulting code to configuration dialogs.


All KDE libraries and components are Open Source and are therefore freely available to all. This has the added benefit of transparency and ongoing development which adds to the value of KDE's infrastructure.

## <a name="tools">Quality Development Tools</a>

The powerful capabilities provided by KDE's libraries and components are just one part of the KDE value proposition for developers. It is said that a craftsman is only as good as their tools, and KDE's software development tools are
some of the best available today.

At the most basic level, every developer needs a text editor. <a href="http://kate.kde.org/">Kate</a> is a fast, flexible editor with syntax highlighting, code folding, bookmarks and more. In true KDE fashion, Kate's
text editor capabilities are wrapped up in a KPart component and used by many other applications,
including the <a href="http://www.kdevelop.org/index.html?filename=awards.html">award winning</a> <a href="http://www.kdevelop.org">KDevelop IDE</a>, which is now in its
third major release. KDevelop <a href="http://www.kdevelop.org/index.html?filename=features.html">sports everything one would expect from a modern IDE</a> including
project management, support for 15 programming languages, an integrated debugger, revision control, documentation
browser, application wizards and more. KDevelop was recently awarded the 2004 LinuxWorld Product
Excellence Award for Best Development Tools in recognition of its leadership in this area.

KDE also provides a UML modeller (<a href="http://uml.sourceforge.net/index.php">Umbrello</a>), a CVS front end (<a href="http://www.kde.org/apps/cervisia/">Cervisia</a>), a graphical diff program (Kompare),
a memory profiler and leak checker in the form of <a href="http://kcachegrind.sourceforge.net/cgi-bin/show.cgi">KCachegrind</a> and <a href="http://valgrind.kde.org/">Valgrind</a>, <a href="http://i18n.kde.org/tools/kbabel/">translation tools</a> and a full featured <a href="http://www.trolltech.com/products/qt/designer.html">GUI builder</a>. KDE's simple yet powerful <a href="http://developer.kde.org/documentation/other/makefile_am_howto.html">build system</a> which is used throughout KDE itself as well as by most third party KDE applications takes the guesswork out of writing reliable, cross-platform configure scripts and Makefiles. There are <a href="http://kde-apps.org/index.php?xcontentmode=260x261">more development tools</a>
to be found at <a href="http://kde-apps.org">kde-apps.org</a> as well.

## Documentation

Of course, all the technology in the world isn't enough on its own. A development environment can be rendered frustratingly useless for the average developer if it isn't documented properly. Fortunately
for KDE developers, Qt has <a href="http://doc.trolltech.com/3.3/index.html">extensive online documentation</a>
as do the <a href="http://developer.kde.org/documentation/library/libraryref.html">KDE libraries</a>.
This is augmented by a library of tutorials, HOWTOs and other documentation housed at the
<a href="http://developer.kde.org">KDE Developer's Corner</a> website. There are also
several books on Qt development available online and at local bookstores.

There's also the excellent documentation provided in the form of the KDE source code itself which contain example
implementations for every part of KDE imaginable.

Armed with this wealth of documentation, it is easy to get up to speed with KDE development and find answers to your development questions.

## Active and Open Development Community

Of course, no matter how much documentation is available eventually every developer ends up with
a question they just can't find an answer to. This is when having an active community of
fellow developers who are familiar with the technologies you are working with is vital.

KDE's developer community is expansive and easy to reach online. There are several
<a href="http://www.kde.org/mailinglists/">KDE development email lists</a> where experienced
KDE developers can be found, and developers are constantly discussing development issues on the OpenProjects.net
IRC network in the #kde-devel channel. Questions are usually answered quickly and accurately.
Annual KDE meetings and <a href="http://www.kde.org/events/">attendance at trade shows around the world</a> makes meeting KDE developers in person a reality as well.

In addition to human resources, the KDE project also holds out several technical resources to KDE
developers. For those writing Open Source applications, there is the <a href="http://extragear.kde.org">Extra
Gear</a> project that offers CVS, mailing list and bug tracking services along with a heightened profile
in the KDE community for third-party applications. The <a href="http://www.kde-apps.org">KDE-apps.org</a> website
provides an excellent platform to announce and distribute your applications to users around the world from.

## A Significant User Base

As the most widely available Open Source desktop system, KDE has a huge following of users
and is commonly available on most Linux and UNIX platforms. Linux and UNIX desktop users often
have the necessary KDE components and libraries installed to run the KDE software you write.
For those who don't, it's usually just a few clicks away in their OS's package management tools.
This vibrant and growing user base ensures that millions can take advantage of your software easily
and effectively.

<table style="border: solid 1px;" align="center" cellpadding="6" cellspacing="0">
<tr>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Users</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Developers</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">About KDE</th>
</tr>
<tr>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../users-whykde">Why KDE?</a><br/>
<a href="../users-deploying">Deploying KDE</a><br/>
<a href="../users-supporting">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../developers-whykde">Why KDE?</a><br/>
<a href="../developers-developing">Developing With KDE</a><br/>
<a href="../developers-supporting">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap"><a href="/">The KDE Project</a><br/>
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a><br/>

</td>
</tr>
</table>

?>