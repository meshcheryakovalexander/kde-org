---
aliases:
- ../announce-3.5.7
custom_about: true
custom_contact: true
date: '2007-05-22'
title: An&uacute;ncio de Lan&ccedil;amento do KDE 3.5.7
---

<p>FOR IMMEDIATE RELEASE</p>


<h3 align="center">
   O Projecto KDE Lan&ccedil;a a S&eacute;tima Vers&atilde;o de Tradu&ccedil;&otilde;es e Servi&ccedil;os para o Ambiente
   de Trabalho Livre
</h3>

<p align="justify">
  O KDE 3.5.7 oferece tradu&ccedil;&otilde;es em 65 l&iacute;nguas, melhorias no pacote KDE PIM
  e em outras aplica&ccedil;&otilde;es.
</p>

<p align="justify">
  O <a href="/">Projecto
  KDE</a> anunciou hoje a disponibilidade imediata do KDE 3.5.7,
  uma vers&atilde;o de manuten&ccedil;&atilde;o para a &uacute;ltima gera&ccedil;&atilde;o do ambiente de trabalho
  poderoso, avan&ccedil;ado e <em>livre</em> para o GNU/Linux e outros UNIXes que &eacute;
  o KDE. Este suporta agora <a href="http://l10n.kde.org/stats/gui/stable/">65 l&iacute;nguas</a>,
  tornando-o dispon&iacute;vel para mais pessoas que a maioria do <em>software</em>
  n&atilde;o-livre e poder&aacute; ser extendido facilmente para suportar outras, atrav&eacute;s
  das comunidades que desejem contribuir para o projecto de c&oacute;digo aberto.
</p>

<p align="justify">
  Esta vers&atilde;o tem um foco renovado sobre as aplica&ccedil;&otilde;es do <a href="http://pim.kde.org/">KDE PIM</a>.
  O <a href="http://pim.kde.org/components/kaddressbook">KAddressBook</a>,
  o <a href="http://kontact.kde.org/korganizer/">KOrganizer</a> e o
  <a href="http://pim.kde.org/components/kalarm">KAlarm</a> tiveram
  alguma aten&ccedil;&atilde;o no que respeita &agrave; correc&ccedil;&atilde;o de erros, enquanto o 
  <a href="http://pim.kde.org/components/kmail">KMail</a> adquiriu
  novas funcionalidades e melhorias a n&iacute;vel da interface e do tratamento
  de IMAP: consegue lidar com as quotas de IMAP e copiar ou mover todas
  as pastas.
</p>

<p>
  Outras aplica&ccedil;&otilde;es receberam tamb&eacute;m novas melhorias de funcionalidades:
</p>
<ul>
<li>
  O <a href="http://kpdf.kde.org/">KPDF</a> mostra dicas para as hiperliga&ccedil;&otilde;es
  ao passar por cima delas, apresenta correctamente os ficheiros PDF mais
  complexos, como este 
  <a href="http://kpdf.kde.org/stuff/nytimes-firefox-final.pdf">an&uacute;ncio 
  publicit&aacute;rio do Firefox</a> e reage aos comandos para abrir a &aacute;rea do &Iacute;ndice
  Anal&iacute;tico.
</li>

<li>
  O <a href="http://uml.sourceforge.net/">Umbrello</a> consegue agora gerar
  e exportar c&oacute;digo em C# e foi-lhe adicionado o suporte para gen&eacute;ricos do
  Java 5.
</li>

<li>
  O <a href="http://www.kdevelop.org/">KDevelop</a> teve uma grande 
  actualiza&ccedil;&atilde;o da vers&atilde;o para a 3.4.1. As novas funcionalidades incluem
  uma Navega&ccedil;&atilde;o e Completa&ccedil;&atilde;o de C&oacute;digo muito melhorada, uma interface
  de depura&ccedil;&atilde;o mais fi&aacute;vel, o suporte para o Qt4 e um melhor suporte para
  a programa&ccedil;&atilde;o em Ruby e para o KDE4.
</li>
</ul>

<p>
  Para al&eacute;m das novas funcionalidades, foram feitas diversas correc&ccedil;&otilde;es de
  erros por todo o projecto, especialmente nos pacotes de 
  <a href="http://edu.kde.org/">Educa&ccedil;&atilde;o/Entretenimento</a></p>

<p>
  Como os utilizadores do KDE j&aacute; est&atilde;o &agrave; espera, esta vers&atilde;o nova inclui um
  trabalho continuado sobre o KHTML e o KJS, os motores de HTML e Javascript
  do KDE. Uma nova funcionalidade interessante de usabilidade do KHTML faz
  com que o cursor do rato indique se uma dada hiperliga&ccedil;&atilde;o ir&aacute; abrir uma nova
  janela de navega&ccedil;&atilde;o ou n&atilde;o.
</p>

<p align="justify">
  Para uma lista mais detalhada das melhorias desde a 
  <a href="/announcements/announce-3.5.6">vers&atilde;o 3.5.6 do KDE</a>,
  a 25 de Janeiro de 2007, consulte por favor o
  <a href="/announcements/changelogs/changelog3_5_6to3_5_7">Registo de Altera&ccedil;&otilde;es do KDE 3.5.7</a>.
</p>

<p align="justify">
  O KDE 3.5.7 vem com um ambiente de trabalho b&aacute;sico e &eacute; acompanhado de quinze 
  pacotes (PIM, administra&ccedil;&atilde;o, redes, educa&ccedil;&atilde;o/entretenimento, utilit&aacute;rios,
	   multim&eacute;dia, jogos, itens art&iacute;sticos, programa&ccedil;&atilde;o Web, entre outros). As
  ferramentas e aplica&ccedil;&otilde;es premiadas do KDE est&atilde;o dispon&iacute;veis em 
 <strong>65 l&iacute;nguas</strong>.
</p>

<h4>
  Distribui&ccedil;&otilde;es que Oferecem o KDE
</h4>
<p align="justify">
  A maior parte das distribui&ccedil;&otilde;es de Linux e UNIX n&atilde;o incorporam imediatamente
  as novas vers&otilde;es do KDE, mas ir&atilde;o integrar os pacotes do KDE 3.5.7 nas suas
  pr&oacute;ximas vers&otilde;es. Veja
  <a href="/distributions">esta lista</a> para
  ver as distribui&ccedil;&otilde;es que oferecem o KDE.
</p>

<h4>
  Instalar os Pacotes Bin&aacute;rios do KDE 3.5.7
</h4>
<p align="justify">
  <em>Criadores de Pacotes</em>. Alguns distribuidores de sistemas operativos ofereceram gentilmente pacotes bin&aacute;rios do KDE 3.5.7 para algumas vers&otilde;es das suas distribui&ccedil;&otilde;es e, noutros casos, os volunt&aacute;rios da comunidade tamb&eacute;m o fizeram. Alguns destes pacotes bin&aacute;rios est&atilde;o dispon&iacute;veis de forma livre no servidor de transfer&ecirc;ncias do KDE em <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.7/">download.kde.org</a>.
  Os pacotes bin&aacute;rios adicionais, assim como as actualiza&ccedil;&otilde;es para os pacotes
  dispon&iacute;veis, poder&atilde;o vir a ficar dispon&iacute;veis nas pr&oacute;ximas semanas.
</p>

<p align="justify">
  <a id="package_locations"><em>Localiza&ccedil;&otilde;es dos Pacotes</em></a>.
  Para uma lista actualizada dos pacotes bin&aacute;rios dispon&iacute;veis, cuja notifica&ccedil;&atilde;o foi prestada ao Projecto KDE, consulte por favor a 
  <a href="/info/1-2-3/3.5.7">P&aacute;gina de Informa&ccedil;&otilde;es do KDE 3.5.7</a>.
</p>

<h4>
  Compilar o KDE 3.5.7
</h4>
<p align="justify">
  <a id="source_code"></a><em>C&oacute;digo-Fonte</em>.
  O c&oacute;digo-fonte completo do KDE 3.5.7 poder&aacute; ser
  <a href="http://download.kde.org/stable/3.5.7/src/">obtido
  de forma livre</a>.  As instru&ccedil;&otilde;es de compila&ccedil;&atilde;o e instala&ccedil;&atilde;o do KDE 3.5.7
  est&atilde;o dispon&iacute;veis na <a href="/info/1-2-3/3.5.7">P&aacute;gina de Informa&ccedil;&otilde;es
  do KDE 3.5.7</a>.
</p>

<h4>
  Suportar o KDE
</h4>
<p align="justify">
O KDE &eacute; um projecto de 
<a href="http://www.gnu.org/philosophy/free-sw.html">'Software' Livre</a> que
existe e cresce apenas devido &agrave; ajuda dos muitos volunt&aacute;rios que doam o seu
tempo e esfor&ccedil;o. O KDE est&aacute; sempre &agrave; procura de novos volunt&aacute;rios e 
contribui&ccedil;&otilde;es, quer seja na programa&ccedil;&atilde;o, correc&ccedil;&atilde;o ou relato de erros,
quer na escrita de documenta&ccedil;&atilde;o, tradu&ccedil;&otilde;es, promo&ccedil;&atilde;o, dinheiro, etc. Todas
as contribui&ccedil;&otilde;es s&atilde;o gratamente recebidas e aceites com todo o gosto. Leia
por favor a  <a href="/community/donations/">p&aacute;gina de Suporte do KDE</a> para mais
informa&ccedil;&otilde;es. </p>

<p align="justify">
Ficamos &agrave; espera de not&iacute;cias suas em breve!
</p>

<h4>
  Acerca do KDE
</h4>
<p align="justify">
  O KDE &eacute; um projecto <a href="/awards/">premiado</a>, independente e com 
  <a href="/people/">centenas</a> de programadores, tradutores, artistas
  e outros profissionais em todo o mundo que colaboram atrav&eacute;s da Internet
  para criar e distribuir de forma livre um ambiente sofisticado, personalizado
  e est&aacute;vel para o seu trabalho, empregando uma arquitectura flex&iacute;vel,
  baseada em componentes e transparente na rede, oferecendo tamb&eacute;m uma
  plataforma de desenvolvimento espantosa.</p>

<p align="justify">
  O KDE oferece um ambiente de trabalho est&aacute;vel, maduro e que inclui um
  navegador avan&ccedil;ado
  (o <a href="http://konqueror.kde.org/">Konqueror</a>), um pacote de
  gest&atilde;o de informa&ccedil;&otilde;es pessoais (o <a href="http://kontact.org/">Kontact</a>),
  um pacote de escrit&oacute;rio completo (o 
  <a href="http://www.koffice.org/">KOffice</a>), um grande conjunto de
  aplica&ccedil;&otilde;es e utilit&aacute;rios de rede e um ambiente de desenvolvimento intuitivo
  e eficiente, o excelente IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  O KDE &eacute; uma prova que o modelo de desenvolvimento "estilo Bazaar"
  poder&aacute; garantir tecnologias de primeira linha a par ou superiores
  &agrave;s aplica&ccedil;&otilde;es comerciais mais complexas.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Avisos de Marcas Registadas.</em>
  O KDE<sup>&#174;</sup> e o log&oacute;tipo do Ambiente de Trabalho 
  K<sup>&#174;</sup> s&atilde;o marcas registadas do KDE e.V.

  O Linux &eacute; uma marca registada de Linus Torvalds.

  O UNIX &eacute; uma marca registada do The Open Group nos Estados Unidos e noutros
  pa&iacute;ses.

  Todas as outras marcas registadas e direitos de c&oacute;pia referidos neste
  an&uacute;ncio s&atilde;o da propriedade dos seus respectivos donos.
  </font>
</p>

<hr />

<h4>Contactos de Imprensa</h4>
<table cellpadding="10" align="center"><tr valign="top">
<td>

<b>&Aacute;frica</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Nam&iacute;bia<br />
Telefone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>&Aacute;sia e &Iacute;ndia</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     &Iacute;ndia 410206<br/>
     Telefone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Su&eacute;cia<br />
Telefone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Am&eacute;rica do Norte</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canad&aacute;<br />
Telefone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Austr&aacute;lia<br />
Telefone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>Am&eacute;rica do Sul</b><br />
H&eacute;lio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brasil<br />
Telefone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>
