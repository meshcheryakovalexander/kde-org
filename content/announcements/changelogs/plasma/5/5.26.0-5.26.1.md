---
title: Plasma 5.26.1 complete changelog
version: 5.26.1
hidden: true
plasma: true
type: fulllog
---
{{< details title="Aura Browser" href="https://commits.kde.org/aura-browser" >}}
+ Print cmake feature summary. [Commit.](http://commits.kde.org/aura-browser/7cedc3a845a33d13b2f10211c6bd704edb0d4eec) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Reviews: Don't check for name field being empty when it's not visible. [Commit.](http://commits.kde.org/discover/ec06b73df69edc21f0bab60cd331b7764601af48) Fixes bug [#460504](https://bugs.kde.org/460504)
+ Fix i18n warning. [Commit.](http://commits.kde.org/discover/d781102c58b071b1227c36a140de0d2ab1054532) 
+ Flatpak: Address transaction progress skipping. [Commit.](http://commits.kde.org/discover/336becd3ad0f8aa5b18d3c61b38c15d81a3f3544) Fixes bug [#404819](https://bugs.kde.org/404819). Fixes bug [#434948](https://bugs.kde.org/434948). Fixes bug [#435450](https://bugs.kde.org/435450). Fixes bug [#448280](https://bugs.kde.org/448280)
+ Delay subCategoriesChanged signals when disabling. [Commit.](http://commits.kde.org/discover/a3dd72f42c92c9a7077164697e5e9b0badf518b4) Fixes bug [#401666](https://bugs.kde.org/401666). Fixes bug [#457408](https://bugs.kde.org/457408)
+ Flatpak: Improve flatpaktest reliability. [Commit.](http://commits.kde.org/discover/c237403e3c9d1dcfbe79bd1528921f9b728c0582) 
+ Flatpak: Just disable the test on the CI. [Commit.](http://commits.kde.org/discover/6179f2f708c3c0b8557eb0a16a4c7bd234f4c408) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Add missing Messages.sh. [Commit.](http://commits.kde.org/kdeplasma-addons/702dbb191f34150012755fec09766c1071f47748) 
+ Revert "wallpapers/potd: show image with cursor when dragging". [Commit.](http://commits.kde.org/kdeplasma-addons/31e5d59353afcb46d703239326e05cce6c8253fd) Fixes bug [#460378](https://bugs.kde.org/460378)
+ Revert "wallpapers/potd: clear `Drag.imageSource` on `dragFinished`". [Commit.](http://commits.kde.org/kdeplasma-addons/3e16df7281b47ab161d94a8f906143b6b6a82e28) 
+ Icon switchers: Use the actual app icon, not the plasma icon for the app. [Commit.](http://commits.kde.org/kdeplasma-addons/107f6f5addb68d78e0a6e268cf3e40609f3b0f91) 
+ Fuzzy-clock: Fix colors in full representation (calendar view). [Commit.](http://commits.kde.org/kdeplasma-addons/20b1d79134354b404c9ca15c59892d97a3046175) 
+ Colorpicker: Make left-clicking a color copy in the default format. [Commit.](http://commits.kde.org/kdeplasma-addons/6b809a159d010cab2065484c47ba01599720d574) Fixes bug [#457311](https://bugs.kde.org/457311)
+ Fuzzy-clock, binary-clock: Fix clicking applets again to close calendar. [Commit.](http://commits.kde.org/kdeplasma-addons/21e864bb6374f17b1e8395cb76cf6070802599e9) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ KCM: wrap resolution label too. [Commit.](http://commits.kde.org/kscreen/fdbf6f8a006ebb7376927f02013f8c91bcdaba35) Fixes bug [#460397](https://bugs.kde.org/460397)
+ Make the Output component accept events. [Commit.](http://commits.kde.org/kscreen/f9d691d9cd9980cc1c2737ebc64cdb8b4cc843e3) Fixes bug [#460280](https://bugs.kde.org/460280)
+ Osd: Specify layer shell namespace. [Commit.](http://commits.kde.org/kscreen/8aa2d95fd926344abcc4687f5123da2e103a5577) 
+ Don't save scale in control file. [Commit.](http://commits.kde.org/kscreen/2ae645bd4fd44791e8411e4ef289a2f1500220b3) 
+ Use the osd window type. [Commit.](http://commits.kde.org/kscreen/e72a8d984c724748cf9034ce0df3a0b93468a975) Fixes bug [#419764](https://bugs.kde.org/419764)
+ Osd: Fix a crash when quitting from QML signal handler. [Commit.](http://commits.kde.org/kscreen/a917bf19436c0e86eb993fe89a0f1df536b8fdc6) See bug [#459368](https://bugs.kde.org/459368)
+ Kcm: Call settingsChanged when resetting if config needs save. [Commit.](http://commits.kde.org/kscreen/a54994c7c5c74864b66d9e35b2bae25769cda0f9) 
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Revert "Fix loading image wallpaper plugin config UI in KCM". [Commit.](http://commits.kde.org/kscreenlocker/8ab2ff78e4d7128123dfa02ee795fae9eea0e24a) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Effects: Make WindowHeap try to fill gaps. [Commit.](http://commits.kde.org/kwin/3583e506e00d38480b5253a9207ec58aaaaa5663) 
+ Handle scale override in locked and confined pointers. [Commit.](http://commits.kde.org/kwin/1a9238f2a7dd3d5ba3b931509ab4a41ee4aed147) Fixes bug [#460532](https://bugs.kde.org/460532)
+ Move installPointerConstraint calls into the constraint handlers. [Commit.](http://commits.kde.org/kwin/ef29411f1c1c178fb3ed8ab699ddb87b2fcd2f6e) 
+ Screencast: Don't report damage on the full screen every time. [Commit.](http://commits.kde.org/kwin/569ab0dfaec9e7573b8dd578af18088de257c394) 
+ Guard against reconfiguring wayland specific input on X11. [Commit.](http://commits.kde.org/kwin/a6f3d44d1a5adcbf46339ed6e5dc47404c1edbab) 
+ Make Workspace::outputAt() more robust to extreme values. [Commit.](http://commits.kde.org/kwin/eb6e7dedad89269207b2468e9ed93a4fd991bd54) Fixes bug [#460446](https://bugs.kde.org/460446)
+ Fix resizing by dragging top-right window corner. [Commit.](http://commits.kde.org/kwin/44d3e43652ee1922d0d9c390cfefcc02518c8b48) Fixes bug [#460501](https://bugs.kde.org/460501)
+ Wayland: Fix handling of removed outputs in DpmsInterface. [Commit.](http://commits.kde.org/kwin/2395fa823d8a9b93768fd352c92367221bbb901c) 
+ Wayland: Sync output device's enabled property. [Commit.](http://commits.kde.org/kwin/ff28dd06936d733ac6d65f41669b1331fa28d1cc) Fixes bug [#460247](https://bugs.kde.org/460247)
+ Backends/drm: Fix leaving dangling dpms input event filter. [Commit.](http://commits.kde.org/kwin/b08511626c0f7ab82c4929abcf8daab521ce0255) Fixes bug [#460322](https://bugs.kde.org/460322)
+ Effects/screenshot: Avoid capturing hidden cursor. [Commit.](http://commits.kde.org/kwin/a76704ade398ef39b5ff8f08ce3f4d0c5836f898) Fixes bug [#460358](https://bugs.kde.org/460358)
+ Make "switch to screen" and "window to screen" code less error-prone. [Commit.](http://commits.kde.org/kwin/87c87cbdf3003f902fac6dc3c05c8811e68953e0) 
+ Fix "window to screen" and "switch to screen" shortcuts. [Commit.](http://commits.kde.org/kwin/23d3fb45e5f1b064a0677caab31c8361c0a298b3) Fixes bug [#460337](https://bugs.kde.org/460337)
+ Fix scripted shader effect animations. [Commit.](http://commits.kde.org/kwin/8599deba2eedab7181339c2555334c7dbab9a2ba) Fixes bug [#460277](https://bugs.kde.org/460277)
+ Drop shaderTrait adjustment on crossfade. [Commit.](http://commits.kde.org/kwin/ec1d0644bbde1931fe9f4f13648c867b7c1c81c9) 
+ Wayland: don't allow minimizing applet popups either. [Commit.](http://commits.kde.org/kwin/84e3ff01123a4c33f3ec76404d679529c5b9469f) 
+ Wayland: don't allow maximization of applet popups. [Commit.](http://commits.kde.org/kwin/e99f8bcb1238628953d81424c61c7fa289c559da) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Backends/kwayland: only consider enabled outputs for Xwayland scale. [Commit.](http://commits.kde.org/libkscreen/e40cab07d63a1e2e72d8d73bebebbe0a93318cec) 
{{< /details >}}

{{< details title="Plank Player" href="https://commits.kde.org/plank-player" >}}
+ Print cmake feature summary. [Commit.](http://commits.kde.org/plank-player/7003f043de098ef66b809987c034392309c0517b) 
{{< /details >}}

{{< details title="Plasma Browser Integration" href="https://commits.kde.org/plasma-browser-integration" >}}
+ Fix excess comma in X-Plasma-Runner-Syntaxes values. [Commit.](http://commits.kde.org/plasma-browser-integration/f081f3ce5145eeb9abaf40a49478ccb17cf0a7f6) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Make drag and drop of pager more reliable. [Commit.](http://commits.kde.org/plasma-desktop/845bc042b4d9907bd363bd28fc0de6d350f3c557) Fixes bug [#460541](https://bugs.kde.org/460541). Fixes bug [#460545](https://bugs.kde.org/460545)
+ Also delete entry with Notify flag. [Commit.](http://commits.kde.org/plasma-desktop/705ab3098df0bfa09c15a10ecbb70db4384ff0d6) See bug [#460345](https://bugs.kde.org/460345)
+ Move the panel's placeholder instead of the target item and animate all of the items inbetween; also fixes mouse input that gets lost when quickly dragging applet to the very end of the panel. [Commit.](http://commits.kde.org/plasma-desktop/e36bfb1f1fe064e1d05ade97230353db012c8bbe) 
+ Revert "containments/panel: iterate all items between start index and end index when pressed". [Commit.](http://commits.kde.org/plasma-desktop/2becaa4e61c6219cc17b6c93920e4db64638eb95) 
+ Desktoptoolbox: fix flickering on closing. [Commit.](http://commits.kde.org/plasma-desktop/1e547fe7ef946ebdfd503a9506f9ffae94555a1f) Fixes bug [#417849](https://bugs.kde.org/417849)
+ Containments/panel: iterate all items between start index and end index when pressed. [Commit.](http://commits.kde.org/plasma-desktop/83a50b7d0f27b5e15598011e486cf9397c306007) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Applet: add missing left padding on toolbar. [Commit.](http://commits.kde.org/plasma-nm/ceaa5891f5414d084d8dfa12aa0991860619fe2e) Fixes bug [#458742](https://bugs.kde.org/458742)
+ Applet: Fix enabled states of Wi-Fi and WWAN checkboxes in Edit Mode. [Commit.](http://commits.kde.org/plasma-nm/860af42f32461c68e2bc5a24fba798e444950872) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Don't crash when the server doesn't respond. [Commit.](http://commits.kde.org/plasma-pa/89d33990bbd4e63e4624a01fb0e74c341ac50fa4) Fixes bug [#454647](https://bugs.kde.org/454647). Fixes bug [#437272](https://bugs.kde.org/437272)
{{< /details >}}

{{< details title="Plasma Remotecontrollers" href="https://commits.kde.org/plasma-remotecontrollers" >}}
+ Print cmake feature summary. [Commit.](http://commits.kde.org/plasma-remotecontrollers/1e7f745ef677b6f3db586f6f97acfcac3b584c9d) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Fix is default locale check. [Commit.](http://commits.kde.org/plasma-workspace/d715520c8527bb85898b73b5226b80a0ea082ad2) Fixes bug [#460418](https://bugs.kde.org/460418)
+ Remove excess pages before pushing new pages. [Commit.](http://commits.kde.org/plasma-workspace/e84a0659d7e5648ab6352e63dabd55c650f95224) 
+ [applets/diskanddevices] Add xdg-activation support to device ations. [Commit.](http://commits.kde.org/plasma-workspace/1f6b9710e119770b948e440b2c3d5e70bfd0b58e) Fixes bug [#460268](https://bugs.kde.org/460268)
+ Krdb: do not crash on file system problems. [Commit.](http://commits.kde.org/plasma-workspace/d747d3c0202eeed4bb6b55903e9d4e020194cf95) 
+ Make the panel's corner mask 1px smaller to avoid artifacts at the corners. [Commit.](http://commits.kde.org/plasma-workspace/b68a93efee8a1b1de86f3689c0db685f6a74adf9) Fixes bug [#417511](https://bugs.kde.org/417511)
+ Handle multiple desktop files with different StartupWMClass. [Commit.](http://commits.kde.org/plasma-workspace/735f350c1506bb06e87fdd9a151eed323384452e) Fixes bug [#358277](https://bugs.kde.org/358277)
+ Add scaling and rotation support to a blur behind desktop plasmoids. [Commit.](http://commits.kde.org/plasma-workspace/8a18babc25ac4641c3743cbd5ca833a7649e3b96) 
+ Wallpapers/image: fix potential dead loop in PackageFinder. [Commit.](http://commits.kde.org/plasma-workspace/707762edd562a439bccec2e7d72236d74e397c00) 
+ Wallpapers/image: fix potential dead loop in ImageFinder. [Commit.](http://commits.kde.org/plasma-workspace/2b993aa7cb7739ade4b80263b08c9c6ba60b2ea8) 
+ Wallpapers/image: fix symlink not being found by ImageFinder. [Commit.](http://commits.kde.org/plasma-workspace/28f362dfce3dd065fffcad0eedf7bb87f5235fea) Fixes bug [#460287](https://bugs.kde.org/460287)
+ Revert "wallpapers/image: Fix when used outside plasmashell". [Commit.](http://commits.kde.org/plasma-workspace/067a6c7562f8805c72ead8dc13a280a2a8711e8c) 
+ Wallpapers/image: Fix when used outside plasmashell. [Commit.](http://commits.kde.org/plasma-workspace/cf061bab65b67fff30194bce16d2cc48221fd286) 
+ If an app is playing media but hasn't provided a title, say as much instead of "No media playing". [Commit.](http://commits.kde.org/plasma-workspace/40b2226c92ba9dcc59b3c66cb4cacb58788b08a4) Fixes bug [#456516](https://bugs.kde.org/456516)
+ Wallpapers/image: reset `scale` to 1 to work around QTBUG-107458. [Commit.](http://commits.kde.org/plasma-workspace/98cc8b3898e80bb6409411af402fff3b162fad05) 
+ Wallpapers/image: reduce the maximum allowed cost of QCache. [Commit.](http://commits.kde.org/plasma-workspace/b45569b212a573023d5333e4c8b07abf78023c7c) 
+ Wallpapers/image: fix side-by-side previews sometimes not showing. [Commit.](http://commits.kde.org/plasma-workspace/b5ef5601bb84e433ce1d33248272dbd914c7de24) 
+ Wallpapers/image: reduce preview cache insertion for single images. [Commit.](http://commits.kde.org/plasma-workspace/a4de7bf6afdc86478a2e99dc0b7b4b5a99b0d9d9) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ ScrollBar & ScrollIndicator: Show over content even if it loaded after. [Commit.](http://commits.kde.org/qqc2-breeze-style/0701295ed55e0e7d6dcecca138f67997ebf8c82a) 
+ Revert "Install po folder". [Commit.](http://commits.kde.org/qqc2-breeze-style/ff0a07677beed2433e934890b7cf6466a506d7c8) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ [runner] Fix desktop file name. [Commit.](http://commits.kde.org/systemsettings/7a7e48764e81207847e00c95393fc25edbca75bc) Fixes bug [#459213](https://bugs.kde.org/459213)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Screenshare SNI: use monochrome-compatible icon. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/047f83cfb4ae62270e1b95036132ce8bb9df335a) Fixes bug [#460514](https://bugs.kde.org/460514)
{{< /details >}}

