---
aliases:
- ../changelog3_5to3_5_1
hidden: true
title: KDE 3.5 to KDE 3.5.1 Changelog
---

<p>
This page tries to present as much as possible the additions
and corrections that occurred in KDE between the 3.5 and 3.5.1 releases.
Nevertheless it should be considered as incomplete.
</p>

<h3><a name="arts">arts</a> <font size="-2">[<a href="3_5_1/arts.txt">all SVN changes</a>]</font></h3>
<h3><a name="kdelibs">kdelibs</a> <font size="-2">[<a href="3_5_1/kdelibs.txt">all SVN changes</a>]</font></h3><ul>
  <li>Improved performance under some conditions on the networking classes.</li>
  <li>(KIO-slave http): Work around Apache2 DAV module's insistence on trailing slashes at the end of requests on directories.(<a href="http://bugs.kde.org/show_bug.cgi?id=119066">Bug #119066</a>)</li>
  <li>(KIO-slave kfile): enabling of the normal permission dialog takes effect only after closing after doing changes on acl (<a href="http://bugs.kde.org/show_bug.cgi?id=119841">Bug #119841</a>)</li>
  <li>(KIO-slave kfile): problems with changing permissions in the mask entry (<a href="http://bugs.kde.org/show_bug.cgi?id=119366">Bug #119366</a>)</li>
  <li>KDirWatch: KDirWatch stopDirScan affects outside KDirWatch instances also. (<a href="http://bugs.kde.org/show_bug.cgi?id=119341">Bug #119341</a>)</li>
  <li>KCrash: Make KCrash more rebust (eg. in malloc()) (<a href="http://lists.kde.org/?l=kde-core-devel&amp;m=113699766611213&amp;w=2">See this thread</a>)</li>
  <li>Mimetype detection: add more "magic" patterns to recognise shell scripts</li>
  <li>KJS: avoid buffer overflow on decoding utf8 uri sequences. See <a href="/info/security/advisory-20060119-1.txt">here</a></li>
  <li>Kate
	  <ul>
		  <li>Wrong syntax highlighting chosen for filetype (<a href="http://bugs.kde.org/show_bug.cgi?id=118705">Bug #118705</a>)</li>
		  <li>Clear selection when closing a document. (<a href="http://bugs.kde.org/show_bug.cgi?id=118588">Bug #118588</a>)</li>
		  <li>Text-files displayed in konqueror are saved with 0 bytes. (<a href="http://bugs.kde.org/show_bug.cgi?id=118743">Bug #118743</a>)</li>
		  <li>Backspace key does not work when shift key is held (<a href="http://bugs.kde.org/show_bug.cgi?id=117662">Bug #117662</a>)</li>
		  <li>PicAsm highlighting is missing IORWF, RLF, RRF (<a href="http://bugs.kde.org/show_bug.cgi?id=118120">Bug #118120</a>)</li>
	  </ul>
   </li>
	<li>KHTML
		<ul>
			<li>Performance improvements:
				<ul>
					<li>Add caching for getElementById</li>
					<li>Much faster appending of options to selects, and clearing of selects</li>
					<li>Simplify dirty region computation, to speed up cases of lots of small updates</li>
					<li>Small improvements in efficiency of URL handling in image loader</li>
				</ul>
			</li>
			<li>Match other browsers and not the DOM on behavior of getAttribute with non-existent attributes. 
			Fixes reply buttons on Yahoo mail (<a href="http://bugs.kde.org/show_bug.cgi?id=116413">bug #116413</a>)</li>
			<li>Hidden field is not sent (Fixing OSNews.com) (<a href="http://bugs.kde.org/show_bug.cgi?id=116790">bug #116790</a>)</li>
			<li>Konqueror consumes all memory when printing specific web page (<a href="http://bugs.kde.org/show_bug.cgi?id=116861">bug #116861</a>)</li>
			<li>Do not lose server-specified encodings when going back in history 
			(<a href="http://bugs.kde.org/show_bug.cgi?id=116320">bug #116320</a>)</li>
			<li>Make behavior of named objects access under document more consistent with other browsers (<a href="http://bugs.kde.org/show_bug.cgi?id=118890">Part of bug #118890</a>)</li>
			<li>Various crash fixes (<a href="http://bugs.kde.org/show_bug.cgi?id=91004">Bug #91004</a>, 
				<a href="http://bugs.kde.org/show_bug.cgi?id=88306">bug #88306</a>, 
				<a href="http://bugs.kde.org/show_bug.cgi?id=110036">bug #110036</a>,
				<a href="http://bugs.kde.org/show_bug.cgi?id=119734">bug #119734</a>,
				<a href="http://bugs.kde.org/show_bug.cgi?id=115680">bug #115680</a>
				others)</li>
			<li>JavaScript debugger more reliable</li>
			<li>Support window.scrollX, window.scrollY (<a href="http://bugs.kde.org/show_bug.cgi?id=117787">Bug #117787</a>), .click on anchors (<a href="http://bugs.kde.org/show_bug.cgi?id=120272">Part of bug #120272</a>)</li>
			<li>behave sanely for Roman counters &gt; 4000 (<a href="http://bugs.kde.org/show_bug.cgi?id=119352">bug #119352</a>)</li>
			<li>Optimize appends and clears to/of selects via explicit APIs. (<a href="http://bugs.kde.org/show_bug.cgi?id=62785">bug #62785</a>)</li>
		</ul>
	</li>
	<li>KDEUI
		<ul>
			<li>Set default parameter in KRandomSequence to 0 instead of 1 for randomness if no seed is provided</li>
		</ul>
	</li>
</ul>

<h3><a name="kdeaccessibility">kdeaccessibility</a> <font size="-2">[<a href="3_5_1/kdeaccessibility.txt">all SVN changes</a>]</font></h3><ul>
	<li>Kttsd
		<ul>
			<li>kttsd freezes when removing a speaking text job on alsa plugin (<a href="http://bugs.kde.org/show_bug.cgi?id=119753">bug #119753</a>)</li>
			<li>appendText method ignored and not working (<a href="http://bugs.kde.org/show_bug.cgi?id=116031">bug #116031</a>)</li>
			<li>Not loading plugins when desktop language is not ISO-8859-1 (<a href="http://bugs.kde.org/show_bug.cgi?id=118016">bug #118016</a>)</li>
		</ul>
	</li>
</ul>

<h3><a name="kdeaddons">kdeaddons</a> <font size="-2">[<a href="3_5_1/kdeaddons.txt">all SVN changes</a>]</font></h3>
<h3><a name="kdeadmin">kdeadmin</a> <font size="-2">[<a href="3_5_1/kdeadmin.txt">all SVN changes</a>]</font></h3><ul>
	<li>KCron
		<ul>
			<li>Don't crash after selecting variable(<a href="http://bugs.kde.org/show_bug.cgi?id=106853">bug #106853</a>)</li>
		</ul>
	</li>
</ul>

<h3><a name="kdeartwork">kdeartwork</a> <font size="-2">[<a href="3_5_1/kdeartwork.txt">all SVN changes</a>]</font></h3><ul>
    <li>
        various KWin decorations: Support for titlebar wheel event actions
	(<a href="http://bugs.kde.org/show_bug.cgi?id=117769">Bug #117769</a>)
    </li>
</ul>

<h3><a name="kdebase">kdebase</a> <font size="-2">[<a href="3_5_1/kdebase.txt">all SVN changes</a>]</font></h3><ul>
	<li>man KIO slave
		<ul>
			<li>
				fix processing of URLs in man documents
				(<a href="http://bugs.kde.org/show_bug.cgi?id=117971">Bug #117971</a>,
				fix for wget(1))
			</li>
			<li>
				allow comment lines after a .TP request
				(<a href="http://bugs.kde.org/show_bug.cgi?id=119223">Bug #119223</a>,
				fix for proc(5))
			</li>
		</ul>
	</li>
   <li>Kicker
	   <ul>
	   		<li>Turning off tooltip doesn't work (<a href="http://bugs.kde.org/show_bug.cgi?id=117277">bug #117277</a>)</li>
	   		<li>Don't crash on logout. (<a href="http://bugs.kde.org/show_bug.cgi?id=113242">bug #113242</a>)</li>
	   		<li>app started by kmail does not show in kicker (<a href="http://bugs.kde.org/show_bug.cgi?id=119850">bug #119850</a>)</li>
                        <li>Fix incorrect handling of background leading to using significant amount of X resources</li>
	   </ul>
   </li>
	<li>Konqueror
		<ul>
			<li>2 Tools menus when embedding KPDF in Konqueror (<a href="http://bugs.kde.org/show_bug.cgi?id=118271">bug #118271</a>)</li>
		</ul>
	</li>
	<li>KDesktop
		<ul>
			<li>Fix initializations for desktops &gt;4 when using DCOP calls. (<a href="http://bugs.kde.org/show_bug.cgi?id=98734">bug #98734</a>)</li>
			<li>Desktop icons move down at each login due to child panel at top of screen (<a href="http://bugs.kde.org/show_bug.cgi?id=117868">bug #117868</a>)</li>
		</ul>
	</li>
	<li>Konsole
		<ul>
			<li>Update blue/red icons to be more distinguishable. (<a href="http://bugs.kde.org/show_bug.cgi?id=117065">#117065</a>)</li>
			<li>The history options (line #/enabled) are now used in the profiles. (<a href="http://bugs.kde.org/show_bug.cgi?id=120046">#120046</a>)</li>
		</ul>
	</li>
    <li>KWin
        <ul>
            <li>Incorrectly restored window geometry upon leaving fullscreen mode under certain circumstances (<a href="http://bugs.kde.org/show_bug.cgi?id=119509">#119509</a>)</li>
            <li>Fixed infinite loop with CDE-style Alt+Tab (<a href="http://bugs.kde.org/show_bug.cgi?id=112737">#112737</a>)</li>
            <li>Keep-above windows are kept even above dock windows (panels) (<a href="http://bugs.kde.org/show_bug.cgi?id=118416">#118416</a>)</li>
            <li>Fix kwallet dialog sometimes not getting focus when it should (<a href="http://bugs.kde.org/show_bug.cgi?id=75175">#75175</a>)</li>
        </ul>
    </li>
</ul>

<h3><a name="kdebindings">kdebindings</a> <font size="-2">[<a href="3_5_1/kdebindings.txt">all SVN changes</a>]</font></h3>
<h3><a name="kdeedu">kdeedu</a> <font size="-2">[<a href="3_5_1/kdeedu.txt">all SVN changes</a>]</font></h3><ul>
	<li>Kalzium
		<ul>
			<li>(Calculator) Visible userfeedback for invalid input</li>
			<li>(Calculator) Don't calculate invalid molecules (<a href="http://bugs.kde.org/show_bug.cgi?id=117774">bug #117774</a>)</li>
			<li>(Plotting) Fix drawing of single points</li>
			<li>(Data) Fix the discovery date of Iodine (was 1804, correct is 1811, <a href="http://bugs.kde.org/show_bug.cgi?id=119430">bug #119430</a>)</li>
			<li>(Data) About 40 new radii (atomic and covalent) have been added</li>
			<li>(Data) Some elements densities (only elements &gt; 70) where displayed wrong (g/L instead of g/cm^3)</li>
		</ul>
	</li>
	<li>Kig
		<ul>
			<li>forcing undo/redo actions disabled while constructing (<a href="http://bugs.kde.org/show_bug.cgi?id=98106">#98106</a>)</li>
			<li>fix redefining of text labels</li>
		</ul>
	</li>
	<li>KStars: NGC 1300 KPNO image link is to an image of NGC 1232 and not
NGC 1300 (<a href="http://bugs.kde.org/show_bug.cgi?id=118917">#118917</a>)</li>
	<li>KTouch: Missing i18n calls. (<a href="http://bugs.kde.org/show_bug.cgi?id=117577">#117577</a>)</li>
	<li>KVoctrain: Make keyboard layout switching work again. (<a href="http://bugs.kde.org/show_bug.cgi?id=117391">#117391</a>)</li>
	<li>KWordQuiz: Don't crash when downloading vocabularies. (<a href="http://bugs.kde.org/show_bug.cgi?id=117391">#119899</a>)</li>
	<li>libkdeedu: (Plotting) Fix drawing of grid</li>
</ul>

<h3><a name="kdegames">kdegames</a> <font size="-2">[<a href="3_5_1/kdegames.txt">all SVN changes</a>]</font></h3><ul>
	<li>Atlantik: Fix can not quit Atlantik (<a href="http://bugs.kde.org/show_bug.cgi?id=112041">bug 112041</a>)</li>
	<li>KMahjongg: Make rendering less CPU intensive (<a href="http://bugs.kde.org/show_bug.cgi?id=118711">bug 118711</a>)</li>
	<li>KSnake: Change in number of enemy balls need a restart to take effect (<a href="http://bugs.kde.org/show_bug.cgi?id=111916">bug 111916</a>)</li>
  
</ul>

<h3><a name="kdegraphics">kdegraphics</a> <font size="-2">[<a href="3_5_1/kdegraphics.txt">all SVN changes</a>]</font></h3><ul>
	<li>KPDF
		<ul>
			<li>Fix constant disk I/O when resizing Navigation panel (<a href="http://bugs.kde.org/show_bug.cgi?id=117658">bug 117658</a>)</li>
			<li>Fix Wrong page size and layout in some documents (<a href="http://bugs.kde.org/show_bug.cgi?id=118110">bug 118110</a>)</li>
			<li>Fix DCT decoding for broken files (<a href="http://bugs.kde.org/show_bug.cgi?id=119569">bug 119569</a>)</li>
			<li>Fix crash in some strange documents (<a href="http://bugs.kde.org/show_bug.cgi?id=120310">bug 120310</a>)</li>
			<li>Fix slowlyness on documents with broken TOC definition</li>
			<li>Make non existant ps file loading fail gracefully (<a href="http://bugs.kde.org/show_bug.cgi?id=120343">bug 120343</a>)</li>
		</ul>
	</li>
	<li>KIconEdit
		<ul>
			<li>Needs configuration dialog help, proper markup for toolbars (<a href="http://bugs.kde.org/show_bug.cgi?id=96387">bug 96387</a>)</li>
		</ul>
	</li>
</ul>

<h3><a name="kdemultimedia">kdemultimedia</a> <font size="-2">[<a href="3_5_1/kdemultimedia.txt">all SVN changes</a>]</font></h3><ul>
	<li>JuK
		<ul>
		<li>Fix memory corruption bug causing crashes with recent glibc libraries. (<a href="http://bugs.kde.org/show_bug.cgi?id=117541">bug 117541</a>)</li>
		<li>Fix incorrect initial volume setting when using aKode as output. (<a href="http://bugs.kde.org/show_bug.cgi?id=118265">bug 118265</a>)</li>
		<li>Corrected list of file types that JuK supports.</li>
		<li>Fix command line handling of music files.  Now files given on the command line are automatically added to the Collection List.</li>
		<li>Fix "Album Random Play" feature to also use Artist information when choosing the next track to play. (<a href="http://bugs.kde.org/show_bug.cgi?id=120363">bug 120363</a>)</li>
		<li>Added musicbrainz 0.4-support (<a href="http://bugs.kde.org/show_bug.cgi?id=116575">bug 116575</a>)</li>
		</ul>
	</li>
</ul>

<h3><a name="kdenetwork">kdenetwork</a> <font size="-2">
[<a href="3_5_1/kdenetwork.txt">all SVN changes</a>]</font></h3><ul>
	<li>Kopete
		<ul>
			<li>Fix disconnects/crashes after connecting to a Yahoo webcam (<a href="http://bugs.kde.org/show_bug.cgi?id=113439">Bug #113439</a>)</li>
			<li>Don't send picture information packets to Yahoo buddies when connecting into invisible state, as one might use these packets to reveal your real connection state</li>
			<li>Don't crash when deleting several contacts that are in severeral groups</li>
			<li>Fix escaping of HTML in Yahoo messages (<a href="http://bugs.kde.org/show_bug.cgi?id=120469">Bug #120469</a>)</li>
		</ul>
	</li>
  <li>KPPP
    <ul>
      <li>fix initialization problem with some modems (Qualcomm 3G CDMA)</li>
      <li>support higher connection speeds (921600 bps)</li>
    </ul>
  </li>
</ul>

<h3><a name="kdepim">kdepim</a> <font size="-2">[<a href="3_5_1/kdepim.txt">all SVN changes</a>]</font></h3><ul>
<li>KAlarm
	<ul>
		<li>Fix email attachments being forgotten when saving alarms (<a href="http://bugs.kde.org/show_bug.cgi?id=118286">bug 118286</a>)</li>
		<li>Make autoclose of message windows work (<a href="http://bugs.kde.org/show_bug.cgi?id=118429">bug 118429</a>)</li>
		<li>Display alarm message windows within current screen in multi-head systems</li>
		<li>Fix toolbar configuration being lost after quitting KAlarm (<a href="http://bugs.kde.org/show_bug.cgi?id=117313">bug 117313</a>)</li>
		<li>Fix New From Template not creating alarm if template contents are not changed (<a href="http://bugs.kde.org/show_bug.cgi?id=119735">bug 119735</a>)</li>
		<li>Fix configuration dialog not fitting in 1024x768 screen in some translations (<a href="http://bugs.kde.org/show_bug.cgi?id=119346">bug 119346</a>)</li>
	</ul>
</li>
<li>KMail
	<ul>
		<li>KMail crashes while inserting files (<a href="http://bugs.kde.org/show_bug.cgi?id=111383">bug 111383</a>)</li>
		<li>KMail composer crashes when inserting file (<a href="http://bugs.kde.org/show_bug.cgi?id=108063">bug 108063</a>)</li>
		<li>KMail crashes when inserting files into email messages (<a href="http://bugs.kde.org/show_bug.cgi?id=111713">bug 111713</a>)</li>
		<li>autocomplete in composer completes section title instead of email-address (<a href="http://bugs.kde.org/show_bug.cgi?id=117217">bug 117217</a>)</li>
		<li>The item "network state" in the KMail file menu is very confusing (<a href="http://bugs.kde.org/show_bug.cgi?id=116054">bug 116054</a>)</li>
		<li>Crash when applying pipe through filters (<a href="http://bugs.kde.org/show_bug.cgi?id=113730">bug 113730</a>)</li>
		<li> subfolders and included mails lost when moving folder to cachedimap account (<a href="http://bugs.kde.org/show_bug.cgi?id=114985">bug 114985</a>)</li>
		<li>Cannot create new folders with IMAP using Dovecot-imapd, regression from 3.4.X (<a href="http://bugs.kde.org/show_bug.cgi?id=115254">bug 115254</a>)</li>
		<li>Retrieving folder contents screen is irritating  (<a href="http://bugs.kde.org/show_bug.cgi?id=116773">bug 116773</a>)</li>
		<li>Interpret old-fashioned time zone in email Date header (<a href="http://bugs.kde.org/show_bug.cgi?id=117848">bug 117848</a>)</li>
		<li>HTML formatting is lost when saving in drafts folder (<a href="http://bugs.kde.org/show_bug.cgi?id=106968">bug 106968</a>)</li>
		<li>composer kaddressbook no adresses at first launch (<a href="http://bugs.kde.org/show_bug.cgi?id=117118">bug 117118</a>)</li>
	</ul>
</li>
<li>KOrganizer
	<ul>
		<li>libkholidays bug in Belgian version (<a href="http://bugs.kde.org/show_bug.cgi?id=119456">bug 119456</a>)</li>
		<li>HTML export does not include location field (<a href="http://bugs.kde.org/show_bug.cgi?id=117676">bug 117676</a>)</li>
	</ul>
</li>
<li>Kontact
	<ul>
		<li>Mistake in reporting upcoming German Holidays on Overview page (<a href="http://bugs.kde.org/show_bug.cgi?id=118381">bug 118381</a>)</li>
		<li>Summary for "special dates" shows wrong time spans  (<a href="http://bugs.kde.org/show_bug.cgi?id=117545">bug 117545</a>)</li>
	</ul>
</li>
<li>KAddressBook
	<ul>
		<li>Allow the selection/copy of the formatted address (<a href="http://bugs.kde.org/show_bug.cgi?id=118634">bug 118634</a>)</li>
		<li>URL images not working in KAddressBook (<a href="http://bugs.kde.org/show_bug.cgi?id=104526">bug 104526</a>)</li>
		<li>Photo image location field is nonfunctional (<a href="http://bugs.kde.org/show_bug.cgi?id=117153">bug 117153</a>)</li>
		<li>Contact's photos are not cleared in "Contact Editor" extension bar (<a href="http://bugs.kde.org/show_bug.cgi?id=117758">bug 117758</a>)</li>
		<li>Need an easy way to copy and paste an address from the editor window (<a href="http://bugs.kde.org/show_bug.cgi?id=118634">bug 118634</a>)</li>
		<li>Exporting contacts to vcard generates _.vcf for EVERY contact without a real name (<a href="http://bugs.kde.org/show_bug.cgi?id=116006">bug 116006</a>)</li>
		<li>kaddressbook hangs on loading vcard (<a href="http://bugs.kde.org/show_bug.cgi?id=117749">bug 117749</a>)</li>
		<li>Use JPEG rather than PNG when storing pictures in vCards to reduce their size and to improve interoperability</li>
	</ul>
</li>
<li>KNotes
	<ul>
		<li>Possibility to sort knotes list of notes titles (<a href="http://bugs.kde.org/show_bug.cgi?id=54293">bug 54293</a>)</li>
		<li>separate "Clear" action visually from Cut/Copy/Paste since it's not doing anything with the clipboard (<a href="http://bugs.kde.org/show_bug.cgi?id=103780">bug 103780</a>)</li>
		<li>added a "Do not show again" box to the delete note dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=110672">bug 110672</a>)</li>
		<li>actually set the "Keep Above/Below" bit when creating a note on startup (<a href="http://bugs.kde.org/show_bug.cgi?id=113223">bug 113223</a>)</li>
		<li>race condition when using NETRootInfo::moveResizeRequest (<a href="http://bugs.kde.org/show_bug.cgi?id=101468">bug 101468</a>)</li>
		<li>line breaks not rendered properly in edit view (<a href="http://bugs.kde.org/show_bug.cgi?id=117437">bug 117437</a>)</li>
		<li>KNotes via network does not work properly (<a href="http://bugs.kde.org/show_bug.cgi?id=110838">bug 110838</a>)</li>
		<li>Confusing error requester while sending to self (<a href="http://bugs.kde.org/show_bug.cgi?id=110915">bug 110915</a>)</li>
		<li>Renaming a note opening it and editing its title does not rename it in the kontact notes view (<a href="http://bugs.kde.org/show_bug.cgi?id=119889">bug 119889</a>)</li>
	</ul>
</li>
<li>Akregator
	<ul>
	        <li>Do not open binary files in the HTML viewer but externally. 
            Fix &quot;Save Link As&quot; for binary files
            (<a href="http://bugs.kde.org/show_bug.cgi?id=120087">bug 120087</a>)</li>
	        <li> Fix article order in Combined View: sort by date
                (<a href="http://bugs.kde.org/show_bug.cgi?id=118055">bug 118055</a>)</li>
	        <li>Fix parsing of Atom 1.0 feeds with escaped HTML in it: Don't show tags
            as text (<a href="http://bugs.kde.org/show_bug.cgi?id=117938">bug 117938</a>)</li>
	        <li>Select next item in article list when deleting the selected article 
	        (<a href="http://bugs.kde.org/show_bug.cgi?id=119724">bug 119724</a>)</li>
	        <li>Avoid crashes as happening when moving a folder and deleting a subitem afterwards
                    (<a href="http://bugs.kde.org/show_bug.cgi?id=118659">bug 118659</a>)</li>
		<li>Fix problems with keyboard navigation getting stuck on duplicated
            articles and prevent creation of new items when selecting an unread
            dupe (<a href="http://bugs.kde.org/show_bug.cgi?id=114997">bug 114997</a>)</li>
		<li>Don't try to reload broken feeds every minute (<a href="http://bugs.kde.org/show_bug.cgi?id=113358">bug 113358</a>)</li>
		<li>CDATA in feed is not handled correctly (<a href="http://bugs.kde.org/show_bug.cgi?id=112491">bug 112491</a>)</li>
	</ul>
</li>
<li>KNode
	<ul>
		<li>Respect background color settings when article viewer is empty (<a href="http://bugs.kde.org/118521">bug #118521</a>)</li>
		<li>Fix crash on startup if auto mark-as-read is disabled (<a href="http://bugs.kde.org/116514">bug #116514</a>)</li>
		<li>Reset busy cursor when folder loading fails. (<a href="http://bugs.kde.org/118879">bug #118879</a>)</li>
	</ul>
</li>
</ul>

<h3><a name="kdesdk">kdesdk</a> <font size="-2">[<a href="3_5_1/kdesdk.txt">all SVN changes</a>]</font></h3><ul>
	<li>Cervisia
		<ul>
		   <li>
			 Do not use absolute pathnames when committing the top level folder of a working copy
			 (<a href="http://bugs.kde.org/show_bug.cgi?id=117220">bug #117220</a>)
		   </li>
	   </ul>
   </li>
	<li>KBabel
		<ul>
		   <li>
			 avoid user-visible strings that need to be translated in two ways 
			 (<a href="http://bugs.kde.org/show_bug.cgi?id=114151">bug #114151</a>)
		   </li>
		   <li>KBabel editor: Fix and improve source references</li>
		   <li>
			 KBabel editor: add a new variable @POFILEDIR@ for source references.
			 This is for allowing search paths starting at the directory of the PO file,
			 like what is needed for GNU projects: starting at the parent directory 
			 (<a href="http://bugs.kde.org/show_bug.cgi?id=114041">bug #114041</a>)
		   </li>
		   <li>
			 KBabel editor: allow backslashes in source references in the PO file
			 (<a href="http://bugs.kde.org/show_bug.cgi?id=116393">bug #116393</a>)
		   </li>
		   <li>
			 KBabelDict: clicking the help button of KBabelDict calls 
			 the corresponding section in the KBabel documentation
		   </li>
		   <li>
			 Improved KBabel documentation (including
			 <a href="http://bugs.kde.org/show_bug.cgi?id=85885">bug #85885</a>)
		   </li>
	   </ul>
   </li>
   <li>Umbrello
	   <ul>
			<li>Code import for <a href="http://java.sun.com/docs/books/jls/third_edition/html/j3TOC.html">Java</a> and <a href="http://www.python.org">Python</a> (<a href="http://bugs.kde.org/79648#c8">#79648</a>)</li>
			<li>fix loading of associationwidget with non-default color</li>
			<li>fix moving of initial and end activity</li>
			<li>fix operation parameter and return types including <a href="http://sourceforge.net/mailarchive/forum.php?thread_id=9298028&amp;forum_id=460">template expressions</a></li>
			<li>Support C++ <i>const</i> methods (aka queries, see <a href="http://bugs.kde.org/60452#c8">#60452</a>)</li>
			<li>Change associations, aggregations, etc. on-the-fly (<a href="http://bugs.kde.org/109963">#109963</a>)</li>
			<li>Collaboration Diagram: labels are reset to default position after moving them (<a href="http://bugs.kde.org/117791">#117791</a>)</li>
			<li>Imported C++ classes not saved correctly in the XMI file (<a href="http://bugs.kde.org/117875">#117875</a>)</li>
			<li>In ER models adding associations will add blank space in the entity attributes (<a href="http://bugs.kde.org/117990">#117990</a>)</li>
			<li>ER diagrams need to underline the attribute name of primary keys (<a href="http://bugs.kde.org/118570">#118570</a>)</li>
			<li>Cannot anchor notes to activity elements in Activity Diagram (<a href="http://bugs.kde.org/118719">#118719</a>)</li>
		</ul>
	</li>
</ul>

<h3><a name="kdetoys">kdetoys</a> <font size="-2">[<a href="3_5_1/kdetoys.txt">all SVN changes</a>]</font></h3>
<h3><a name="kdeutils">kdeutils</a> <font size="-2">[<a href="3_5_1/kdeutils.txt">all SVN changes</a>]</font></h3><ul>
	<li>SuperKaramba
		<ul>
			<li>
				Allow to remove a local theme (<a href="http://bugs.kde.org/show_bug.cgi?id=112011">bug #112011</a>)
			</li>
			<li>
				Enhanced itemDropped-func with x,y-value  (<a href="http://bugs.kde.org/show_bug.cgi?id=120262">bug #120262</a>)
			</li>
			<li>
				some major strategic feature enhancements to superkaramba, which make it possible to write kicker-replacement themes (<a href="http://bugs.kde.org/show_bug.cgi?id=116618">bug #116618</a>)
			</li>
			<li>
				locale in python themes (<a href="http://bugs.kde.org/show_bug.cgi?id=115486">bug #115486</a>)
			</li>
			<li>
				Applets appear on top after switch desktop (<a href="http://bugs.kde.org/show_bug.cgi?id=116227">bug #116227</a>)
			</li>
			<li>
				Get new Superkaramba theme from internet (with KHotNewStuff) doesn't work (<a href="http://bugs.kde.org/show_bug.cgi?id=116789">bug #116789</a>)
			</li>
			<li>
				SuperKaramba does not use the result for its test for KNewStuff (<a href="http://bugs.kde.org/show_bug.cgi?id=118611">bug #118611</a>)
			</li>
			<li>
				Don't popup dialog at startup when there are no themes on desktop (<a href="http://bugs.kde.org/show_bug.cgi?id=118920">bug #118920</a>)
			</li>
			<li>
				Non-intuitive theme removing in Theme Dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=119762">bug #119762</a>)
			</li>
		</ul> 
	</li>
	<li>KCalc
		<ul>
			<li>KCalc returns wrong value (<a href="http://bugs.kde.org/show_bug.cgi?id=118909">bug #118909</a>)</li>
			<li>KCalc truncates decimals sporadically (<a href="http://bugs.kde.org/show_bug.cgi?id=118300">bug #118300</a>)</li>
			<li>Cannot increase the precision to 10 digits (<a href="http://bugs.kde.org/show_bug.cgi?id=116835">bug #116835</a>)</li>
			<li>Cut buffer is filled with wrong value when you click on results (<a href="http://bugs.kde.org/show_bug.cgi?id=118279">bug #118279</a>)</li>
		</ul>
	</li>
	<li>KLaptopDaemon
		<ul>
			<li>KLaptopDaemon improperly links against libstdc++ (<a href="http://bugs.kde.org/show_bug.cgi?id=111497">bug #111497</a>)</li>
		</ul>
	</li>
</ul>

<h3><a name="kdevelop">kdevelop</a> <font size="-2">[<a href="3_5_1/kdevelop.txt">all SVN changes</a>]</font></h3><ul>
	<li>
		adding externally created source files to project (<a href="http://bugs.kde.org/show_bug.cgi?id=115421">bug #115421</a>)
	</li>
	<li>
		 cannot use the right click "add 'filename' to project" function (<a href="http://bugs.kde.org/show_bug.cgi?id=111355">bug #111355</a>)
	</li>
	<li>
		 Adding files from subdirectory to project fails (<a href="http://bugs.kde.org/show_bug.cgi?id=108268">bug #108268</a>)
	</li>
	<li>
		 Cannot add files to/remove files from project correctly if the project is opened by a symbolic-linked path (<a href="http://bugs.kde.org/show_bug.cgi?id=102706">bug #102706</a>)
	</li>
	<li>
		 custom c++ project: add/remove files broken (<a href="http://bugs.kde.org/show_bug.cgi?id=98852">bug #98852</a>)
	</li>
</ul>

<h3><a name="kdewebdev">kdewebdev</a> <font size="-2">[<a href="3_5_1/kdewebdev.txt">all SVN changes</a>]</font></h3>
<h4>Quanta Plus</h4>
<ul>
  <li> better handling of quotation marks when editing tags inside a script area (<a href="http://bugs.kde.org/show_bug.cgi?id=118693">bug #118693</a>)</li>
  <li>don't show the file changed dialog after using save as and save again</li>
  <li>crash fixes in VPL (<a href="http://bugs.kde.org/show_bug.cgi?id=118686">bug #118686</a>)</li>
  <li>don't loose the comment closing character when formatting the XML code (<a href="http://bugs.kde.org/show_bug.cgi?id=118453">bug #118453</a>)</li>
  <li>insert valid img tag for XHTML documents (<a href="http://bugs.kde.org/show_bug.cgi?id=118805">bug #118805</a>)</li>
  <li>don't show the Pages tab in DTEP editing dialog more than once (<a href="http://bugs.kde.org/show_bug.cgi?id=118840">bug #118840</a>)</li>
  <li>set the DTEP of the document to the one selected in the Quick Start dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=118814">bug #118814</a>)</li>
  <li>don't have two Close actions (<a href="http://bugs.kde.org/show_bug.cgi?id=118448">bug #118448</a>)</li>
  <li>don't show CSS pseudo-classes in autocompletion for the class attribute (<a href="http://bugs.kde.org/show_bug.cgi?id=119373">bug #119373</a>)</li>
  <li>avoid deadlock when loading the DTEPs</li>
  <li>add XHTML 1.1 and XHTML 1.0 Basic to the quickstart dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=118813">bug #118813</a>)</li>
  <li>new DCOP interfaces/methods: WindowManagerIf::setDtep</li>
  <li>Doesn't parse PHP class methods which returns references (those with ampersand) (<a href="http://bugs.kde.org/show_bug.cgi?id=118914">bug #118914</a>)</li>
  
</ul>

<h4>Kommander</h4>