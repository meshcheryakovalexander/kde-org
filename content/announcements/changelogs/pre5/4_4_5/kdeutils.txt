------------------------------------------------------------------------
r1133566 | scripty | 2010-06-02 14:00:27 +1200 (Wed, 02 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1134705 | scripty | 2010-06-05 14:02:47 +1200 (Sat, 05 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1134976 | rkcosta | 2010-06-06 11:11:35 +1200 (Sun, 06 Jun 2010) | 10 lines

Backport r1134973.

Connect to QDialog::finished() instead of reimplementing closeEvent().

closeEvent() was not called when the dialog was closed via the Close
button, which lead, for example, to the part not being closed some
times.

See r1134650 and bug 211880.

------------------------------------------------------------------------
r1135272 | scripty | 2010-06-07 13:57:12 +1200 (Mon, 07 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1135706 | cfeck | 2010-06-08 11:49:01 +1200 (Tue, 08 Jun 2010) | 5 lines

Fix crash when accessing empty action list (backport r1135705)

CCBUG: 187966
CCBUG: 206831

------------------------------------------------------------------------
r1136132 | scripty | 2010-06-09 14:05:03 +1200 (Wed, 09 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1137237 | scripty | 2010-06-12 14:13:59 +1200 (Sat, 12 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1137714 | rkcosta | 2010-06-14 16:53:21 +1200 (Mon, 14 Jun 2010) | 3 lines

Backport r1137708.

Simplify the setChecked() call: we don't need an if here.
------------------------------------------------------------------------
r1137715 | rkcosta | 2010-06-14 16:53:53 +1200 (Mon, 14 Jun 2010) | 3 lines

Backport r1137709.

Simplify the setSingleFolderArchive() call: we don't need an if here.
------------------------------------------------------------------------
r1137716 | rkcosta | 2010-06-14 16:53:57 +1200 (Mon, 14 Jun 2010) | 8 lines

Backport r1137712.

Revert most of r948055.

Single-file archives should _be_ considered single-folder archives.

BUG: 225426
FIXED-IN: 4.4.5
------------------------------------------------------------------------
r1140176 | rkcosta | 2010-06-20 16:09:27 +1200 (Sun, 20 Jun 2010) | 5 lines

Backport r1140165.

SVN_SILENT

Make things fit in 80 columns.
------------------------------------------------------------------------
r1140177 | rkcosta | 2010-06-20 16:09:31 +1200 (Sun, 20 Jun 2010) | 3 lines

Backport r1140166.

Make member variables private.
------------------------------------------------------------------------
r1140178 | rkcosta | 2010-06-20 16:09:35 +1200 (Sun, 20 Jun 2010) | 6 lines

Backport r1140168.

Make m_headerString static, and change its type to QLatin1String.

Furthermore, its name has been changed to headerString to show it is not
a member variable.
------------------------------------------------------------------------
r1140179 | rkcosta | 2010-06-20 16:09:39 +1200 (Sun, 20 Jun 2010) | 3 lines

Backport r1140172.

If we get a repeating entry, sum the compressed size.
------------------------------------------------------------------------
r1140180 | rkcosta | 2010-06-20 16:09:44 +1200 (Sun, 20 Jun 2010) | 23 lines

Backport r1140173.

Revamp the clirar plugin.

This was prompted by bug 242071: RAR files have this concept of
subheaders, which include comments, NTFS streams and other things I have
no idea since the format is undocumented.

Subheaders can be ignored during the listing, but they confused the old
parser code because they were unexpected and broke the expected listing
pattern.

In order to fix that, we now call unrar with 'vt' instead of just 'v' so
that we get more information. However, this means more lines are output
for each entry, so our "parser" needed to be improved.

The code looks much cleaner and (hopefully) easier to understand now.

Furthermore, the old code probably ignored files whose names started
with '*'. Finally, some TODOs have been added for some aspects we should
watch out when dealing with the code.

CCBUG: 242071
------------------------------------------------------------------------
r1140181 | rkcosta | 2010-06-20 16:09:49 +1200 (Sun, 20 Jun 2010) | 5 lines

Backport r1140174.

Check if we have a thread before executing operations on it.

CCBUG: 221551
------------------------------------------------------------------------
r1140185 | rkcosta | 2010-06-20 16:32:22 +1200 (Sun, 20 Jun 2010) | 10 lines

Backport r1140184.

Ignore entries called "/".

'/' is the path separator, so the entry will ultimately become an empty
QStringList and crash things.

CCBUG: 241967
FIXED-IN: 4.4.5

------------------------------------------------------------------------
r1141480 | kossebau | 2010-06-23 09:31:12 +1200 (Wed, 23 Jun 2010) | 6 lines

fixed: PODTableView called markPOD()/unmarkPOD() on PODDecoderTool, even if there was no view -> crash

fix should be part of upcoming 4.4.5

BUG:242214

------------------------------------------------------------------------
r1141483 | kossebau | 2010-06-23 09:34:26 +1200 (Wed, 23 Jun 2010) | 1 line

changed: update version number
------------------------------------------------------------------------
