------------------------------------------------------------------------
r1168978 | scripty | 2010-08-28 14:26:22 +1200 (Sat, 28 Aug 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1169305 | scripty | 2010-08-29 14:28:17 +1200 (Sun, 29 Aug 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1170658 | lueck | 2010-09-02 07:27:25 +1200 (Thu, 02 Sep 2010) | 1 line

doc backport for 4.5.2
------------------------------------------------------------------------
r1170803 | lueck | 2010-09-02 18:19:09 +1200 (Thu, 02 Sep 2010) | 1 line

revert backport
------------------------------------------------------------------------
r1170922 | wrohdewald | 2010-09-02 22:26:38 +1200 (Thu, 02 Sep 2010) | 1 line

fix another small leak in DeferredBlock
------------------------------------------------------------------------
r1172868 | scripty | 2010-09-08 15:20:42 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173556 | lueck | 2010-09-10 05:57:32 +1200 (Fri, 10 Sep 2010) | 1 line

backport from trunk r1173555: add i18n() call to make the game name in the window caption translated
------------------------------------------------------------------------
r1174230 | ossi | 2010-09-12 03:25:09 +1200 (Sun, 12 Sep 2010) | 2 lines

backport molecule name fix

------------------------------------------------------------------------
r1174637 | scripty | 2010-09-13 14:37:10 +1200 (Mon, 13 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1175702 | wrohdewald | 2010-09-16 04:15:30 +1200 (Thu, 16 Sep 2010) | 1 line

SQL: speed up table creation by wrapping it into a transaction
------------------------------------------------------------------------
r1175703 | wrohdewald | 2010-09-16 04:16:07 +1200 (Thu, 16 Sep 2010) | 1 line

Query: do not translate log message (Albert Astals Cid)
------------------------------------------------------------------------
r1175704 | wrohdewald | 2010-09-16 04:20:32 +1200 (Thu, 16 Sep 2010) | 4 lines

SQL: writing an entire ruleset took up to 10 seconds. Speed up by
putting all queries into one transaction


------------------------------------------------------------------------
r1175705 | wrohdewald | 2010-09-16 04:22:12 +1200 (Thu, 16 Sep 2010) | 3 lines

When selecting a server that does not yet exist in the database,
the user add dialog showed an empty or the wrong server name

------------------------------------------------------------------------
r1175706 | wrohdewald | 2010-09-16 04:23:23 +1200 (Thu, 16 Sep 2010) | 3 lines

with uploading own voice disabled, own voice was not even locally
played

------------------------------------------------------------------------
r1175707 | wrohdewald | 2010-09-16 04:26:45 +1200 (Thu, 16 Sep 2010) | 3 lines

starting kajonggserver: if kajonggserver.py exists in working
directory, use that one. Otherwise call kajonggserver

------------------------------------------------------------------------
r1175709 | wrohdewald | 2010-09-16 04:29:18 +1200 (Thu, 16 Sep 2010) | 2 lines

fix arg passing to kajonggserver subprocess

------------------------------------------------------------------------
r1175710 | wrohdewald | 2010-09-16 04:31:26 +1200 (Thu, 16 Sep 2010) | 2 lines

clear moves list between hands - we do not need up to 15000 moves of an entire game

------------------------------------------------------------------------
r1175932 | wrohdewald | 2010-09-16 19:35:01 +1200 (Thu, 16 Sep 2010) | 1 line

revert previous change
------------------------------------------------------------------------
r1176358 | coates | 2010-09-18 00:50:10 +1200 (Sat, 18 Sep 2010) | 8 lines

Make About dialog copyright statement translateable.

Backport of commit 1176233.

Add a missing I18N_NOOP macro.

CCBUG:251494

------------------------------------------------------------------------
r1179295 | scripty | 2010-09-25 14:48:17 +1200 (Sat, 25 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179695 | scripty | 2010-09-26 16:13:05 +1300 (Sun, 26 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179824 | ianw | 2010-09-26 22:08:01 +1300 (Sun, 26 Sep 2010) | 2 lines

-m'BUG: 221132 fixed. KMahjongg layouts with odd numbers of tiles, making them impossible to solve, are modified to have even numbers. Many thanks to Alexey Charkov for these modifications.'

------------------------------------------------------------------------
