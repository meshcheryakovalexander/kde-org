2007-11-28 13:15 +0000 [r742646]  mueller

	* branches/KDE/3.5/kdebindings/smoke/kde/qtguess.pl.in,
	  branches/KDE/3.5/kdebindings/smoke/qt/qtguess.pl.in: only handle
	  those #define that have no value. the others are likely not
	  feature enable/disable macros, and defining them twice fails with
	  gcc 4.3.

2008-02-13 09:53 +0000 [r774466]  coolo

	* branches/KDE/3.5/kdebindings/kdebindings.lsm: 3.5.9

