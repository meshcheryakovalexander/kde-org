------------------------------------------------------------------------
r1016845 | scripty | 2009-08-29 03:44:33 +0000 (Sat, 29 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1017219 | scripty | 2009-08-30 03:01:57 +0000 (Sun, 30 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1017641 | scripty | 2009-08-31 03:00:41 +0000 (Mon, 31 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019171 | scripty | 2009-09-03 04:09:44 +0000 (Thu, 03 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019466 | kossebau | 2009-09-03 17:39:02 +0000 (Thu, 03 Sep 2009) | 3 lines

backport of 1019464: fixed: "exec=kbattleship &u" is needed, otherwise connecting to a game from network:/ doesn't work (hardcoding to KBattleship is acceptable currently)


------------------------------------------------------------------------
r1019502 | lueck | 2009-09-03 18:50:41 +0000 (Thu, 03 Sep 2009) | 1 line

games doc backport for 4.3
------------------------------------------------------------------------
r1019519 | lueck | 2009-09-03 19:41:31 +0000 (Thu, 03 Sep 2009) | 1 line

missed games doc backport for 4.3
------------------------------------------------------------------------
r1020018 | scripty | 2009-09-05 03:20:16 +0000 (Sat, 05 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1021215 | binner | 2009-09-08 14:44:44 +0000 (Tue, 08 Sep 2009) | 2 lines

fix word duplication

------------------------------------------------------------------------
r1021379 | coates | 2009-09-09 02:06:00 +0000 (Wed, 09 Sep 2009) | 4 lines

Stop the demo if the user changes the game options in Klondike and Solitaire.

Otherwise the menu and the game internals can get out of sync. 

------------------------------------------------------------------------
r1021387 | scripty | 2009-09-09 03:36:57 +0000 (Wed, 09 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1021789 | scripty | 2009-09-10 03:10:24 +0000 (Thu, 10 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1022200 | scripty | 2009-09-11 03:18:30 +0000 (Fri, 11 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1022566 | scripty | 2009-09-12 03:05:15 +0000 (Sat, 12 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1023165 | scripty | 2009-09-14 03:13:34 +0000 (Mon, 14 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1023622 | scripty | 2009-09-15 03:11:46 +0000 (Tue, 15 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024113 | scripty | 2009-09-16 03:15:38 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024439 | scripty | 2009-09-16 16:12:07 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1025127 | scripty | 2009-09-18 03:07:33 +0000 (Fri, 18 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1026203 | scripty | 2009-09-21 03:00:25 +0000 (Mon, 21 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1026635 | scripty | 2009-09-22 03:09:26 +0000 (Tue, 22 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1026974 | scripty | 2009-09-23 03:16:07 +0000 (Wed, 23 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1027328 | mkraus | 2009-09-23 20:53:28 +0000 (Wed, 23 Sep 2009) | 1 line

backport from 1027318 (fix update bug in kgamethemeselector)
------------------------------------------------------------------------
r1028738 | scripty | 2009-09-28 03:05:25 +0000 (Mon, 28 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1029129 | rkcosta | 2009-09-29 02:13:51 +0000 (Tue, 29 Sep 2009) | 6 lines

Backport r1004745.

Fix compile with Qt 4.6

CCMAIL: nicolas-kde@roffet.com

------------------------------------------------------------------------
r1030278 | scripty | 2009-10-02 02:56:57 +0000 (Fri, 02 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
