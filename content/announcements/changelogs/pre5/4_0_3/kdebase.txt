2008-02-28 19:32 +0000 [r780318]  wstephens

	* branches/KDE/4.0/kdebase/apps/lib/konq/favicons/favicons.cpp:
	  backport r778158 by mkretz - fix endless loop

2008-02-29 12:56 +0000 [r780552]  hein

	* branches/KDE/4.0/kdebase/apps/konsole/src/ViewManager.cpp:
	  Backport r779983: Disable some (unused) actions for the KPart to
	  avoid shortcut clashes between it and KPart users.

2008-03-03 07:08 +0000 [r781544-781543]  ppenz

	* branches/KDE/4.0/kdebase/apps/dolphin/src/viewpropsprogressinfo.cpp:
	  Backport of #156752: fixed crash when cancelling the "apply view
	  properties" dialog

	* branches/KDE/4.0/kdebase/apps/dolphin/src/statusbarspaceinfo.cpp:
	  Backport for #153778, #156271 and #155608: fixed problems with
	  wrong space information

2008-03-03 08:43 +0000 [r781557]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/khotkeys/kcontrol/actions_listview_widget.cpp:
	  Backport: Fix crash when we delete actions/group etc.

2008-03-03 12:25 +0000 [r781650]  mlaurent

	* branches/KDE/4.0/kdebase/runtime/knewstuff/ox32-action-khotnewstuff.png
	  (added),
	  branches/KDE/4.0/kdebase/runtime/knewstuff/ox16-action-khotnewstuff.png
	  (added),
	  branches/KDE/4.0/kdebase/runtime/knewstuff/ox32-action-knewstuff.png
	  (removed),
	  branches/KDE/4.0/kdebase/runtime/knewstuff/ox64-action-khotnewstuff.png
	  (added),
	  branches/KDE/4.0/kdebase/runtime/knewstuff/ox16-action-knewstuff.png
	  (removed),
	  branches/KDE/4.0/kdebase/runtime/knewstuff/ox64-action-knewstuff.png
	  (removed): Backport: Rename icon name (now we can show icons)

2008-03-03 12:43 +0000 [r781663]  mueller

	* branches/KDE/4.0/kdebase/apps/ConfigureChecks.cmake: fix
	  kinfocenter

2008-03-03 12:59 +0000 [r781669]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/khotkeys/kcontrol/condition_list_widget.cpp:
	  Backport: Fix crash when we delete condition

2008-03-03 19:20 +0000 [r781852]  rivol

	* branches/KDE/4.0/kdebase/workspace/kwin/scene.cpp: Backport
	  r781850: Return empty shape region if XShapeGetRectangles()
	  returns null.

2008-03-04 07:39 +0000 [r781981]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/khotkeys/kcontrol/triggers_tab.cpp:
	  Backport: Fix crash when we delete action

2008-03-04 08:10 +0000 [r781987]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/khotkeys/kcontrol/condition_list_widget.cpp,
	  branches/KDE/4.0/kdebase/workspace/khotkeys/kcontrol/action_list_widget.cpp:
	  Backport: Fix crash when we delete action

2008-03-04 20:34 +0000 [r782315]  dfaure

	* branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/filetypedetails.h,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/filetypesview.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/kservicelistwidget.h,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/keditfiletype.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/mimetypedata.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/filetypesview.h,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/tests/filetypestest.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/filetypedetails.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/mimetypedata.h,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/filetypes/kservicelistwidget.cpp:
	  Backport 782260 782265 782290 under heavy pressure from Pino
	  (just joking :) Details: - Write less useless stuff into
	  mimeapps.list (e.g. don't write part services if we only modified
	  app services and vice-versa). Also, removing an app and then
	  another app from the same mimetype, would ignore the first
	  removed app; fixed (and unit-tested). - ... which leads to
	  another bug: re-adding a removed application didn't work because
	  we didn't remove it from the removed list... - Update mimetype
	  details for all mimetypes when ksycoca announces that things have
	  changed

2008-03-05 12:07 +0000 [r782508]  mlaurent

	* branches/KDE/4.0/kdebase/runtime/kcontrol/kded/kcmkded.cpp:
	  Backport: Disable Help button we don't have doc

2008-03-05 17:50 +0000 [r782674]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/clients/oxygen/oxygenclient.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/clients/oxygen/oxygenclient.h:
	  Fix shaping - KCommonDecoration has a function where to do it,
	  and I don't see the point in resetting the shape every time in
	  paintEvent(). Partially fixes resizing problems with Qt4.4.

2008-03-05 21:43 +0000 [r782749]  mlaurent

	* branches/KDE/4.0/kdebase/apps/konqueror/settings/kio/kcookiesmanagement.cpp:
	  Backport: Fix error between message caption

2008-03-05 21:50 +0000 [r782756]  edulix

	* branches/KDE/4.0/kdebase/apps/dolphin/src/dolphinpart.cpp:
	  backport of bugfix r782282

2008-03-05 21:53 +0000 [r782760]  mlaurent

	* branches/KDE/4.0/kdebase/apps/konqueror/settings/kio/kcookiesmanagement.cpp:
	  Backport

2008-03-05 22:18 +0000 [r782766]  mlaurent

	* branches/KDE/4.0/kdebase/apps/konqueror/settings/kio/useragentselectordlg.cpp:
	  Backport: Fix display real user agent

2008-03-05 23:19 +0000 [r782782]  ereslibre

	* branches/KDE/4.0/kdebase/apps/dolphin/src/dolphinpart.cpp:
	  Backport of rev 782768. Fixes a crash when right clicking on the
	  viewport and no item is selected. If any changes are done on
	  trunk with this issue, remember to backport them.

2008-03-06 09:08 +0000 [r782842]  ppenz

	* branches/KDE/4.0/kdebase/runtime/kioslave/thumbnail/svgcreator.cpp:
	  Backport of #152603: keep aspect ratio for SVG previews (thanks
	  to Glenn Ergeerts for the patch)

2008-03-06 12:12 +0000 [r782883]  mlaurent

	* branches/KDE/4.0/kdebase/apps/konqueror/settings/konqhtml/filteropts.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/settings/konqhtml/filteropts.h:
	  Backport: Fix enable/disable buttons

2008-03-06 12:21 +0000 [r782884]  mueller

	* branches/KDE/4.0/kdebase/apps/kfind/kfwin.cpp: merge r758252 from
	  trunk/

2008-03-06 12:26 +0000 [r782891]  mlaurent

	* branches/KDE/4.0/kdebase/runtime/knetattach/knetattach.cpp,
	  branches/KDE/4.0/kdebase/runtime/knetattach/knetattach.h:
	  Backport: Fix help button

2008-03-07 01:53 +0000 [r783104]  pino

	* branches/KDE/4.0/kdebase/apps/konqueror/kttsplugin/CMakeLists.txt,
	  branches/KDE/4.0/kdebase/apps/konqueror/kttsplugin/khtmlkttsd.cpp:
	  Partially backport SVN commit 783095 by pino: > - use the
	  generated interface from org.kde.KSpeech.xml, and apply the right
	  function calls > - port the rich text detection hopefully in the
	  right way from that, reverted the changed to an i18n message
	  (branch is in string freeze) Also, backport from r783096: - the
	  change of a the failure of the getTalkerCapabilities2 into a
	  debug message - the icon fix

2008-03-07 15:08 +0000 [r783242]  mueller

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/applet.cpp: merge
	  r771845 from trunk

2008-03-07 19:54 +0000 [r783315]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/client.cpp: QObject: Do
	  not delete object, 'unnamed', during its event handler! BUG:
	  156998

2008-03-07 21:20 +0000 [r783334]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/clients/b2/b2client.cpp:
	  Don't do 'foo->setAlphaChannel(*foo)', the function temporarily
	  destroys the internal data and the argument is just a reference
	  to this (temporarily broken) object.

2008-03-07 23:05 +0000 [r783362]  pino

	* branches/KDE/4.0/kdebase/apps/dolphin/src/dolphin.desktop: fix
	  documentation path

2008-03-09 20:06 +0000 [r783861]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/workspace.h,
	  branches/KDE/4.0/kdebase/workspace/kwin/events.cpp: Add a hack
	  that somewhat improves screen repaint after X session switch with
	  drivers that have a problem with this.

2008-03-09 20:15 +0000 [r783864]  sebsauer

	* branches/KDE/4.0/kdebase/workspace/plasma/containments/desktop/desktop.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/containments/desktop/desktop.h:
	  fix changing wallpaper causes desktop to go white Thanks for this
	  backported patch goes to Jan Mette :) BUG:158784

2008-03-09 23:38 +0000 [r783906]  sebsauer

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/widgets/icon.cpp,
	  branches/KDE/4.0/kdebase/workspace/libs/plasma/widgets/icon.h:
	  fix icons on panel and desktop dont execute programs when
	  doubleclick option is activated thanks goes to CSights! :)
	  BUG:155413

2008-03-10 12:45 +0000 [r784017]  craig

	* branches/KDE/4.0/kdebase/workspace/kcontrol/kfontinst/kio/KioFonts.h,
	  branches/KDE/4.0/kdebase/workspace/kcontrol/kfontinst/kio/KioFonts.cpp:
	  Correctly use KTemporaryFile - fixes installing fonts from zip://

2008-03-10 15:11 +0000 [r784055]  dfaure

	* branches/KDE/4.0/kdebase/workspace/plasma/runners/locations/locationrunner.cpp:
	  not all urls and paths are lowercase BUG: 159017

2008-03-10 23:04 +0000 [r784334]  lunakl

	* branches/KDE/4.0/kdelibs/phonon/videowidget.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/actions/ktogglefullscreenaction.cpp,
	  branches/KDE/4.0/kdebase/apps/konsole/src/MainWindow.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/actions/ktogglefullscreenaction.h,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/konqmisc.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/konqmainwindow.cpp:
	  Backport r784333 (remove usage of QWidget::showFullScreen()
	  etc.). BUG: 157941

2008-03-12 17:02 +0000 [r784854]  ppenz

	* branches/KDE/4.0/kdebase/apps/dolphin/src/dolphinmainwindow.cpp:
	  Backport of bug #158317: menu actions have not been updated
	  correctly when changing between the split views (regression from
	  KDE 4.0.1 to KDE 4.0.2)

2008-03-13 11:44 +0000 [r785141]  lunakl

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/systemtray/systemtraycontainer.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/systemtray/systemtraycontainer.h:
	  Check whether the to-be-embedded window has been destroyed
	  meanwhile. BUG: 159167

2008-03-13 12:42 +0000 [r785165]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/kmenuedit/menufile.cpp:
	  Backport: Fix crash when we delete entry and save it

2008-03-14 01:49 +0000 [r785436]  knight

	* branches/KDE/4.0/kdebase/apps/konsole/src/Part.cpp: Backport
	  startup focus fix for embedded terminal. BUG: 159275

2008-03-14 20:25 +0000 [r785737]  lunakl

	* branches/KDE/4.0/kdebase/apps/konqueror/src/konqviewmanager.cpp:
	  One more removing of QWidget::showFullScreen/Normal(), no idea
	  how I missed it.

2008-03-16 15:25 +0000 [r786273-786272]  ianjo

	* branches/KDE/4.0/kdebase/apps/kinfocenter/info/info.cpp: Changed
	  GetInfo_ReadfromPipe to use QProcess instead. Fixes PCI info
	  module.

	* branches/KDE/4.0/kdebase/apps/kinfocenter/info/info_linux.cpp:
	  Fixed usage of stream.atEnd() in various places, it doesn't work
	  for files in /proc (as per QFile docs). Fixes interrupts,
	  io-ports, scsi, sound, dma channels and devices not working on
	  linux. Kinfocenter is back to its former qt3 glory. BUG: 155192

2008-03-16 18:06 +0000 [r786316]  ianjo

	* branches/KDE/4.0/kdebase/apps/kinfocenter/info/opengl.cpp: Fix
	  rarely hit path where glXDestroyContext would be called twice.

2008-03-16 18:42 +0000 [r786320]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/options.cpp: Don't
	  disable xinerama options if xinerama is not available, this may
	  change with xrandr1.2 and in the worst case those options simply
	  will degenerate to the trivial one-screen case. BUG: 142860

2008-03-17 20:26 +0000 [r786727]  lunakl

	* branches/KDE/4.0/kdebase/workspace/krunner/lock/lockprocess.cc:
	  "ERROR: Something removed SubstructureNotifyMask from the root
	  window!!!"

2008-03-18 14:57 +0000 [r787048]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/manage.cpp: When a dialog
	  is opened for a minimized window, minimize it too.

2008-03-18 22:30 +0000 [r787303]  hein

	* branches/KDE/4.0/kdebase/apps/konsole/src/ViewManager.cpp:
	  Backport 787302: Disable some more actions not relevant to the
	  KPart.

2008-03-19 12:04 +0000 [r787504]  hein

	* branches/KDE/4.0/kdebase/apps/konsole/src/ViewManager.cpp:
	  Backport r787503: Disable another action not relevant to the
	  KPart.

2008-03-19 18:13 +0000 [r787723]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/workspace.cpp: Backport
	  r787722 (more efficient QTimer usage).

2008-03-19 18:59 +0000 [r787735]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/client.cpp: Fix wrong
	  code removal from r558258.

2008-03-19 19:34 +0000 [r787742]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/client.h,
	  branches/KDE/4.0/kdebase/workspace/kwin/geometry.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/client.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/workspace.cpp: Make
	  border sizes while maximizing take effect even when the actual
	  geometry does not change. BUG: 158252

2008-03-20 09:59 +0000 [r787946]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/events.cpp: Discard
	  window pixmap only if the window actually resizes.

2008-03-20 10:06 +0000 [r787949]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/lib/kwinglutils.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/scene_opengl.h,
	  branches/KDE/4.0/kdebase/workspace/kwin/lib/kwinglutils.h,
	  branches/KDE/4.0/kdebase/workspace/kwin/scene_opengl.cpp: Don't
	  discard window texture when only the shape changes but the window
	  geometry actually stays the same. Avoids large number of rebinds
	  (with no strict binding) with the launch feedback icon.

2008-03-20 11:18 +0000 [r787979]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/scene.h,
	  branches/KDE/4.0/kdebase/workspace/kwin/scene.cpp: Cache window
	  quads since they usually don't change and this gives few fps.

2008-03-20 11:43 +0000 [r787994]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/nvidiahack.cpp: After
	  recent optimizations __GL_YIELD=NOTHING does not give noticeable
	  performance impact with either glxgears or the launch feedback
	  icon, so KWIN_NVIDIA_HACK=1 is the default again.

2008-03-20 17:28 +0000 [r788105]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/main.cpp: Use printf for
	  the backtrace, just like with the X error itself, so that it can
	  be seen also with non-debug builds.

2008-03-21 12:58 +0000 [r788372]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/client.h,
	  branches/KDE/4.0/kdebase/workspace/kwin/client.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/workspace.cpp: Repaint
	  decoration after configuration change when it doesn't need a
	  reset.

2008-03-21 16:35 +0000 [r788478]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/group.cpp: This is
	  backwards - remove the connection from the Client that has it
	  indirectly, not the direct one.

2008-03-22 13:37 +0000 [r788782]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/systemsettings/kcmodulemodel.cpp:
	  Fix mem leak

2008-03-22 19:09 +0000 [r788879]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/kcmkwin/kwinrules/detectwidget.cpp:
	  Make the helper dialog modal, probably a Qt4-porting error. BUG:
	  156872

2008-03-24 08:46 +0000 [r789407]  annma

	* branches/KDE/4.0/kdebase/apps/kinfocenter/aboutwidget.cpp: fix
	  title CCBUG==157387

2008-03-24 12:10 +0000 [r789477]  annma

	* branches/KDE/4.0/kdebase/apps/kinfocenter/aboutwidget.cpp: revert
	  as it is not allowed to change strings here. I'll stop working on
	  those bug fixing as it's pretty upsetting to always been told of
	  for such meaningfull changes. Nobody never knows when it's
	  allowed to change strings/docs in branch and in the end we ship
	  4.0.3 with no real improvements in strings, docs and translations
	  and we keep bug reports opened for a small typo... Also people
	  should NOT report these typos anymore in l18n mailing list but
	  issue proper bug reports (that was done in that case, I'll reopen
	  it)

2008-03-24 13:15 +0000 [r789500]  porten

	* branches/KDE/4.0/kdebase/apps/konqueror/src/konqmainwindow.cpp:
	  Even though the user might have a general preference for new
	  windows being opened in a tab there is one special case: popup
	  windows being opened by a JS programmer using window.open(..,
	  ..., "width=xxx,..."). Give those dialog types a toplevel window
	  unless the user has set the "Open popups in new tab" option.

2008-03-24 14:22 +0000 [r789531]  lunakl

	* branches/KDE/4.0/kdebase/workspace/plasma/containments/desktop/desktop.cpp,
	  branches/KDE/4.0/kdebase/workspace/ksmserver/startup.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/containments/desktop/desktop.h:
	  Re-enable startup suspending in ksmserver and use it while
	  wallpaper is drawn, to allow fullscreen splashscreens to hide all
	  the initial plasma flicker.

2008-03-24 15:00 +0000 [r789548]  lunakl

	* branches/KDE/4.0/kdebase/workspace/ksplash/ksplashx/splash.cpp:
	  Actually recognize state "ready", even though it's only used as
	  the last WAIT_STATE and splash never really reaches it.

2008-03-25 19:58 +0000 [r790069-790057]  ruphy

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/abstractrunner.cpp,
	  branches/KDE/4.0/kdebase/workspace/libs/plasma/abstractrunner.h:
	  provide a bigLock() for runners to use whenever accessing common
	  but non-reentrant api (e.g. most everything in kdelibs ;)
	  Conflicts: workspace/libs/plasma/abstractrunner.cpp
	  workspace/libs/plasma/abstractrunner.h

	* branches/KDE/4.0/kdebase/workspace/krunner/runners/services/servicerunner.cpp:
	  use the bigLock, luke.

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/menuview.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/menuview.h,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.h:
	  * Introduced menu-views to allow to define what menu should be
	  displayed. Supported views are: Combined, Favorites,
	  Applications, Computer, Recently Used or Leave * Introduced
	  format-option to allow to define the menu-caption. This is equal
	  to the "Menu item format" option in KDE3. Supported formats are:
	  "Name only", "Description only", "Name Description" (default) or
	  "Description (Name)" FEATURE BUG:155362 Conflicts:
	  workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.cpp
	  Signed-off-by: Riccardo Iaconelli <riccardo@kde.org>

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/tabbar.cpp:
	  prettier tab buttons. based on patch by Luca Gambetta.
	  CCBUG:155411

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/applet/applet.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/launcher.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/launcher.h:
	  make Kickoff freely resizable (#152052) Conflicts:
	  workspace/plasma/applets/kickoff/applet/applet.cpp
	  workspace/plasma/applets/kickoff/ui/launcher.cpp Signed-off-by:
	  Riccardo Iaconelli <riccardo@kde.org>

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/applet/applet.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.cpp:
	  Don't show "Switch to Kickoff/Classic Menu Style" if immutable
	  (else the applet is destroyed but no new one is created).
	  Conflicts: workspace/plasma/applets/kickoff/applet/applet.cpp
	  workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.cpp
	  Signed-off-by: Riccardo Iaconelli <riccardo@kde.org>

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/applet/applet.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/applet/applet.h,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.h:
	  Allow to switch the launcher menu style via context menu entry
	  Conflicts: workspace/plasma/applets/kickoff/applet/applet.cpp
	  workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.cpp
	  Signed-off-by: Riccardo Iaconelli <riccardo@kde.org>

	* branches/KDE/4.0/kdebase/workspace/kmenuedit/kmenuedit.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/core/org.kde.kickoff.xml
	  (added),
	  branches/KDE/4.0/kdebase/workspace/plasma/plasma/dashboardview.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/CMakeLists.txt,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/core/applicationmodel.cpp,
	  branches/KDE/4.0/kdebase/workspace/kmenuedit/treeview.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/core/applicationmodel.h:
	  Reload menu when we change menu from kmenuedit (otherwise it's
	  not sync between kickoff and menu) Conflicts:
	  workspace/plasma/applets/kickoff/core/applicationmodel.h
	  Signed-off-by: Riccardo Iaconelli <riccardo@kde.org>

	* branches/KDE/4.0/kdebase/workspace/plasma/containments/panel/panel.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/containments/desktop/desktop.cpp:
	  Don't show Add/Remove Panel when we're locked. Conflicts:
	  workspace/plasma/containments/panel/panel.cpp Signed-off-by:
	  Riccardo Iaconelli <riccardo@kde.org>

	* branches/KDE/4.0/kdebase/workspace/plasma/plasma/plasmaapp.h,
	  branches/KDE/4.0/kdebase/workspace/plasma/plasma/rootwidget.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/plasma/panelview.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/plasma/rootwidget.h,
	  branches/KDE/4.0/kdebase/workspace/plasma/plasma/plasmaapp.cpp:
	  run time addition and removal of containments Conflicts:
	  workspace/plasma/plasma/panelview.cpp
	  workspace/plasma/plasma/plasmaapp.cpp
	  workspace/plasma/plasma/rootwidget.cpp Signed-off-by: Riccardo
	  Iaconelli <riccardo@kde.org>

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/launcher.cpp:
	  Invert the header gradient, fade on borders. Now it's nice. ;-)
	  Signed-off-by: Riccardo Iaconelli <riccardo@kde.org> Conflicts:
	  Signed-off-by: Riccardo Iaconelli <riccardo@kde.org>

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/urlitemview.cpp:
	  Invert the header gradient, fade on borders. Now it's nice. ;-)
	  Signed-off-by: Riccardo Iaconelli <riccardo@kde.org> Conflicts:
	  workspace/plasma/applets/kickoff/ui/urlitemview.cpp
	  Signed-off-by: Riccardo Iaconelli <riccardo@kde.org>

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/tabbar.cpp:
	  Align to grid. Signed-off-by: Riccardo Iaconelli
	  <riccardo@kde.org> Conflicts:
	  workspace/plasma/applets/kickoff/ui/tabbar.cpp Signed-off-by:
	  Riccardo Iaconelli <riccardo@kde.org>

2008-03-25 21:07 +0000 [r790091]  aseigo

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/abstractrunner.cpp,
	  branches/KDE/4.0/kdebase/workspace/libs/plasma/abstractrunner.h:
	  build

2008-03-25 21:28 +0000 [r790099]  aseigo

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/CMakeLists.txt:
	  build

2008-03-25 21:41 +0000 [r790104]  aseigo

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/urlitemview.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/applet/applet.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/launcher.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/applet/applet.h,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.h,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/launcher.h:
	  build

2008-03-25 21:50 +0000 [r790114]  aseigo

	* branches/KDE/4.0/kdebase/workspace/plasma/containments/panel/panel.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/containments/desktop/desktop.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/containments/panel/panel.h,
	  branches/KDE/4.0/kdebase/workspace/plasma/runners/webshortcuts/webshortcutrunner.cpp:
	  more build fixes

2008-03-25 21:53 +0000 [r790117]  aseigo

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/abstractrunner.cpp,
	  branches/KDE/4.0/kdebase/workspace/libs/plasma/abstractrunner.h:
	  pointer instead of a reference, easier to use and what is in
	  trunk/.

2008-03-25 22:07 +0000 [r790121]  aseigo

	* branches/KDE/4.0/kdebase/workspace/plasma/plasma/dashboardview.cpp:
	  build

2008-03-26 01:23 +0000 [r790184-790183]  aseigo

	* branches/KDE/4.0/kdebase/workspace/plasma/plasma/dashboardview.h:
	  forgot to commit this one: build

	* branches/KDE/4.0/kdebase/workspace/krunner/runners/services/servicerunner.cpp:
	  prevent crashes with the BigLock

2008-03-26 01:26 +0000 [r790185]  alake

	* branches/KDE/4.0/kdebase/workspace/plasma/desktoptheme/widgets/panel-background.svg:
	  For 4.0.3, replaced SVG elements with embedded raster images to
	  work around qt bug in qt 4.3 with svg rendering. Provided by
	  Cornelius Maihöfer. CCMAIL:panel-devel@kde.org,
	  cornelius.maihoefer@googlemail.com

2008-03-26 08:43 +0000 [r790225]  aseigo

	* branches/KDE/4.0/kdebase/workspace/plasma/plasma/rootwidget.cpp:
	  root widget does not call corona as we now respond to
	  containments as they happen

2008-03-26 08:59 +0000 [r790230]  aseigo

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/corona.h,
	  branches/KDE/4.0/kdebase/workspace/libs/plasma/corona.cpp: the
	  other half of "runtime reaction to containments added" ... the
	  half that makes it work.

2008-03-26 11:52 +0000 [r790292]  dfaure

	* branches/KDE/4.0/kdebase/apps/konqueror/src/konqdraggablelabel.h:
	  backport missing ifndef/define

2008-03-26 11:58 +0000 [r790295]  dfaure

	* branches/KDE/4.0/kdebase/apps/konqueror/src/KonqViewAdaptor.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/tests/CMakeLists.txt,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/tests/konqhtmltest.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/KonqViewAdaptor.h,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/konqview.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/tests/konqviewtest.cpp
	  (added),
	  branches/KDE/4.0/kdebase/apps/konqueror/src/konqmainwindow.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/konqview.h: Backport
	  788539: The fact that text/html derives from text/plain brought
	  back a bug that I fixed in 2002: "embed katepart and then type a
	  website URL -> loaded into katepart". Fixed by changing the logic
	  in changePart(). BUG: 155211

2008-03-26 14:56 +0000 [r790340]  dfaure

	* branches/KDE/4.0/kdebase/apps/konqueror/src/konqactions.cpp: Fix
	  porting error -- with K_GLOBAL_STATIC the pointer is never 0...

2008-03-26 16:57 +0000 [r790356]  binner

	* branches/KDE/4.0/kdebase/workspace/plasma/containments/panel/panel.cpp:
	  unbreak after backport mess

2008-03-26 17:59 +0000 [r790378-790376]  aseigo

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/containment.cpp:
	  signal screen changes

	* branches/KDE/4.0/kdebase/workspace/plasma/plasma/rootwidget.cpp:
	  don't make views for screens we don't have

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/containment.h: and
	  your little header too *cackle*

2008-03-26 18:16 +0000 [r790380]  aseigo

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/containment.cpp:
	  don't create a submenu if there aren't any applet actions

2008-03-26 18:19 +0000 [r790382]  binner

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/ui/launcher.cpp:
	  backport searchbar margin: don't let resize handle overlap
	  searchbar

2008-03-26 18:36 +0000 [r790391-790389]  aseigo

	* branches/KDE/4.0/kdebase/workspace/plasma/containments/panel/panel.cpp:
	  reset the min/max before setting the size so that we don't get
	  bound by those constraints accidently when changing locations

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/containment.cpp:
	  emit geometry changes when necessary on location change

2008-03-26 18:57 +0000 [r790454]  aseigo

	* branches/KDE/4.0/kdebase/workspace/plasma/applets/kickoff/simpleapplet/simpleapplet.cpp:
	  remove 5 of the 7 introduced strings. the remaining two are
	  slightly more problematic.

2008-03-26 19:31 +0000 [r790488]  aseigo

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/containment.cpp:
	  still need the setScreen() call on setLocation in the 4.0 branch
	  as setScreen is still magic there

2008-03-27 13:05 +0000 [r790780]  binner

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/containment.h,
	  branches/KDE/4.0/kdebase/workspace/libs/plasma/corona.h,
	  branches/KDE/4.0/kdebase/workspace/libs/plasma/corona.cpp:
	  backport missing parts of r784941 to fix desktop not displaying
	  for new user on first login

