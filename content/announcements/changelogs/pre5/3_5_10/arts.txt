2008-04-03 16:18 +0000 [r793315]  lunakl

	* branches/arts/1.5/arts/artsc/artsdsp.in: Fix support for bi-arch
	  platforms. BUG: 158501

2008-07-28 09:50 +0000 [r838600]  lunakl

	* branches/arts/1.5/arts/artsc/artsdsp.in: Fix last commit.

2008-08-19 19:46 +0000 [r849590]  coolo

	* branches/arts/1.5/arts/arts.lsm: update for 3.5.10

2008-08-19 19:51 +0000 [r849612]  coolo

	* branches/arts/1.5/arts/configure.in.in: update for 3.5.10

