---
aliases:
- ../../fulllog_releases-20.08.2
hidden: true
release: true
releaseDate: 2020-10
title: Release Service 20.08.2 Full Log Page
type: fulllog
version: 20.08.2
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Fix AppArmor policy for Mariadb 10.5. <a href='http://commits.kde.org/akonadi/3d85f3726bffe11a89f1188ecfb4b606d3375ada'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425606'>#425606</a></li>
<li>Fix(apparmor/postgres): Add kill signals. <a href='http://commits.kde.org/akonadi/8ea1c94ca89c713270e6b82fe0784d7ce0e0c3a2'>Commit.</a> </li>
<li>Fix(postgresql): Initialise database without locale en_US.UTF8 avalaible systemwide. <a href='http://commits.kde.org/akonadi/268bfc0830bee2012ab05f3a6055fff27a8c9010'>Commit.</a> </li>
<li>Fix cancelation of CollectionSync. <a href='http://commits.kde.org/akonadi/dad3399cce3f788625586563d2f997e9df91b306'>Commit.</a> </li>
<li>AgentBase: Fix crash in setOnline. <a href='http://commits.kde.org/akonadi/66d1135d127331fb3a2a7e047b0577d7e67d3dc2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418844'>#418844</a></li>
</ul>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Hide]</a></h3>
<ul id='ulartikulate' style='display: block'>
<li>Fix CMakeLists. <a href='http://commits.kde.org/artikulate/88edf1ceb5c8a18515e7bd47e31c38eef476e81f'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>[Juila] Don't show internal Julia variables in Variable Manager. <a href='http://commits.kde.org/cantor/22e1f386e2f6c20e29b92583365cf25336dc466b'>Commit.</a> </li>
<li>[Julia] Add some code for working with wrong path to Julia internal file "sys.so". <a href='http://commits.kde.org/cantor/76c6c50126b469181856ff6ff17a9ef19c382ada'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425695'>#425695</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Don't warn when closing multiple tabs if restoring window state. <a href='http://commits.kde.org/dolphin/6cf628cbd0841847795740dd02affec281d9a89a'>Commit.</a> </li>
<li>Fix untranslated spinbox suffix strings. <a href='http://commits.kde.org/dolphin/9fffa4b253f58bd0aa891a173ba240d3364ec371'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426286'>#426286</a></li>
<li>Revert "Set a better defaultDropAction for dragging". <a href='http://commits.kde.org/dolphin/aee26d29f188427186b6aaba1c54a205c17b3dc6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425757'>#425757</a>. Fixes bug <a href='https://bugs.kde.org/426196'>#426196</a></li>
</ul>
<h3><a name='elisa' href='https://cgit.kde.org/elisa.git'>elisa</a> <a href='#elisa' onclick='toggle("ulelisa", this)'>[Hide]</a></h3>
<ul id='ulelisa' style='display: block'>
<li>Fix list items with no subtitle not being centered. <a href='http://commits.kde.org/elisa/dabac2291f335844420b1eb10ce74a586912c3a0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426417'>#426417</a></li>
<li>Fix incorrect version display in about dialog. <a href='http://commits.kde.org/elisa/bcc25fc51cded5b91b382501bd142a33eec6a9c5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424932'>#424932</a></li>
<li>Clip placeholder message within playlist. <a href='http://commits.kde.org/elisa/5c2f2c877820e825938ba99362b99287d273ec03'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix deadlock when closing gwenview importer. <a href='http://commits.kde.org/gwenview/d15622e1655ba7fcd33479f9677112340b2de7ff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425971'>#425971</a></li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Fix kauth helper install location. <a href='http://commits.kde.org/k3b/beef1480c38a470da204b70d6d80004d36f93524'>Commit.</a> </li>
</ul>
<h3><a name='kajongg' href='https://cgit.kde.org/kajongg.git'>kajongg</a> <a href='#kajongg' onclick='toggle("ulkajongg", this)'>[Hide]</a></h3>
<ul id='ulkajongg' style='display: block'>
<li>Make popenReadlines use the C locale. <a href='http://commits.kde.org/kajongg/39db0c122583d0eca56a77624d514e03b5283daf'>Commit.</a> </li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Update change log. <a href='http://commits.kde.org/kalarm/41a0438985b3cc8a0fca3099371ca85755d1a0df'>Commit.</a> </li>
<li>Fix memory leak when objects disconnect from DailyTimer. <a href='http://commits.kde.org/kalarm/05ad24cc9dd4e9030b88c0ef39dfa3ab9f43723d'>Commit.</a> </li>
<li>Change methods not publicly accessible to protected. <a href='http://commits.kde.org/kalarm/3a81c48337676110e9c69475a58a1ce1b85f5215'>Commit.</a> </li>
<li>Fix memory leak in destructor. <a href='http://commits.kde.org/kalarm/c400a872047592380344bce9e3eab0c6405476df'>Commit.</a> </li>
<li>Update screenshots. <a href='http://commits.kde.org/kalarm/89fc85e81cff43096f53b3c7b8a1dd7b263a5281'>Commit.</a> </li>
<li>Fix inability to edit an existing calendar resource's configuration. <a href='http://commits.kde.org/kalarm/0c4f5ffd73b5591d5370dc0fa42e4ec4dd0507dc'>Commit.</a> </li>
<li>Initially show alarms in time order; fix alarm & template sorting. <a href='http://commits.kde.org/kalarm/bef72264c9535ddbed4db3034d2e84d5632b89cc'>Commit.</a> </li>
<li>Fix startup order for resource classes, to ensure all signals are emitted. <a href='http://commits.kde.org/kalarm/dce6b6311d13945096bd23c2394e2bf5096695d4'>Commit.</a> </li>
<li>Fix repeat-at-login alarms not triggering when KAlarm starts. <a href='http://commits.kde.org/kalarm/fc39709ec75a41444315946ef7e7e76d3629d0af'>Commit.</a> </li>
<li>Don't show Defer button in alarm window after clicking Try more than once. <a href='http://commits.kde.org/kalarm/fd8a58eb4a23698aa42b69d350fae7f880ed36d6'>Commit.</a> </li>
<li>Don't show alarm time as "Never" after using Try button. <a href='http://commits.kde.org/kalarm/e2661f25e7ba9c613d1d4c76380d178730fbfdc1'>Commit.</a> </li>
<li>Fix bugs when command generating text for a display alarm fails. <a href='http://commits.kde.org/kalarm/d9031c5344c23abe8719fa9fef443b919f593f22'>Commit.</a> </li>
</ul>
<h3><a name='kamoso' href='https://cgit.kde.org/kamoso.git'>kamoso</a> <a href='#kamoso' onclick='toggle("ulkamoso", this)'>[Hide]</a></h3>
<ul id='ulkamoso' style='display: block'>
<li>Make sure we don't add cameras twice. <a href='http://commits.kde.org/kamoso/3fbd6db22ef5e8a21a5c285036c0e787f8e413d2'>Commit.</a> </li>
<li>Port away from udi+path to objectid. <a href='http://commits.kde.org/kamoso/8a52d2ae9dce7271738f0471a6867a74b6fa6506'>Commit.</a> </li>
<li>Do not set a dummy input element. <a href='http://commits.kde.org/kamoso/be3085cfff817d4572f70eef2540fd01f8fc4f6d'>Commit.</a> </li>
</ul>
<h3><a name='kanagram' href='https://cgit.kde.org/kanagram.git'>kanagram</a> <a href='#kanagram' onclick='toggle("ulkanagram", this)'>[Hide]</a></h3>
<ul id='ulkanagram' style='display: block'>
<li>When checking typed letters also check stripping accents. <a href='http://commits.kde.org/kanagram/cdcca65a80fd4ca02277644cf4cf1fe76b1bfc42'>Commit.</a> </li>
<li>Add missing content_rating tag to appdata file. <a href='http://commits.kde.org/kanagram/202c3f8d87fdf79696b9c7f75b31260714cddee9'>Commit.</a> </li>
</ul>
<h3><a name='kapptemplate' href='https://cgit.kde.org/kapptemplate.git'>kapptemplate</a> <a href='#kapptemplate' onclick='toggle("ulkapptemplate", this)'>[Hide]</a></h3>
<ul id='ulkapptemplate' style='display: block'>
<li>Kde-frameworks5 template: use only SPDX license docs. <a href='http://commits.kde.org/kapptemplate/51279eac2c9c17237f63be49132be0e850add93f'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Ensure that even if the url is empty, we create some parent to avoid accessing nullptr parent. <a href='http://commits.kde.org/kate/229443cc15e61fd23e5d616a46cdc3b36bb1d128'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427270'>#427270</a></li>
<li>Addons/externaltools: Really respect BUILD_TESTING. <a href='http://commits.kde.org/kate/ec9d93e932ba92fa738d0b6c64b4ee4a3161b3a6'>Commit.</a> </li>
<li>Close tabs again with middle mouse button click. <a href='http://commits.kde.org/kate/4eaa3b75bd91febbcf9823aeeb45b91e1158f98a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426042'>#426042</a></li>
<li>Avoid that the KTextEditor::View XMLGUI elements are lost after a tab is closed. <a href='http://commits.kde.org/kate/972903d2620932477c91c62b8b8c56e8d3bf40bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426605'>#426605</a></li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Hide]</a></h3>
<ul id='ulkde-dev-scripts' style='display: block'>
<li>Ibragimov said yes to all 5 questions. <a href='http://commits.kde.org/kde-dev-scripts/299fe5c16b020549b4d82f3c2cbc59a7fdae0776'>Commit.</a> </li>
<li>Add alias for bhash. <a href='http://commits.kde.org/kde-dev-scripts/13dadc59b5dc560835fac468e534dbae1502bea0'>Commit.</a> </li>
</ul>
<h3><a name='kdeconnect-kde' href='https://cgit.kde.org/kdeconnect-kde.git'>kdeconnect-kde</a> <a href='#kdeconnect-kde' onclick='toggle("ulkdeconnect-kde", this)'>[Hide]</a></h3>
<ul id='ulkdeconnect-kde' style='display: block'>
<li>Fix kdeconnect_open MimeType. <a href='http://commits.kde.org/kdeconnect-kde/c58c37ac52120620b3e0cc65047ef4f6e2bedd39'>Commit.</a> </li>
<li>Do not replace connections for a given deviceId if the certs have changed. <a href='http://commits.kde.org/kdeconnect-kde/48180b46552d40729a36b7431e97bbe2b5379306'>Commit.</a> </li>
<li>Limit the ports we try to connect to to the port range of KDE Connect. <a href='http://commits.kde.org/kdeconnect-kde/85b691e40f525e22ca5cc4ebe79c361d71d7dc05'>Commit.</a> </li>
<li>Do not remember more than a few identity packets at a time. <a href='http://commits.kde.org/kdeconnect-kde/66c768aa9e7fba30b119c8b801efd49ed1270b0a'>Commit.</a> </li>
<li>Limit number of connected sockets from unpaired devices. <a href='http://commits.kde.org/kdeconnect-kde/ae58b9dec49c809b85b5404cee17946116f8a706'>Commit.</a> </li>
<li>Don't brute-force reading the socket. <a href='http://commits.kde.org/kdeconnect-kde/721ba9faafb79aac73973410ee1dd3624ded97a5'>Commit.</a> </li>
<li>Do not let lanlink connections stay open for long without authenticating. <a href='http://commits.kde.org/kdeconnect-kde/5310eae85dbdf92fba30375238a2481f2e34943e'>Commit.</a> </li>
<li>Limit identity packets to 8KiB. <a href='http://commits.kde.org/kdeconnect-kde/b496e66899e5bc9547b6537a7f44ab44dd0aaf38'>Commit.</a> </li>
<li>Fix use after free in LanLinkProvider::connectError(). <a href='http://commits.kde.org/kdeconnect-kde/d35b88c1b25fe13715f9170f18674d476ca9acdc'>Commit.</a> </li>
<li>Do not leak the local user in the device name. <a href='http://commits.kde.org/kdeconnect-kde/b279c52101d3f7cc30a26086d58de0b5f1c547fa'>Commit.</a> </li>
<li>Do not ignore SSL errors, except for self-signed cert errors. <a href='http://commits.kde.org/kdeconnect-kde/f183b5447bad47655c21af87214579f03bf3a163'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Add xml for qtcrop filter. <a href='http://commits.kde.org/kdenlive/c7dc53a88daab21a65705d0b5ac8f4ba8d4f5a6f'>Commit.</a> </li>
<li>Fix freeze on memory usage loading invalid clips. <a href='http://commits.kde.org/kdenlive/e23e35034cae9a9639305d580a52a2ac4b4867ac'>Commit.</a> </li>
<li>Fix track insertion in mixed view mode. <a href='http://commits.kde.org/kdenlive/1156c6d74a6d32f6607e346ffa95f8183f002b9d'>Commit.</a> See bug <a href='https://bugs.kde.org/403443'>#403443</a></li>
<li>Fix track order in mixed track view. <a href='http://commits.kde.org/kdenlive/0f2968db9bdb631bdb48b089a9020eb58eee5aa3'>Commit.</a> </li>
<li>Fix compilation. <a href='http://commits.kde.org/kdenlive/25e5eb2998d5ed21ce01d97f015b03d02da698b5'>Commit.</a> </li>
<li>Fix monitor preview messing monitor zoom. <a href='http://commits.kde.org/kdenlive/a6937dc349e05c90edbafbfe0d8b29b829191d63'>Commit.</a> </li>
<li>Restore toolbars in default editing layout. <a href='http://commits.kde.org/kdenlive/ef83deec31c204951b49abc1148a55e2e4447390'>Commit.</a> </li>
<li>Fix saving lift/gamma/gain effect results in broken ui. <a href='http://commits.kde.org/kdenlive/eab02f5fdac5fa5102752539f28654f998804897'>Commit.</a> </li>
<li>Fix PreviewJob memory leak. <a href='http://commits.kde.org/kdenlive/5034c4fed4a16d0a366c0cdcb385d8a9a3c6ac9b'>Commit.</a> </li>
<li>Fix deprecated install location. <a href='http://commits.kde.org/kdenlive/8f521325d0a629236ed4cd859470cebf84a01bdd'>Commit.</a> </li>
<li>Fix audio mixer track effects applied twice when reopening project, leading to incorrect volume. <a href='http://commits.kde.org/kdenlive/8064b54b29596876ff78c85a82171b7dabf24c09'>Commit.</a> </li>
<li>Fix mem leak when another process was writing a clip that is included in a project. <a href='http://commits.kde.org/kdenlive/2bc6ae1bea62a3203343650746874e9fb61e5423'>Commit.</a> </li>
<li>Fixed automatic scene split (bug #421772). <a href='http://commits.kde.org/kdenlive/1bddf7ca59d32806003b95645713b11f85f57ccb'>Commit.</a> </li>
<li>On project opening, fix detection of proxied clips with missing source and proxy. <a href='http://commits.kde.org/kdenlive/bdd32223f23481f1e53aa4335f008fb3d5acf473'>Commit.</a> </li>
<li>Fix incorrect hash check causing incorrect reload dialog on project opening. <a href='http://commits.kde.org/kdenlive/145d2f16fa11d48129397a1aba3201153144e753'>Commit.</a> </li>
<li>Fix corrupted slowmotion clips on document opening. <a href='http://commits.kde.org/kdenlive/5b74aaba9090325a3a16d1a4919c5f316de2f19b'>Commit.</a> </li>
<li>Fix speed change effect lost when opening project with missing clip, and broken handling of missing proxied clips with speed effect. <a href='http://commits.kde.org/kdenlive/def6f3f9f712ad2c376a0f724fa38a154e1008e8'>Commit.</a> </li>
<li>Ensure we check file hash on every project opening to ensure clips have not changed and an incorrect hash is not stored. <a href='http://commits.kde.org/kdenlive/4d5b1a353c6afefab99552a570bf540ff0885992'>Commit.</a> </li>
<li>Add missing "=" symbol in GPU profile. <a href='http://commits.kde.org/kdenlive/1f1251bd7ffebb57a1bea07df006a71c3e6127d3'>Commit.</a> </li>
<li>Add GPU profiles provided by Constantin Aanicai. https://kdenlive.org/en/2020/08/kdenlive-20-08-is-out/#comment-5089. <a href='http://commits.kde.org/kdenlive/083bfd2803e62fe23f779c00617cd9bd06d0dd4f'>Commit.</a> </li>
<li>[Experimental] Added GPU profiles for rendering proxies and timeline preview ... <a href='http://commits.kde.org/kdenlive/9203c8ca9a7c2845cfb3667c2f86a731feca59d0'>Commit.</a> </li>
<li>Fix crash on some projects opening. <a href='http://commits.kde.org/kdenlive/988439dbcd881a536ba0c06e1f406b37b96bead1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409477'>#409477</a></li>
<li>Fix compilation. <a href='http://commits.kde.org/kdenlive/ac0d8439507f871d65186bc5d27919d8ba301a77'>Commit.</a> </li>
<li>Fix cut/resize audio clip invalidating timeline preview. <a href='http://commits.kde.org/kdenlive/3716ef32b964515483b37c6d42b85eee8a01cc6e'>Commit.</a> </li>
<li>Remove test stuff. <a href='http://commits.kde.org/kdenlive/0bec5caedb8a5bf4c739a1856c723fdf6056ee35'>Commit.</a> </li>
<li>Delay locale reset to allow correct ui translation. <a href='http://commits.kde.org/kdenlive/23ff749fd1e9ce6b976d7f2d92e3df4f14be865f'>Commit.</a> </li>
<li>Attempt to mimic Shotcut's locale handling. <a href='http://commits.kde.org/kdenlive/b07c788901de13d28bfd8a9eb81c2cfd3d559803'>Commit.</a> </li>
<li>Ensure default layout names are translatable. <a href='http://commits.kde.org/kdenlive/56d54a5c703b5aac13416981e9dcf7021365b2e8'>Commit.</a> </li>
<li>Fix clicking on clip marker label moving timeline cursor to approximate position. <a href='http://commits.kde.org/kdenlive/6cd8e3d35aed4695dfc33007802e4b5c0117ff8f'>Commit.</a> </li>
<li>Use another ref on the producer when saving project (might help in case another operation is performed on the producer). <a href='http://commits.kde.org/kdenlive/40afc62f9067c5f7cda15a10fd8b6c19932c8283'>Commit.</a> </li>
<li>Add corruption check before creating backup file. <a href='http://commits.kde.org/kdenlive/1413dc409fd46c0f7db63a5f96c92c2fa388b706'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Also set Internal Date in ChangeItemTask::doStart AppendJob. <a href='http://commits.kde.org/kdepim-runtime/40797e87f8a772ee0bb5df301b41824b1b5007b3'>Commit.</a> </li>
</ul>
<h3><a name='kdialog' href='https://cgit.kde.org/kdialog.git'>kdialog</a> <a href='#kdialog' onclick='toggle("ulkdialog", this)'>[Hide]</a></h3>
<ul id='ulkdialog' style='display: block'>
<li>[kdialog] Fixes the always asking even without dontagain flag usage. <a href='http://commits.kde.org/kdialog/2fd7c381e713de5a229fbfca2c48cdabbe7579ae'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426979'>#426979</a></li>
</ul>
<h3><a name='kget' href='https://cgit.kde.org/kget.git'>kget</a> <a href='#kget' onclick='toggle("ulkget", this)'>[Hide]</a></h3>
<ul id='ulkget' style='display: block'>
<li>Fix icons in transfer history. <a href='http://commits.kde.org/kget/61a9f575318e2d42688be749f569280d85793613'>Commit.</a> </li>
</ul>
<h3><a name='kirigami-gallery' href='https://cgit.kde.org/kirigami-gallery.git'>kirigami-gallery</a> <a href='#kirigami-gallery' onclick='toggle("ulkirigami-gallery", this)'>[Hide]</a></h3>
<ul id='ulkirigami-gallery' style='display: block'>
<li>Port to invent. <a href='http://commits.kde.org/kirigami-gallery/cf7ef9ffe51fa0028c324185422744bf3829fc8f'>Commit.</a> </li>
</ul>
<h3><a name='kiten' href='https://cgit.kde.org/kiten.git'>kiten</a> <a href='#kiten' onclick='toggle("ulkiten", this)'>[Hide]</a></h3>
<ul id='ulkiten' style='display: block'>
<li>Fix potential crash in sessionConfig. <a href='http://commits.kde.org/kiten/72fade827b6909d74cdc14ccb42d5547e42d1241'>Commit.</a> </li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>Don't crash when trying to fix JSON-LD content. <a href='http://commits.kde.org/kitinerary/a8820546265dd8f89a1edfffd78bd8de6b05e54c'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>It was a error to call setAutoDelete(false). It will create a mem leak. <a href='http://commits.kde.org/kmail/d4ead9ef1e7256aaed6cbee096d2e4a3dc1565c5'>Commit.</a> </li>
<li>Fix Bug 426596 - Application: "akonadi_archivemail_agent" crashed after closing "Configure automatic archving..."-Dialog. <a href='http://commits.kde.org/kmail/0922a95a692f6424773cd28f21a83237a60d226f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426596'>#426596</a>. Fixes bug <a href='https://bugs.kde.org/425786'>#425786</a>. Fixes bug <a href='https://bugs.kde.org/424232'>#424232</a></li>
</ul>
<h3><a name='kmailtransport' href='https://cgit.kde.org/kmailtransport.git'>kmailtransport</a> <a href='#kmailtransport' onclick='toggle("ulkmailtransport", this)'>[Hide]</a></h3>
<ul id='ulkmailtransport' style='display: block'>
<li>SmtpJob: Fix crash in startLoginJob. <a href='http://commits.kde.org/kmailtransport/14894befa0274e124cdde611dfb7ada8b1131b07'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421930'>#421930</a></li>
<li>Revert wrong commit, bad merge. <a href='http://commits.kde.org/kmailtransport/bfa9ccce8fa89eb47e66aaa79303730d0198bec6'>Commit.</a> </li>
<li>Put back KIO in the package search. <a href='http://commits.kde.org/kmailtransport/e15cd5ec54508b2e968a74efa3c3bc00007e1d49'>Commit.</a> </li>
</ul>
<h3><a name='kmime' href='https://cgit.kde.org/kmime.git'>kmime</a> <a href='#kmime' onclick='toggle("ulkmime", this)'>[Hide]</a></h3>
<ul id='ulkmime' style='display: block'>
<li>Based on patch from Thomas Monjalon <thomas@monjalon.net>. <a href='http://commits.kde.org/kmime/6d637f9a34e09f6577c52436003fd102b65a8f43'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426238'>#426238</a></li>
</ul>
<h3><a name='kmix' href='https://cgit.kde.org/kmix.git'>kmix</a> <a href='#kmix' onclick='toggle("ulkmix", this)'>[Hide]</a></h3>
<ul id='ulkmix' style='display: block'>
<li>Remove empty MimeType key in kmix_autostart.desktop. <a href='http://commits.kde.org/kmix/791abe272d11187aca255ff542fffc522cf6351b'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Fix crash when extending selection after switching between primary and secondary screen. <a href='http://commits.kde.org/konsole/4cbe74476820d96421627689ac66a596381c74e5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398320'>#398320</a></li>
<li>Set a minimum thumbnail size. <a href='http://commits.kde.org/konsole/5ea7a9d0734074b602ef8cfacb7a50947cd24d96'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425544'>#425544</a></li>
<li>Fix vertical scrolling with touchpads and trackpoints. <a href='http://commits.kde.org/konsole/a1d843f92d445448a9193a6d28a36d0cba5a1940'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425765'>#425765</a></li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Hide]</a></h3>
<ul id='ulkpat' style='display: block'>
<li>Fix opening files. <a href='http://commits.kde.org/kpat/788a230c60e62d2508d3a12b772c5df847e2b635'>Commit.</a> </li>
</ul>
<h3><a name='krfb' href='https://cgit.kde.org/krfb.git'>krfb</a> <a href='#krfb' onclick='toggle("ulkrfb", this)'>[Hide]</a></h3>
<ul id='ulkrfb' style='display: block'>
<li>Make sure to save passwords each time they are modified. <a href='http://commits.kde.org/krfb/fd362fd642491ee9b4ad42346776b2d4ec089b22'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340411'>#340411</a></li>
<li>Pipewire: Only aspire to use dmabuf if linux/dma-buf.h is present. <a href='http://commits.kde.org/krfb/8afd5c8df28f7c259337fd37a2e130b563419554'>Commit.</a> </li>
<li>Fixes for builds without pipewire3. <a href='http://commits.kde.org/krfb/ef6b3a609321b86cd34a2f8e31b4f75b1652da8f'>Commit.</a> </li>
<li>Support DMABuf streams. <a href='http://commits.kde.org/krfb/3e00ff22f983395f71240afb21a05b6dedc4736d'>Commit.</a> </li>
<li>Compensate for global scale factor when using xcb fb plugin. <a href='http://commits.kde.org/krfb/024ce87b3af0c7b8cffbf2065c3f083a057edb70'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419814'>#419814</a></li>
</ul>
<h3><a name='kwave' href='https://cgit.kde.org/kwave.git'>kwave</a> <a href='#kwave' onclick='toggle("ulkwave", this)'>[Hide]</a></h3>
<ul id='ulkwave' style='display: block'>
<li>Include <sys/select.h> for fd_set, FD_SET, FD_ZERO. <a href='http://commits.kde.org/kwave/af6f0ce8d4669819955aff9e6c1020973f2767cc'>Commit.</a> </li>
</ul>
<h3><a name='libkcompactdisc' href='https://cgit.kde.org/libkcompactdisc.git'>libkcompactdisc</a> <a href='#libkcompactdisc' onclick='toggle("ullibkcompactdisc", this)'>[Hide]</a></h3>
<ul id='ullibkcompactdisc' style='display: block'>
<li>Add missing TRANSLATION_DOMAIN definition. <a href='http://commits.kde.org/libkcompactdisc/3593bee2b6de5c4071567a9dbe09ad4ce8ac2407'>Commit.</a> </li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Hide]</a></h3>
<ul id='ullibkgapi' style='display: block'>
<li>Accept HTTP status 307 as a redirect. <a href='http://commits.kde.org/libkgapi/6e799a18f89b2db0d3abc82d962cb66fa2cce211'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425261'>#425261</a></li>
</ul>
<h3><a name='libkmahjongg' href='https://cgit.kde.org/libkmahjongg.git'>libkmahjongg</a> <a href='#libkmahjongg' onclick='toggle("ullibkmahjongg", this)'>[Hide]</a></h3>
<ul id='ullibkmahjongg' style='display: block'>
<li>RKMahjonggConfigDialog: remove attempt to add another button box. <a href='http://commits.kde.org/libkmahjongg/affdd4adeba498deab13717f2d01b6e6828eb759'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Fix compilation with -DQT_NO_URL_CAST_FROM_STRING. <a href='http://commits.kde.org/lokalize/8896c21a3a39ef586dd5a5236af0f82478a79c18'>Commit.</a> </li>
<li>Fix job deletion. <a href='http://commits.kde.org/lokalize/1435ee3ccb9857f4e9a96593b8ac99b43cb69703'>Commit.</a> </li>
<li>Sort language name with locale awareness. <a href='http://commits.kde.org/lokalize/479bb0309f8efe56e06ac51371d2408983be2eca'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix crash xdg-email foo@kde.org bar@kde.org baz@kde.org. <a href='http://commits.kde.org/messagelib/4fe8b98ea691c32994bb7815bf6d6837d4f04efa'>Commit.</a> </li>
<li>Fix Bug 426639 - Kmail Undo Send option doesn't work. <a href='http://commits.kde.org/messagelib/57f4f57c7d95516cdc67fd5977b0d0d7badf98a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426639'>#426639</a></li>
<li>Fix crash when we forward inline a message. <a href='http://commits.kde.org/messagelib/889dba645f2453d2ed3a8a9b09541cccc568d28b'>Commit.</a> </li>
<li>Fix here too: We need to show action when we can see selected item. <a href='http://commits.kde.org/messagelib/9bd81ca709cb8d9bea84334a1128dc9d2203db82'>Commit.</a> </li>
<li>Hide actions if we don't show item. <a href='http://commits.kde.org/messagelib/90b08630e44ed711783f2c8bb5fac13b1bca248f'>Commit.</a> </li>
<li>Warning--. <a href='http://commits.kde.org/messagelib/90323b5a01afd3aef6e43b2429724dcd85076b0f'>Commit.</a> </li>
<li>Fix Bug 425161 - Default keyboard shortcut for adding attachments. <a href='http://commits.kde.org/messagelib/6074ec194aee70baededfd3090cfe880b4b9a4c8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425161'>#425161</a></li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Re-add support for pageup/pagedown overlap. <a href='http://commits.kde.org/okular/740318df9022f27ec07eb77f2d01fda802ce4018'>Commit.</a> </li>
<li>Fix forms when inertially scrolling. <a href='http://commits.kde.org/okular/bdb2df773d774c15cf0a17fcae198d3f2452cdfa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421354'>#421354</a></li>
<li>Show "Follow link" if right clicking over link and annotation. <a href='http://commits.kde.org/okular/3f89246317ecbf2b2c19a4c4416583db72d463bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425291'>#425291</a></li>
<li>AnnotationPopup: Add possibility of filling an outside menu. <a href='http://commits.kde.org/okular/9b86535312c4b555b5823d4987a01dc822dd9596'>Commit.</a> </li>
<li>Rework how AnnotationPopup handles user choices. <a href='http://commits.kde.org/okular/4e91ab85fdc8fef636e0eb9e24c730eaf49ec662'>Commit.</a> </li>
<li>Don't use i18n in static variables. <a href='http://commits.kde.org/okular/8eb0c0c56c86d486f3c5bb42a6dc6c70e4e7d937'>Commit.</a> </li>
<li>Fix crash on files that have the same signature in more than 1 page. <a href='http://commits.kde.org/okular/9bf171a108cd525eab8d01c4045e22c3e9d5d142'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426467'>#426467</a></li>
<li>CI: fix build_clazy_clang_tidy. <a href='http://commits.kde.org/okular/f1b29aca4f928f99bc9d8574cb9ba54b32bfb4a4'>Commit.</a> </li>
<li>Restore mouse drag scrolling, so it starts immediately. <a href='http://commits.kde.org/okular/a5be0149ec627fbc086e5b26cbbe7be13cde49b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420842'>#420842</a></li>
<li>Re-add smooth scrolling for arrow key and mouse wheel scrolling. <a href='http://commits.kde.org/okular/c54c38f761fb79efd0d552bdb88c7977f22b3a88'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422041'>#422041</a>. Fixes bug <a href='https://bugs.kde.org/425850'>#425850</a></li>
<li>Remove if statement that disables pgup/dn key handling while scrolling. <a href='http://commits.kde.org/okular/e637e2ed519cb2dfe523981b2d6dc34a9855716c'>Commit.</a> </li>
<li>Maintain quick annotation shortcut compatibility with Okular < 1.11.0. <a href='http://commits.kde.org/okular/cd96735ae236499e0597317c8967a2a2ac551af1'>Commit.</a> See bug <a href='https://bugs.kde.org/426009'>#426009</a></li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Properly position QuickEditor when HiDPI support is on. <a href='http://commits.kde.org/spectacle/ccf8d5220dc5ac2a5722247386d6cde82494dbb3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385885'>#385885</a></li>
</ul>
