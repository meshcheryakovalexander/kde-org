---
date: 2022-03-03
appCount: 120
image: true
layout: gear
---
+ kcron: Improve temporary file handling, [Commit,](http://commits.kde.org/kcron/ef4266e3d5ea741c4d4f442a2cb12a317d7502a1) [CVE-2022-24986](https://kde.org/info/security/advisory-20220216-1.txt)
+ kio-extras: SFTP can use random access, [Commit,](http://commits.kde.org/kio-extras/3259705bb974979c8c3ee819f12882d942951b80) [#450198](https://bugs.kde.org/450198)
+ kontact: Fix Manager Crash when clicking New, [Commit,](http://commits.kde.org/kontact/060fb5f4bfdeb4ce50e6b5ad47aebddba2a228c6) [#424252](https://bugs.kde.org/424252)
