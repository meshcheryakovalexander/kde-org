---
date: 2023-07-06
appCount: 120
image: true
layout: gear
---
+ gwenview:  Avoid a crash when opening a .nef image in exiv2 library ([Commit](http://commits.kde.org/gwenview/c15f7b79d68a583359ae597ca5cb405719501c0e), fixes bug [#470880](https://bugs.kde.org/470880))
+ kalendar: Fix a few issues with reminders ([Commit](http://commits.kde.org/kalendar/e3718ccda895dbea01232c1e8eefd8744477040f), fixes bugs [#470288](https://bugs.kde.org/470288) and [#470525](https://bugs.kde.org/470525))
+ kreversi: Fix board position in portrait mode ([Commit](http://commits.kde.org/kreversi/ff30be0a59296491e666a2c96ad8f681ba563838), fixes bug [#445131](https://bugs.kde.org/445131))
