---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE 发布 Plasma 工作空间、应用程序和平台 4.11 版。
title: KDE 软件集合 4.11 版本
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`KDE Plasma 工作空间 4.11 版本` >}} <br />

2013 年 8 月 14 日，KDE 社区推出了 Plasma 工作空间、应用程序和开发平台的最新重要更新，带来了新功能和程序缺陷修复，并为软件的未来发展进行准备工作。Plasma 工作空间 4.11 将作为一个长期支持版本，直至我们的开发团队完成将全套软件转换至 KDE 软件框架第 5 版。这将是 KDE 工作空间、应用程序和开发平台最后一次发布第 4 版的软件合集。<br/>

我们谨以版软件发布来纪念 <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis (阿图尔·屈尼思)</a>。阿图尔是一位来自印度的自由开源软件先驱者，从 2001 年起他主导了印度自由开源软件界标志性的 Linux 班加罗尔社区和 FOSS.IN 峰会。KDE 印度始建于 2005 年 FOSS.IN 峰会，众多印度的 KDE 贡献者便是从这里开始参加 KDE 工作的。在阿图尔的大力支持下，FOSS.IN 的 KDE 项目日活动总能大获成功。阿图尔因癌症于 6 月 3 日逝世。愿他的灵魂安息，我们不会忘记他为了建设一个更加美好的世界所做的贡献。

这些版本均已翻译为了 54 种语言。在接下来的每月 KDE 程序缺陷小幅更新中将会有更多语言加入进来。KDE 文档小组为此版软件集合更新了 91 款应用程序的使用手册。

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma 工作空间 4.11 版继续优化用户体验</a>

作为一个长期维护版本，此版 Plasma 工作空间在基本功能方面作出了进一步改进，带来了更平顺的任务栏，更智能的电池挂件和改进的混音器。新增的 KScreen 程序带来了智能多屏幕处理。此外还有对性能的大幅改善和易用性的小幅调整，带来更棒的整体使用体验。

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

这次发布标志着 KDE PIM 堆栈的大量改进，提供了更好的性能和许多新功能。 Kate 通过新插件提高了 Python 和 JavaScript 开发者的生产率。Dolphin 变得更快，教育应用程序带来了各种新功能。

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE 4.11平台提供更好的性能</a>

This release of KDE Platform 4.11 continues to focus on stability. New features are being implemented for our future KDE Frameworks 5.0 release, but for the stable release we managed to squeeze in optimizations for our Nepomuk framework.

<br />
When upgrading, please observe the <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>release notes</a>.<br />

## Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.11 releases of KDE software.

## 发布会

As usual, KDE community members organize release parties all around the world. Several have already been scheduled and more will come later. Find <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>a list of parties here</a>. Anyone is welcome to join! There will be a combination of interesting company and inspiring conversations as well as food and drinks. It's a great chance to learn more about what is going on in KDE, get involved, or just meet other users and contributors.

We encourage people to organize their own parties. They're fun to host and open for everyone! Check out <a href='http://community.kde.org/Promo/Events/Release_Parties'>tips on how to organize a party</a>.

## About these release announcements

These release announcements were prepared by Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin and other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.

#### 支持 KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.
