---
aliases:
- ../announce-applications-15.04-beta1
date: '2015-03-06'
description: KDE veröffentlicht die Anwendungen 15.04 Beta 1.
layout: application
title: KDE veröffentlicht die erste Beta-Version der KDE-Anwendungen 15.04
---
6. März 2015. Heute veröffentlicht KDE die Beta-Version der neuen Ausgaben der KDE-Anwendungen. Mit dem Einfrieren von Abhängigkeiten und Funktionen konzentriert sich das KDE-Team auf die Behebung von Fehlern und Verbesserungen.

Bei den verschiedenen Anwendungen auf der Grundlage der KDE Frameworks 5 sind gründliche Tests für die Veröffentlichung der KDE Anwendungen 15.04 nötig, um die Qualität und Benutzererfahrung beizubehalten und zu verbessern. Anwender, die KDE täglich benutzen, sind sehr wichtig, um die hohe Qualität der KDE-Software zu erhalten, weil Entwickler nicht jede mögliche Kombination von Anwendungsfällen testen können. Diese Benutzer können Fehler finden, so dass sie vor der endgültigen Veröffentlichung korrigiert werden können. Beteiligen Sie sich beim Team und installieren Sie die Beta-Version und berichten Sie alle <a href='https://bugs.kde.org/'>Fehler</a>.
