---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE veröffentlicht die Anwendungen 14.12.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 14.12.3
version: 14.12.3
---
03. März 2015. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../14.12.0'>KDE-Anwendungen 14.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

With 19 recorded bugfixes it also includes improvements to the anagram game Kanagram, Umbrello UML Modeller, the document viewer Okular and the geometry application Kig.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.17, KDE Development Platform 4.14.6 and the Kontact Suite 4.14.6.
