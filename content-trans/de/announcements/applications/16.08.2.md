---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE veröffentlicht die KDE-Anwendungen 16.08.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 16.08.2
version: 16.08.2
---
13. Oktober 2016. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../16.08.0'>KDE-Anwendungen 16.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 30 aufgezeichnete Fehlerkorrekturen enthalten unter andere und Okular.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
