---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE toob välja KDE rakendused 18.04.1
layout: application
title: KDE toob välja KDE rakendused 18.04.1
version: 18.04.1
---
May 10, 2018. Today KDE released the first stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Umbes 20 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Cantori, Dolphini, Gwenview, JuKi, Okulari ja Umbrello täiustused.

Täiustused sisaldavad muu hulgas:

- Topeltkirjed Dolphini asukohtade paneelil ei põhjusta enam krahhe
- Gwenview vana viga SVG-failide laadimisel parandati ära
- Umbrello C++ import oskab nüüd kasutada võtmesõna "explicit"
