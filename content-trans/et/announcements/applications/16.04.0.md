---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE toob välja KDE rakendused 16.04.0
layout: application
title: KDE toob välja KDE rakendused 16.04.0
version: 16.04.0
---
20. aprill. KDE laskis täna välja KDE rakendused 16.04 märkimisväärse hulga uuendustega, mis on nüüd veelgi hõlpsamini kasutatavad, veelgi rohkemate võimalustega ja aina vabamad kõigist neist seni nördimust tekitanud pisivigadest, sõnaga, teel aina lähemale just sinu seadmele ideaalselt sobivate rakendusteni.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> and <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release. We would also highly encourage your support for <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a> as we welcome it to our KDE Applications and your input on what more you'd like to see.

### KDE uusim lisandus

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

A new application has been added to the KDE Education suite. <a href='https://minuet.kde.org'>Minuet</a> is a Music Education Software featuring full MIDI support with tempo, pitch and volume control, which makes it suitable for both novice and experienced musicians.

Minuet sisaldab 44 heliridade, akordi, intervallide ja rütmi kuulamisharjutust, võimaldab muusikat visualiseerida klaveri klahvistikul ning lubab sujuvalt tarvitada ka oma harjutusi.

### Rohkem abi

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

KDE abikeskus, mida varem levitati koos Plasmaga, kuulub nüüd KDE rakenduste hulka.

KDE abikeskuse meeskonna võimas vigade sõelumise ja puhastamise kampaania suutis lahendada 49 veateadet, mis enamasti tõid kaasa varem halvasti või üldse mitte toiminud otsingufunktsionaalsuse taastamise ja täiustamise.

Sisemine dokumendiotsingu, mis seni tugines iganenud ht::/dig tarkvarale, aluseks sai uus Xapiani-põhine indekseerimis- ja otsingusüsteem, mis lubab taas otsida manuaalilehekülgedel, infolehekülgedel ja KDE tarkvara loodud dokumentatsioonis. Lisaks saab dokumentatsiooni lehekülgi järjehoidjatesse panna.

KDELibs4 toetuse eemaldamine ning koodi täiendav puhastamine ja mõne väiksema vea parandamine muutis koodi hooldamise palju lihtsamaks, tuues muu hulgas kaasa KDE abikeskuse palju klanituma välimuse.

### Agressiivne võitlus kõikvõimalike vigadega

Kontacti komplektis lahendati ära lausa 55 veateadet, millest mõned olid seotud häirete määramise probleemidega, samuti kirjade importimisega Thunderbirdist, Skype'i ja Google Talki ikoonide suuruse vähendamisega kontaktide paneeli vaates, KMailis kaustade impordi, vCardi impordi, ODF-vormingus manuste avamise, Chromiumist URL-ide lisamise, tööriistade menüü erinevusega autonoomsel ja Kontacti koosseisus kasutamisel, puuduva menüükirjega "Saada" ja veel mitme probleemiga. Lisati Brasiilia portugali keeles RSS-voogude toetus ning parandati ungari- ja hispaaniakeelsete voogude aadresse.

Uuteks omadusteks on KDE aadressiraamatu kontaktiredaktori välimuse ümberkujundamine, uus vaikimisi KMaili päise teema, seadistuste eksportija täiustamine ja lemmikikoonide toetuse parandamine Akregatoris. KMaili kirjakoostaja liides sai korraliku puhastuse ja seal saab nüüd tarvitada uut KDE vaikimisi kirjapäise teemat, Grantlee teema on aga kasutusel nii KMaili kui ka Kontacti ja Akregatori teabe dialoogis. Akregator kasutab nüüd QtWebKit'i - üht tähtsamat veebilehtede renderdamise ja JavaScripti koodi täitmise mootorit. Ühtlasi on tehtud algust nii Akregatorile kui ka teistele Kontacti komplekti rakendustele QtWebEngine'i toetuse lisamisega. Mitmed omadused lisati pluginatena paketti kdepim-addons, samal aja PIM-i teegid tükeldati paljudeks spetsialiseeritud pakettideks.

### Arhiveerimise edenemine

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, the archive manager, has had two major bugs fixed so that now Ark warns the user if an extraction fails due to lack of sufficient space in the destination folder and it does not fill up the RAM during the extraction of huge files via drag and drop.

Ark pakub nüüd ka omaduste dialoogi, mis näitab näiteks arhiivi tüüpi, suurust tihendatud ja tihendamata kujul, parajasti avatud arhiivi MD5/SHA-1/SHA-256 räsi jms. 

Ark suudab nüüd avada ja lahti pakkida RAR-arhiivifaile ilma mittevabu tööriistu kasutamata ning avada, lahti pakkida ja luua lzop/lzip/lrzip vormingus tihendatud TAR-arhiive.

Arki kasutajaliidest viimistleti omajagu, korraldades ümber menüü- ja tööriistariba kasutusmugavuse suurendamiseks ja arusaamatuste vähendamiseks. Ühtlasi suurendati püstsuunas ruumi aknas, jättes olekuriba vaikimisi peidetuks.

### Rohkem täpsust videode redigeerimisel

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, the non-linear video editor has numerous new features implemented. The titler now has a grid feature, gradients, the addition of text shadow and adjusting letter/line spacing.

Helitasemete lõimitud näitamine laseb vähese vaevaga jälgida heli kogu projektis, videomonitori uued kihid aga lubavad näha taasesituse kaadrisagedust, tsoone ja helilainevorme, tööriistariba võimaldab otsida markereid ja suurendad monitori.

Teegi uus omadus lubab kopeerida/asetada kaadrikogumit projektide vahel ning tükeldada ajatelge, et võrrelda ja visualiseerida klippe, millele on efekte rakendatud ja millele mitte.

Renderdamisdialoog on ümber kirjutatud ning sinna lisatud võimalus kiiremaks kodeerimiseks, suuremate failide loomiseks, samuti on kiiruseefekt nüüd heli esitamise toetusega.

Mõnes efektis on lisatud võtmekaadritele kõverad ning kasutaja enim valitud efektid leiab nüüd kiiresti lemmikefektide vidinas. Tükeldamise tööriista kasutades näeb nüüd ajateljel püstjoont täpselt kohas, kus kaadrid katki lõigatakse, ning uued klipigeneraatorid lubavad luua värviribadega klippe ja ajaloendureid. Lisaks on projektipuusse tagasi toodud klippide kasutuse näitamine ning helipisipildid on saanud samuti lihvi ja on nüüd palju kiiremad.

Suuremate veaparanduste seas võib ära märkida krahhi likvideerimise tiitrite kasutamisel (mis nõuab MLT uusimat versiooni), riknemiste kaotamist juhtudel, kui projekti kaadrisagedus sekundis oli midagi muud kui 25, krahhide likvideerimise radade kustutamisel, probleeme tekitanud ajatelje ülekirjutamise režiimi parandamise ning riknemiste teket ja efektide kadu lokaatide puhul, milles eraldajaks on koma. Kõige selle kõrval on loomulikult jätkuvalt pingutatud stabiilsuse, sujuvama töövoo tagamise ja pisemate kasutamismugavust suurendavate omaduste lisamise nimel.

### Ja veel palju rohkem!

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, the document viewer brings in new features in the form of customizable inline annotation width border, allowance of directly opening embedded files instead of only saving them and also the display of a table of content markers when the child links are collapsed.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> introduced improved support for GnuPG 2.1 with a fixing of selftest errors and the cumbersome key refreshes caused by the new GnuPG (GNU Privacy Guard) directory layout. ECC Key generation has been added with the display of Curve details for ECC certificates now. Kleopatra is now released as a separate package and support for .pfx and .crt files has been included. To resolve the difference in the behaviour of imported secret keys and generated keys, Kleopatra now allows you to set the owner trust to ultimate for imported secret keys. 
