---
aliases:
- ../announce-applications-15.12-beta
date: 2015-11-20
description: KDE Ships Applications 15.12 Beta.
layout: application
release: applications-15.11.80
title: KDE toob välja rakenduste 15.12 beetaväljalaske
---
20. november 2015. KDE laskis täna välja rakenduste uute versioonide beetaväljalaske. Kuna API, sõltuvused ja omadused on külmutatud, on KDE meeskonnad keskendunud vigade parandamisele ja tarkvara viimistlemisele.

Et paljud rakendused on juba viidud KDE Frameworks 5 peale, vajavad KDE rakendused 15.12 põhjalikku testimist kvaliteedi ja kasutajakogemuse tagamiseks ja parandamiseks. Kasutajatel on tihtipeale õigus suhtuda kriitiliselt KDE taotlusse hoida kõrget kvaliteeti, sest arendajad pole lihtsalt võimelised järele proovima kõiki võimalikke kombinatsioone. Me loodame oma kasutajate peale, kes oleksid suutelised varakult vigu üles leidma, et me võiksime need enne lõplikku väljalaset ära parandada. Niisiis - palun kaaluge mõtet ühineda meeskonnaga beetat paigaldades <a href='https://bugs.kde.org/'>ja kõigist ette tulevatest vigadest teada andes</a>.
