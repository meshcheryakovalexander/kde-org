---
aliases:
- ../../kde-frameworks-5.45.0
date: 2018-04-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Vormiandmetele määratakse ilmutatult sisutüüp

### Baloo

- Term'i opersatoritr &amp;&amp; ja || lihtsustamine
- Vahele jäetud tulemuskirjete dokumendi ID-d ei hakata tõmbama
- mtime'i ei tõmmata sortimise käigus andmebaasist korduvalt
- Vaikimisi ei ekspordita databasesanitizer'it
- baloodb: eksperimentaalse teate lisamine
- baloodb CLI tööriista pakkumine
- Klassi sanitizer pakkumine
- [FileIndexerConfig] kataloogide täitmisega viivitamine, kuni neid päriselt kasutatakse
- src/kioslaves/search/CMakeLists.txt - linkimine Qt5Network'iga pärast kio muudatusi
- balooctl: checkDb peaks samuti kontrollima documentid viimast teadaolevat url-i
- balooctl jälgija: taasalustamine teenuse järel ootamisel

### Breeze'i ikoonid

- window-pin ikooni lisamine (veateade 385170)
- elisa'le lisatud 64px ikoonide nime muutmine
- esitusloendi järjekorra segamise ja kordamise 32px ikoonide muutmine
- reapealsete sõnumite puuduvad ikoonid (veateade 392391)
- Muusikamängija Elisa uus ikoon
- Meediaoleku ikoonide lisamine
- Raami eemaldamine meediatoimingute ikoonide ümbert
- media-playlist-append ja play ikoonide lisamine
- view-media-album-cover'i lisaminer babe'ile

### CMake'i lisamoodulid

- CMake'i enda taristu ärakasutamine kompilaatori tööriistaahela tuvastamiseks
- API dox: mõne "koodiploki" ridade parandus, et neil oleks ees/järel tühje ridu
- ECMSetupQtPluginMacroNames'i lisamine
- androiddeployqt pakkumine koos kõigi prefiksi asukohtadega
- "stdcpp-path" kaasamine json-faili
- Nimeviitade lahendamine QML-i impordi asukohtades
- QML-i impordi asukohtade pakkumine androiddeployqt'ile

### Raamistike lõimimine

- kpackage-install-handlers/kns/CMakeLists.txt - linkimine Qt::Xml'iga pärast muutusi knewstuff'is

### KActivitiesStats

- Ei eeldata SQLite'i töötamist ja ei lõpetata tõrgete korral

### KDE Doxygeni tööriistad

- Abi genereerimiseks pöördutakse kõigepealt qhelpgenerator-qt5 poole

### KArchive

- karchive, kzip: topetfaile püütakse käidelda pisut viisakamalt
- nullptr kasutamine nullviida edastamiseks crc32'le

### KCMUtils

- Võimaldamine pärida plugina seadistusmooduli järele programmaatiliselt
- Järjekindel X-KDE-ServiceTypes'i kasutamine ServiceTypes'i asemel
- X-KDE-OnlyShowOnQtPlatforms'i lisamine KCModule'i teenusetüübi definitsiooni

### KCoreAddons

- KTextToHTML: tagastamine, kui url on tühi
- m_inotify_wd_to_entry puhastamine enne sisestusviitade märkimist kehtetuks (veateade 390214)

### KDeclarative

- QQmlEngine häälestatakse QmlObject'is ainul ühel korral

### KDED

- X-KDE-OnlyShowOnQtPlatforms'i lisamine KDEDModule'i teenusetüübi definitsiooni

### KDocTools

- Elisa, Markdown'i, KParts'i, DOT-i, SVG olemid lisati general.entities'isse
- customization/ru: underCCBYSA4.docbook'i ja underFDL.docbook'i tõlke parandus
- Topelt lgpl-notice/gpl-notice/fdl-notice parandus
- customization/ru: fdl-notice.docbook'i tõlge
- kwave'i õigekirja muutmine vastavalt hooldaja nõudmisele

### KFileMetaData

- taglibextractor: ümberkorraldamine parema loetavuse huvides

### KGlobalAccel

- Ei eeldata, kui kasutatakse valesti dbus'ist (veateade 389375)

### KHolidays

- holidays/plan2/holiday_in_en-gb - India pühadefaili uuendamine (veateade 392503)
- Seda paketti ei ole uuendatud. Arvatavasti skripti probleem
- Saksamaa pühadefailide läbivaatamine (veateade 373686)
- README.md vormindamine nii, nagu seda ootavad tööriistad (koos sissejuhatava sektsiooniga)

### KHTML

- välditakse tühja protokolli pärimist

### KI18n

- Tagamine, et Ki18n saaks ehitada oma tõlked
- PythonInterp.cmake'i ei kutsuta välja KF5I18NMacros'is
- Võimaldamine genereerida po-faile paralleelselt
- Konstruktori loomine KLocalizedStringPrivate'ile

### KIconThemes

- KIconEngine'i ekspordi kommentaari muutmine täpseks
- Välditakse asan'i käitusaja tõrget

### KInit

- Ajutiselt autenditud IdleSlave'i kustutamine

### KIO

- Tagamine, et mudel oleks määratud, kui kutsutakse välja resetResizing
- pwd.h puudub windows'is
- Viimati salvestatud sel kuul ja Viimati salvestatud eelmisel kuul vaikimisi eemaldamine
- KIO ehitatakse ka Androidi peal
- Faili IO-mooduli kauth'i abilise ja reeglifaili paigaldamise ajutine keelamine
- Privilegeeritud toimingu kinnituse küsimuse käitlemine SlaveBase'is, mitte KIO::Job'is
- "Avamine rakendusega" ühtluse täiustus tipprakendust alati reasisesena näidates
- Krahhi vältimine, kui seade väljastab pärast töö lõppu, et on lugemiseks valmis
- Valitud elementide esiletõstmine eellaskataloogi näitamisel avamis/salvestamisdialoogis (veateade 392330)
- NTFS-i peidetud failide toetus
- Järjekindel X-KDE-ServiceTypes'i kasutamine ServiceTypes'i asemel
- Eelduse parandus concatPaths'is täieliku asukoha asetamise korral KFileWidget'i redigeerimisreale
- [KPropertiesDialog] kontrollsumma kaardi toetus kõigi kohalike asukohtae korral (veateade 392100)
- [KFilePlacesView] KDiskFreeSpaceInfo väljakutsumine ainult vajadusel
- FileUndoManager: kohalikke faile, mida ei ole olemas, ei kustutata
- [KProtocolInfoFactory] puhvrit ei puhastata, kui see on äsja loodud
- Ei püüta leida ka suhtelise URL-i (nt '~') ikooni
- Korrektse elemendi URL-i kasutamine "Loo uus" kontekstimenüüs (veateade 387387)
- Veel rohkem findProtocol'i valede parameetrite parandusi
- KUrlCompletion: varane tagastamine, kui URL on vigane, nt ":/"
- Tühjale URLile-ei püüta ikooni leida

### Kirigami

- Suuremad ikoonid mobiiirežiimis
- Jõuga sisu suurus taustastiili elemendis
- InlineMessage'i tüübi js galeriirakenduse näidislehekülje lisamine
- valikulise toonimise parem heuristika
- laadimine kohalikest svg-dest toimib nüüd ka tegelikult
- samuti androidi ikooni laadimise meetodi toetamine
- varasematele erinevatele stiilidele sarnase toonimisstrateegia kasutamine
- [Card] enda"findIndex" teostuse kasutamine
- võrguülekannete tapmine, kui me selle ajal muudame ikooni
- delegaadi recycler esimene prototüüp
- OverlaySheet'i klientidel lubatakse sisseehitatud sulgemisnupp tähelepanuta jätta
- Kaartide komponendid
- Toimingunupu suuruse parandus
- passiveNotifications'i näitamisaja pikendamine, et kasutaja jõuaks neid ka tegelikult lugeda
- Kasutuseta QQC1 sõltuvuse eemaldamine
- ToolbarApplicationHeader'i paigutus
- Tiitli näitamise võimaldamine sõltumata ctx toimingutest

### KNewStuff

- Tegelik hääletamine loendivaates tärnidele klõpsates (veateade 391112)

### KPackage raamistik

- Püüti parandada ehitamist FreeBSD peal
- Qt5::rcc kasutamine täitmisfaili otsimise asemel
- NO_DEFAULT_PATH'i kasutamine õige käsu valimise tagamiseks
- Samuti prefiksiga rcc täitmisfailide otsimine
- komponendi määramine qrc korrektseks genereerimiseks
- rcc binaarpaketi genereerimise parandus
- rcc-faili genereerimine alati paigaldamise ajal
- org.kde. komponendid sisaldavad nüüdsest annetamise URL-i
- kpackage_install_package'i märkimine mittrigsnrnukd plasma_install_package'i jaoks

### KPeople

- PersonData::phoneNumber'i avalikustamine QML-ile

### Kross

- Pole põhjust nõuda kdoctools'i

### KService

- API dox: järjekindel X-KDE-ServiceTypes'i kasutamine ServiceTypes'i asemel

### KTextEditor

- Võimaldamine ehitada KTextEditori  Androidi NDK gcc 4.9 peal
- Asani käitusaja tõrke vältimine: nihkeeksponent -1 on negatiivne
- TextLineData::attribute'i optimeerimine
- attribute() ei arvutata kaks korda
- Paranduse tagasivõtmine: vaade hüppab, kui kerimine üle dokumendi lõpu on lubatud (veateade 391838)
- lõikepuhvri ajalugu ei risustata duplikaatidega

### KWayland

- Kaugligipääsu liidese lisamine KWaylandile
- [server] Pointer'i versiooni 5 kaadrisemantika toetuse lisamine (veateade 389189)

### KWidgetsAddons

- KColorButtonTest: TODO koodi eemaldamine
- ktooltipwidget: veeriste mahaarvamine saadaolevast suurusest
- [KAcceleratorManager] iconText() määramine ainult siis, kui seda tegelikult on muudetud (veateade 391002)
- ktooltipwidget: ekraanivälise näitamise vältimine
- KCapacityBar: QStyle::State_Horizontal'i oleku määramine
- Sünkroonimine KColorScheme'i muudatustega
- ktooltipwidget: kohtspikri asukoha parandus (veateade 388583)

### KWindowSystem

- "SkipSwitcher" lisamine API-sse
- [xcb] _NET_WM_FULLSCREEN_MONITORS'i teostuse parandus (veateade 391960)
- plasmashell'i hangumisaja vähendamine

### ModemManagerQt

- cmake: libnm-util'it ei märgita leituks, kui leitakse ModemManager

### NetworkManagerQt

- NetworkManageri kaasatute kataloogide eksport
- Hakatakse nõudma NM 1.0.0
- seade: StateChangeReason'i ja MeteredStatus'i defineerimine Q_ENUM'ina
- Tugijaama lippude võimeteks teisendamise parandus

### Plasma raamistik

- Taustapildimallid: taustavärvi määramine näidisteksti sisu kontrasti tagamiseks
- QML-i laiendusega Plasma taustapildi malli lisamine
- [ToolTipArea] signaali "aboutToShow" lisamine
- windowthumbnail: gammakorrektsiooni skaleerimise kasutamine
- windowthumbnail: mipmap'i tekstuuri filtreerimise kasutamine (veateade 390457)
- Kasutuseta X-Plasma-RemoteLocation'i kirjete eemaldamine
- Mallid: kasutuseta X-Plasma-DefaultSize'i eemaldamine apleti metaandmetest
- Järjekindel X-KDE-ServiceTypes'i kasutamine ServiceTypes'i asemel
- Mallid: kasutuseta X-Plasma-Requires-* kirjete eemaldamine apleti metaandmetest
- elemendi ankrute eemaldamine paigutuses
- plasmashell'i hangumisaja vähendamine
- eellaadimine alles pärast seda, kui konteiner on väljastanud uiReadyChanged'i
- Liitkasti katkimineku parandus (veateade 392026)
- Teksti mittetäisarvuliste skaleerimisteguritega skaleerimise parandus, kui on määratud PLASMA_USE_QT_SCALING=1 (veateade 356446)
- lahutatud/keelatud seadmete uued ikoonid
- [Dialoog] Lubamine määrata outputOnly NoBackground-dialoogile
- [Kohtspikker] failinime kontroll KDirWatch'i käitlejas
- kpackage_install_package'i iganemishoiatuse keelamine praegu
- [Breeze'i Plasma teema] currentColorFix.sh rakendamine muudetud meediaikoonidele
- [Breeze'i Plasma teema]ringikestega meediaoleku ikoonide lisamine
- Raamide eemaldamine meedianuppude ümbert
- [Akna pisipilt] atlas-tekstuuri kasutamise lubamine
- [Dialoog] nüüdseks aegunud KWindowSystem::setState väljakutsete eemaldamine
- Atlas-tekstuuride toetus FadingNode'is
- Tuumprofiiliga FadingMateria'l fragmendi parandus

### QQC2StyleBridge

- Renderdamise parandus, kui on keelatud
- parem paigutus
- eksperimentaalne automnemoonika toetus
- Tagamine, et stiili rakendamisel võetakse arvesse elemendi suurust
- Renderdamise parandus HiDPI ja täisarvuliste skaleerimistegurite korral (veateade 391780)
- värvivalikuga ikoonide värvide parandus
- tööriistanuppude ikoonivärvide parandus

### Solid

- Solid suudab nüüd pärida aku oleku järele näiteks juhtmeta juhtpultides ja juhtkangides
- Hiljuti välja toodud UP enumeraatorite kasutamine
- gaming_input devices'i ja teise lisamine Battery'sse
- Akuseadmete enumeraatori lisamine
- [UDevManager] samuti päritakse otseselt kaamerate järele
- [UDevManager] juba enne päringut allsüsteemide filtreerimine (veateade 391738)

### Sonnet

- Vaikimisi kliendi kasutamist ei sunnita peale, vaid valitakse see, mis toetab nõutud keelt
- Asendusstringide kaasamine pakkumiste loendisse
- implement NSSpellCheckerDict::addPersonal()
- NSSpellCheckerDict::suggest() returns a list of suggestions
- initialise NSSpellChecker language in NSSpellCheckerDict ctor
- implement NSSpellChecker logging category
- NSSpellChecker requires AppKit
- Move NSSpellCheckerClient::reliability() out of line
- use the preferred Mac platform token
- Use correct directory to lookup trigrams in windows build dir

### Süntaksi esiletõstmine

- Make it possible to fully build the project when crosscompiling
- Redesign CMake syntax generator
- Optimize highlighting Bash, Cisco, Clipper, Coffee, Gap, Haml, Haskell
- Add syntax highlighting for MIB files

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
