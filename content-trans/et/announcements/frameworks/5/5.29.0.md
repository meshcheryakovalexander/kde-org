---
aliases:
- ../../kde-frameworks-5.29.0
date: 2016-12-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Uus raamistik

This release includes Prison, a new framework for barcode generation (including QR codes).

### Üldine

FreeBSD was added to metainfo.yaml in all frameworks tested to work on FreeBSD.

### Baloo

- Jõudluse täiustused kirjutamisel (4x kiirem andmete kirjutamine)

### Breeze'i ikoonid

- BINARY_ICONS_RESOURCE on nüüd vaikimisi SEES
- vnd.rar MIME lisamine shared-mime-info 1.7-le (veateade 372461)
- claws'i ikooni lisamine (veateade 371914)
- gdrive'i ikooni lisamine üldise pilveikooni asemel (veateade 372111)
- vea "list-remove-symbolic kasutab vale pilti" parandus (veateade 372119)
- muud lisandused ja täiustused

### CMake'i lisamoodulid

- Pythoni seoste test jäetakse vahele, kui PyQt ei ole paigaldatud
- Testi lisamine ainult siis, kui leitakse python
- CMake'i nõutava miinimumi vähendamine
- Mooduli ecm_win_resolve_symlinks lisamine

### Raamistike lõimimine

- QDBus'i otsimine, mida vajab appstream'i kpackage'i käitleja
- KPackage hakkab sõltuma packagekit'ist ja appstream'ist

### KActivitiesStats

- Lingitud sündmuse kohane saatmine ressurssi

### KDE Doxygeni tööriistad

- Kohandamine quickgit -&gt; cgit muudatusega
- Veaparandus, kui grupi nimi oli defineerimata. Eriti halval juhul võib siiski ka edaspidi probleeme tekitada,

### KArchive

- Meetodi errorString() lisamine veateabe pakkumiseks

### KAuth

- Ajaületuse omaduse lisamine (veateade 363200)

### KConfig

- kconfig_compiler - koodi genereerimine tühistustega
- Funktsiooni võtmesõnade kohane parsimine (veateade 371562)

### KConfigWidgets

- Tagamine, et menüütoimingud saavad kavatsetud MenuRole'i

### KCoreAddons

- KTextToHtml: vea "hüperlingi lõppu lisatakse [1]" parandus (veateade 343275)
- KUser: avatari otsitakse ainult siis, kui loginName ei ole tühi

### KCrash

- Joondamine KInit'iga ja DISPLAY mittekasutamine Mac'i peal
- OS X peal ei suleta kõiki failideskriptoreid

### KDesignerPlugin

- src/kgendesignerplugin.cpp - genereeritud koodile tühistuste lisamine

### KDESU

- XDG_RUNTIME_DIR'i määramise tühistamine kdesu'ga käivitatud protsessides

### KFileMetaData

- FFMpeg libpostproc leitakse ka tegelikult

### KHTML

- java: nimede omistamine õigetele nuppudele
- java: nimede määramine õiguste dialoogis

### KI18n

- Viida ebavõrdsuse kohane kontroll dngettext'ist (veateade 372681)

### KIconThemes

- Kõigi kategooriate ikoonide näitamise lubamine (veateade 216653)

### KInit

- Uue protsessi käivitamisel keskkonnamuutujate määramine KLaunchRequest'i alusel

### KIO

- Portimine kategoriseeritud logimise peale
- Kompileerimise parandus WinXP SDK peal
- Suurtäheliste kontrollsummade vastete lubamine kontrollsumma kaardil (veateade 372518)
- Failidialoogis ei venitata kunagi viimast (kuupäeva)veergu (veateade 312747)
- kcontrol'i docbooki koodi importimine ja uuendamine kde-runtime'i põhiharu järgi
- [OS X] KDE prügikast kasutab edaspidi OS X prügikasti
- SlaveBase: dokumentatsiooni lisamine sündmusesilmuste, märguannete ja kded moodulite kohta

### KNewStuff

- Uue arhiivihalduse valiku (alamkataloog) lisamine knsrc'sse
- Uute tõrkesignaalide kasutamine (töö määramise tõrked)
- Kummalise olukorra käitlemine, kui alles loodud failid kaotsi lähevad
- Tuumikpäiste tegelik paigaldamine kaamelkirjas

### KNotification

- [KStatusNotifierItem] salvestamis/taastamisvidina asukoht selle akna peitmise/taastamise käigus (veateade356523)
- [KNotification] märguannete annoteerimise lubamine URL-iga

### KPackage raamistik

- metadata.desktop paigaldatakse ka edaspidi (veateade 372594)
- metaandmete käsitsi laadimine absoluutse asukoha edastamisel
- Võimaliku nurjumise vältimine, kui pakett ei ühildu appstream'iga
- KPackage'ile antakse teada X-Plasma-RootPath
- Faili metadata.json genereerimise parandus

### KPty

- Põhjalikum utempter'i asukoha otsing (kaasa arvatud /usr/lib/utempter/)
- Teegi asukoha lisamine, et utempter'i binaarfail leitaks üles Ubuntu 16.10-s

### KTextEditor

- Qt hoiatuste vältimine toetuseta lõikepuhvrirežiimi kohta Mac'i peal
- KF5::SyntaxHighlighting'i süntaksi definitsioonide kasutamine

### KTextWidgets

- Nurjunud otsingu korral ei asendata aknaikoone otsingutulemustega

### KWayland

- [klient] nullptr dereferentsimise parandus ConfinedPointer'is ja LockedPointer'is
- [klient] pointerconstraints.h paigaldamine
- [server] tagasilanguse parandus SeatInterface::end/cancelPointerPinchGesture'is
- PointerConstraints'i protokolli teostus
- [server] pointersForSurface'i ballasti vähendamine
- SurfaceInterface::size'i tagastamine globaalses komposiitoriruumis
- [tööriistad/generaator] enumeraatori FooInterfaceVersion genereerimine serveri poolel
- [tööriistad/generaator] wl_fixed nõude argumentide mähkimine in wl_fixed_from_double'is
- [tööriistad/generaator] kliendi poole nõuete teostuse genereerimine
- [tööriistad/generaator] kliendi poole ressursikogumite genereerimine
- [tööriistad/generaator] tagasiviidete ja jälgija genereerimine kliendi poolel
- [tööriistad/generaator] this edastatakse Client::Resource::Private'i viidana
- [tööriistad/generaator] Private::setup(wl_foo *arg) genereerimine kliendi poolel
- PointerGestures'i protokolli teostus

### KWidgetsAddons

- Krahhi vältimine Mac'i peal
- Nurjunud otsingu korral ei asendata ikoone otsingutulemustega
- KMessageWidget: paigutuse parandus, kui WordWrap on lubatud ilma toiminguteta
- KCollapsibleGroupBox: vidinaid ei peideta, selle asemel tühistatakse fookuse reegel

### KWindowSystem

- [KWindowInfo] pid() ja desktopFileName() lisamine

### Oxygeni ikoonid

- application-vnd.rar ikooni lisamine (veateade 372461)

### Plasma raamistik

- Metaandmete kehtivuse kontroll settingsFileChanged'is (veateade 372651)
- Vertikaalse kaardiriba paigutust ei peegeldata
- radialGradient4857 eemaldamine (veateade 372383)
- [AppletInterface] fookust ei võeta kunagi ära fullRepresentation'ilt (veateade 372476)
- SVG ikooni ID prefiksi parandus (veateade 369622)

### Solid

- winutils_p.h: ühilduvuse taastamine WinXP SDK-ga

### Sonnet

- Ka hunspell-1.5 otsimine

### Süntaksi esiletõstmine

- XML litsentsi atribuudi väärtuste normaliseerimine
- Süntaksi definitsioonide sünkroonimine ktexteditor'iga
- Voltimispiirkonna ühendamise parandus

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
