---
aliases:
- ../../kde-frameworks-5.62.0
date: 2019-09-14
layout: framework
libCount: 70
---
### Attica

- Corregir el archivo «pkgconfig» de attica.

### Baloo

- Se ha corregido un fallo en Peruse provocado por baloo.

### Iconos Brisa

- Añadir nuevos iconos de actividades y de escritorios virtuales.
- Hacer que los iconos pequeños de los documentos recientes se parezcan a los documentos y mejorar los emblemas del reloj.
- Crear el nuevo icono de «Carpetas recientes» (error 411635).
- Añadir el icono para «preferences-desktop-navigation» (error 402910).
- Añadir «dialog-scripts» de 22 píxeles; cambiar los iconos de acciones/lugares de scripts para que se coincidan con él.
- Mejorar el icono «user-trash».
- Usar el estilo vacío/lleno para la papelera vacía/llena en monocromo.
- Hacer que los iconos de las notificaciones usen el estilo con contorno.
- Hacer que los iconos «user-trash» se parezcan a cubos de basura (error 399613).
- Añadir iconos Brisa para los archivos cern ROOT.
- Eliminar «applets/22/computer» (error 410854).
- Añadir iconos para «view-barcode-qr».
- Krita se ha separado de Calligra y ahora usa el nombre «Krita» en lugar de «calligrakrita» (error 411163).
- Añadir iconos para «battery-ups» (error 411051).
- Hacer que el icono de «monitor» sea un enlace al de «computer».
- Añadir iconos de FictionBook 2.
- Añadir icono para kuiviewer (necesita actualizar -&gt; error 407527).
- Enlace simbólico de «port» a «anchor», que muestra iconos más apropiados.
- Cambiar el icono de radio al de dispositivo; añadir más tamaños.
- Hacer que el icono <code>pin</code> apunte a un icono que se asemeja a un alfiler en lugar de a otra cosa no relacionada.
- Corregir el dígito que faltaba y la alineación de píxel perfecto de los iconos de la acción de profundidad (error 406502).
- Hacer que el icono de 16 píxeles de «folder-activities» se parezca más a los de tamaños mayores.
- Añadir el icono de «latte-dock» del repositorio de Latte Dock para kde.org/applications.
- Se ha retocado el icono para «kdesrc-build» usado por kde.org/applications.
- Se ha cambiado el nombre de «media-show-active-track-amarok» a «media-track-show-active».

### Módulos CMake adicionales

- ECMAddQtDesignerPlugin: pass code sample indirectly via variable name arg
- Keep 'lib' as default LIBDIR on Arch Linux based systems
- Activar «autorcc» por omisión.
- Definir la ubicación de instalación para los archivos «JAR/AAR» en Android.
- Se ha añadido «ECMAddQtDesignerPlugin».

### KActivitiesStats

- Se han añadido «Term::Type::files()» y «Term::Type::directories()» para filtrar solo directorios o excluirlos.
- Añadir «@since 5.62» para los nuevos «setters» añadidos.
- Añadir registro adecuado usando «ECMQtDeclareLoggingCategory».
- Se ha añadido un «setter» a los campos de consulta «Type», «Activity», «Agent» y «UrlFilter».
- Usar valores de constantes especiales en «terms.cpp».
- Allow date range filtering of resource events using Date Term

### KActivities

- [kactivities] Usar el nuevo icono de actividades.

### KArchive

- Fix creating archives on Android content: URLs

### KCompletion

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KConfig

- Se ha corregido una fuga de memoria en KConfigWatcher.
- Desactivar «KCONFIG_USE_DBUS» en Android.

### KConfigWidgets

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)
- [KColorSchemeManager] Se ha optimizado la generación de vistas previas.

### KCoreAddons

- KProcessInfo::name() now returns only the name of the executable. For the full command line use KProcessInfo::command()

### KCrash

- Avoid enabling kcrash if it's only included via a plugin (bug 401637)
- Desactivar «kcrash» cuando se ejecuta bajo «rr».

### KDBusAddons

- Fix race on kcrash auto-restarts

### KDeclarative

- Mostrar una advertencia si «KPackage» no es válido.
- [GridDelegate] Don't select unselected item when clicking on any of its action buttons (bug 404536)
- [ColorButton] Forward accepted signal from ColorDialog
- use zero-based coordinate system on the plot

### KDesignerPlugin

- Deprecate kgendesignerplugin, drop bundle plugim for all KF5 widgets

### KDE WebKit

- Usar «ECMAddQtDesignerPlugin» en lugar de una copia privada.

### KDocTools

- KF5DocToolsMacros.cmake: Usar variables «KDEInstallDirs» no marcadas como obsoletas (error 410998).

### KFileMetaData

- Se ha implementado la escritura de imágenes.

### KHolidays

- Display filename where we return an error

### KI18n

- Localizar cadenas de números grandes (error 409077).
- Support passing target to ki18n_wrap_ui macro

### KIconThemes

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KIO

- Undoing trashing files on the desktop has been fixed (bug 391606)
- kio_trash: split up copyOrMove, for a better error than "should never happen"
- FileUndoManager: clearer assert when forgetting to record
- Fix exit and crash in kio_file when put() fails in readData
- [CopyJob] Fix crash when copying an already existing dir and pressing "Skip" (bug 408350)
- [KUrlNavigator] Add MIME types supported by krarc to isCompressedPath (bug 386448)
- Added dialog to set execute permission for executable file when trying to run it
- [KPropertiesDialog] Always check mount point being null (bug 411517)
- [KRun] Check mime type for isExecutableFile first
- Add an icon for the trash root and a proper label (bug 392882)
- Add support for handling QNAM SSL errors to KSslErrorUiData
- Making FileJob behave consistently
- [KFilePlacesView] Use asynchronous KIO::FileSystemFreeSpaceJob
- rename internal 'kioslave' helper executable to 'kioslave5' (bug 386859)
- [KDirOperator] Middle-elide labels that are too long to fit (bug 404955)
- [KDirOperator] Add follow new directories options
- KDirOperator: Only enable "Create New" menu if the selected item is a directory
- KIO FTP: Fix file copy hanging when copying to existing file (bug 409954)
- KIO: port to non-deprecated KWindowSystem::setMainWindow
- Make file bookmark names consistent
- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)
- [KDirOperator] Use more human-readable sort order descriptions
- [Permissions editor] Port icons to use QIcon::fromTheme() (bug 407662)

### Kirigami

- Replace the custom overflow button with PrivateActionToolButton in ActionToolBar
- If a submenu action has an icon set, make sure to also display it
- [Separator] Match Breeze borders' color
- Se ha añadido el componente ListSectionHeader de Kirigami.
- Fix context menu button for pages not showing up
- Fix PrivateActionToolButton with menu not clearing checked state properly
- Permitir la asignación de un icono personalizado para el asa del cajón izquierdo.
- Rework the visibleActions logic in SwipeListItem
- Allow usage of QQC2 actions on Kirigami components and now make K.Action based on QQC2.Action
- Kirigami.Icon: Fix loading bigger images when source is a URL (bug 400312)
- Se ha añadido el icono usado por «Kirigami.AboutPage».

### KItemModels

- Se ha añadido la interfaz «Q_PROPERTIES» a «KDescendantsProxyModel».
- Se ha adaptado el uso de métodos obsoletos de Qt.

### KItemViews

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KNotification

- Avoid duplicate notifications from showing up on Windows and remove whitespaces
- Have 1024x1024 app icon as fallback icon in Snore
- Se ha añadido el parámetro <code>-pid</code> a las llamadas al motor Snore.
- Se ha añadido el motor «snoretoast» para las «KNotifications» en Windows.

### KPeople

- Hacer posible el borrado de contactos desde los motores.
- Hacer posible la modificación de contactos.

### KPlotting

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KRunner

- Make sure we're checking whether tearing down is due after finishing a job
- Se ha añadido una señal de finalización a «FindMatchesJob» en lugar de usar «QObjectDecorator» erróneamente.

### KTextEditor

- Permitir la personalización de atributos en los temas de KSyntaxHighligting.
- Corrección para todos los temas: permitir la desactivación de atributos en los archivos de resaltado XML.
- simplify isAcceptableInput + allow all stuff for input methods
- simplify typeChars, no need for return code without filtering
- Mimic QInputControl::isAcceptableInput() when filtering typed characters (bug 389796)
- try to sanitize line endings on paste (bug 410951)
- Corrección: permitir la desactivación de atributos en los archivos de resaltado XML.
- improve word completion to use highlighting to detect word boundaries (bug 360340)
- Más conversiones de «QRegExp» en «QRegularExpression».
- properly check if diff command can be started for swap file diffing (bug 389639)
- KTextEditor: Fix left border flicker when switching between documents
- Migrate some more QRegExps to QRegularExpression
- Allow 0 in line ranges in vim mode
- Use CMake find_dependency instead of find_package in CMake config file template

### KTextWidgets

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KUnitConversion

- Add decibel power units (dBW and multiples)

### Framework KWallet

- KWallet: fix starting kwalletmanager, the desktop file name has a '5' in it

### KWayland

- [server] Wrap proxyRemoveSurface in smart pointer
- [server] Use cached current mode more and assert validness
- [servidor] usar caché para el modo actual.
- Implement zwp_linux_dmabuf_v1

### KWidgetsAddons

- [KMessageWidget] Pass widget to standardIcon()
- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KWindowSystem

- KWindowSystem: add cmake option KWINDOWSYSTEM_NO_WIDGETS
- Deprecate slideWindow(QWidget *widget)
- Add KWindowSystem::setMainWindow(QWindow *) overload
- KWindowSystem: add setNewStartupId(QWindow *...) overload

### KXMLGUI

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### NetworkManagerQt

- Rename WirelessDevice::lastRequestScanTime to WirelessDevice::lastRequestScan
- Add property lastScanTime and lastRequestTime to WirelessDevice

### Framework de Plasma

- Hacer que los iconos de las notificaciones usen el estilo con contorno.
- make the sizing of the toolbuttons more coherent
- Allow applets/containments/wallpaper to defer UIReadyConstraint
- Make notification icons look like bells (bug 384015)
- Fix incorrect initial tabs position for vertical tab bars (bug 395390)

### Purpose

- Fixed Telegram Desktop plugin on Fedora

### QQC2StyleBridge

- Prevent dragging QQC2 ComboBox contents outside menu

### Solid

- Make battery serial property constant
- Expose technology property in battery interface

### Sonnet

- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### Resaltado de sintaxis

- C &amp; ISO C++: add digraphs (folding &amp; preprocessor) (bug 411508)
- Markdown, TypeScript &amp; Logcat: some fixes
- Format class: add functions to know if XML files set style attributes
- combine test.m stuff into existing highlight.m
- Permitir el uso de cadenas nativas de Matlab.
- Gettext: Add "Translated String" style and spellChecking attribute (bug 392612)
- Set the OpenSCAD indentor to C-style instead of none
- Possibility to change Definition data after loading
- Highlighting indexer: check kateversion
- Markdown: multiple improvements and fixes (bug 390309)
- JSP: support of &lt;script&gt; and &lt;style&gt; ; use IncludeRule ##Java (bug 345003)
- LESS: import CSS keywords, new highlighting and some improvements
- JavaScript: remove unnecessary "Conditional Expression" context
- New syntax: SASS. Some fixes for CSS and SCSS (bug 149313)
- Use CMake find_dependency in CMake config file instead of find_package
- SCSS: fix interpolation (#{...}) and add the Interpolation color
- fix additionalDeliminator attribute (bug 399348)
- C++: contracts are not in C++20
- Gettext: fix "previous untranslated string" and other improvements/fixes
- Jam: Fix local with variable without initialisation and highlight SubRule
- implicit fallthrough if there is fallthroughContext
- Add common GLSL file extensions (.vs, .gs, .fs)
- Latex: several fixes (math mode, nested verbatim, ...) (bug 410477)
- Lua: fix color of end with several levels of condition and function nesting
- Highlighting indexer: all warnings are fatal

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
