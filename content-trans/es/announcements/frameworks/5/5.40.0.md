---
aliases:
- ../../kde-frameworks-5.40.0
date: 2017-11-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Consider DjVu files to be documents (bug 369195)
- Fix spelling so WPS Office presentations are recognized correctly

### Iconos Brisa

- add folder-stash for the stash Dolphin toolbar icon

### KArchive

- Corregir una fuga de memoria potencial. Se ha corregido la lógica.

### KCMUtils

- no margins for qml modules from qwidget side
- Inicializar variables (encontrado por «coverity»).

### KConfigWidgets

- Se ha corregido el icono de «KStandardAction::MoveToTrash».

### KCoreAddons

- fix URL detection with double urls like "http://www.foo.bar&lt;http://foo.bar/&gt;"
- Usar «https» en las URL de KDE.

### Soporte de KDELibs 4

- full docu for disableSessionManagement() replacement
- Hacer que «kssl» se pueda compilar con OpenSSL 1.1.0 (fallo 370223).

### KFileMetaData

- Se ha corregido el nombre a mostrar de la propiedad «Generator».

### KGlobalAccel

- KGlobalAccel: Se ha corregido el uso de las teclas del bloque numérico (otra vez).

### KInit

- Correct installation of start_kdeinit when DESTDIR and libcap are used together

### KIO

- Corregir la visualización de «remote:/» el «qfiledialog».
- Implement support for categories on KfilesPlacesView
- HTTP: fix error string for the 207 Multi-Status case
- KNewFileMenu: clean up dead code, spotted by Coverity
- IKWS: Fix possible infinite loop, spotted by Coverity
- Función «KIO::PreviewJob::defaultPlugins()».

### Kirigami

- syntax working on older Qt 5.7 (bug 385785)
- stack the overlaysheet differently (bug 386470)
- Show the delegate highlighted property as well when there's no focus
- preferred size hints for the separator
- correct Settings.isMobile usage
- Allow applications to be somewhat convergent on a desktop-y system
- Make sure the content of the SwipeListItem doesn't overlap the handle (bug 385974)
- Overlaysheet's scrollview is always ointeractive
- Add categories in gallery desktop file (bug 385430)
- Se ha actualizado el archivo «kirigami.pri».
- use the non installed plugin to do the tests
- Desaconsejar el uso de Kirigami.Label.
- Port gallery example use of Labels to be consistently QQC2
- Port Kirigami.Controls uses of Kirigami.Label
- make the scrollarea interactive on touch events
- Move the git find_package call to where it's used
- default to transparent listview items

### KNewStuff

- Remove PreferCache from network requests
- Don't detach shared pointers to private data when setting previews
- KMoreTools: Actualizar y corregir archivos de escritorio (error 369646).

### KNotification

- Remove check for SNI hosts when chosing whether to use legacy mode (bug 385867)
- Only check for legacy system tray icons if we're going to make one (bug 385371)

### Framework KPackage

- Usar los archivos de servicio no instalados.

### KService

- Inicializar valores.
- Inicializar algún puntero.

### KTextEditor

- API dox: fix wrong names of methods and args, add missing \\since
- Avoid (certain) crashes while executing QML scripts (bug 385413)
- Avoid a QML crash triggered by C style indentation scripts
- Aumentar el tamaño de la marca de cola.
- fix some indenters from indenting on random characters
- Se ha corregido una advertencia sobre obsolescencia.

### KTextWidgets

- Inicializar valor.

### KWayland

- [client] Drop the checks for platformName being "wayland"
- Don't duplicate connect to wl_display_flush
- Protocolo extranjero de Wayland.

### KWidgetsAddons

- fix createKMessageBox focus widget inconsistency
- more compact password dialog (bug 381231)
- Definir la anchura de KPageListView correctamente.

### KWindowSystem

- KKeyServer: fix handling of Meta+Shift+Print, Alt+Shift+arrowkey etc
- Permitir el uso de la plataforma «flatpak».
- Use KWindowSystem's own platform detection API instead of duplicated code

### KXMLGUI

- Usar «https» en las URL de KDE.

### NetworkManagerQt

- 8021xSetting: domain-suffix-match is defined in NM 1.2.0 and newer
- Permitir «domain-suffix-match» en «Security8021xSetting».

### Framework de Plasma

- Dibujar manualmente el arco circular.
- [Menú de PlasmaComponents] Se ha añadido «ungrabMouseHack».
- [FrameSvg] Optimizar «updateSizes».
- Don't position a Dialog if it's of type OSD

### QQC2StyleBridge

- Improve compilation as a static plugin
- Hacer que el botón de opciones parezca realmente un botón de opciones.
- Usar «qstyle» para dibujar «Dial».
- Usar una «ColumnLayout» para los menús.
- Corregir «Dialog».
- remove invalid group property
- Fix formatting of the md file so it matches the other modules
- behavior of combobox closer to qqc1
- Parche temporal para «QQuickWidgets».

### Sonnet

- Añadir el método «assignByDictionnary».
- Signal if we are able to assign dictionary

### Resaltado de sintaxis

- Makefile: fix regexpr matching in "CXXFLAGS+"

### ThreadWeaver

- CMake cleanup: Don't hardcode -std=c++0x

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
