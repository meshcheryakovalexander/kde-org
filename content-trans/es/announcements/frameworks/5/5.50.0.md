---
aliases:
- ../../kde-frameworks-5.50.0
date: 2018-09-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Se ha añadido la implementación de las etiquetas propuestas en OCS 1.7.

### Baloo

- Se ha corregido un error tipográfico en la salida del tamaño del índice (error 397843).
- Eliminar la URL de origen, no la de destino, cuando una URL se convierte en no indexable.
- [tags_kio] Simplificar la coincidencia de la petición de ruta de archivo usando un grupo de captura.
- Revertir «Omitir encolar archivos recientemente no indexables y eliminarlos del índice permanentemente».
- [tags_kio] Simplificar la búsqueda de ruta de archivos en bucle.

### Iconos Brisa

- Se ha añadido un icono para archivos de proyectos de LabPlot.
- ScalableTes: añadir la integración «escalable» de navegador en Plasma (error 393999).

### Módulos CMake adicionales

- Bindings: Check if bindings can be generated for a specific python version
- Bindings: Make generator forward compatible with Python 3
- Disable alteration of QT_PLUGIN_PATH by ECM when running tests
- Bindings: Add support for scoped enums (bug 397154)
- Se ha hecho posible que ECM detecte archivos po en el momento de la configuración.

### Integración con Frameworks

- [KStyle] Usar «dialog-question» para el icono de preguntas.

### KArchive

- Manejar codificaciones no ASCII en nombres de archivos en los archivos comprimidos tar (error 266141).
- KCompressionDevice: no llamar a «write» tras un error de escritura (error 397545).
- Añadir las macros Q_OBJECT que faltaban a las subclases de QIODevice.
- KCompressionDevice: propagar errores de QIODevice::close() (error 397545).
- Corregir la página principal de bzip.

### KCMUtils

- Usar un QScrollArea personalizado con sugerencia de tamaño no limitada por el tamaño del tipo de letra (error 389585).

### KConfig

- Eliminar advertencia sobre una antigua función de «kiosk» que ya no se usa.
- Definir el acceso rápido de teclado del sistema por omisión Ctrl+0 para la acción «Tamaño real».

### KCoreAddons

- No eliminar el espacio entre dos URL cuando la línea empieza por " (error de kmail).
- KPluginLoader: usar «/» incluso en Windows, ya que libraryPaths() devuelve rutas con «/».
- KPluginMetaData: convertir cadenas de texto vacía a listas de cadenas de texto vacías.

### KDeclarative

- Revertir «Asegurar que escribimos siempre en el contexto raíz del motor».
- Adjuntar propiedad a «delegate» (error 397367).
- [KCM GridDelegate] Use layer effect only on OpenGL backend (bug 397220)

### KDocTools

- Se ha añadido el acrónimo ASCII a «general.entities».
- Se ha añadido JSON a «general.entities».
- Permitir que «meinproc5» dé más información en la prueba automática «install».
- Usar rutas absolutas en «kdoctools_install» para encontrar archivos instalados.

### KFileMetaData

- Se ha añadido un alias del enumerador «Property::Language» para el error tipográfico «Property::Langauge».

### KHolidays

- Se ha implementado un algoritmo correcto para calcular los equinoccios y los solsticios (erro 396750).
- src/CMakeLists.txt: Instalar cabecera a la manera de frameworks.

### KI18n

- Impedir que se intente usar un KCatalog sin una QCoreApplication.
- Adaptar ki18n de QtScript a QtQml.
- Comprobar también el directorio de construcción para «po/».

### KIconThemes

- Usar Brisa como tema de iconos al que recurrir en caso de fallo.

### KIO

- [KSambaShareData] Aceptar espacios en el nombre de la máquina ACL.
- [KFileItemListProperties] Usar «mostLocalUrl» para capacidades.
- [KMountPoint] Comprobar también «smb-share» por si es un montaje SMB (error 344146).
- [KMountPoint] Resolver los montajes «gvfsd» (error 344146).
- [KMountPoint] Eliminar los restos de «supermount».
- [KMountPoint] Eliminar el uso de AIX y Windows CE.
- Display mounted file system type and mounted from fields in properties dialog (bug 220976)
- kdirlistertest doesn't fail at random
- [KUrlComboBox] Corregir el error de adaptación a KIcon.
- Port KPATH_SEPARATOR "hack" to QDir::listSeparator, added in Qt 5.6
- Corregir una pérdida de memoria en KUrlComboBox::setUrl.
- [KFileItem] Don't read directory comment on slow mounts
- Use QDir::canonicalPath instead
- Ignore NTFS hidden flag for root volume (bug 392913)
- Give the "invalid directory name" dialog a cancel button
- KPropertiesDialog: switch to label in setFileNameReadOnly(true)
- Refine wording when a folder with an invalid name could not be created
- Use appropriate icon for a cancel button that will ask for a new name
- Hacer que los archivos de solo lectura se puedan seleccionar.
- Usar las mayúsculas para títulos en algunas etiquetas de botones.
- Usar KLineEdit para nombres de carpeta si tienen permiso de escritura; en caso contrario, usar QLabel.
- KCookieJar: Corregir conversión errónea de zona horaria.

### Kirigami

- Permitir el uso de «fillWidth» en los elementos.
- Impedir el borrado externo de páginas.
- Mostrar siempre la cabecera cuando estamos en modo plegable.
- Corregir el comportamiento de «showContentWhenCollapsed».
- Corregir huecos en los menús del estilo «Material».
- standard actionsmenu for the page contextmenu
- Explicitly request Qt 5.7's QtQuick to make use of Connections.enabled
- use Window color instead of a background item
- Asegurarse de que el cajón se cierra incluso cuando se pulsa uno nuevo.
- export separatorvisible to the globaltoolbar
- Corregir la generación de complementos estáticos QRC de Kirigami.
- Corregir la compilación en el modo estático LTO.
- Asegurar que la propiedad drawerOpen se sincroniza correctamente (error 394867).
- Cajón: Mostrar el widget del contenido al arrastrar.
- Allow qrc assets to be used in Actions icons
- ld on old gcc (bug 395156)

### KItemViews

- Desaconsejar el uso de KFilterProxySearchLine.

### KNewStuff

- Poner providerId en caché.

### KNotification

- Permitir el uso de libcanberra para notificaciones sonoras.

### KService

- KBuildSycoca: Procesar siempre los archivos de escritorio de las aplicaciones.

### KTextEditor

- Turn enum Kate::ScriptType into an enum class
- Manejo de error correcto para QFileDevice y KCompressedDevice.
- InlineNotes: Do not print inline notes
- Remove QSaveFile in favor of plain old file saving
- InlineNotes: Use screen global coordinates everywhere
- InlineNote: Initialize position with Cursor::invalid()
- InlineNote: Pimpl inline note data without allocs
- Se ha añadido la interfaz de notas en línea.
- Show text preview only if main window is active (bug 392396)
- Se ha corregido un fallo de la aplicación al ocultar el widget «TextPreview» (error 397266).
- Fusionar ssh://git.kde.org/ktexteditor
- improve hidpi rendering of icon border
- Se ha mejorado el tema de colores de vim (error 361127).
- Search: Add workaround for missing icons in Gnome icon-theme
- fix overpainting for _ or letters like j in the last line (bug 390665)
- Se ha extendido la API de scripts para permitir la ejecución de órdenes.
- Script de sangrado para R.
- Se ha corregido un cuelgue al sustituir n alrededor de líneas vacías (error 381080).
- remove highlighting download dialog
- no need to new/delete hash on each doHighlight, clearing it is good enough
- ensure we can handle invalid attribute indices that can happen as left overs after HL switch for a document
- let smart pointer handle deletion of objects, less manual stuff to do
- remove map to lookup additional hl properties
- KTextEditor usa el framework KSyntaxHighlighting para todo.
- Usar las codificaciones de caracteres como se proporcionan en las definiciones.
- Se ha fusionado la rama «master» en el resaltado de sintaxis.
- non-bold text no longer renders with font weight thin but (bug 393861)
- Usar foldingEnabled.
- Se ha eliminado EncodedCharaterInsertionPolicy.
- Printing: Respect footer font, fix footer vertical position, make header/footer separator line visually lighter
- Se ha fusionado la rama «master» en el resaltado de sintaxis.
- let syntax-highlighting framework handle all definition management now that there is the None definition around in the repo
- completion widget: fix minimum header section size
- Fix: Scroll view lines instead of real lines for wheel and touchpad scrolling (bug 256561)
- remove syntax test, that is now tested in the syntax-highlighting framework itself
- KTextEditor configuration is now application local again, the old global configuration will be imported on first use
- Usar KSyntaxHighlighting::CommentPosition en lugar de KateHighlighting::CSLPos.
- Usar isWordWrapDelimiter() de KSyntaxHighlighting.
- Cambiar el nombre de isDelimiter() a isWordDelimiter().
- implement more lookup stuff via format -&gt; definition link
- Ahora siempre se obtienen formatos válidos.
- Mejor modo de obtener el nombre del atributo.
- fix python indentation test, safer accessor to property bags
- Se han vuelto a añadir los prefijos de definiciones correctos.
- Merge branch 'syntax-highlighting' of git://anongit.kde.org/ktexteditor into syntax-highlighting
- try to bring back lists needed to do the configuration per scheme
- Usar KSyntaxHighlighting::Definition::isDelimiter().
- make can break bit more like in word code
- no linked list without any reason
- cleanup properties init
- fix order of formats, remember definition in highlighting bag
- handle invalid formats / zero length formats
- remove more old implementation parts, fixup some accessors to use the format stuff
- fix indentation based folding
- remove exposure of context stack in doHighlight + fix ctxChanged
- start to store folding stuff
- rip out highlighting helpers, no longer needed
- remove need to contextNum, add FIXME-SYNTAX marker to stuff that needs to be fixed up properly
- adapt to includedDefinitions changes, remove contextForLocation, one only needs either keywords for location or spellchecking for location, can be implemented later
- Se han eliminado más cosas que ya no se usan en el resaltado de sintaxis.
- fixup the m_additionalData and the mapping for it a bit, should work for attributes, not for context
- create initial attributes, still without real attribute values, just a list of something
- Llamar al resaltado de sintaxis.
- derive from abstract highlighter, set definition

### Framework KWallet

- Move example from techbase to own repo

### KWayland

- Sync set/send/update methods
- Add serial number and EISA ID to OutputDevice interface
- Output device color curves correction
- Se ha corregido la gestión de memoria en WaylandOutputManagement.
- Aislar todas las pruebas de WaylandOutputManagement.
- Escalado fraccionario de OutputManagement.

### KWidgetsAddons

- Crear un primer ejemplo del uso de KMessageBox.
- Se han corregido dos errores en KMessageWidget.
- [KMessageBox] Call style for icon
- Add workaround for labels with word-wrapping (bug 396450)

### KXMLGUI

- Hacer que Konqi se vea bien en las pantalla de alta densidad.
- Se han añadido los paréntesis que faltaban.

### NetworkManagerQt

- Require NetworkManager 1.4.0 and newer
- manager: add support to R/W the GlobalDnsConfiguration property
- Actually allow to set the refresh rate for device statistics

### Framework de Plasma

- Workaround bug with native rendering and opacity in TextField text (bug 396813)
- [Icon Item] Watch KIconLoader icon change when using QIcon (bug 397109)
- [Icon Item] Use ItemEnabledHasChanged
- Deshacerse del uso desaconsejado de QWeakPointer.
- Se ha corregido la hoja de estilos para «22-22-system-suspend» (error 397441).
- Improve Widgets' removal and configure text

### Solid

- solid/udisks2: Añadir la posibilidad de crear un registro categorizado.
- [Dispositivo de Windows] Mostrar la etiqueta del dispositivo solo si hay una.
- Forzar la reevaluación de los predicados si se eliminan las interfaces (error 394348).

### Sonnet

- hunspell: Restaurar la compilación con hunspell &lt;=v1.5.0.
- Incluir cabeceras de hunspell como cabeceras del sistema.

### syndication

Nuevo módulo

### Resaltado de sintaxis

- Resaltar 20.000 líneas por caso de prueba.
- make highlighting benchmark more reproducible, we anyways want to measure this execution with e.g. perf from the outside
- Tune KeywordList lookup &amp; avoid allocations for implicit capture group
- remove captures for Int, never implemented
- deterministic iteration of tests for better result comparison
- handle nested include attributes
- Actualizar el resaltado de sintaxis de Modula-2 (error 397801).
- precompute attribute format for context &amp; rules
- avoid word delimiter check at start of keyword (bug 397719)
- Se ha añadido resaltado de sintaxis para el lenguaje de políticas del kernel de SELinux.
- hide bestCandidate, can be static function inside file
- Add some improvements to kate-syntax-highlighter for use in scripting
- added := as a valid part of an identifier
- use our own input data for benchmarking
- try to fix line ending issue in compare of results
- try trivial diff output for Windows
- add defData again for valid state check
- decrease StateData space by more than 50% and half the number of needed mallocs
- improve performance of Rule::isWordDelimiter and KeywordListRule::doMatch
- Improve skip offset handling, allow to skip full line on no match
- check extensions wildcard list
- more asterisk hl, I tried some asterisk configs, they are just ini style, use .conf as ini ending
- fix highlighting for #ifdef _xxx stuff (bug 397766)
- fix wildcards in files
- MIT relicensing of KSyntaxHighlighting done
- JavaScript: add binaries, fix octals, improve escapes &amp; allow Non-ASCII identifiers (bug 393633)
- Allow to turn of the QStandardPaths lookups
- Allow to install syntax files instead of having them in a resource
- handle context switch attributes of the contexts themselves
- change from static lib to object lib with right pic setting, should work for shared + static builds
- avoid any heap allocation for default constructed Format() as used as "invalid"
- Respetar la variable de cmake para bibliotecas estáticas en lugar de dinámicas, como «karchive».
- MIT relicensing, https://phabricator.kde.org/T9455
- remove old add_license script, no longer needed
- Fix includedDefinitions, handle definition change in context switch (bug 397659)
- SQL: various improvements and fix if/case/loop/end detection with SQL (Oracle)
- fix reference files
- SCSS: update syntax. CSS: fix Operator and Selector Tag highlighting
- debchangelog: add Bookworm
- Se ha cambiado la licencia de «Dockerfile» a la licencia de MIT.
- remove the no longer supported configuration part of the spellchecking that always had just one mode we now hardcode
- Se ha añadido el resaltado de sintaxis para Stan.
- add back indenter
- Optimize many syntax highlighting files and fix the '/' char of SQL
- Modelines: add byte-order-mark &amp; small fixes
- Se ha cambiado la licencia de «modelines.xml» a la licencia de MIT (error 198540).
- Add QVector&lt;QPair&lt;QChar, QString&gt;&gt; Definition::characterEncodings() const
- Add bool Definition::foldingEnabled() const
- Se ha añadido el resaltado de sintaxis «Ninguno» al repositorio como predeterminado.
- Update Logtalk language syntax support
- Add Autodesk EAGLE sch and brd file format to the XML category
- C# highlighting: Prefer C-Style indenter
- AppArmor: update syntax and various improvements/fixes
- Java: add binaries &amp; hex-float, and support underscores in numbers (bug 386391)
- Cleanup: indentation was moved from general to language section
- Definition: Expose command markers
- Añadir resaltado de sintaxis para JavaScript React.
- YAML: fix keys, add numbers and other improvements (bug 389636)
- Add bool Definition::isWordWrapDelimiter(QChar)
- Definition: Rename isDelimiter() to isWordDelimiter()
- Note KF6 API improvement ideas from the KTE porting
- Proporcionar también un formato válido para las líneas vacías.
- Make Definition::isDelimiter() also work for invalid definitions
- Definition: Expose bool isDelimiter() const
- Sort returned formats in Definition::formats() by id

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
