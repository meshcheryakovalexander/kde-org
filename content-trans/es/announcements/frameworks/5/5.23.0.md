---
aliases:
- ../../kde-frameworks-5.23.0
date: 2016-06-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Hacer que sea realmente posible detectar los proveedores de la URL que se nos indica.
- Proporcionar algunos auxiliares de «QDebug» para algunas clases de «Attica».
- Corregir redirección de URL absolutas (error 354748)

### Baloo

- Se ha corregido el uso de espacios en las etiquetas «kioslave» (error 349118).

### Iconos Brisa

- Añadir una opción de CMake para compilar recursos binarios de Qt fuera del directorio de iconos.
- Muchos iconos nuevos y otros actualizados
- Actualizar el icono de «desconectado» de la red para que tenga más diferencias con el de «conectado» (error 353369).
- Actualizar el icono de montado y de desmontado (error 358925).
- Añadir algunos avatares de «plasma-desktop/kcms/useraccount/pics/sources».
- Eliminar el icono «chromium» porque el icono predeterminado de Chromium ya es bueno (fallo 363595).
- Hacer más claros los iconos de Konsole (error 355697).
- Añadir iconos de correo electrónico a thunderbird (error 357334).
- Añadir icono de clave pública (error 361366).
- Eliminar «process-working-kde» porque se deben usar los iconos de Konqueror (fallo 360304).
- Actualizar iconos de krusader (error 359863).
- Cambiar el nombre de los iconos del micrófono según D1291 (error D1291).
- Añadir algunos iconos de tipo MIME de scripts (fallo 363040).
- Añadir el teclado virtual y la funcionalidad de encender/apagar panel táctil para OSD.

### Integración con Frameworks

- Eliminar dependencias no usadas y manejo de traducción.

### KActivities

- Añadir la propiedad «runningActivities» a «Consumer».

### Herramientas KDE Doxygen

- Trabajo importante de la generación de documentación de la API.

### KCMUtils

- Usar «QQuickWidget» para los KCM en QML (error 359124).

### KConfig

- Evitar que se omita la comprobación de «KAuthorized».

### KConfigWidgets

- Permitir el uso del nuevo estilo de sintaxis para la conexión con «KStandardAction::create()».

### KCoreAddons

- Muestra el complemento defectuoso al notificar una advertencia de reinterpretación.
- [kshareddatacache] Corregir el uso no válido de &amp; para evitar lecturas no alineadas.
- Kdelibs4ConfigMigrator: Omitir la repetición del análisis si no se ha migrado nada.
- krandom: Añadir un caso de prueba para detectar el error 362161 (fallo en semilla automática).

### KCrash

- Comprobar el tamaño de la ruta del conector del dominio UNIX antes de copiar en ella.

### KDeclarative

- Permitir el estado de seleccionado.
- La importación de KCMShell se puede usar ahora para consultar si la apertura de un KCM está realmente permitida.

### Soporte de KDELibs 4

- Advertir de que «KDateTimeParser::parseDateUnicode» no está implementado.
- K4TimeZoneWidget: Se ha corregido la ruta de las imágenes de las banderas.

### KDocTools

- Añadir entidades de uso común para teclas a «en/user.entities».
- Actualizar las plantillas de man-docbook.
- Actualizar las plantillas de los libros y de las páginas de manual y añadir plantilla para artículos.
- Llamar «kdoctools_create_handbook» solo para «index.docbook» (error 357428).

### KEmoticons

- Se ha implementado el uso de emojis en «KEmoticon» y se han añadido los iconos de «Emoji One».
- Permitir el uso de tamaños de emoticonos personalizados.

### KHTML

- Corregir una fuga de memoria potencial notificada por Coverity y simplificar el código.
- El número de capas está determinado por el número de valores separados por comas en la propiedad «background-image».
- Se ha corregido el análisis de la posición de fondo en la declaración abreviada.
- No crear una nueva «fontFace» si no existe una fuente válida.

### KIconThemes

- Hacer que «KIconThemes» no dependa de «Oxígeno» (fallo 360664).
- Concepto de estado seleccionado para los iconos.
- Usar los colores del sistema en los iconos monocromáticos.

### KInit

- Se ha corregido la carrera en la que el archivo que contiene la cookie de X11 tiene los permisos incorrectos durante cierto tiempo.
- Se han corregido los permisos de «/tmp/xauth-xxx-_y».

### KIO

- Mostrar un mensaje de error más claro cuando se indica en «KRun(URL)» una URL sin esquema (error 363337).
- Añadir «KProtocolInfo::archiveMimetypes()».
- Usar el modo de icono seleccionado en la barra lateral del diálogo de abrir archivo.
- kshorturifilter: Se ha corregido una regresión con «mailto:», que no se añadía cuando no había un programa de correo instalado.

### KConfigWidgets

- Definir el indicador de «diálogo» correcto para el diálogo con la barra de avance.

### KNewStuff

- No inicializar «KNS3::DownloadManager» con categorías incorrectas.
- Extender la API pública de «KNS3::Entry».

### KNotification

- Usar «QUrl::fromUserInput» para construir URL de sonido (error 337276).

### KNotifyConfig

- Usar «QUrl::fromUserInput» para construir URL de sonido (error 337276).

### KService

- Se han corregido las aplicaciones asociadas a los tipos MIME con contienen caracteres en mayúsculas.
- Convertir a minúsculas la clave de búsqueda de tipos MIME para hacerla indiferente al uso de mayúsculas y minúsculas.
- Corregir las notificaciones de «ksycoca» cuando todavía no existe la base de datos.

### KTextEditor

- Corregir codificación por omisión a UTF-8 (error 362604).
- Se ha corregido la configurabilidad del color de «Error» del estilo predeterminado.
- Buscar y sustituir: Se ha corregido el color de fondo al sustituir (regresión introducida en v5.22) (fallo 363441).
- Nuevo esquema de color «Brisa oscuro», consulte https://kate-editor.org/?post=3745.
- KateUndoManager::setUndoRedoCursorOfLastGroup(): Pasar «Cursor» como una referencia constante.
- sql-postgresql.xml ha mejorado el resaltado de sintaxis al ignorar cuerpos de funciones multilínea.
- Añadir resaltado de sintaxis para Elixir y Kotlin.
- Resaltado de sintaxis de VHDL en ktexteditor: Se permite el uso de funciones dentro de sentencias de arquitectura.
- Modo VI: No fallar cuando se proporciona un intervalo para una orden no existente (fallo 360418).
- Eliminar correctamente los caracteres compuestos cuando se usan configuraciones regionales índicas.

### KUnitConversion

- Corregir la descarga de tipos de cambio de moneda (error 345750).

### Framework KWallet

- Migración de KWalletd: Corregir el manejo de errores; evitar que se produzca la migración en cada arranque.

### KWayland

- [cliente] No comprobar la versión de recursos para «PlasmaWindow».
- Introducir un evento de estado inicial en el protocolo de ventanas de Plasma.
- [servidor] Disparar un error si una petición efímera trata de convertirse en padre de sí misma.
- [servidor] Manejar correctamente el caso en el que una PlasmaWindow no está mapeada antes de que el cliente la enlace.
- [servidor] Manejar correctamente el destructor en «SlideInterface».
- Añadir soporte para eventos táctiles en el protocolo «fakeinput» y la interfaz.
- [servidor] Estandarizar el manejo de peticiones del destructor de los recursos.
- Implementar las interfaces «wl_text_input» y «zwp_text_input_v2».
- [servidor] Impedir un doble borrado de recursos a los que recurrir en «SurfaceInterface».
- [servidor] Se ha añadido la comprobación de puntero nulo a recurso en «ShellSurfaceInterface».
- [servidor] Comparar «ClientConnection» en lugar de «wl_client» en «SeatInterface».
- [servidor] Se ha mejorado la respuesta a la desconexión de clientes.
- servidor/plasmawindowmanagement_interface.cpp: Se ha corregido la advertencia «-Wreorder».
- [cliente] Se ha añadido un puntero de contexto a las conexiones en «PlasmaWindowModel».
- Numerosas correcciones relacionadas con la destrucción.

### KWidgetsAddons

- Usar el efecto de icono seleccionado para la página actual de «KPageView».

### KWindowSystem

- [plataforma xcb] Respetar la solicitud de tamaño de icono (fallo 362324).

### KXMLGUI

- Al pulsar la barra de menú de una aplicación con el botón derecho ya se permite que se ignore.

### NetworkManagerQt

- Revertir «descartar el uso de WiMAX para NM 1.2.0+», ya que rompe la ABI

### Iconos de Oxígeno

- Sincronizar los iconos meteorológicos con Brisa.
- Se han añadido iconos de actualización.

### Framework de Plasma

- Añadir soporte para «cantata» a la bandeja del sistema (fallo 363784).
- Estado de «seleccionado» para «Plasma::Svg» e «IconItem».
- DaysModel: Reiniciar «m_agendaNeedsUpdate» cuando el complemento envía nuevos eventos.
- Actualizar los iconos «audio» y «network» para que tengan mejor contraste (fallo 356082).
- Marcar como obsoleto «downloadPath(const QString &amp;file)» en favor de «downloadPath()».
- [miniatura de icono] Solicitar el tamaño preferido del icono (fallo 362324).
- Los plasmoides pueden saber ahora si los widgets han sido bloqueados por el usuario o por restricciones de «sysadmin».
- [ContainmentInterface] No intentar mostrar un menú emergente con un «QMenu» vacío.
- Usar «SAX» como sustituto del hoja de estilos de «Plasma::Svg».
- [DialogShadows] Mantener en caché el acceso a «QX11Info::display()».
- Restaurar los iconos del tema Aire de Plasma, de KDE4.
- Volver a cargar el esquema de color seleccionado al cambiar los colores.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
