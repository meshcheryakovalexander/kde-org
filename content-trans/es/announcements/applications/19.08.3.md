---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDE lanza las Aplicaciones 19.08.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE lanza las Aplicaciones 19.08.3
version: 19.08.3
---
{{% i18n_date %}}

KDE ha lanzado hoy la tercera actualización de estabilización para las <a href='../19.08.0'>Aplicaciones de KDE 19.08</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 12 correcciones de errores registradas, se incluyen mejoras en Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- En el editor de vídeo Kdenlive, las composiciones ya no desaparecen cuando se vuelve a abrir un proyecto con pistas bloqueadas.
- El visor de anotaciones de Okular muestra ahora la hora de creación en la zona horaria local en lugar de en UTC.
- El control del teclado se ha mejorado en la utilidad de capturas de pantalla Spectacle.
