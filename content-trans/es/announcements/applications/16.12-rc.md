---
aliases:
- ../announce-applications-16.12-rc
date: 2016-12-02
description: KDE lanza la versión candidata de las Aplicaciones 16.12.
layout: application
release: applications-16.11.90
title: KDE lanza la candidata a versión final para las Aplicaciones 16.12
---
Hoy, 2 de diciembre de 2016, KDE ha lanzado la candidata a versión final de las nuevas Aplicaciones. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Consulte las <a href='https://community.kde.org/Applications/16.12_Release_Notes'>notas de lanzamiento de la comunidad</a> para obtener información sobre nuevos paquetes que ahora se basan en KF5 y sobre los problemas conocidos. Se realizará un anuncio más completo para la versión final.

Los lanzamientos de las Aplicaciones de KDE 16.12 necesitan una prueba exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son críticos para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera temprana de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la versión candidata <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.
