---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE lanza las Aplicaciones 19.04.
layout: application
release: applications-19.04.0
title: KDE lanza las Aplicaciones de KDE 19.04.0
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

La comunidad KDE se complace al anunciar el lanzamiento de las Aplicaciones de KDE 19.04.

Nuestra comunidad trabaja continuamente en la mejora del software incluido en nuestra serie de Aplicaciones de KDE. Además de nuevas funcionalidades, hemos mejorado el diseño, la usabilidad y la estabilidad de todas nuestras utilidades, juegos y herramientas creativas. Nuestro objetivo es simplificar su experiencia haciendo que el software de KDE sea más agradable de usar. Esperamos que le gusten todas las nuevas mejoras y las correcciones de errores que encontrará en 19.04.

## Novedades de las Aplicaciones de KDE 19.04

Se han resuelto más de 150 fallos. Estas correcciones vuelven a implementar funciones desactivadas, normalizan accesos rápidos de teclado y resuelven cuelgues, haciendo que las Aplicaciones de KDE sean más amigables y que le permitan ser más productivo.

### Gestión de archivos

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> es el gestor de archivos de KDE. También se conecta a servicios de red, como servidores SSH, FTP y Samba, además de incluir herramientas avanzadas para encontrar y organizar sus datos.

Nuevas funcionalidades:

+ Hemos expandido el uso de miniaturas, de modo que Dolphin puede mostrar ahora miniaturas de varios tipos nuevos: archivos de <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a>, archivos <a href='https://phabricator.kde.org/D18738'>eBook .epub y .fb2</a>, archivos de <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a> y archivos <a href='https://phabricator.kde.org/D19679'>PCX</a>. De forma adicional, las miniaturas de los archivos de texto muestran ahora <a href='https://phabricator.kde.org/D19432'>resaltado de sintaxis</a> para el texto que incluye la miniatura. </li>
+ Ahora puede escoger <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>qué panel de vista dividida se cierra</a> al pulsar el botón «Cerrar vista dividida». </li>
+ Esta versión de Dolphin introduce <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>posiciones de pestañas más inteligentes</a>. Al abrir una carpeta en una nueva pestaña, dicha pestaña se situará ahora inmediatamente a la derecha de la actual, en lugar de aparecer siempre al final de la barra de pestañas. </li>
+ <a href='https://phabricator.kde.org/D16872'>Etiquetar elementos</a> es ahora mucho más práctico, ya que puede añadir y eliminar etiquetas usando el menú de contexto. </li>
+ Hemos <a href='https://phabricator.kde.org/D18697'>mejorado los parámetros de ordenación predeterminados</a> para algunas carpetas de uso común. Por omisión, la carpeta «Descargas» se ordena ahora por fecha con agrupación activada, mientras que la vista de «Documentos recientes» (a la que puede acceder explorando «recentdocuments:/») se ordena por fecha con una vista en forma de lista. </li>

Las correcciones de errores incluyen:

+ Si usa una versión moderna del protocolo SMB, ahora puede <a href='https://phabricator.kde.org/D16299'>descubrir recursos compartidos de Samba</a> en máquinas Mac y Linux. </li>
+ <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>La reorganización de elementos del panel «Lugares»</a> vuelve a funcionar correctamente cuando algunos elementos están ocultos. </li>
+ Tras abrir una nueva pestaña en Dolphin, la vista que contiene gana el <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>foco del teclado</a> de forma automática. </li>
+ Dolphin le muestra una advertencia ahora si intenta salir mientras el <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>panel del terminal está abierto</a> con un programa en ejecución. </li>
+ Hemos corregido muchas fugas de memoria, mejorando el rendimiento general de Dolphin.</li>

<a href='https://cgit.kde.org/audiocd-kio.git/'>AudioCD-KIO</a> permite que otras aplicaciones de KDE puedan leer sonido de CD y convertirlo automáticamente a otros formatos.

+ AudioCD-KIO permite ahora ripear en formato <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ Hemos conseguido que el <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>texto de información del CD</a> sea realmente transparente para su visualización. </li>

### Edición de vídeo

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

Esta es una versión relevante del editor de vídeo de KDE. El código principal de <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> se ha reescrito de forma exhaustiva y más del 60%% de sus interioridades ha cambiado, mejorando su arquitectura en general.

Las mejoras incluyen:

+ La línea de tiempo se ha reescrito para que use QML.
+ Al poner un clip en la línea de tiempo, el sonido y el vídeo siempre van a pistas separadas.
+ La línea de tiempo permite ahora usar navegación mediante el teclado: los clips, las composiciones y los fotogramas clave se pueden mover con el teclado. Además, la altura de las pistas se puede ajustar.
+ En esta versión de Kdenlive, la grabación de sonido en la pista contiene una nueva funcionalidad de <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>voz en off</a>.
+ Hemos mejorado copiar y pegar: funciona entre ventanas de proyectos diferentes. La gestión de clips también se ha mejorado, por lo que ahora puede borrar clips individualmente.
+ La versión 19.04 contempla la vuelta del uso de pantallas de monitor BlackMagic y también existen nuevas guías de preajustes en el monitor.
+ Hemos mejorado el manejo de fotogramas clave, dándoles un aspecto y un flujo de trabajo más consistente. El titulador también se ha mejorado haciendo que los botones de alineación se ajusten a zonas seguras, añadiendo guías y colores de fondo configurables y mostrando los elementos que faltaban.
+ Hemos corregido un error de corrupción de la línea de tiempo que situaba incorrectamente o perdía clips que se activaban al mover un grupo de clips.
+ Hemos corregido un error en imágenes JPG que las mostraba como pantallas en blanco en Windows. También hemos corregido los errores que afectaban a la captura de pantalla en Windows.
+ Además de todo lo anterior, hemos añadido muchas mejoras menores de usabilidad que harán que Kdenlive sea más fácil de usar y más fluido.

### Oficina

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/okular/'>Okular</a> es el visor de documentos multipropósito de KDE. Es ideal para leer y anotar PDF. También puede abrir archivos PDF (tal y como los usan LibreOffice y OpenOffice), libros electrónicos publicados como archivos ePub, los archivos de cómics más populares y archivos PostScript, entre otros muchos.

Las mejoras incluyen:

+ Para ayudarle a asegurar un ajuste completo de sus documentos, hemos añadido <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>opciones de escalado</a> al diálogo de impresión de Okular.
+ Okular permite ahora ver y verificar <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>firmas digitales</a> en archivos PDF.
+ Gracias a la integración mejorada entre aplicaciones, Okular permite ahora editar documentos LaTeX en <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ La implementación mejorada de <a href='https://phabricator.kde.org/D18118'>navegación en pantallas táctiles</a> significa que podrá moverse atrás y adelante usando una pantalla táctil cuando esté en el modo de presentación.
+ Los usuarios que prefieran manejar documentos desde la línea de órdenes podrán realizar <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>búsquedas de texto</a> inteligentes con los nuevos indicadores de la línea de órdenes que permiten abrir un documento y resaltar todas las coincidencias de un determinado trozo de texto.
+ Okular muestra ahora de forma correcta los enlaces en los <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>documentos Markdown</a> que se extienden a lo largo de más de una línea.
+ Las <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>herramientas de recorte</a> disponen de nuevos iconos más elaborados.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

<a href='https://kde.org/applications/internet/kmail/'>KMail</a> es el cliente de correo electrónico de KDE que protege su privacidad. Forma parte de la <a href='https://kde.org/applications/office/kontact/'>suite de trabajo en grupo Kontact</a> y permite usar cualquier sistema de correo electrónico y organizar los mensajes en un buzón de entrada virtual compartido o en cuantas separadas (según su elección). Permite todo tipo de cifrado de mensajes y firma digital, además de dejarle compartir datos, como contactos, fechas de reuniones e información de viajes, con otras aplicaciones de Kontact.

Las mejoras incluyen:

+ Además, gramática. Esta versión de KMail integra el uso de «languagetools» (un verificador de gramática) y de «grammalecte» (un verificador gramatical para francés).
+ Los números de teléfono presentes en los mensajes se detectan y se pueden marcar directamente usando <a href='https://community.kde.org/KDEConnect'>KDE Connect</a>.
+ KMail dispone ahora de una opción para <a href='https://phabricator.kde.org/D19189'>arrancar directamente en la bandeja del sistema</a> sin abrir la ventana principal.
+ Hemos mejorado el uso del complemento de Markdown.
+ La obtención de mensajes usando IMAP ya no se atasca cuando falla el inicio de sesión.
+ También hemos realizado numerosas correcciones en el motor Akonadi de KMail para mejorar la fiabilidad y el rendimiento.

<a href='https://kde.org/applications/office/korganizer/'>KOrganizer</a> es el componente de calendarios de Kontact que gestiona sus eventos.

+ Los eventos recurrentes de <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a> se vuelven a sincronizar correctamente.
+ La ventana de recordatorios de eventos recuerda ahora <a href='https://phabricator.kde.org/D16247'>mostrarse en todos los escritorios</a>.
+ Hemos modernizado el aspecto de la <a href='https://phabricator.kde.org/T9420'>vista de eventos</a>.

<a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> es el nuevo y flamante asistente de Kontact que le ayudará a obtener su ubicación y le aconsejará durante el camino.

- Contiene un nuevo extractor genérico para billetes RCT2 (que usan algunas compañías ferroviarias, como DSB, ÖBB, SBB y NS).
- Se han mejorado notablemente la detección y la desambiguación de nombres de aeropuertos.
- Hemos añadido nuevos extractores personalizados para proveedores no permitidos anteriormente (como BCD Travel y NH Group), además de mejorar las variaciones de formato y de idioma de los proveedores permitidos (como SNCF, Easyjet, Booking.com y Hertz).

### Desarrollo

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

<a href='https://kde.org/applications/utilities/kate/'>Kate</a> es el editor de texto cargado de funcionalidades de KDE, ideal para la programación gracias a sus características, como el uso de pestañas, el modo de vista dividida, el resaltado de sintaxis, un panel de terminal integrado, la terminación automática de palabras, la búsqueda y sustitución mediante expresiones regulares, y muchas más mediante su flexible infraestructura de complementos.

Las mejoras incluyen:

- Kate puede mostrar ahora todos los caracteres invisibles de espacios en blanco, en lugar de solo algunos.
- Se puede activar y desactivar fácilmente el ajuste estático de palabras por documento usando su propia entrada de menú, sin necesidad de tener que cambiar el ajuste global por omisión.
- Los menús de contexto para archivos y pestañas incluyen ahora un buen número de nuevas y útiles acciones, como cambiar el nombre, borrar, abrir la carpeta contenedora, copiar la ruta del archivo, comparar (con otro archivo abierto) y mostrar las propiedades.
- Esta versión de Kate contiene más complementos activados por omisión, incluyendo la popular y útil función de terminal en línea.
- Al salir, Kate ya no le solicita reconocer los archivos que han sido modificados en disco por otros procesos, como un cambio de control de código fuente.
- La vista en árbol de los complementos muestra ahora correctamente todos los elementos del menú para las entradas «git» que usan diéresis en sus nombres.
- Al abrir múltiples archivos usando la línea de órdenes, los archivos se abren en nuevas pestañas en el mismo orden que se especifica en la línea de órdenes.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

<a href='https://kde.org/applications/system/konsole/'>Konsole</a> es el emulador de terminal de KDE. Permite el uso de pestañas, fondos translúcidos, modo de vista dividida, esquemas de color personalizables, accesos rápidos de teclado, marcadores de directorios y de SSH, entre otras muchas funcionalidades.

Las mejoras incluyen:

+ La gestión de pestañas ha recibido un gran número de mejoras que le ayudarán a ser más productivo. Puede crear nuevas pestañas haciendo clic con el botón central en las partes vacías de la barra de pestañas, y también existe una opción que le permite cerrar pestañas haciendo clic con el botón central sobre ellas. Los botones de cierre se muestran en las pestañas de forma predeterminada, mientras que los iconos solo se mostrarán cuando use un perfil con un icono personalizado. Finalmente, aunque no menos importante, el acceso rápido Ctrl+Tab le permite cambiar rápidamente entre las pestañas actual y anterior.
+ El diálogo de edición de perfil ha recibido una enorme <a href='https://phabricator.kde.org/D17244'>revisión de la interfaz de usuario</a>.
+ El esquema de color Brisa se usa ahora como esquema por omisión de Konsole, y se ha mejorado su contraste y consistencia con el tema Brisa global del sistema.
+ Se han resuelto los problemas al mostrar texto en negrita.
+ Konsole muestra ahora correctamente el cursor de estilo subrayado.
+ Hemos mejorado la visualización de los <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>caracteres de cuadros y de líneas</a>, así como de los <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>caracteres de emojis</a>.
+ Los accesos rápidos de cambio de perfil cambian ahora el perfil de la pestaña actual en lugar de abrir una nueva pestaña con el otro perfil.
+ Las pestañas inactivas que reciben una notificación y cambian el color del texto de su título vuelven a reiniciar ahora este color al del color de texto de título normal cuando se activan dichas pestañas.
+ La opción «Variar el color del fondo de cada pestaña» funciona ahora cuando el color de fondo base es muy oscuro o negro.

<a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> es un sistema de traducción asistido por ordenador que se centra en la productividad y en garantizar la calidad. Está pensado para la traducción de software, aunque también integra herramientas de conversión externas para traducir documentos de oficina.

Las mejoras incluyen:

- Lokalize permite ahora ver la fuente de la traducción con un editor personalizado.
- Se ha mejorado la ubicación de los paneles de anclaje y el modo de guardar y de restaurar las preferencias.
- La posición en los archivos .po se preserva ahora al filtrar mensajes.
- Se han corregido varios fallos en la interfaz de usuario (comentarios del desarrollador, conmutación de expresiones regulares en sustituciones en masa, contador de mensajes no preparados vacíos, etc.).

### Utilidades

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> es un avanzado visor y organizador de imágenes con herramientas de edición intuitivas y fáciles de usar.

Las mejoras incluyen:

+ La versión de Gwenview que de distribuye con las Aplicaciones 19.04 incluye una completa <a href='https://phabricator.kde.org/D13901'>integración con pantallas táctiles</a>, que permite usar gestos para deslizar el dedo, ampliar, hacer barridos y más.
+ Otra mejora añadida a Gwenview es la completa <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>integración con pantallas de alta densidad de puntos</a>, lo que hace que las imágenes se vean mejor en pantallas de alta resolución.
+ El uso mejorado de los <a href='https://phabricator.kde.org/D14583'>botones atrás y adelante del ratón</a> le permite navegar entre imágenes pulsando dichos botones.
+ Ahora es posible usar Gwenview para abrir archivos de imágenes creados con <a href='https://krita.org/'>Krita</a>, la herramienta de dibujo digital que todo el mundo prefiere.
+ Gwenview puede usar ahora grandes <a href='https://phabricator.kde.org/D6083'>miniaturas de 512 píxeles</a>, lo que le permite realizar vistas previas de las imágenes más fácilmente.
+ Gwenview usa ahora el <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>acceso rápido de teclado Ctrl+L estándar</a> para mover el foco al campo de la URL.
+ Ahora puede usar la <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>funcionalidad de filtrar por nombre</a> mediante el acceso rápido Ctrl+I, como en Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> es la aplicación para realizar capturas de pantalla de Plasma. Puede capturar escritorios completos que se extienden a lo largo de varias pantallas, pantallas individuales, ventanas, secciones de ventanas o regiones personalizadas usando la función de selección de área rectangular.

Las mejoras incluyen:

+ Se ha extendido el modo de región rectangular con varias opciones nuevas. Se puede configurar para <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>aceptar automáticamente</a> el cuadro arrastrado en lugar de solicitarle que lo ajuste primero. También existe una nueva opción para recordar el cuadro de selección de la <a href='https://phabricator.kde.org/D19117'>región rectangular</a> actual, aunque solo hasta que se cierra el programa.
+ Es posible configurar lo que ocurre cuando se pulsa el acceso rápido de captura de pantalla <a href='https://phabricator.kde.org/T9855'>mientras Spectacle está en funcionamiento</a>.
+ Spectacle le permite cambiar el <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>nivel de compresión</a> para los formatos de imagen con pérdida de calidad.
+ Las preferencias para guardar le muestran cómo quedará el <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>nombre de archivo de la captura de pantalla</a>. También puede configurar fácilmente la <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>plantilla del nombre de archivo</a> según sus preferencias haciendo clic sobre los parámetros de sustitución.
+ Spectacle ya no muestra las opciones «Pantalla completa (todos los monitores)» ni «Pantalla actual» si su equipo solo dispone de una única pantalla.
+ El texto de ayuda para el modo de región rectangular se muestra ahora en el centro de la pantalla primaria en lugar de dividido entre las pantallas.
+ Al ejecutarse en Wayland, Spectacle incluye solo las características que funcionan bien.

### Juegos y aplicaciones educativas

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

Nuestra serie de aplicaciones incluye numerosos <a href='https://games.kde.org/'>juegos</a> y <a href='https://edu.kde.org/'>aplicaciones educativas</a>.

<a href='https://kde.org/applications/education/kmplot'>KmPlot</a> es un trazador de funciones matemáticas. Posee un potente analizador sintáctico integrado. Los gráficos se pueden colorear y el visor es escalable, permitiéndole ajustar el nivel de ampliación según sus necesidades. El usuario puede representar varias funciones simultáneamente y combinarlas para crear nuevas funciones.

+ Ahora es posible ampliar manteniendo pulsada la tecla Ctrl y usando la <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>rueda del ratón</a>.
+ Esta versión de KmPlot introduce la opción de <a href='https://phabricator.kde.org/D17626'>vista previa de la impresión</a>.
+ Ahora es posible copiar el valor raíz o el par (x,y) en el <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>portapapeles</a>.

<a href='https://kde.org/applications/games/kolf/'>Kolf</a> es un juego de minigolf.

+ Se ha restaurado el <a href='https://phabricator.kde.org/D16978'>uso de sonido</a>.
+ Kolf se ha portado con éxito de kdelibs4.
