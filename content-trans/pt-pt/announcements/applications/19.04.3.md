---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: O KDE Lança as Aplicações do KDE 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: O KDE Lança as Aplicações do KDE 19.04.3
version: 19.04.3
---
{{% i18n_date %}}

Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../19.04.0'>Aplicações do KDE 19.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 60 correcções de erros registadas incluem as melhorias no Kontact, no Ark, no Cantor, no Juk, no K3b, no Kdenlive, no KTouch, no Okular, no Umbrello, entre outros.

As melhorias incluem:

- O Konqueror e o Kontact já não estoiram mais à saída com o QtWebEngine 5.13
- O corte de grupos com composições não estoira mais o editor de vídeos Kdenlive
- O importador de Python no desenhador de UML Umbrello agora lidar com parâmetros com argumentos predefinidos
