---
aliases:
- ../announce-applications-19.08-beta
date: 2019-07-19
description: O KDE Lança as Aplicações do KDE 19.08 Beta.
layout: application
release: applications-19.07.80
title: O KDE Lança a Primeira Versão 19.08 Beta das Aplicações
version_number: 19.07.80
version_text: 19.08 Beta
---
19 de Julho de 2019. Hoje o KDE lançou a primeira das versões beta das novas Aplicações do KDE. Com as dependências e as funcionalidades estabilizadas, o foco da equipa do KDE é agora a correcção de erros e mais algumas rectificações.

Veja mais informações nas <a href='https://community.kde.org/Applications/19.08_Release_Notes'>notas de lançamento da comunidade</a> sobre novos pacotes e problemas conhecidos. Será disponibilizado um anúncio mais completo para a versão final.

As versões Aplicações do KDE 19.08 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do utilizador. Os utilizadores actuais são críticos para manter a alta qualidade do KDE, dado que os programadores não podem simplesmente testar todas as configurações possíveis. Contamos consigo para nos ajudar a encontrar erros antecipadamente, para que possam ser rectificados antes da versão final. Por favor, pense em juntar-se à equipa, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
