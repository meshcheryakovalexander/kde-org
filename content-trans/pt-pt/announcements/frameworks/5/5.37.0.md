---
aliases:
- ../../kde-frameworks-5.37.0
date: 2017-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nova plataforma: kirigami, um conjunto de 'plugins' em QtQuick para construir interfaces de utilizador baseadas nas linhas-mestras de usabilidade do KDE

### Ícones do Brisa

- actualização das cores do .h e .h++ (erro 376680)
- remoção do pequeno ícone monocromático do KTorrent (erro 381370)
- os favoritos são um ícone de acção e não um ícone de pasta (erro 381383)
- actualização do 'utilities-system-monitor' (erro 381420)

### Módulos Extra do CMake

- Adição do '--gradle' ao 'androiddeployqt'
- Correcção da instalação do alvo APK
- Correcção do uso do 'query_qmake': diferenças nas chamadas que esperam o 'qmake' ou não
- Adição de documentação da API para o KDE_INSTALL_USE_QT_SYS_PATHS no KDEInstallDirs
- Adição de um metainfo.yaml para tornar o ECM uma plataforma adequada
- Android: pesquisar pelos ficheiros QML na pasta de origem, não na pasta de instalação

### KActivities

- emissão do 'runningActivityListChanged' ao criar a actividade

### Ferramentas de Doxygen do KDE

- Escape do HTML na pesquisa

### KArchive

- Adição dos ficheiros do Conan, como primeira experiência para o suporte do Conan

### KConfig

- Possibilidade de compilar o KConfig sem o Qt5Gui
- Atalhos-padrão: uso do Ctrl+PageUp/PageDown para a página anterior/seguinte

### KCoreAddons

- Remoção da declaração não usada do init() no K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY_SKEL
- Nova API de 'spdx' para obter as expressões da licença do SPDX
- kdirwatch: Evitar um potencial estoiro se o 'd-ptr' for destruído antes do KDirWatch (erro 381583)
- Correcção da apresentação do 'formatDuration' com arredondamento (erro 382069)

### KDeclarative

- Correcção do 'plasmashell' na limpeza do QSG_RENDER_LOOP

### Suporte para a KDELibs 4

- Correcção da mensagem 'Deprecated hint for KUrl::path() is wrong on Windows' (Sugestão desactualizada do KUrl::path errada no Windows' (erro 382242)
- Actualização do 'kdelibs4support' para usar o suporte baseado no destino que é oferecido no kdewin
- Marcar os construtores também como obsoletos
- Sincronização com o KDE4Defaults.cmake do 'kdelibs'

### KDesignerPlugin

- Adição do suporte para o novo elemento 'kpasswordlineedit'

### KHTML

- Suporte também para SVG (erro 355872)

### KI18n

- Possibilidade de carregar os catálogos de 'i18n' de locais arbitrários
- Validação de que é gerado o destino do 'tsfiles'

### KIdleTime

- Só necessitar do Qt5X11Extras quando realmente for necessário

### KInit

- Uso da opção de funcionalidade adequada para incluir o kill(2)

### KIO

- Adição do novo método 'urlSelectionRequested' ao KUrlNavigator
- KUrlNavigator: exposição do KUrlNavigatorButton que recebeu um evento de largada
- Gravação temporária sem perguntar ao utilizador com uma mensagem Copiar/Cancelar
- Garantia que o KDirLister actualiza os itens cujo URL de destino foi alterado (erro 382341)
- Tornar as opções avançadas da janela "abrir com" colapsáveis e escondidas por omissão (erro 359233)

### KNewStuff

- Atribuição de um pai aos menus KMoreToolsMenuFactory
- Ao pedir da 'cache', comunicar todos os elementos de uma vez

### Plataforma KPackage

- O 'kpackagetool' pode agora gerar os dados do AppStream para um ficheiro
- adopção de um novo KAboutLicense::spdx

### KParts

- Reposição do URL no closeUrl()
- Adição de um modelo para uma aplicação simples baseada em KParts
- Eliminação do uso do KDE_DEFAULT_WINDOWFLAGS

### KTextEditor

- Tratamento do evento de roda com precisão na ampliação
- Adição de um modelo para um 'plugin' do KTextEditor
- cópia das permissões do ficheiro original na gravação de uma cópia (erro 377373)
- impedimento possível de estoiro no 'stringbuild' (erro 339627)
- correcção de problema com a adição do '* às linhas fora dos comentários (erro 360456)
- correcção da gravação como cópia, onde faltava a permissão de sobreposição do ficheiro de destino (erro 368145)
- Comando 'set-highlight': Juntar os argumentos com espaços
- correcção de estoiro na destruição da vista por causa da limpeza não-determinista dos objectos
- Emissão de sinais do contorno dos ícones quando não se carregar em nenhuma marca
- Correcção de estoiro no modo de introdução do VI (sequência: "o" "Esc" "O" "Esc" ".") (erro 377852)
- Uso do grupo com exclusão mútua no Tipo de Marcação Predefinido.

### KUnitConversion

- Marcar o MPa e o PSI como unidades comuns

### Plataforma da KWallet

- Uso do CMAKE_INSTALL_BINDIR para a geração de serviços do D-Bus

### KWayland

- Destruição de todos os objectos do Kwayland criados no registo quando este for destruído
- Emissão do 'connectionDied' se o QPA for destruído
- [cliente] Seguir todas as ConnectionThreads criadas e adição de uma API para aceder a elas
- [servidor] Envio do abandono da introdução de texto se a superfície em foco ficar sem associação
- [servidor] Envio de abandono do ponteiro se a superfície em foco ficar sem associação
- [cliente] Seguir adequadamente a 'enteredSurface' no teclado
- [servidor] Enviar um abandono do teclado quando o cliente destruir a superfície em foco (erro 382280)
- verificação da validade do Buffer (erro 381953)

### KWidgetsAddons

- Extracção do elemento de senha =&gt; nova classe KPasswordLineEdit
- Correcção do estoiro ao pesquisar com o suporte de acessibilidade activo (erro 374933)
- [KPageListViewDelegate] Passar o elemento do 'drawPrimitive' no 'drawFocus'

### KWindowSystem

- Remoção da dependência do cabeçalho no QWidget

### KXMLGUI

- Eliminação do uso do KDE_DEFAULT_WINDOWFLAGS

### NetworkManagerQt

- Adição do suporte para o 'ipv*.route-metric'
- Correcção dos enumerados NM_SETTING_WIRELESS_POWERSAVE_XPTO não definidos (erro 382051)

### Plataforma do Plasma

- [Interface de Contentores] emitir sempre o 'contextualActionsAboutToShow' no contentor
- Tratar das legendas do Button/ToolButton como texto simples
- Não efectuar correcções específicas do Wayland se estiver no X (erro 381130)
- Adição do KF5WindowSystem à interface ligada
- Declaração do AppManager.js como biblioteca 'pragma'
- [PlasmaComponents] Remoção do Config.js
- uso de texto simples por omissão nas legendas
- Carregamento das traduções dos ficheiros KPackage se forem fornecidas (erro 374825)
- [Menu do PlasmaComponents] Não estoirar com uma acção nula
- [Janela do Plasma] Correcção das condições das opções
- actualização do ícone de bandeja do Akregator (erro 379861)
- [Interface de Contentores] Manter o contentor no RequiresAttentionStatus enquanto o menu de contexto for aberto (erro 351823)
- Correcção do tratamento de chaves de disposição na barra de páginas em RTL (erro 379894)

### Sonnet

- Possibilidade de compilar o Sonnet sem o Qt5Widgets
- cmake: modificação do FindHUNSPELL.cmake para usar o 'pkg-config'

### Realce de Sintaxe

- Possibilidade de compilar o KSyntaxHighlighter sem o Qt5Gui
- Adição do suporte de compilação multi-plataforma da indexação do realce
- Temas: Remoção de todos os meta-dados não usados (licença, autor, apenas para leitura)
- Temas: Remoção dos campos de licença e autor
- Temas: Derivação da opção 'apenas-para-leitura' do ficheiro no disco
- Adição do realce de sintaxe para a linguagem de modelação de dados YANG
- PHP: Adição das palavras-chave do PHP 7 (erro 356383)
- PHP: Limpeza da informação do PHP 5
- correcção do Gnuplot: tornar os espaços iniciais/finais fatais
- correcção da detecção do 'else if' por necessidade de mudar de contexto e adicionar uma regra extra
- verificações de indexação para os espaços iniciais/finais no realce de sintaxe do XML
- Doxygen: Adição do realce de sintaxe do Doxyfile
- adição de tipos-padrão em falta no realce de C e actualização para o C11 (erro 367798)
- Q_PI D =&gt; Q_PID
- PHP: Melhoria no realce de variáveis entre chavetas nas aspas duplas (erro 382527)
- Adição do realce de PowerShell
- Haskell: Adição da extensão de ficheiro '.hs-boot' (módulo de arranque) (erro 354629)
- Correcção do replaceCaptures() para lidar com mais de 9 capturas
- Ruby: Uso do WordDetect em vez do StringDetect para corresponder a palavras inteiras
- Correcção do realce incorrecto para o BEGIN e END em palavras como "EXTENDED" (erro 350709)
- PHP: Remoção do mime_content_type() da lista de funções obsoletas (erro 371973)
- Adição da extensão de ficheiro/tipo MIME XBEL ao realce de sintaxe de XML (erro 374573)
- Bash: Correcção do realce incorrecto nas opções dos comandos (erro 375245)
- Perl: Correcção do realce do 'heredoc' com os espaços iniciais no separador (erro 379298)
- Actualização do ficheiro de realce de sintaxe SQL (Oracle) (erro 368755)
- C++: Correcção do '-' como não fazendo parte do Texto UDL (erro 380408)
- C++: especificidades do formato do 'printf': adição do 'n' e 'p' e remoção do 'P' (erro 380409)
- C++: Correcção do valor em carácter para ter a cor das cadeias de caracteres (erro 380489)
- VHDL: Correcção de erro de realce ao usar parêntesis rectos e atributos (erro 368897)
- Realce do zsh: Correcção de expressão matemática numa expressão em sub-texto (erro 380229)
- Realce de JavaScript: Adição do suporte para a extensão de XML E4X (erro 373713)
- Remoção da regra de extensão "*.conf"
- Sintaxe do Pug/Jade

### ThreadWeaver

- Adição de exportação em falta para o QueueSignals

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
