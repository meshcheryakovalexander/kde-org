---
aliases:
- ../../kde-frameworks-5.77.0
date: 2020-12-12
layout: framework
libCount: 83
qtversion: 5.13
---
### Attica

* Correcção de estoiro no carregamento de fornecedores ao verificar o ponteiro de resposta antes da remoção da referência (erro 427974)

### Baloo

* [DocumentUrlDB] Eliminação do item da lista de filhos da BD se vazio
* Adição do tipo de documento Presentation para as apresentações e modelos em OpenXML do Office
* [MetaDataMover] Correcção da pesquisa do ID do documento-pai
* [DocumentUrlDB] Adição de método para mudanças de nome e local triviais
* [MetaDataMover] Tornar as mudanças de nome uma operação apenas com a BD
* [Document] Adição do ID do documento-pai e preenchimento do mesmo
* Substituição de chamadas directas ao 'syslog' por mensagens de registo por categorias

### Ícones do Brisa

* Adição das variantes do 'text-field': -frameless (-> text-field), -framed
* Adição de ligação simbólica do nome para os ícones 'input-*'
* Adição de ícone para o tipo de letra True Type em XML
* Adição do 'add-subtitle'
* Mudança do ícone de MathML para usar uma fórmula e usar o tipo MIME específico
* Adição de ícone para as imagens de discos do QEMU e de SquashFS
* Adição do ícone de acção 'edit-move'
* Adição de ícone para estoiros de aplicações
* Adição de um conjunto de tipos MIME de legendas
* Remoção de borrão desnecessário no ícone do Kontrast

### Módulos Extra do CMake

* Correcção da extracção da categoria dos ficheiros 'desktop'
* Definição de variável da pasta de instalação para os modelos de ficheiros
* Adição de geração de meta-dados rápidos para as compilações de Android
* (Qt)WaylandScanner: Marcação adequada dos ficheiros como SKIP_AUTOMOC

### KActivitiesStats

* ResultModel: exposição do MimeType do recurso
* Adição de filtragem de eventos para os ficheiros e pastas (erro 428085)

### KCalendarCore

* Correcção do responsável de manutenção, que deveria ser o Allen, e não eu :)
* Adição do suporte para a propriedade CONFERENCE
* Adição do método de conveniência 'alarmsTo' ao Calendar
* Verificação que as recorrências por dia não antecedem o 'dtStart'

### KCMUtils

* Remoção de truque que prejudicava os KCM's multi-níveis no modo de ícones

### KConfig

* Correcção do KConfigGroup::copyTo com o KConfigBase::Notify (erro 428771)

### KCoreAddons

* Evitar um estoiro quando a 'factory' está vazia (quando devolvemos um erro)
* KFormat: Adição de mais casos de datas-horas relativas
* Possibilidade de o KPluginFactory passar opcionalmente o KPluginMetaData aos 'plugins'

### KDAV

* Remoção do mesmo, dado criar demasiados erros

### KDeclarative

* Sincronização das margens do AbstractKCM para o SimpleKCM
* Remoção do texto de licença obsoleto
* Mudança da licença do ficheiro para LGPL-2.0-ou-posterior
* Mudança da licença do ficheiro para LGPL-2.0-ou-posterior
* Mudança da licença do ficheiro para LGPL-2.0-ou-posterior
* Mudança da licença do ficheiro para LGPL-2.0-ou-posterior
* Remodelação do KeySequenceItem (e utilitário) para usar o KeySequenceRecorder (erro 427730)

### KDESU

* Processamento adequado de aspas duplas escapadas
* Adição do suporte para o doas(1) do OpenBSD

### KFileMetaData

* Correcção de algumas fugas nas classes de extracção de OpenDocument e Office OpenXML
* Adição de diversos sub-tipos para os documentos OpenDocument e OpenXML

### KGlobalAccel

* Carregamento dos 'plugins' de interface 'kglobalacceld' compilados estaticamente

### Extensões da GUI do KDE

* Possibilitar a inibição dos atalhos actuar logo desde o início (erro 407395)
* Correcção de potencial estoiro no fim de testes de inibição do Wayland (erro 429267)
* CMake: Procurar o Qt5::GuiPrivate quando o suporte de Wayland está activo
* Adição do KeySequenceRecorder como base para o KKeySequenceWidget e o KeySequenceItem (erro 407395)

### KHolidays

* Correcção do arredondamento dos eventos da posição do Sol para menos de 30s antes da hora seguinte
* Impedimento de processar duas vezes cada ficheiro de feriados no defaultRegionCode()
* Cálculo das estações do ano astronómico apenas uma vez por cada ocorrência
* Correcção da pesquisa da região dos feriados para os códigos ISO 3166-2
* Possibilidade de copiar/mover um HolidayRegion
* Adição do suporte para calcular as horas de crepúsculo civis

### KIdleTime

* Carregamento dos 'plugins' de sondagem do sistema compilados estaticamente

### KImageFormats

* Não diminuir mais para 8 bits a profundidade de cor dos ficheiros PSD não comprimidos

### KIO

* Janela de Novo Ficheiro: permitir aceitar a criação da pasta antes de executar o 'stat' (erro 429838)
* Não libertar a tarefa DeleteJob
* KUrlNavBtn: possibilitar a abertura de sub-pastas do menu com o teclado (erro 428226)
* DropMenu: Uso de atalhos traduzidos
* [ExecutableFileOpenDialog] colocação do botão Cancelar em primeiro plano
* Área de Locais: realçar o local apenas quando é apresentado (erro 156678)
* Adição de propriedade para mostrar as acções do 'plugin' no submenu "Acções"
* Remoção de método recém-introduzido
* KIO::iconNameForUrl: resolução do ícone para os ficheiros remotos com base no nome (erro 429530)
* [kfilewidget] Uso de um novo atalho-padrão para "Criar uma Pasta"
* Remodelação do carregamento do menu de contexto e possibilidade de ajustar a escala
* RenameDialog: permitir a substituição quando os ficheiros são mais antigos (erro 236884)
* DropJob: uso do novo ícone 'edit-move' para 'Mover para Aqui'
* Correcção da geração do 'moc_predefs.h' quando a 'ccache' está activa para o -DCMAKE_CXX_COMPILER=ccache CMAKE_CXX_COMPILER_ARG1=g++
* Agora é obrigatório o Qt 5.13, por isso remove-se o 'ifdef'
* Migração do KComboBox para o QComboBox
* Correcção da documentação pedida por Méven Car @meven
* Remodelação de alguns ciclos com C++ moderno
* Limpeza de código inútil
* Remoção da verificação desnecessária se a chave existe
* Simplificação do código do RequiredNumberOfUrls
* Remoção de algumas linhas em branco
* Simplificação de código e modificação para maior consistência
* ioslaves: correcção das permissões de 'root' do remote:/
* KFileItem: o 'isWritable' usa o KProtocolManager para os ficheiros remotos
* Adição de substituto para anteceder as acções ao menu de "Acções"
* Uso de estilo de código moderno
* Não adicionar separadores desnecessários (erro 427830)
* Uso de um initializer_list com parêntesis em vez de um operador '<<'
* MkPathJob: remodelação de código compilado condicionalmente para melhorar a legibilidade

### Kirigami

* Fazer com que o GlobalDrawer defina a posição dos ToolBars/TabBars/DialogButtonBoxes
* propriedade anexada do 'inViewport'
* correcção do deslizamento do cabeçalho em ecrãs tácteis
* Remodelação do AbstractapplicationHeader com um novo conceito "ScrollIntention"
* no ecrã preencher sempre as âncoras para o pai
* [controls/BasicListItem]: Não ancorar a um item inexistente
* Não mostrar o texto do avatar com tamanho pequeno
* Remoção do # e @ do processo de extracção do avatar inicial
* Inicialização da propriedade no 'sizeGroup'
* uso da interacção do rato no 'isMobile' para testes mais fáceis
* Correcção dos iniciais/finais ao usar valores finais para a margem de separação inicial
* [controls/BasicListItem]: Adição das propriedades leading/trailing
* correcção do posicionamento da folha ao dimensionar o conteúdo
* Aplicação do comportamento antigo no modo amplo e não adicionar o 'topMargin' no FormLayout
* Correcção da disposição do formato para ecrãs pequenos
* Não pode usar um AbstractListItem num SwipeListItem
* Correcção do aviso 'não é possível atribuir um valor indefinido a um int' no OverlaySheet
* [overlaysheet]: Não fazer uma transição tremida quando a altura do conteúdo mudar
* [overlaysheet]: Animar as mudanças de altura
* Correcção do posicionamento da camada sobreposta
* definir sempre o índice ao carregar numa página
* Correcção do arrastamento do FAB no modo RTL
* Afinação da aparência dos separadores da lista (erro 428739)
* correcção das pegas das áreas no modo RTL
* Correcção do desenho dos contornos com o tamanho correcto no modo alternativo em 'software' (erro 427556)
* Não colocar um item de salvaguarda em 'software' fora dos limites do item de rectângulo sombreado
* Uso do fwidth() para suavizar no modo de baixa energia (erro 427553)
* Desenhar também uma cor de fundo no modo de baixa energia
* Activação do desenho transparente para o Shadowed(Border)Texture em baixa energia
* Não cancelar os componentes alfa dos rectângulos sombreados no modo de baixa energia
* Não usar um valor de suavização inferior ao desenhar a textura do ShadowedBorderTexture
* Remoção dos passos de "recorte" do rectângulo sombreado e dos 'shaders' relacionados
* Uso do nome.icone em vez de nomeIcone na documentação
* [Avatar] Fazer com que o ícone use um tamanho de ícones próximo do tamanho do texto
* [Avatar] Fazer com que as iniciais use mais espaço e melhoria do alinhamento vertical
* [Avatar] Exposição das propriedades 'sourceSize' e 'smooth' para tudo o que queira animar o tamanho
* [Avatar] Definição do tamanho original para evitar que as imagens fiquem borradas
* [Avatar] Definição apenas uma vez da cor principal
* [Avatar] Mudança do gradiente do fundo
* [Avatar] Mudança da espessura do contorno para 1px para corresponder às outras espessuras
* [Avatar] Colocar o 'padding', 'horizontalPadding' e 'verticalPadding' a funcionar
* [avatar]: Adição do gradiente às cores

### KItemModels

* KRearrangeColumnsProxyModel: só a coluna 0 tem filhas

### KMediaPlayer

* Instalação dos ficheiros de definição de tipos de serviços do leitor & motor com um tipo correspondente ao nome do ficheiro

### KNewStuff

* Correcção da desinstalação quando o item não está em 'cache'
* Quando for invocada a verificação de actualizações, esperam-se actualizações (erro 418082)
* Reutilização da janela dos QWidgets (erro 429302)
* Envolvência do bloco de compatibilidade no KNEWSTUFFCORE_BUILD_DEPRECATED_SINCE
* Não gravar na 'cache' estados intermédios
* Uso de enumerado para a descompressão em vez de valores de texto
* Correcção do desaparecimento demasiado prévio de itens da página actualizável (erro 427801)
* Adição do enumerado DetailsLoadedEvent ao novo sinal
* Remodelação da API de adopção (erro 417983)
* Correcção de alguns problemas com o URL antigo do fornecedor
* Remoção do item da 'cache' antes de inserir um novo (erro 424919)

### KNotification

* Não passar uma sugestão transitória (erro 422042)
* Correcção de erro de capitalização do cabeçalho do AppKit no macOS
* Não invocar acções de notificação inválidas
* Correcção do tratamento da memória no 'notifybysnore'

### Plataforma KPackage

* Eliminação do X-KDE-PluginInfo-Depends

### KParts

* Descontinuação do método embed() por falta de utilização
* Mudança do KParts para usar o KPluginMetaData em vez do KAboutData

### KQuickCharts

* Remodelação ao algoritmo de suavização de linhas
* Passagem da aplicação da interpolação para a fase de polimento
* Alinhamento adequado dos pontos centraais nos gráficos de linhas e ajuste dos mesmos à espessura da linha
* Adição de uma opção de marcação "suave" no exemplo do gráfico de linhas
* Garantia que as delegações de pontos do gráfico são devidamente limpas
* Mostrar também o nome na dica no exemplo da página do gráfico de linhas
* Documentação do LineChartAttached e correcção de erros ortográficos na documentação do LineChart
* Adição das propriedades 'name' e 'shortName' no LineChartAttached
* Documentação da propriedade 'pointDelegate' mais pormenorizada
* Remoção do membro 'previousValues' e correcção dos gráficos de linhas sobrepostos
* Uso do 'pointDelegate' no exemplo dos gráficos de linhas para mostrar valores à passagem
* Adição do suporte para a "delegação de pontos" dos gráficos de linhas
* LineChart: Passagem do cálculo de pontos do 'updatePaintNode' para o 'polish'

### KRunner

* Descontinuação de vestígios de pacotes do KDE4
* Tirar partido do suporte do novo construtor do 'plugin' com KPluginMetaData no KPluginLoader

### KService

* [kapplicationtrader] Correcção da documentação da API
* KSycoca: recriação da BD quando a versão < versão esperada
* KSycoca: Manter o registos dos ficheiros de recursos do KMimeAssociation

### KTextEditor

* Migração do KComboBox para o QComboBox
* uso do 'themeForPalette' do KSyntaxHighlighting
* correcção da chamada 'i18n' com argumento em falta (erro 429096)
* Melhoria da selecção automática de temas

### KWidgetsAddons

* Não enviar um sinal 'passwordChanged' duplicado
* Adição do KMessageDialog, uma variante assíncrona do KMessageBox
* Reposição do modo instantâneo antigo do KActionMenu
* Migração do KActionMenu para o QToolButton::ToolButtonPopupMode

### KWindowSystem

* Carregamento de 'plugins' de integração compilados estaticamente
* Migração do pid() para processId()

### KXMLGUI

* Introdução do HideLibraries e descontinuação do HideKdeVersion
* Remodelação do KKeySequenceWidget para usar o KeySequenceRecorder (erro 407395)

### Plataforma do Plasma

* [Representation] Só remover o preenchimento superior/inferior quando o cabeçalho/rodapé estiver visível
* [PlasmoidHeading] Uso da técnica do Representation para as margens/relevos
* Adição de um componente Representation
* [Tema do ambiente de trabalho] Mudança do nome 'hint-inset-side-margin' para 'hint-side-inset'
* [FrameSvg] Mudança de nome de 'insetMargin' para 'inset'
* [PC3] Uso do ScrollBar do PC3 no ScrollView
* [Brisa] Comunicação da sugestão de relevo
* [FrameSvg*] Mudança de nome do 'shadowMargins' para 'inset'
* [FrameSvg] 'Cache' das margens das sombras e respeito dos prefixos
* Correcção da animação antes de mudar o comprimento do realce da barra de progresso (erro 428955)
* [textfield] Correcção do texto sobreposto no botão de limpeza (erro 429187)
* Mostrar o menu suspenso na posição global correcta
* Uso do 'gzip -n' para evitar tempos de compilação incorporados
* Uso do KPluginMetaData para listar as 'containmentActions'
* Migração do carregamento do 'packageStructure' do KPluginTrader
* Uso do KPluginMetaData para enumerar os DataEngines
* [TabBar] Adição do realce com o foco do teclado
* [FrameSvg*] Exposição das margens das sombras
* Tornar o valor do MarginAreasSeparator mais claro
* [TabButton] Alinhar o ícone e o texto ao centro quando o texto está ao lado do ícone
* [SpinBox] Correcção de erro lógico na direcção do deslocamento
* correcção da barra de deslocamento móvel no modo RTL
* [ToolBar do PC3] Não desactivar os contornos
* [ToolBar do PC3] Uso das propriedades de margens correctas do SVG para o preenchimento
* [pc3/scrollview] Remoção do 'pixelAligned'
* Adição de áreas das margens

### Purpose

* [bluetooth] Correcção da partilha de vários ficheiros (erro 429620)
* Leitura da legenda de acção do 'plugin' traduzida (erro 429510)

### QQC2StyleBridge

* botão: validar o 'down' e não o 'pressed' nos estilos
* Redução do tamanho dos botões arredondados em dispositivos móveis
* correcção da barra de deslocamento móvel no modo RTL
* correcção da barra de progresso no modo RTL
* correcção da apresentação RTL do RangeSlider

### Solid

* Inclusão do errno.h no EBUSY/EPERM
* FstabBackend: retorno de DeviceBusy quando o 'umount' falhava com EBUSY (erro 411772)
* Correcção da detecção de 'libplist' e 'libimobiledevice' recentes

### Realce de Sintaxe

* correcção das dependências dos ficheiros gerados
* indexação: correcção de alguns problemas e desactivação de 2 verificações (grupo de captura e palavra-chave com separador)
* indexação: carregamento de todos os ficheiros XML em memória para uma verificação mais simples
* realce do C++: actualização para o Qt 5.15
* reinvocação dos geradores de sintaxe quando o ficheiro original é modificado
* unidade do 'systemd': actualizaçao para o 'systemd' v247
* ILERPG: simplificação e testes
* Zsh, Bash, Fish, Tcsh: adição do 'truncate' e 'tsort' nas palavras-chave 'unixcommand'
* Latex: alguns ambientes matemáticos podem ser encadeados (erro 428947)
* Bash: Muitas correcções e melhorias
* adição do --syntax-trace=stackSize
* php.xml: Correcção do 'endforeach' correspondente
* Passagem do 'bestThemeForApplicationPalette' do KTextEditor para aqui
* debchangelog: adição do Trixie
* alert.xml: Adição do alerta pouco conhecido `NOQA` no código-fonte
* cmake.xml: A versão oficial decidiu adiar o `cmake_path` para a próxima versão

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
