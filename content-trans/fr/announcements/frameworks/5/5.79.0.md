---
aliases:
- ../../kde-frameworks-5.79.0
date: 2021-02-13
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Port from QNetworkRequest::FollowRedirectsAttribute to QNetworkRequest::RedirectPolicyAttribute

### Baloo

* [SearchStore] Remove filesystem dependencies from includeFolder prop
* [FileIndexerConfig] Fix hidden check for explicitly included directories
* [FilteredDirIterator] Request hidden files from QDirIterator

### Icônes « Breeze »

* Nouvelles icônes « telegram-panel »
* Use correct style for align-horizontal-left-out icons (bug 432273)
* Ajout de nouvelles icônes pour Kickoff (bogue 431883)
* Add rating-half, 100% opacity for rating-unrated, Text color for rating in dark theme
* Suppression des icônes « KeePassXC » (bogue 431593)
* Correction de l'icône « @3x » (bogue 431475)
* Ajout d'une icône pour Neochat

### Modules additionnels « CMake »

* Only enable GNU_TAR_FOUND when --sort=name is available
* Supprimer la génération de métadonnées « fastlane » d'un paquet « APK » donné.
* KDEFrameworksCompilerSettings: define -DQT_NO_KEYWORDS and -DQT_NO_FOREACH by default
* [KDEGitCommitHooks] Create copy of scripts in source dir
* Support the new Appstream file extension as well
* fetch-translations: Resolve the URL before passing it to fetchpo.rb
* Consider Appstream donation URLs for creating F-Droid metadata
* Correction des permissions pour les scripts (bogue 431768)
* ECMQtDeclareLoggingCategory: create .categories files in build, not configure
* Add cmake function to configure git pre-commit hooks

### Intégration avec l'environnement de développement

* Fix window decorations not being uninstallable (bug 414570)

### Outils KDE avec « DOxygen »

* Vérification de la non-utilisation de la police par défaut pour doxygen
* Improve QDoc rendering and fix a few bug in the dark theme
* Mettre à jour les informations sur la maintenance
* Brand new theme consistent with develop.kde.org/docs

### KCalendarCore

* Dé-enregistrement de « MemoryCalendar » en tant qu'observateur lors de la suppression d'une incidence.
* Utilisez le paramètre « recurrenceId » pour supprimer l'occurrence de droite.
* Also clear notebook associations when closing a MemoryCalendar

### KCMUtils

* S'assurer du mode simple de colonne

### KCodecs

* Remove the usage of non-UTF-8 string literals

### KCompletion

* Fix regression caused due to porting from operator+ to operator|

### KConfig

* Refactor window geometry save/restore code to be less fragile
* Fix restoring window size when closed while maximized (bug 430521)
* KConfig: preserve the milliseconds component of QDateTime

### KCoreAddons

* Add KFuzzyMatcher for fuzzy filtering of strings
* KJob::infoMessage: document that the richtext argument will be removed, unused
* KJobUiDelegate::showErrorMessage: implement with qWarning()
* Dépréciation des méthodes concernant « X-KDE-PluginInfo-Depends »
* Ignorer les clés « X-KDE-PluginInfo-Depends »

### KDeclarative

* Autoriser des éléments simples de colonne
* KeySequenceItem: Assign empty string on clear instead of undefined (bug 432106)
* Disambiguate selected vs hovered states for GridDelegate (bug 406914)
* Utiliser le mode simple par défaut

### KFileMetaData

* ffmpegextractor: Use av_find_default_stream_index to find video stream

### KHolidays

* Mise à jour des jours fériés de l'île Maurice pour 2021
* Mettre à jour les jours fériés de Taiwan

### KI18n

* Don't set codec for textstream when building against Qt6

### KImageFormats

* Simplify portion of NCLX color profile code
* [imagedump] Add "list MIME type" (-m) option
* Correction d'un plantage avec des fichiers malformés
* ani: Make sure riffSizeData is of the correct size before doing the quint32_le cast dance
* Add plugin for animated Windows cursors (ANI)

### KIO

* Use Q_LOGGING_CATEGORY macro instead of explicit QLoggingCategory (bug 432406)
* Fix default codec being set to "US-ASCII" in KIO apps (bug 432406)
* CopyJob : correction d'un plantage avec ignorer / ré-essayer (bogue 431731)
* KCoreDirLister: un-overload canceled() and completed() signals
* KFilePreviewGenerator: modernise code base
* KCoreDirLister: un-overload clear() signal
* MultiGetJob : décharger les signaux
* FileJob : déchargement du signal « close() »
* SkipDialog: deprecate result(SkipDialog *_this, int _button) signal
* Fix lockup when renaming a file from properties dialog (bug 431902)
* Dépréciation de « addServiceActionsTo » et « addPluginActionsTo »
* [KFilePlacesView] Lower opacity for hidden items in "show all"
* Don't change directories when opening non listable urls
* Tweak KFileWidget::slotOk logic when in files+directory mode
* FileUndoManager: fix undo of copy empty directory
* FileUndoManager : ne pas écraser les fichier lors d'une annulation
* FileUndoManager : dépréciation de la méthode « undoAvailable() »
* ExecutableFileOpenDialog : rendre l'étiquette de texte plus générique
* KProcessRunner: only emit processStarted() signal once
* Revert "kio_trash: fix the logic when no size limit is set"

### Kirigami

* Utilisation d'une icône non symbolique pour une action de fermeture.
* Fix toolbar menu buttons depress correctly
* Utiliser la sous-section plutôt que la section
* [controls/BasicListItem]: Add reserveSpaceForSubtitle property
* make nav button implicit height explicit
* Correction de l'alignement vertical pour « BasicListItem »
* [basiclistitem] Ensure icons are square
* [controls/ListItem]: Remove indented separator for leading items
* Re-add right list item separator margin when there's a leading item
* Don't manually call reverseTwinsChanged when destructing FormLayout (bug 428461)
* [org.kde.desktop/Units] Make durations match controls/Units
* [Units] Reduce veryLongDuration to 400ms
* [Units] Reduce shortDuration and longDuration by 50ms
* Don't consider Synthetized mouse events as Mouse (bug 431542)
* more aggressively use implicitHeight instead of preferredHeight
* Utilisation des textures d'atlas pour les icônes
* controls / AbstractApplicationHeader : centrer verticalement les éléments fils
* [actiontextfield] Fix inline action margins and sizing
* correctly update header size (bug 429235)
* [controls/OverlaySheet]: Respect Layout.maximumWidth (bug 431089)
* [controls/PageRouter]: Expose set parameters when dumping currentRoutes
* [controls/PageRouter]: Expose top level parameters in 'params' property map
* move pagerouter related examples in a subfolder
* Possibilité de faire glisser la fenêtre grâce à des zones non interactives
* AbstractApplicationWindow: Use wide window on desktop systems for all styles
* Don't hide list item separator on hover when background is transparent
* [controls/ListItemDragHandle] Fix wrong arrangement on no-scrolling case (bug 431214)
* [controls/applicationWindow]: Account for drawer widths when computing wideScreen

### KNewStuff

* Refactor the KNSQuick Page header and footer for Kirigami-ness
* Add number label to Ratings component for easier readability
* Reduce minimum size of QML GHNS dialog window
* Match lighter hovered appearance for KCM grid delegates
* Ensure the minimum width for the QML Dialog is screen-width or less
* Fix the BigPreview delegate's layout
* Revalidate cached entries before showing dialog
* Add support for kns:/ urls to the knewstuff-dialog tool (bug 430812)
* filecopyworker : ouvrir les fichiers avant de lire / écrire
* Reset entry to updateable when no payload is identified for updating (bug 430812)
* Fix occasional crash due to holding a pointer incorrectly
* Obsolescence de la classe « DownloadManager »
* Deprecate AcceptHtmlDownloads property
* Deprecate ChecksumPolicy and SignaturePolicy properties
* Obsolescence de la propriété de portée
* Obsolescence de la propriété « CustomName »

### KNotification

* Emit NewMenu when new context menu is set (bug 383202)
* Obsolescence de « KPassivePopup »
* Make sure all backends ref the notification before doing work
* Make the notification example app build and work on Android
* Move notification id handling into KNotification class
* Fix removing pending notification from queue (bug 423757)

### Environnement de développement « KPackage »

* Document PackageStructure ownership when using PackageLoader

### KPty

* Fix generating the full path to kgrantpty in the code for ! HAVE_OPENPTY

### KQuickCharts

* Add a "first" method to ChartDataSource and use it in Legend (bug 432426)

### KRunner

* Check for selected action in case of informational match
* Correction de la chaîne vide de résultats pour l'activité courante
* Deprecate overloads for QueryMatch ids
* [Lanceur D-Bus] Tester « RemoteImage »

### KService

* Obsolescence de « KPluginInfo::dependencies() »
* CMake: Specify add_custom_command() dependencies
* Explicitly deprecate overload for KToolInvocation::invokeTerminal
* Ajout d'une méthode pour obtenir le paramètre « KServicePtr » de l'application terminal par défaut.
* KService : ajout de la méthode pour définir « workingDirectory »

### KTextEditor

* [Vimode] Do not switch view when changing case (~ command) (bug 432056)
* Increase maximum indentation width to 200 (bug 432283)
* ensure we update the range mapping e.g. on invalidation of now empty ranges
* Only show bookmark chars error when in vimode (bug 424172)
* [vimode] Fix motion to matching item off-by-one
* Retain replacement text as long as the power search bar is not closed (bug 338111)
* KateBookMarks : modernisation de la base de code
* Don't ignore alpha channel when there is a mark
* Fix alpha channel being ignored when reading from config interface
* Prevent bracket match preview from overextending outside the view
* Prevent bracket match preview from sometimes lingering after switching to a different tab
* Make the bracket match preview more compact
* Do not show bracket match preview if it will cover the cursor
* Maximize width of bracket match preview
* Masquer l'aperçu des correspondances de parenthèses lors du défilement des pages
* avoid duplicated highlighting ranges that kill ARGB rendering
* exposer la référence globale « KSyntaxHighlighting::Repository » en lecture seule
* Correct indentation bug when line contains "for" or "else"
* Correction d'un bogue d'indentation
* remove the special case tagLine, it did lead to random update faults in e.g. the
* fix rendering of word wrap makers + selection
* Fix indent for when pressing enter and the function param has a comma at the end
* paint the small gap in selection color, if previous line end is in selection
* Simplification du code + correction des commentaires
* revert cut error, did delete too much code for extra renderings
* avoid full line selection painting, more in line with other editors
* Adapter l'indenteur pour les fichiers « hl » modifiés
* [Vimode] Portage de la commande vers « QRegularExpression »
* [Vimode] Port findPrevWordEnd and findSurroundingBrackets to QRegularExpression
* [Vimode] Port QRegExp::lastIndexIn to QRegularExpression and QString::lastIndexOf
* [Vimode] Port ModeBase::addToNumberUnderCursor to QRegularExpression
* [Vimode] Port simple uses of QRegExp::indexIn to QRegularExpression and QString::indexOf
* Use rgba(r,g,b,aF) to handle the alpha channel for html export
* Respect alpha colors while exporting html
* Introduce a helper method for getting the color name correctly
* Prise en charge des couleurs : couche de configuration
* avoid that line changed markers kill current line highlight
* paint current line highlighting over iconborder, too
* Activation du canal alpha pour les couleurs de l'éditeur
* Fix current line highlight has a tiny gap at the beginning for dyn. wrapped lines
* Dont do needles computation, just compare values directly
* Fix current line highlight has a tiny gap at the beginning
* [Vimode] Do not skip folded range when the motion lands inside
* Use a range-for loop over m_matchingItems instead of using QHash::keys()
* Portage de « normalvimode » vers « QRegularExpression »
* Exportation correcte des dépendances adaptées
* Présentation du thème « KSyntaxHighlighting »
* add configChanged to KTextEditor::Editor, too, for global configuration changes
* Réorganiser légèrement les éléments de configuration
* Correction de la gestion des clés
* add doc/view parameters to new signals, fix old connects
* allow to access & alter the current theme via config interface
* Remove const qualifier when passing LineRanges around
* Use .toString() since QStringView is lacking .toInt() in older Qt versions
* Declare toLineRange() constexpr inline whenever possible
* Reuse QStringView version from QStringRev, remove const qualifier
* Move TODO KF6 comment out of doxygen comment
* Cursor, Range: Add fromString(QStringView) overload
* Allow "Dynamic Word Wrap Align Indent" to be disabled (bug 430987)
* LineRange::toString(): Avoid '-' sign, as for negative numbers it's confusing
* Use KTextEditor::LineRange in notifyAboutRangeChange() mechanism
* Port tagLines(), checkValidity() and fixLookup() to LineRanges
* Ajout de « KTextEditor::LineRange »
* Move KateTextBuffer::rangesForLine() impl to KateTextBlock and avoid unnecessary container constructions
* [Vimode] Correction de la recherche dans les zones de repliement (bogue 376934)
* [Vimode] Fix Macro Completion Replay (bug 334032)

### KTextWidgets

* Have more private classes inherit from those of the parents

### KUnitConversion

* Définir une variable avant de l'utiliser

### KWidgetsAddons

* Faire usage de « AUTORCC »
* Have more private classes inherit from those of the parents
* Inclure explicitement « QStringList »

### KWindowSystem

* Add fractional opacity convenience helpers
* Correction complète des « includes »
* Corriger les « include »
* xcb: Work with the active screen as reported by QX11Info::appScreen()

### KXMLGUI

* Corriger les « include »
* Add KXMLGUIFactory::shortcutsSaved signal
* Use the correct kde get involved url (bug 430796)

### Environnement de développement de Plasma

* [plasmoidheading] Use intended color for footers
* [plasmacomponents3/spinbox] Correction de la couleur sélectionnée de texte
* widgets>lineedit.svg: fix pixel misalignment woes (bug 432422)
* Correction des remplissages gauche et droit, incohérents dans « PlasmoidHeading »
* Mise à jour des couleurs pour « breeze-light » et « breeze-dark »
* Revert "[SpinBox] Fix logic error in scroll directionality"
* [calendar] Correction de noms manquants d'importation
* [ExpandableListItem] L'affichage de la liste des actions développées doit respecter la police.
* Agenda journaliers : portage vers PC3 / QQC2, lorsque possible
* Suppression de l'animation de survol des boutons plats, des calendriers, des éléments de liste et des boutons
* Vider les erreurs des composants graphiques dans la console
* Résoudre la dépendance cyclique de « Units.qml »
* [Élément de menus des composants de Plasma] Créer une action fictive lorsque l'action est détruite.
* ne pas faire de pixmaps plus grands que nécessaire
* Ajout des fichiers de projet à ignorer pour les environnements de développement « JetBrains »
* Correction des alarmes de connexion
* Ajouter une réinitialisation à la propriété « globalShortcut » (bogue 431006)

### Motif

* [Nextcloud] Remaniement de l'interface utilisateur pour la configuration
* Évaluation de la configuration initiale
* Figer « ListViews » dans « kdeconnect » et dans la configuration du Bluetooth
* Suppression des propriétés non nécessaires attachées à la mise en page
* [Modules externes / nextcloud] Utilisation de l'icône « Nextcloud »
* [cmake] Déplacer « find_package » vers le fichier de haut niveau « CMakeLists.txt »

### QQC2StyleBridge

* [combobox] Correction de la vitesse de défilement du pavé tactile (bogue 400258)
* « qw » peut être « Null »
* Prise en charge de « QQuickWidget » (bogue 428737)
* Autoriser le glisser de fenêtre vers des zones vides

### Opaque

* CMake : utilisation de « configure_file() » pour assurer des compilations incrémentales de type « noop ».
* [Fstab] Ignorer les montages superposés de conteneurs (bogue 422385)

### Sonnet

* Ne faites pas de recherches multiples quand une seule suffit.

### Coloration syntaxique

* Ajout de l'incrément manquant de la version de « context.xml »
* Prise en charge des terminaisons de fichiers officielles (Voir https://mailman.ntg.nl/pipermail/ntg-context/2020/096906.html)
* Mise à jour des résultats de référence
* couleur d'opérateur moins vibrante pour les thèmes « Breeze »
* Correction de la syntaxe pour « new hugo »
* Correction des erreurs de syntaxe
* Amélioration de la lisibilité pour le thème sombre solarisé
* supprimer les captures inutiles avec une règle dynamique
* mise en évidence de l'opérateur de fusion => mise à jour des références
* Fusionner la coloration syntaxique de l'opérateur
* Ajout de surcharges de constantes pour certains mécanismes d'accès
* Bash, Zsh : correction de commande « ;; » dans un cas (bogue 430668)
* Atom Clair, Breeze Sombre / Clair : nouvelle couleur pour l'opérateur. Breeze Clair : nouvelle couleur pour « ControlFlow »
* email.xml : détection des commentaires imbriqués et des caractères d'échappement (bogue 425345)
* Mise à jour du thème « Atom Clair » pour utiliser les couleurs Alpha
* Nouvelle correspondance pour certains styles de symboles et d'opérateurs à « dsOperator »
* Bash : correction de } dans ${!xy*} et opérateur avec plus d'expansion de paramètres (# dans ${#xy} ; !,*,@, [*], [@] dans ${!xy*}) (bogue 430668)
* Mise à jour des révisions de thèmes
* Mise à jour du module de coloration syntaxique de « kconfig » pour Linux 5.9
* Utiliser la fonction rgba(r, g, b, aF) pour gérer correctement le canal Alpha pour les navigateurs sous Qt.
* Non utilisation de « rgba » lorsque dans « themeForPalette »
* Assurez-vous que « rgba » est respecté dans le « html » et dans le formatage.
* Corrections de « atom-one-dark »
* Quelques mises à jour supplémentaires pour « Atom One » sombre
* Ne pas vérifier le « rgba » lors de la recherche de la meilleure correspondance.
* Autorisation d'utiliser le canal alpha pour les couleurs de thème
* Mise à jour des couleurs pour « monokai » et correction du bleu incorrect
* C++ : correction du suffixe américain
* Bash : correction #5 : ajout de « $ » à la fin d'une chaîne de caractères doublement quotée
* VHDL : correction des fonctions, procédures, plages / unités de type et autres améliorations
* Mise à jour de « breeze-dark.theme »
* Raku : #7 : correction des symboles débutant par « Q »
* Correction rapide du contraste manquant pour l'extension
* Ajouter le thème de couleurs Oblivion depuis GtkSourceView / Pluma / gEdit

### ThreadWeaver

* Correction des itérateurs de cartes lors de la compilation avec Qt 6
* Ne pas initialiser explicitement les mutex comme « Non Récursif »

### Informations sur la sécurité

Le code publié a été signé en « GPG » en utilisant la clé suivante : pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
