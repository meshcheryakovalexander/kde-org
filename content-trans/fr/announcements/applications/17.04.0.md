---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: KDE publie les applications de KDE en version 17.04.0
layout: application
title: KDE publie les applications de KDE en version 17.04.0
version: 17.04.0
---
20 avril 2017 . Les applications KDE 17.04 sont arrivées. Dans l'ensemble, nous avons travaillé pour rendre à la fois les applications et les bibliothèques sous-jacentes plus stables et plus faciles à utiliser. En prenant compte de vos commentaires, nous avons rendu la suite KDE Applications beaucoup plus conviviale.

Profitez bien de nos nouvelles applications !

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

Les développeurs de KAlgebra sont sur le chemin de la convergence, ayant porté la version mobile du programme éducatif complet sur Kirigami 2.0 (notre outil de développement dédié pour l'intégration des applications KDE sur les plateformes de bureau et mobiles).

En outre, la version de bureau a également migré le back-end 3D vers GLES, le logiciel qui permet au programme de réaliser le rendu des fonctions 3D à la fois sur le bureau et sur les appareils mobiles. Le code est ainsi plus simple et plus facile à maintenir.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

L'éditeur vidéo de KDE devient plus stable et plus complet à chaque nouvelle version. Cette fois, les développeurs ont remanié la boîte de dialogue de sélection de profil pour faciliter le réglage de la taille de l'écran, de la fréquence d'images et d'autres paramètres de votre film.

Désormais, vous pouvez également lire votre vidéo directement à partir de la notification lorsque le rendu est terminé. Certains bug qui se produisaient lors du déplacement des clips sur la ligne de temps ont été corrigés, et l'assistant DVD a été amélioré.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

Notre explorateur de fichiers préféré a subi plusieurs transformations et améliorations de l'utilisation pour le rendre encore plus puissant.

Les menus contextuels du panneau <i>Places</i> (par défaut à gauche de la zone de visualisation principale) ont été nettoyés, et il est désormais possible d'interagir avec les widgets de métadonnées dans les infobulles. Les infobulles, soit dit en passant, fonctionnent maintenant aussi sur Wayland.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

L'application graphique populaire pour créer, décompresser et gérer des archives compressées pour des fichiers et des dossiers est maintenant dotée d'une fonction <i>Recherche</i> pour vous aider à trouver des fichiers dans des archives encombrées.

Il vous permet aussi d'activer ou désactiver les modules externes directement à partir de la boîte de dialogue <i>Configurer</i> . En parlant des modules externes, le nouveau module externe « libzip » améliore la prise en charge des archives « zip ».

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Si vous enseignez ou apprenez à jouer de la musique, vous devez découvrir Minuet. La nouvelle version propose davantage d'exercices de gammes et de tâches de formation à l'écoute pour les gammes bebop, mineure/major harmonique, pentatonique et symétrique.

Vous pouvez également définir ou passer des tests en utilisant le nouveau <i>Mode test</i> pour répondre aux exercices. Vous pouvez suivre vos progrès en exécutant une séquence de 10 exercices et vous obtiendrez des statistiques sur votre taux de réussite à la fin.

### Et encore plus !

<a href='https://okular.kde.org/'>Okular</a>, le visualiseur de documents de KDE, a subi au moins une demi-douzaine de modifications qui ajoutent des fonctionnalités et améliorent sa convivialité sur les écrans tactiles. <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> et plusieurs autres applications qui composent <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (la suite email/calendrier/groupware de KDE) ont été révisées, déboguées et optimisées pour vous aider à devenir plus productif.

<a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> et plus encore (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Release Notes</a>) ont maintenant été portés sur KDE Frameworks 5. Nous attendons avec impatience vos commentaires à propos des nouvelles fonctionnalités introduites par cette version.

<a href='https://userbase.kde.org/K3b'>K3b</a> a rejoint la publication des applications de KDE.

### Éradication des bogues

Plus de 95 corrections de bogues ont été apportées dans les applications dont la suite Kopete, KWalletManager, Marble, Spectacle et bien d'autres !

### Journal complet des changements
