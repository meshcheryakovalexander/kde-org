---
aliases:
- ../announce-applications-15.04-rc
date: '2015-03-26'
description: KDE publie la version candidate 15.04 des applications de KDE.
layout: application
title: KDE publie la version candidate de KDE Applications 15.04
---
26 mars 2015. Aujourd'hui, KDE publie la version candidate de KDE Applications. Les dépendances et les fonctionnalités sont figées. L'attention de l'équipe KDE se porte à présent sur la correction des bogues et les dernières finitions.

Avec les différentes applications reposant sur KDE Frameworks 5, les publications de KDE Applications 15.04 nécessitent des tests poussés pour maintenir et améliorer la qualité ainsi que l'expérience utilisateur. Les utilisateurs ont un grand rôle à jouer dans la qualité de KDE car les développeurs ne peuvent tout simplement pas tester toutes les configurations possibles. Nous comptons sur vous pour nous aider à détecter les bogues suffisamment tôt pour pouvoir les corriger avant la livraison finale. N'hésitez pas à rejoindre l'équipe en installant la bêta <a href='https://bugs.kde.org/'>et en signalant les bogues</a>.
