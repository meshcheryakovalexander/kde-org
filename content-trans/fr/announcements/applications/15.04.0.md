---
aliases:
- ../announce-applications-15.04.0
changelog: true
date: '2015-04-15'
description: KDE publie la version 15.04 des applications de KDE.
layout: application
title: KDE publie KDE Applications 15.04.0
version: 15.04.0
---
15 avril 2015. Aujourd'hui, KDE publie les applications 15.04. Avec cette version, c'est un total de 72 applications qui ont été portées vers <a href='https://dot.kde.org/2013/09/25/frameworks-5'>l'environnement de développement 5</a>. L'équipe s'efforce d'apporter la meilleure qualité à votre bureau et à ses applications. Nous comptons donc sur vous pour nous fournir vos retours.

Dans cette version, il y a plusieurs nouveaux ajouts à la liste des applications reposant sur l'environnement de développement de KDE 5, incluant <a href='https://www.kde.org/applications/education/khangman/'>KHangMan</a>, <a href='https://www.kde.org/applications/education/rocs/'>Rocs</a>, <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, <a href='https://www.kde.org/applications/development/kompare'>Kompare</a>, <a href='https://kdenlive.org/'>Kdenlive</a>, <a href='https://userbase.kde.org/Telepathy'>KDE Telepathy</a> et <a href='https://games.kde.org/'>certains jeux de KDE</a>.

Kdenlive est l'un des meilleurs logiciels disponible pour le montage vidéo non linéaire. Il a récemment fini son <a href='%1'>processus d'incubation</a> pour devenir un projet KDE officiel et a été porté vers l'environnement de développement de KDE version 5. L'équipe derrière ce chef d'œuvre a décidé que Kdenlive devrait être publié au sein des applications de KDE. Certaines nouvelles fonctionnalités sont l'enregistrement automatique des nouveaux projets et la correction de la stabilisation des vidéos.

KDE Telepathy est l'outil de messagerie instantanée. Il a été porté vers KDE Frameworks 5 et Qt5. C'est un nouveau membre de KDE Applications. Il est quasiment complet, excepté l'interface utilisateur pour les appels audio et vidéo qui est encore manquante.

Lorsque c'est possible, KDE utilise des technologies existantes comme cela a été fait avec le nouveau KAccounts qui est également utilisé dans SailfishOS et dans l'environnement de bureau Unity de Canonical. A l'heure actuelle, dans KDE Applications, il est seulement utilisé par KDE Telepathy. Mais, dans le futur, son utilisation sera élargie à des applications comme Kontact et Akonadi.

Dans le <a href='https://edu.kde.org/'>module KDE Education</a>, Cantor a reçu des nouvelles fonctionnalités concernant le support de Python : un nouveau moteur Python 3 et de nouvelles catégories dans « Obtenir les dernières nouveautés » (« Get Hot New Stuff »). Rocs a été revu de fond en comble : le cœur de théorie des graphes a été réécrit, la séparation des structures de données a été supprimée et un document comme entité central de graphe a été introduit. De plus, l'API de scripts pour les algorithmes de graphes a été revu en détail et fournit maintenant une seule API unifiée. KHangMan a été porté vers QtQuick et a reçu une couche de peinture fraîche dans le processus. Et Kanagram a hérité d'un nouveau mode de jeu pour 2 joueurs et les lettres sont maintenant des boutons cliquables, mais pouvant être appuyées comme avant.

Outre les traditionnelles corrections de bogues, <a href='https://www.kde.org/applications/development/umbrello/'>Umbrello</a> a reçu des améliorations d'ergonomie et de stabilité pour cette version. De plus, la fonction « Rechercher » peut désormais être limitée par catégorie : classe, interface, paquet, méthodes ou attributs.
