---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE publie les applications de KDE en version 16.12.2
layout: application
title: KDE publie les applications de KDE en version 16.12.2
version: 16.12.2
---
09 Septembre 2017. Aujourd'hui, KDE a publié la seconde mise à jour de stabilisation des <a href='../16.12.0'>applications 16.12 de KDE</a>. Cette mise à jour ne contient que des corrections de bogues et des mises à jour de traduction, elle sera sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à kdepim, dolphin, kate, kdenlive, ktouch, okular et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.29 de KDE.
