---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: O KDE disponibiliza o KDE Applications 17.04.0
layout: application
title: O KDE disponibiliza o KDE Applications 17.04.0
version: 17.04.0
---
20 de abril de 2017. O KDE Applications 17.04 chegou. Em geral, nós nos esforçamos em tornar tanto os aplicativos quanto as bibliotecas mais estáveis e fáceis de usar. Ao corrigir esses problemas e ouvir o seu feedback, nós conseguimos tornar os aplicativos do KDE muito mais amigáveis e menos suscetíveis a glitches.

Aproveite os seus novos apps!

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

Os desenvolvedores do KAlgebra estão seguindo seu próprio caminho para a convergência, portando a versão de celular do compreensivo programa educacional para o Kirigami 2.0 - a infraestrutura preferencial para integrar os aplicativos do KDE entre as plataformas de desktop e celular.

Além disso, a versão de desktop também teve sua infraestrutura 3D migrada para GLES, o software que permite ao programa renderizar funções 3D tanto no desktop quanto em dispositivos móveis. Isso torna o código mais simples e fácil de manter.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

O editor de vídeo do KDE fica mais estável e completo a cada nova versão. Desta vez, os desenvolvedores refizeram a janela de seleção de perfil de modo a tornar mais fácil definir o tamanho de tela, taxa de quadros e outros parâmetros do seu vídeo.

Agora também é possível reproduzir seu vídeo diretamente da notificação assim que a renderização terminar. Alguns crashes que ocorriam ao mover clipes na linha do tempo foram corrigidos e o assistente de DVD recebeu melhorias.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

O nosso gerenciador de arquivos predileto e portal para tudo (exceto talvez o submundo) recebeu várias mudanças de visual e melhorias de usabilidade, tornando-se ainda mais poderoso.

Os menus de contexto no painel <i>Locais</i> (à esquerda da área de visualização principal por padrão) foram limpos e agora é possível interagir com os widgets de metadados nas dicas. Aliás, agora as dicas também funcionam no Wayland.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

O popular app gráfico para criar, descompactar e administrar arquivos compactados de arquivos e pastas agora vem com uma função de <i>Localizar</i> para ajudá-lo a encontrar arquivos dentro de pacotes cheios.

Também se tornou possível habilitar e desabilitar extensões diretamente da janela <i>Configurar</i>. Falando em extensões, a nova extensão Libzip teve o suporte ao formato Zip melhorado.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Se você está lecionando ou aprendendo a tocar música, você precisa dar uma olhada no Minuet. A nova versão oferece mais exercícios de escala e tarefas para treinar o ouvido para as escalas bebop, harmônica menor/maior, pentatônica e simétrica.

Também poderá definir ou fazer testes com o novo <i>Modo de testes</i> para exercícios de respostas. Poderá vigiar o seu progresso, executando uma sequência de 10 exercícios e irá obter estatísticas proporcionais quando terminar.

### E mais!

O <a href='https://okular.kde.org/'>Okular</a>, o visualizador de documentos do KDE, recebeu pelo menos uma dúzia de alterações que adicionam funcionalidades e que adequam a sua usabilidade aos ecrãs tácteis. O <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> e diversas outras aplicações que compõem o <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (o pacote de e-mail/calendários/'groupware' do KDE) foram revistas, depuradas e optimizadas para o ajudar a tornar-se mais produtivo.

O <a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, o <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> e outros (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Notas de Lançamento</a>) foram agora migrados para as Plataformas do KDE 5 e estamos à espera da sua reacção e opiniões sobre as funcionalidades mais recentes que foram introduzidas com esta versão.

O <a href='https://userbase.kde.org/K3b'>K3b</a> juntou-se ao pacote lançado das Aplicações do KDE.

### Pisoteando bugs

Mais de 95 bugs foram resolvidos nos aplicativos KDE, incluindo o Kopete, KWalletManager, Marble, Spectacle e mais!

### Registro completo das alterações
