---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE wydało Aplikacje KDE 16.04.3
layout: application
title: KDE wydało Aplikacje KDE 16.04.3
version: 16.04.3
---
12 lipca 2016. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../16.04.0'>Aplikacji KDE 16.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 20 zarejestrowanych poprawek błędów uwzględnia ulepszenia do cantor, kate, kdepim, umbrello, i innych.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.22.
