---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE wydało Aplikacje KDE 17.08.2
layout: application
title: KDE wydało Aplikacje KDE 17.08.2
version: 17.08.2
---
12 października 2017. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../17.08.0'>Aplikacji KDE 17.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than 25 recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, Kdenlive, Marble, Okular, among others.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.37.

Wśród ulepszeń znajdują się:

- A memory leak and crash in Plasma events plugin configuration was fixed
- Read messages are no longer removed immediately from Unread filter in Akregator
- Gwenview Importer now uses the EXIF date/time
