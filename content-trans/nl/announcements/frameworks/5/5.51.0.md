---
aliases:
- ../../kde-frameworks-5.51.0
date: 2018-10-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Aanroepen van KIO::UDSEntry::reserve in timeline/tags ioslaves toegevoegd
- [balooctl] Flush gebufferde "Indexering &lt;file&gt;" regel bij starten van indexering
- [FileContentIndexer] signaal beëindigd verbinden vanuit extractieproces
- [PositionCodec] crash vermijden in geval van gegevens met fouten (bug 367480)
- Ongeldige tekenconstante repareren
- [Balooctl] controle op bovenliggende map verwijderen (bug 396535)
- Verwijderen van niet-bestaande mappen uit lijsten met include en exclude (bug 375370)
- String gebruiken om UDS_USER en UDS_GROUP van type String op te slaan (bug 398867)
- [tags_kio] haakjes repareren. Op de een of andere manier glipte dit door mijn controle op de code
- Gnome-bestanden uitsluiten van indexering

### BluezQt

- Media en MediaEndpoint API implementeren

### Breeze pictogrammen

- "stack-use-after-scope" gedetecteerd door ASAN in CI repareren
- Monochrome pictogrammen met ontbrekende stylesheets repareren
- Drive-harddisk wijzigen naar meer aanpasbare stijl
- Pictogrammen voor firewall-config en firewall-applet toevoegen
- Vergrendeling op pictogram plasmavault zichtbaar maken met breeze-dark
- Plussymbool toevoegen aan document-new.svg (bug 398850)
- Pictogram voor 2x schalen leveren

### Extra CMake-modules

- Python bindings compileren met dezelfde sip-vlaggen gebruikt door PyQt
- Android: doorgeven van een relatief pad als de apk-map toestaan
- Android: biedt op de juiste manier terugvallen naar toepassingen die geen manifest hebben
- Android: Ga na dat Qm-vertalingen worden geladen
- Android builds repareren met cmake 3.12.1
- l10n: overeenkomende cijfers in naam van de opslagruimte repareren
- QT_NO_NARROWING_CONVERSIONS_IN_CONNECT toevoegen als standaard vlag bij compileren
- Bindingen: juiste behandeling van bronnen met utf-8
- Loop echt CF_GENERATED door, in plaats van voortdurend item 0 te controleren

### KActivities

- Hangende referentie repareren met "auto" wordt "QStringBuilder"

### KCMUtils

- terugkeergebeurtenissen beheren
- Wijzig grootte handmatig van KCMUtilDialog naar sizeHint() (bug 389585)

### KConfig

- Probleem repareren bij lezen van lijst met paden
- kcfg_compiler documenteert nu geldige invoer voor zijn type 'Color'

### KFileMetaData

- eigen implementatie van conversie van QString naar TString verwijderen voor taglibwriter
- testdekking van taglibwriter vergroten
- meer basis tags implementeren voor taglibwriter
- gebruik van eigen conversiefunctie TString naar QString verwijderen
- verhoog vereiste taglib versie naar 1.11.1
- lezen van afspeelversterkingstag implementeren

### KHolidays

- Ivoorkust vakantiedagen toevoegen (Frans) (bug 334305)
- holiday*hk** - datum repareren voor Tuen Ng Festival in 2019 (bug 398670)

### KI18n

- Op de juiste manier scope CMAKE_REQUIRED_LIBRARIES wijziging
- Android: ga na dat we zoeken naar .mo bestanden in het juiste pad

### KIconThemes

- Beginnen met emblemen te tekenen in de rechtsonder hoek

### KImageFormats

- kimg_rgb: optimaliseer weg QRegExp en QString::fromLocal8Bit
- [EPS] crash repareren bij afsluiten toepassing (waarvan geprobeerd wordt de klembordafbeeling te laten bestaan) (bug 397040)

### KInit

- Overbodige berichten verminderen door niet te controleren op het bestaan van bestand met lege naam (bug 388611)

### KIO

- toestaan niet-locaal file:// door te verwijzen naar een Windows WebDav URL
- [KFilePlacesView] pictogram voor het 'Bewerken' contextmenu-item in paneel Plaatsen
- [Places panel] meer toepasselijk netwerkpictogram gebruiken
- [KPropertiesDialog] aankoppelinformatie voor mappen in / (root) tonen
- Verwijdering van bestanden uit DAV repareren (bug 355441)
- QByteArray::remove vermijden in AccessManagerReply::readData (bug 375765)
- Niet proberen ongeldige gebruikersplaatsen te herstellen
- Het mogelijk maken om omhoog naar de hoofdmap te gaan zelfs met achteraan aanwezige slashes in de URL
- Crashes van KIO slave worden nu behandeld door KCrash in plaats van eigen ondermaatse code
- Een bestand dat wordt aangemaakt uit geplakte inhoud van het klembord dat zich toont na een vertraging repareren
- [PreviewJob] ingeschakelde plug-ins verzenden naar de miniatuurslave (bug 388303)
- Foutmelding "onvoldoende schijfruimte" verbeteren
- IKWS: niet-verouderde "X-KDE-ServiceTypes" gebruiken in generatie van desktop-bestanden
- WebDAV bestemmingsheader repareren bij COPY en MOVE acties
- Gebruiker waarschuwen voor acties copy/move als beschikbare ruimte niet genoeg is (bug 243160)
- SMB KCM verplaatsen naar categorie Netwerkinstellingen
- prullenbak: ontleden van cachegrootte van mappen repareren
- kioexecd: bewaak het aanmaken of wijzigen van de tijdelijke bestanden (bug 397742)
- Geen frames en schaduw tekenen rond afbeeldingen met transparantie (bug 258514)
- Bestandstypepictogram in dialoog over bestandseigenschappen vervaagd getoond op hoge dpi schermen gerepareerd

### Kirigami

- de lade op de juiste manier openen bij slepen met handvat
- extra marge bij de paginarij globale werkbalk is ToolBar
- ook Layout.preferredWidth ondersteunen voor grootte van vel
- overblijfselen van laatste controls1 kwijt maken
- Aanmaken van scheidingsacties toestaan
- stem in met een willekeurig # kolommen in CardsGridview
- Niet actief menu-items vernietigen (bug 397863)
- pictogrammen in actionButton zijn monochroom
- pictogrammen niet monochroom maken wanneer dat niet zou moeten
- herstel de willekeurige *1.5 grootte van pictogrammen op de mobiel
- delegate recycler: het context-object niet tweemaal opvragen
- de interne materiaal rimpel implementatie gebruiken
- bestuur kopbreedte door brongrootte indien horizontaal
- laat alle eigenschappen zien van BannerImage in Cards
- DesktopIcon gebruiken zelfs in plasma
- file:// paden op de juiste manier laden
- Draai "Begin met zoeken naar de context van de gedelegeerde zelf" om
- Testgeval toevoegen die probleem met scope laat zien in DelegateRecycler
- stel expliciet een hoogte voor overlayDrawers in (bug 398163)
- Begin met zoeken naar de context van de gedelegeerde zelf

### KItemModels

- Referentie in for-loop voor type met niet-triviale kopieconstructie gebruiken

### KNewStuff

- Ondersteuning toevoegen voor ondersteuning van Attica tags (bug 398412)
- [KMoreTools] het menu-item "Configureren..." een toepasselijk pictogram geven (bug 398390)
- [KMoreTools] hiërarchie in menu verminderen
- 'Onmogelijkheid on knsrc-bestand te gebruiken voor uploads uit niet-standaard locatie' repareren (bug 397958)
- Laak testhulpmiddelen koppelen onder Windows
- Unbreak gebouwd met Qt 5.9
- Ondersteuning toevoegen voor ondersteuning van Attica tags

### KNotification

- Een crash veroorzaakt door slecht beheer van leeftijd van op canberra gebaseerde geluidsmelding gerepareerd (bug 398695)

### KNotifyConfig

- UI-bestandshint repareren: KUrlRequester heeft nu QWidget als basis klasse

### KPackage-framework

- Referentie in for-loop voor type met niet-triviale kopieconstructie gebruiken
- Qt5::DBus verplaatsen naar de 'PRIVATE' koppelingsdoelen
- Signalen uitzenden wanneer pakket wordt geïnstalleerd/verwijderd

### KPeople

- Signalen die niet worden uitgezonden wanneer twee personen worden samengevoegd repareren
- Niet crashen als persoon wordt verwijderd
- PersonActionsPrivate definiëren als klasse, zoals eerder gedeclareerd
- API van PersonPluginManager publiek maken

### Kross

- core: commentaar voor acties beter behandelen

### KTextEditor

- Markering voor invouwen van code alleen tonen voor gebieden met meerdere ingevouwen regels met code
- m_lastPosition initialiseren
- Scripting: isCode() geeft false terug voor dsAlert tekst (bug 398393)
- R Script hl gebruiken voor R tests met inspringen
- Het R-script voor inspringen bijgewerkt
- Overbelicht repareren voor lichte en donkere kleurschema's (bug 382075)
- Geen Qt5::XmlPatterns vereisen

### KTextWidgets

- ktextedit: het QTextToSpeech object langzaam laden

### KWallet Framework

- Fouten bij portefeuille openen loggen

### KWayland

- Geen stille fout als beschadiging wordt verzonden voor de buffer (bug 397834)
- [server] niet vroeg terugkeren bij mislukken in touchDown code voor terugvallen
- [server] behandeling van buffer bij toegang van afstand repareren wanneer uitvoer niet is gebonden
- [server] geen aanbiedingen van gegevens proberen te maken zonder bron
- [server] begin van slepen afbreken bij juiste voorwaarden en zonder een fout te verzenden

### KWidgetsAddons

- [KCollapsibleGroupBox] animatie van widgetduur van stijl respecteren (bug 397103)
- Verouderde controle op Qt-versie verwijderen
- Compile

### KWindowSystem

- _NET_WM_WINDOW_TYPE_COMBO gebruiken in plaats van _NET_WM_WINDOW_TYPE_COMBOBOX

### KXMLGUI

- OCS-provider-URL repareren in about-dialoog

### NetworkManagerQt

- Overeenkomende enum-waarde AuthEapMethodUnknown gebruiken om een AuthEapMethod te vergelijken

### Plasma Framework

- Versietekst van thema verhogen omdat er nieuwe pictogrammen zijn in 5.51
- Ook configuratievenster verhogen bij opnieuw gebruik
- Ontbrekende component toevoegen: RoundButton
- Scherm-OSD-pictogrambestanden combineren en verplaatsen naar plasma pictogramthema (bug 395714)
- [Plasma Components 3 Slider] impliciete grootte van handel repareren
- [Plasma Components 3 ComboBox] items omschakelen met muiswiel
- Pictogrammen voor knoppen gebruiken wanneer aanwezig
- Gerepareerde weeknamen worden niet juist getoond in agenda wanneer de week begint met een dag anders dan maandag of zondag (bug 390330)
- [DialogShadows] offset van 0 gebruiken voor uitgeschakelde randen in Wayland

### Prison

- Weergeven van Aztec codes met een beeldverhouding != 1 repareren
- Aanname over de beeldverhouding van de barcode uit de QML integratie
- Glinsteringen repareren van glinsteringen veroorzaakt door afrondingsfouten in Code 128
- Ondersteuning voor Code 128 barcodes toevoegen

### Omschrijving

- cmake 3.0 de minimale versie maken van cmake

### QQC2StyleBridge

- Kleine standaard opvulling wanneer er een achtergrond is

### Solid

- Geen embleem tonen voor aangekoppelde schijven, alleen van afgekoppelde schijven
- [Fstab] ondersteuning voor AIX verwijderen
- [Fstab] Tru64 (**osf**) ondersteuning verwijderen
- [Fstab] niet-lege share-naam tonen in geval root fs wordt geëxporteerd (bug 395562)
- Geef ook voorkeur voor label van apparaat voor loop-apparaten

### Sonnet

- Breken van raden van taal repareren
- Voorkomen dat accentuering geselecteerde tekst wist (bug 398661)

### Accentuering van syntaxis

- i18n: extractie van themanamen repareren
- Fortran: alarmering accentueren in commentaar (bug 349014)
- vermijden dat de hoofdcontext kan worden #poped
- Bewaking van eindeloze toestandsovergang
- YAML: constante &amp; ingevouwen blokstijlen toevoegen (bug 398314)
- Logcat &amp; SELinux: verbeteringen voor de nieuwe schijnende schema's
- AppArmor: crashes repareren in open regels (in KF5.50) en verbeteringen voor de nieuwe schijnende schema's
- git://anongit.kde.org/syntax-highlighting samenvoegen
- In git te negeren zaken bijwerken
- Referentie in for-loop voor type met niet-triviale kopieconstructie gebruiken
- Repareren: e-mail accentuering voor niet gesloten haken in onderwerp-header (bug 398717)
- Perl: blokhaakjes, variabelen, referenties naar tekenreeksen en andere repareren (bug 391577)
- Bash: parameter &amp; accolade uitbreiding repareren (bug 387915)
- Overbelicht voor lichte en donkere kleurschema's toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
