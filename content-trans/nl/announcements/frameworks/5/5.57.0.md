---
aliases:
- ../../kde-frameworks-5.57.0
date: 2019-04-13
layout: framework
libCount: 70
---
### Attica

- Elke HTTP-status tussen 100 en 199 als gewoon accepteren

### Baloo

- [DocumentIdDB] niet-fout debugbericht stil maken, waarschuwing bij fouten
- [baloosearch] specificeren van een tijd toestaan bij gebruik van bijv. mtime
- [indexcleaner] verwijderen van ingevoegde mappen onder uitgeslotenen vermijden
- [MTimeDB] opzoeken van de LessEqual reeks repareren
- [MTimeDB] opzoeken repareren wanneer tijdreeks een lege set zou teruggeven
- Behandeling van toekenning/fout in MTimeDB corrigeren
- Tegen ongeldige ouders beschermen in de IdTreeDB
- Document verwijderen uit MTimeDB/DocumentTimeDB zelfs wanneer tijdstempel 0 is
- Met MIME-type detectie meer precies zijn (bug 403902)
- [timeline] Url canoniek maken
- [timeline] ontbrekende/misplaatste SlaveBase::finished() aanroepen repareren
- [balooshow] verschillende extensies van uitvoer van basis bestandsinformatie
- [timeline] waarschuwing repareren, ontbrekende UDS item voor "." toevoegen
- [balooctl] Nestinglevel voor argumenten van addOption verminderen, opschonen
- Op bijgewerkte configuraties reageren binnen indexeerder (bug 373430)
- Regressie repareren bij openen van DB in modus lezen-schrijven (bug 405317)
- [balooctl] Witruimte aan einde wissen
- [engine] code opbreken, hernoemen van Transaction::abort() terugdraaien
- Behandeling van underscore in query-parser harmoniseren
- Baloo engine: elke code niet-succes als een mislukking beschouwen (bug 403720)

### BluezQt

- Media-interface in Adapter verplaatsen
- Manager: geen Media1-interface vereisen voor initialisatie (bug 405478)
- Apparaat: pad van object in interfaces controleren verwijdert slot (bug 403289)

### Breeze pictogrammen

- Pictogrammen voor "meldingen" en "meldingen-uitgeschakeld" toevoegen (bug 406121)
- start-here-kde ook beschikbaar maken als start-here-kde-plasma
- Sublieme samenvoegpictogram
- Toepassingsspellen en invoerspellen meer contrast geven met Breeze Dark
- 24px go-up echt 24px maken
- Pictogrammen preferences-desktop-theme-applications en preferences-desktop-theme-windowdecorations toevoegen
- Symbolische koppelingen uit "preferences-desktop-theme" toevoegen aan "preferences-desktop-theme-applications"
- preferences-desktop-theme in voorbereiding om het een symbolische koppeling te maken verwijderen
- Invouwen/uitvouwen-alles toevoegen, venster-schaduw/deze weghalen toevoegen (bug 404344)
- Consistentie van window-* verbeteren en meer toevoegen
- ga-onderaan/eerste/laatste/bovenaan meer laten lijken op media-skip*
- go-up/down-search doelen van symbolische koppelingen wijzigen naar go-up/down
- Pixelrasteruitlijning verbeteren van go-up/down/next/previous/jump
- Stijl van media-skip* en media-seek* wijzigen
- Nieuwe gemuteerde pictogramstijl in alle actiepictogrammen afdwingen

### Extra CMake-modules

- De instelling van QT_PLUGIN_PATH opnieuw inschakelen
- ecm_add_wayland_client_protocol: foutmeldingen verbeteren
- ECMGeneratePkgConfigFile: alle variabelen laten afhangen van ${prefix}
- UDev zoekmodule toevoegen
- ECMGeneratePkgConfigFile: variabelen gebruikt door pkg_check_modules toevoegen
- Achterwaartse compatibiliteit van FindFontconfig voor plasma-bureaublad herstellen
- Fontconfig zoekmodule toevoegen

### Frameworkintegratie

- meer toepasselijke plasma-specifieke pictogrammen gebruiken voor categorie plasma
- plasma pictogram gebruiken als pictogram voor categorie plasma-melding

### KDE Doxygen hulpmiddelen

- URL's bijwerken naar gebruik van https

### KArchive

- Crash in KArchive::findOrCreate met gebroken bestanden repareren
- Niet geïnitialiseerd geheugen lezen in KZip repareren
- Q_OBJECT toevoegen aan KFilterDev

### KCMUtils

- [KCModuleLoader] argumenten doorgeven aan aangemaakt KQuickAddons::ConfigModule
- Focus doorgeven aan onderliggende zoekbalk wanneer KPluginSelector focus heeft (bug 399516)
- De KCM foutmelding verbeteren
- Bewaking toevoegen bij actief zijn dat pagina's KCM's zijn in KCMultiDialog (bug 405440)

### KCompletion

- Zet geen lege keuze op een niet-bewerkbare keuzelijst

### KConfig

- Mogelijkheid van melding toevoegen aan revertToDefault
- readme naar de wiki-pagina laten wijzen
- kconfig_compiler: nieuwe kcfgc argumenten HeaderExtension &amp; SourceExtension
- [kconf_update] van aangepaste loggingtechniek gaan naar qCDebug
- Referentie van const KConfigIniBackend::BufferFragment &amp; verwijderen
- KCONFIG_ADD_KCFG_FILES macro: verzeker dat wijziging van File= in kcfg wordt opgepikt

### KCoreAddons

- "* foo *" repareren, we willen deze tekenreeks niet vet maken
- Bug 401996 repareren - klikken op contact web url =&gt; niet volledige url wordt geselecteerd (bug 401996)
- Strerror afdrukken wanneer inotify mislukt (typische reden: "teveel open bestanden")

### KDBusAddons

- Twee oude-stijl verbindingen naar nieuwe-stijl converteren

### KDeclarative

- [GridViewKCM] impliciete berekening van breedte repareren
- de rasterweergave in een apart bestand verplaatsen
- Fracties in grootte van GridDelegate en uitlijning vermijden

### Ondersteuning van KDELibs 4

- Zoekmodules geleverd door ECM verwijderen

### KDocTools

- Oekraïense vertalingen bijwerken
- Catalaans bijgewerkt
- it-entities: URL's bijwerken naar gebruik van https
- URL's bijwerken naar gebruik van https
- Indonesische vertalingen gebruiken
- Ontwerp bijwerken om meer te lijken op kde.org
- Noodzakelijke bestanden toevoegen om inheemse Indonesche taal voor alle Indonesche documenten te gebruiken

### KFileMetaData

- Ondersteuning implementeren voor schrijven van informatie over waardering voor taglibwriter
- Meer tags voor taglibwriter implementeren
- Taglibwriter herschrijven om interface voor eigenschappen te gebruiken
- ffmpeg-extractor testen met behulp van MIME-type helper (bug 399650)
- Stefan Bruns voorstellen als onderhouder van KFileMetaData
- PropertyInfo declareren als QMetaType
- Tegen ongeldige bestanden waken
- [TagLibExtractor] het juiste MIME-type gebruiken in geval van overerving
- Een helper toevoegen om actuele ondersteunde bovenliggend MIME-type te bepalen
- [taglibextractor] uitpakken van eigenschappen met meerdere waarden testen
- Header voor nieuwe MimeUtils genereren
- Qt-functie voor formattering van lijst met tekenreeksen
- Aantal localisaties voor eigenschappen repareren
- MIME-types verifiëren voor alle bestaande samplebestanden, voeg er enige toe
- Helperfunctie toevoegen om MIME-type te bepalen gebaseerd op inhoud en extensie (bug 403902)
- Ondersteuning voor uitpakken van gegevens uit ogg- en ts-bestanden (bug 399650)
- [ffmpegextractor] Matroska video testgeval toevoegen (bug 403902)
- De taglibextractor herschrijven om het generieke PropertyMap interface te gebruiken (bug 403902)
- [ExtractorCollection] plug-ins voor uitpakken langzaam laden
- Extractie van beeldverhoudingseigenschap repareren
- Precisie van framesnelheidseigenschap vergroten

### KHolidays

- De categorie Poolse vakantiedagen sorteren

### KI18n

- Leesbare fout rapporteren als Qt5Widgets vereist wordt maar niet gevonden

### KIconThemes

- Opvullen van pictogram repareren die niet exact overeenkomt met de gevraagde grootte (bug 396990)

### KImageFormats

- ora:kra: qstrcmp -&gt; memcmp
- RGBHandler::canRead repareren
- xcf: niet crashen met bestanden met niet ondersteunde modi van lagen

### KIO

- currentDateTimeUtc().toTime_t() vervangen door currentSecsSinceEpoch()
- QDateTime::to_Time_t/from_Time_t vervangen door to/fromSecsSinceEpoch
- Pictogrammen voor dialoogknoppen van uitvoerbare programma's verbeteren (bug 406090)
- [KDirOperator] standaard gedetailleerde boomstructuurweergave
- KFileItem: stat() op aanvraag aanroepen, optie SkipMimeTypeDetermination toevoegen
- KIOExec: fout repareren wanneer de URL op afstand geen bestandsnaam heeft
- KFileWidget In modus opslaan van enkel bestand start, indrukken van enter/return op de KDirOperator, slotOk (bug 385189)
- [KDynamicJobTracker] Gegenereerde DBus-interface gebruiken
- [KFileWidget] Bij opslaan, bestandsnaam accentueren na klikken op bestaand bestand ook bij gebruik van dubbelklik
- Geen miniaturen aanmaken voor versleutelde kluizen (bug 404750)
- Hernoemen van WebDAV-map repareren als KeepAlive uit is
- Lijst met tags in PlacesView tonen (bug 182367)
- Bevestigingsdialoog verwijderen/in prullenbak: misleidende titel repareren
- Het juiste bestand/pad in foutmelding "too bit for fat32" tonen (bug 405360)
- Foutmelding geven met GiB, niet GB (bug 405445)
- openwithdialog: recursieve vlag in proxy-filter gebruiken
- URL's verwijderen die opgehaald worden wanneer lijst met jobs maken gereed is (bug 383534)
- [CopyJob] URL behandelen als vies bij hernoemen van bestand als oplossing van conflict
- Lokaal bestandspad doorgeven aan KFileSystemType::fileSystemType()
- Hoofd-/kleine letter hernoemen repareren bij hoofdletterongevoelig bestandssysteem
- Waarschuwingen "Ongeldige URL: QUrl("enige.txt")" repareren in opslaandialoog (bug 373119)
- Crash bij bestanden verplaatsen repareren
- Verborgen controle van NTFS repareren voor symbolische koppelingen naar NTFS aankoppelpunten (bug 402738)
- Bestand overschrijven een beetje veiliger maken (bug 125102)

### Kirigami

- listItems implicitWidth repareren
- shannon entropy om monochroom pictogram te raden
- Opvangbak voor context voorkomen om te verdwijnen
- actionmenuitembase verwijderen
- probeer niet de versie van statische builds op te halen
- [Mnemonic Handling] alleen eerste keer verschijnen vervangen
- synchroniseren wanneer een modeleigenschap is bijgewerkt
- icon.name gebruiken in terug/vooruit
- werkbalken voor lagen repareren
- Fouten repareren in kirigami voorbeeldbestanden
- Een SearchField- en PasswordField-component toevoegen
- hendelpictogrammen repareren (bug 404714)
- [InlineMessage] Geen schaduwen maken rond het bericht
- onmiddellijk indelen bij wijziging volgorde
- broodkruimelindeling repareren
- nooit werkbalk tonen wanneer het huidige item vraagt om het niet te doen
- terug/vooruit in het filter ook beheren
- terug/voorwaarts muisknoppen ondersteunen
- Langzaam verschijnen voor submenu's toevoegen
- werkbalken voor lagen repareren
- kirigami_package_breeze_icons: ook in grootte 16 zoeken
- Op Qmake gebaseerd bouwen repareren
- de aangeplakte eigenschap van het juiste item ophalen
- logica repareren bij tonen van de werkbalk
- mogelijk werkbalk uitschakelen voor pagina's van lagen
- globale werkbalk altijd tonen in globale modi
- signaal Page.contextualActionsAboutToShow
- een beetje ruimte rechts van de titel
- relayout bij wijziging van zichtbaarheid
- ActionTextField: acties op de juiste manier plaatsen
- topPadding en BottomPadding
- tekst op afbeeldingen heeft het nodig altijd wit te zijn (bug 394960)
- clip overlaysheet (bug 402280)
- parenting OverlaySheet naar ColumnView vermijden
- een qpointer gebruiken voor het thema-exemplaar (bug 404505)
- broodkruimel verbergen op pagina's die geen werkbalk willen (bug 404481)
- niet proberen de ingeschakelde eigenschap te overschrijven (bug 404114)
- Mogelijkheid voor aangepaste kop- en voettekst in ContextDrawer (bug 404978)

### KJobWidgets

- [KUiServerJobTracker] destUrl bijwerken alvorens de job te beëindigen

### KNewStuff

- URL's naar https omschakelen
- Koppeling naar fsearch-project bijwerken
- Niet ondersteunde OCS commando's behandelen en niet overstemmen (bug 391111)
- Nieuwe locatie voor KNSRC bestanden
- [knewstuff] qt5.13 verouderde methode verwijderen

### KNotification

- [KStatusNotifierItem] tips voor bureaubladitem versturen
- Aangepaste tips instellen voor meldingen toestaan

### KNotifyConfig

- Selecteren van alleen ondersteunde geluidsbestanden toestaan (bug 405470)

### KPackage-framework

- Zoeken naar het host-tools-targets-bestand in de Android dockeromgeving repareren
- Ondersteuning voor kruislings compileren voor kpackagetool5 toevoegen

### KService

- X-GNOME-UsesNotifications toevoegen als herkende sleutel
- minimum versie van 2.4.1 van bison toevoegen vanwege %code

### KTextEditor

- Reparatie: de tekstkleurren van het gekozen schema toepassen (bug 398758)
- DocumentPrivate: optie "Auto Reload Document" toevoegen aan menu Beeld (bug 384384)
- DocumentPrivate: instellen van woordenboek bij selectie van blok ondersteunen
- Woorden &amp; tekenreeks op katestatusbalk repareren
- Minimap met QtCurve stijl repareren
- KateStatusBar: Vergrendelpictogram tonen op gewijzigd label wanneer in modus alleen-lezen
- DocumentPrivate: automatische aanhalingstekens overslaan wanneer het lijkt dat ze al gebalanceerd zijn(bug 382960)
- Variabele toevoegen aan interface van KTextEditor::KTextEditor
- relaxeer code om alleen toe te kennen in bouwen van debug, werken in bouwen voor vrijgave
- compatibiliteit verzekeren met oude configuratie
- meer gebruik maken van generiek configuratie-interface
- QString KateDocumentConfig::eolString() vereenvoudigen
- sonnet instelling overzetten naar KTextEditor instelling
- zorg nu voor gaten in configuratie sleutels
- meer zaken naar generiek configuratie-interface converteren
- meer gebruik maken van het generieke configuratie-interface
- generiek configuratie-interface
- Niet crashen op verkeerd gevormde bestanden met syntaxisaccentuering
- IconBorder: gebeurtenissen slepen&amp;loslaten accepteren (bug 405280)
- ViewPrivate: selectie ongedaan maken met pijltjestoetsen handiger maken (bug 296500)
- Reparatie voor tonen van tipboomstructuur over argument op niet-primair scherm
- Een verouderde methode overzetten
- Het ingevouwen zoekbericht herstellen naar zijn voormalige type en positie (bug 398731)
- ViewPrivate: 'Woord afbreken toepassen' comfortabel maken (bug 381985)
- ModeBase::goToPos: ga na dat sprongdoel geldig is (bug 377200)
- ViInputMode: niet ondersteunde tekstattributen uit de statusbalk verwijderen
- KateStatusBar: knop voor woordenboek toevoegen
- voorbeeld voor probleem met regelhoogte toevoegen

### KWidgetsAddons

- KFontRequester consistent maken
- kcharselect-data bijwerken tot Unicode 12.0

### KWindowSystem

- Vervaging-/achtergrondcontrast verzenden in apparaatpixels (bug 404923)

### NetworkManagerQt

- WireGuard: marshalling/demarshalling van geheimen uit map werkend maken
- Ontbrekende ondersteuning voor WireGuard toevoegen in basis instellingklasse
- Wireguard: private sleutel als geheim behandelen
- Wireguard: eigenschap van peers zouden NMVariantMapList moeten zijn
- Wireguard verbindingtypeondersteuning toevoegen
- ActiveConnecton: stateChangedReason-signaal toevoegen waar we de reden kunnen xien van wijziging in status

### Plasma Framework

- [AppletInterface] op corona controleren alvorens er toegang tot zoeken
- [Dialog] geen gebeurtenis voorwaarts bij zweven wanneer er nergens voorwaarts toe is te gaan
- [Menu] aangestoten signaal repareren
- Belangrijkheid van sommige debuginformatie verminderen zodat actuele waarschuwingen gezien worden
- [PlasmaComponents3 ComboBox] textColor repareren
- FrameSvgItem: wijzigingen in marge van FrameSvg vangen ook buiten eigen methoden
- Theme::blurBehindEnabled() toevoegen
- FrameSvgItem: textureRect voor tiled subitems repareren om niet naar 0 te krimpen
- Achtergrond van breeze-dialoog met Qt 5.12.2 repareren (bug 405548)
- Crash in plasmashell verwijderen
- [Icon Item] Ook afbeeldingspictogram verwijderen bij gebruik van Plasma Svg (bug 405298)
- hoogte van tekstveld alleen gebaseren op zichtbare tekst (bug 399155)
- bind alternateBackgroundColor

### Omschrijving

- KDE Connect SMS plug-in toevoegen

### QQC2StyleBridge

- de Plasma bureaubladstijl ondersteunt kleuren van pictogrammen
- [SpinBox] Gedrag ban muiswiel verbeteren
- een beetje opvulling in werkbalken toevoegen
- RoundButton-pictogrammen repareren
- opvulling van schuifbalk baseren op alle afgeleiden
- zoek naar een schuifweergave om zijn schuifbalk voor marges te nemen

### Solid

- Bouwen zonder UDev op Linux toestaan
- Alleen clearTextPath ophalen bij gebruik

### Accentuering van syntaxis

- Syntaxis definities voor Elm-taal naar syntaxisaccentuering toevoegen
- AppArmor &amp; SELinux: één indentatie in XML-bestanden verwijderen
- Doxygen: geen zwarte kleur in tags gebruiken
- Context omschakelen bij regeleinde in lege regels toestaan (bug 405903)
- endRegion invouwen in regels met beginRegion+endRegion repareren (gebruik length=0) (bug 405585)
- Extensies naar groovy accentuering toevoegen (bug 403072)
- Syntaxisaccentueringsbestand voor Smali toevoegen
- "." als weakDeliminator in Octave syntaxisbestand toevoegen
- Logcat: dsError kleur met underline="0" repareren
- crash bij accentuering repareren voor gebroken hl-bestand
- target link libraries bewaken voor oudere CMake versie (bug 404835)

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
