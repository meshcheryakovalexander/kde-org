---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Algemeen

- Een aantal reparaties voor compileren met het komende Qt 5.5

### KActivities

- Activiteiten starten en stoppen is gerepareerd
- Voorbeeld van activiteit die soms een verkeerde achtergrondafbeelding toonde

### KArchive

- Maak tijdelijke bestanden aan in de tijdelijke map in plaats van in de huidige map

### KAuth

- Generatie van KAuth DBus helper-service-bestanden gerepareerd

### KCMUtils

- Gerepareerd toekennen wanneer dbus-paden een '.' bevatten

### KCodecs

- Toegevoegde ondersteuning voor CP949 aan KCharsets

### KConfig

- kconf_update verwerkt niet langer *.upd bestanden uit KDE SC 4. Voeg "Version=5" toe bovenaan het upd-bestand voor bijwerken die moeten worden toegepast op Qt5/KF5-toepassingen
- KCoreConfigSkeleton gerepareerd bij omschakelen van een waarde met tussendoor opslaan

### KConfigWidgets

- KRecentFilesAction: volgorde van menu-items gerepareerd (zodat het overeenkomt met de volgorde in kdelibs4)

### KCoreAddons

- KAboutData: roep addHelpOption en addVersionOption automatisch aan, voor gemak en consistentie
- KAboutData: breng "Gebruik http://bugs.kde.org om bugs te rapporteren." terug wanneer geen ander e-mailadres/url is ingesteld
- KAutoSaveFile: allStaleFiles() werkt nu zoals verwacht voor lokale bestanden, ook staleFiles() gerepareerd
- KRandomSequence gebruikt nu intern int's en biedt int-api voor 64-bits ondubbelzinnigheid
- Mimetype-definities: *.qmltypes en *.qmlproject bestanden hebben ook het mime-type text/x-qml
- KShell: zorg er voor dat quoteArgs urls aanhaalt met QChar::isSpace(), ongebruikelijke spaties werden niet juist behandeld
- KSharedDataCache: aanmaken van een map met de cache repareren (porteren van bug)

### KDBusAddons

- Helpermethodo KDEDModule::moduleForMessage toegevoegd voor het schrijven van meer op kded lijkende daemons, zoals kiod

### KDeclarative

- Een plottercomponent toegevoegd
- Overloadmethode voor Formats::formatDuration toegevoegd die een int accepteert
- Nieuwe eigenschappen paintedWidth en paintedHeight toegevoegd aan QPixmapItem en QImageItem
- Tekenen met QImageItem en QPixmapItem gerepareerd

### Kded

- Ondersteuning voor het laden van kded modules met JSON metadata

### KGlobalAccel

- Bevat nu de component bij uitvoeren, waarmee dit een tier3-framework is
- De Windows-backend weer werkend gemaakt
- De Mac-backend weer ingeschakeld
- Crash in KGlobalAccel X11 runtime shutdown gerepareerd

### KI18n

- Markeer resultaten zoals vereist om te waarschuwen wanneer API misbruikt wordt
- Optie BUILD_WITH_QTSCRIPT bij bouwsysteem toegevoegd om een gereduceerde feature-set toe te staan op ingebedde systemen

### KInit

- OSX: laad de juiste gedeelde bibliotheken bij uitvoeren
- Reparaties bij compileren van Mingw

### KIO

- Crash in jobs gerepareerd bij koppelingen naar KIOWidgets maar alleen bij gebruik van een QCoreApplication
- Bewerken van websnelkoppelingen gerepareerd
- Optie KIOCORE_ONLY toegevoegd om alleen KIOCore en zijn helperprogramma's te compileren, maar niet KIOWidgets of KIOFileWidgets, waarmee de noodzakelijke afhankelijkheden zeer gereduceerd worden
- Klasse KFileCopyToMenu toegevoegd, die Kopiëren naar / Verplaatsen naar aan popup-menus toevoegt
- Protocollen met SSL ingeschakeld: ondersteuning voor TLSv1.1 en TLSv1.2 protocollen toegevoegd, SSLv3 verwijderd
- negotiatedSslVersion en negotiatedSslVersionName gerepareerd om het actueel onderhandelde protocol terug te geven
- De ingevoerde URL toepassen op de weergave bij klikken op de knop die de URL-navigator terugschakelt naar de broodkruimelmodus
- Twee voortgangsbalken/dialogen gerepareerd die verschijnen bij jobs voor kopiëren/verplaatsen
- KIO gebruikt nu zijn eigen daemon, kiod, voor "out-of-process services", die eerder in kded draaiden, om afhankelijkheden te verminderen; vervangt nu alleen kssld
- Fout "Could not write to &lt;path&gt;" gerepareerd wanneer kioexec wordt gestart
- "QFileInfo::absolutePath gerepareerd: gemaakt met waarschuwing "lege bestandsnaam" bij gebruik van KFilePlacesModel

### KItemModels

- KRecursiveFilterProxyModel gerepareerd voor Qt 5.5.0+, vanwege QSortFilterProxyModel die nu de rollenparameter naar het signaal dataChanged gebruikt

### KNewStuff

- Laad xml gegevens opnieuw van url's op afstand

### KNotifications

- Documentatie: de vereisten aan bestandsnamen van .notifyrc bestanden worden genoemd
- Hangende pointer naar KNotification gerepareerd
- Lek in knotifyconfig gerepareerd
- Installeer ontbrekende header in knotifyconfig

### KPackage

- Manpagina van kpackagetool hernoemd tot kpackagetool5
- Installatie op bestandssystemen ongevoelig voor hoofd- en kleine letters gerepareerd

### Kross

- Kross::MetaFunction gerepareerd zodat het werkt met het meta-objectsysteem van Qt5

### KService

- Neem onbekende eigenschappen mee bij converteren van KPluginInfo uit KService
- KPluginInfo: gerepareerde eigenschappen die niet gekopieerd worden uit KService::Ptr
- OS X: reparatie van prestaties voor kbuildsycoca4 (skip app-bundels)

### KTextEditor

- Schuiven met hoge precisie met het touchpad gerepareerd
- Stuur geen documentUrlChanged uit bij herladen
- De cursorpositie bij document herladen in regels met tabs niet breken
- Vouw de eerste regel niet (uit/in) als het handmatig was (in/uit)gevouwen
- vimode: geschiedenis van commando's via pijltjestoetsen
- Probeer geen samenvatting te maken wanneer we een KDirWatch::deleted() signaal ontvangen
- Prestatie: verwijder globale initialisaties

### KUnitConversion

- Oneindige recursie gerepareerd in Unit::setUnitMultiplier

### KWallet

- Detecteer en converteer oude ECB portefeuilles naar CBC
- Het ECB encryptie-algoritme is gerepareerd
- De lijst met portefeuilles wordt bijgewerkt wanneer een portefeuillebestand verwijderd wordt van de schijf
- Verwijder vreemde &lt;/p&gt; in tekst zichtbaar voor de gebruiker

### KWidgetsAddons

- Gebruik kstyleextensions om aangepaste besturingselementen te specificeren voor rendering van kcapacity-bar indien ondersteund, dit stelt het widget in staat om op de juiste stijl aan te nemen
- Bied een toegankelijke naam voor KLed

### KWindowSystem

- NETRootInfo::setShowingDesktop(bool) gerepareerd bij werken op Openbox
- Gemaksmethode toegevoegd aan KWindowSystem::setShowingDesktop(bool)
- Reparaties in format behandeling van pictogrammen
- Methode NETWinInfo::icccmIconPixmap toegevoegd die pictogram-pixmap levert uit eigenschap WM_HINTS
- Overladen toegevoegd aan KWindowSystem::icon wat roundtrips naar de X-Server vermindert
- Ondersteuning voor _NET_WM_OPAQUE_REGION toegevoegd

### NetworkmanagerQt

- Druk geen bericht af over eigenschap niet behandelde "AccessPoints"
- ondersteuning voor NetworkManager 1.0.0 toegevoegd (niet vereist)
- Afhandeling van VpnSetting-secrets gerepareerd
- Klasse GenericSetting voor verbindingen niet beheerd door NetworkManager toegevoegd
- Eigenschap AutoconnectPriority aan ConnectionSettings toegevoegd

#### Plasma framework

- Foutieve opening van een gebroken contextmenu bij middelklik in Plasma-popup gerepareerd
- Startsignaal-knopschakelaar op muiswiel
- Maak een dialoogvak nooit groter dan het scherm
- Panelen verwijderen wanneer applet worden verwijderd
- Sneltoetsen gerepareerd
- Ondersteuning van hint-apply-kleurschema hersteld
- De configuratie opnieuw laden wanneer plasmarc wijzigt
- ...

### Solid

- Toegevoegd energyFull en energyFullDesign aan Batterij

### Wijzigingen aan het bouwsysteem (extra-cmake-modules)

- Nieuwe module ECMUninstallTarget om een target te maken voor installatie ongedaan maken
- Zorg dat KDECMakeSettings ECMUninstallTarget standaard importeert
- KDEInstallDirs: geef een waarschuwing over het mengen van relatieve en absolute installatiepaden op de commandoregel
- ECMAddAppIcon module toegevoegd om pictogrammen toe te voegen aan uit te voeren doelen op Windows en Mac OS X
- CMP0053 waarschuwing gerepareerd met CMake 3.1
- Haal instellingen in cachevariabelen niet weg in KDEInstallDirs

### Frameworkintegratie

- Reparatie aan het bijwerken van instellingen bij een enkele klik bij uitvoeren
- Meerdere reparaties aan de integratie met het systeemvak
- Installeer het kleurenschema op widgets op het topniveau (om QQuickWidgets te repareren)
- Werk XCursor instellingen bij op het X11-platform

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
