---
aliases:
- ../../kde-frameworks-5.38.0
date: 2017-09-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Ontbrekend in de aankondiging van vorige maand: in KF5 is een nieuw framework, Kirigami, ingevoegd, een set van QtQuick componenten om gebruikersinterfaces te bouwen gebaseerd op de KDE UX richtlijnen

### Baloo

- Op mappen gebaseerd zoeken gerepareerd

### Extra CMake-modules

- CMAKE_*_OUTPUT_5.38 zetten om tests uit te voeren zonder installeren
- Een module invoegen voor zoeken naar qml-import als afhankelijkheid tijdens uitvoeren

### Frameworkintegratie

- Wis-pictogram voor regelbewerking met hoge resolutie teruggeven
- Dialogen accepteren met ctrl+return wanneer knoppen hernoemd zijn repareren

### KActivitiesStats

- De zoekopdracht herschrijven die gekoppelde en gebruikte hulpbronnen combineren
- Het model opnieuw laden wanneer de hulpbron wordt ontkoppeld
- De zoekopdracht repareren wanneer gekoppelde en gebruikte hulpbronnen worden gemengd

### KConfig

- Labels va DeleteFile/RenameFile acties repareren (bug 382450)
- kconfigini: voorloopwitruimte verwijderen bij lezen van waarden van items (bug 310674)

### KConfigWidgets

- KStandardAction::Help en KStandardAction::SaveOptions als verouderd markeren
- Labels va DeleteFile/RenameFile acties repareren (bug 382450)
- "Document-sluiten" gebruiken als pictogram voor KStandardAction::close

### KCoreAddons

- DesktopFileParser: terugvalopzoeken in ":/kservicetypes5/*" toevoegen
- Ondersteuning toegevoegd voor niet geïnstalleerde plug-ins in kcoreaddons_add_plugin
- desktopfileparser: niet-compliant sleutel/waarde ontleding repareren (bug 310674)

### KDED

- X-KDE-OnlyShowOnQtPlatforms ondersteunen

### KDocTools

- CMake: doelnaam afkorting repareren wanneer bouwmap speciale tekens heeft (bug 377573)
- CC BY-SA 4.0 International toevoegen en het als standaard instellen

### KGlobalAccel

- KGlobalAccel: overzetten naar nieuwe methode symXModXToKeyQt van KKeyServer, om toetsen op numerieke pad te repareren (bug 183458)

### KInit

- klauncher: appId overeenkomend voor flatpak apps repareren

### KIO

- Overbrengen van de websneltoetsen KCM van KServiceTypeTrader naar KPluginLoader::findPlugins
- [KFilePropsPlugin] Locale-format totalSize gedurende berekening
- KIO: al langer bestaand geheugenlek bij beëindigen repareren
- MIME-type filtering functies toevoegen aan KUrlCompletion
- KIO: de URI-filterplug-ins overbrengen van KServiceTypeTrader naar json+KPluginMetaData
- [KUrlNavigator] tabRequested uitsturen wanneer plaats in menu een middenklik krijgt (bug 304589)
- [KUrlNavigator] tabRequested uitsturen wanneer plaatsselector een middenklik krijgt (bug 304589)
- [KACLEditWidget] dubbelklikken toestaan om item te bewerken
- [kiocore] de logische fout in vorige commit repareren
- [kiocore] controleren dat klauncher actief is of niet
- Echte snelheidslimiet INF_PROCESSED_SIZE berichten (bug 383843)
- De SSL CA certificaatopslag van Qt niet wissen
- [KDesktopPropsPlugin] bestemmingsmap aanmaken als deze niet bestaat
- [KIO slavevan bestand] toepassen speciale bestandsattributen repareren (bug 365795)
- Controle op bezige loop in TransferJobPrivate::slotDataReqFromDevice verwijderen
- kiod5 een "agent" op Mac maken
- Proxy KCM die handmatig laden van proxies niet juist doet repareren

### Kirigami

- schuifbalken verbergen wanneer ze geen nut hebben
- Basis voorbeeld toevoegen voor aanpassen van kolombreedte van te verslepen handvat
- ider-lagen in positionering van handvatten
- plaatsing van handvat bij overlap in de laatste pagina repareren
- gefingeerd handvat op de laatste kolom niet tonen
- geen zaken opslaan stuff in de gedelegeerden (bug 383741)
- omdat we al keyNavigationEnabled zetten, ook afbreking zetten
- betere links uitlijnen voor de terugkop (bug 383751)
- geen tweemaal rekening houden met de header bij schuiven (bug 383725)
- breek de header-labels niet af
- address FIXME: resetTimer verwijderen (bug 383772)
- toepassingsheader in niet-mobiel niet wegschuiven
- Een eigenschap om het PageRow scheidingsteken te verbergen als het overeenkomt met AbstractListItem toevoegen
- schuiven met originY en onder-naar-boven vloeien repareren
- Waarschuwingen over instellen van zowel pixel- als puntgroottes wegwerken
- bereikbare modus bij omgekeerde weergaven niet triggeren
- met paginavoet rekening houden
- een iets complexer voorbeeld van een chat-app toevoegen
- veiliger bij falen om de juist voettekst te vinden
- Geldigheid van item controleren alvorens deze te gebruiken
- Laagpositie voor isCurrentPage honoreren
- een animatie gebruiken in plaats van een animator (bug 383761)
- benodigde ruimte voor paginavoet laten, indien mogelijk
- beter minder fel voor laden met item voor toepassing
- achtergrond minder fel voor item van toepassing
- marges van knop voor terug juist repareren
- juiste marges voor knop voor terug
- minder waarschuwingen in header van toepassing
- geen schaling van plasma gebruiken voor pictogramgroottes
- nieuw uiterlijk voor handvatten

### KItemViews

### KJobWidgets

- De status van de knop "Pauzeren" initialiseren in de widget-tracker

### KNotification

- Opstart van meldingenservice niet blokkeren (bug 382444)

### KPackage-framework

- kpackagetool wegsturen van langdradige opties

### KRunner

- Vorige acties wissen bij bijwerken
- Uitvoerders op afstand over DBus toevoegen

### KTextEditor

- Document/View scripting API overbrengen naar op QJSValue gebaseerde oplossing
- Pictogrammen tonen in contextmenu met pictogramrand
- KStandardAction::PasteText vervangen door KStandardAction::Paste
- Met fracties schalen ondersteunen bij generatie van het zijbalkvoorbeeld
- Van QtScript naar QtQml omschakelen

### KWayland

- Invoer-RGB-buffers behandelen als vooraf vermenigvuldigd
- SurfaceInterface-uitvoer bijwerken wanneer een globale instelling voor uitvoer  wordt verwijderd
- KWayland::Client::Surface uitvoer verwijdering volgen
- Verzending van aanbiedingen van gegevens uit een ongeldige bron vermijden (bug 383054)

### KWidgetsAddons

- setContents vereenvoudigen door Qt meer van het werk te laten doen
- KSqueezedTextLabel: isSqueezed() voor het gemak toevoegen
- KSqueezedTextLabel: kleine verbeteringen aan API-documenten
- [KPasswordLineEdit] focus van proxy op regelbewerking instellen (bug 383653)
- [KPasswordDialog] eigenschap van geometrie resetten

### KWindowSystem

- KKeyServer: behandeling van KeypadModifier repareren (bug 183458)

### KXMLGUI

- Een flink aantal stat() aanroepen opsparen bij opstarten van de toepassing
- Positie KHelpMenu repareren onder Wayland (bug 384193)
- Gebroken behandeling van klik met midden-knop laten vallen (bug 383162)
- KUndoActions: actionCollection gebruiken om de sneltoets in te stellen

### Plasma Framework

- [ConfigModel] waken tegen toevoegen van een nul-ConfigCategory
- [ConfigModel] Programmatisch toevoegen en verwijderen van ConfigCategory toestaan (bug 372090)
- [EventPluginsManager] pluginPath in model zichtbaar maken
- [Icon Item] niet nodeloos imagePath weghalen
- [FrameSvg] QPixmap::mask() gebruiken in plaats van verouderde manier via alphaChannel()
- [FrameSvgItem] object margins/fixedMargins op aanvraag aanmaken
- controlestatus voor menu-items repareren
- Plasma-stijl voor QQC2 in applets afdwingen
- De PlasmaComponents.3/privémap installeren
- Overblijfsels van "locolor" thema's laten vallen
- [Theme] KConfig SimpleConfig gebruiken
- Enige niet noodzakelijk opzoeken van thema-inhoud vermijden
- zeldzame gebeurtenissen om grootte te wijzigen naar lege grootten (bug 382340)

### Accentuering van syntaxis

- Syntaxisdefinitie voor filterlijsten in Adblock Plus toevoegen
- De Sieve-syntaxisdefinitie herschrijven
- Accentuering voor QDoc configuratiebestanden toevoegen
- Definitie voor accentuering voor Tiger toevoegen
- Escape minteken in reguliere expressies in rest.xml (bug 383632)
- reparatie: platte tekst is geaccentueerd als powershell
- Syntaxisaccentuering voor Metamath toevoegen
- Accentuering van syntaxis in less op SCSS one kreeg nieuwe basis (bug 369277)
- Accentuering voor Pony toevoegen
- De definitie van e-mailsyntaxis herschrijven

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
