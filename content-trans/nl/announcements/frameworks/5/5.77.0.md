---
aliases:
- ../../kde-frameworks-5.77.0
date: 2020-12-12
layout: framework
libCount: 83
qtversion: 5.13
---
### Attica

* Crash in laden van leverancier repareren door controleren van antwoordaanwijzer vóór referentie weghalen (bug 427974)

### Baloo

* [DocumentUrlDB] onderliggend lijstitem ui DB verwijderen indien leeg
* Presentatiedocumenttype voor Office OpenXML diashow en sjabloon toevoegen
* [MetaDataMover] opzoeken van ouderdocument-id repareren
* [DocumentUrlDB] methode voor triviaal hernoemen en verplaatsen toevoegen
* [MetaDataMover] hernoemen van een DB de enige bewerking maken
* [Document] Ouderdocument-ID toevoegen en bevolk het
* Alle directe aanroepen van syslog vervangen door gecategoriseerde logberichten

### Breeze pictogrammen

* Varianten van tekstveld toevoegen: -frameless (-> text-field), -framed
* Symbolische naam symlink voor input-* pictogrammen toevoegen
* Pictogram voor True Type XML lettertype toevoegen
* Ondertitel-toevoegen toevoegen
* MathML pictogram wijzigen om een formule te gebruiken en specifiek mime-type
* Pictogram voor QEMU schijfimage en SquashFS-image toevoegen
* Pictogrammen voor actie bewerken-verplaatsen toevoegen
* Pictogram voor core-dumps toevoegen
* Een stapel MIME-typen van ondertitels toevoegen
* nutteloos vervagen uit pictogram voor kontrast verwijderen

### Extra CMake-modules

* Extractie van categorie uit desktop-bestanden repareren
* Variabele voor installatiemap definiëren voor bestandssjablonen
* Generatie van fastlane metagegevens voor Android bouwen toevoegen
* (Qt)WaylandScanner: bestanden juist markeren als SKIP_AUTOMOC

### KActivitiesStats

* ResultModel: hulpbron MimeType blootstellen
* Filtering van gebeurtenissen voor bestanden en mappen toevoegen (bug 428085)

### KCalendarCore

* Onderhouder repareren, die Allen moet zijn, niet mijzelf :)
* Ondersteuning voor eigenschap CONFERENCE
* alarmsTo gemaksmethod toevoegen aan agenda
* Controleer dat overdag herhalingen niet vóór dtStart gaan

### KCMUtils

* Hack die multi-level kcms in modus pictogram brak verwijderen

### KConfig

* KConfigGroup::copyTo met KConfigBase::Notify repareren (bug 428771)

### KCoreAddons

* Crash voorkomen wanneer factory leeg is (wanneer we een fout terugkrijgen)
* KFormat: meer relatieve gevallen van datum tijd toevoegen
* KPluginFactory inschakelen om optioneel KPluginMetaData aan plug-ins door te geven

### KDAV

* Het verwijderen omdat het te veel fouten maakt

### KDeclarative

* Marges uit AbstractKCM in SimpleKCM synchroniseren
* Afgekeurde licentietekst verwijderen
* Licentie van bestand wijzigen naar LGPL-2.0-of-later
* Licentie van bestand wijzigen naar LGPL-2.0-of-later
* Licentie van bestand wijzigen naar LGPL-2.0-of-later
* Licentie van bestand wijzigen naar LGPL-2.0-of-later
* KeySequenceItem (en helper) herschrijven om KeySequenceRecorder te gebruiken (bug 427730)

### KDESU

* Escaped aanhalingstekens juist ontleden
* doas(1) ondersteuning van OpenBSD toevoegen

### KFileMetaData

* Enige lekken in OpenDocument en Office OpenXML uitpakkers repareren
* Verschillende subtypes voor OpenDocument en OpenXML documenten toevoegen

### KGlobalAccel

* Statistieken gekoppelde kglobalacceld interfaceplug-ins laden

### KDE GUI-addons

* Schorsing van sneltoetsen laten werken uit ophalen-gaan (bug 407395)
* Potentiële crash repareren in afsluiten van starter van wayland (bug 429267)
* CMake: Zoek Qt5::GuiPrivate wanneer ondersteuning van Wayland is ingeschakeld
* KeySequenceRecorder toevoegen als basis voor KKeySequenceWidget en KeySequenceItem (bug 407395)

### KHolidays

* Afronden van positiegebeurtenissen van de zon minder dan 30 s voor het volgende uur repareren
* Ontleden van elk vakantiebestand tweemaal in defaultRegionCode() vermijden
* De astro-seizoenen slecht een keer per voorkomen berekenen
* Opzoeken van vakantieregio voor ISO 3166-2 codes repareren
* HolidayRegion kopieerbar/verplaatsbaar maken
* Ondersteuning toegevoegd voor berekenen van civiele tijden van schemering

### KIdleTime

* Statisch gekoppelde systeempoller-plug-ins laden

### KImageFormats

* Kleurdiepte niet langer naar 8 verlagen voor 16 bits niet-gecomprimeerde PSD-bestanden

### KIO

* NewFile-dialoog: toestaan om map maken te accepteren voordat stat actief is geweest (bug 429838)
* DeleteJob thread niet laten lekken
* KUrlNavBtn: openen van submappen uit afrolmenu met het toetsenbord laten werken (bug 428226)
* DropMenu: vertaalde sneltoetsen gebruiken
* [ExecutableFileOpenDialog] op knop Annuleren focusseren
* Weergave plaatsen: plaats alleen accentueren wanneer het wordt getoond (bug 156678)
* Eigenschap aan acties van tonen-plug-in in submenu "Acties" toevoegen
* Nieuw geïntroduceerde methode verwijderen
* KIO::iconNameForUrl: pictogram voor bestanden op afstand gebaseerd op naam oplossen (bug 429530)
* [kfilewidget] nieuwe standaard sneltoets gebruiken voor "Map aanmaken"
* Laden van contextmenu opnieuw ontwerpen en meer schaalbaar maken
* RenameDialog: overschrijven toestaan wanneer bestanden ouder zijn (bug 236884)
* DropJob: nieuwe edit-move-pictogram voor 'Hierheen verplaatsen' gebruiken
* moc_predefs.h gen repareren wanneer ccache is ingeschakeld via -DCMAKE_CXX_COMPILER=ccache CMAKE_CXX_COMPILER_ARG1=g++
* Qt 5.13 is nu vereist, dus ifdef verwijderen
* KComboBox naar QComboBox overzetten
* Reparatie van document verzocht door Méven Car @meven
* Enige lussen opnieuw ontwerpen met moderne C++
* Dode code opschonen
* Onnodige controle verwijderen als sleutel bestaat
* Code voor RequiredNumberOfUrls vereenvoudigen
* Enige blanco regels verwijderen
* Code vereenvoudigen en meer consistent maken
* ioslaves: root-rechten van remote:/ repareren
* KFileItem: isWritable gebruikt KProtocolManager voor bestanden op afstand
* Overladen toevoegen om acties aan het menu "Acties" eerder te plaatsen
* Moderne stijl van coderen gebruiken
* Geen onnodige scheidingstekens toevoegen (bug 427830)
* Een van accolades voorziene initializer_list gebruiken in plaats van operator <<
* MkPathJob: voorwaardelijk gecompileerde code herschrijven om leesbaarheid te verbeteren

### Kirigami

* GlobalDrawer-kop de positie van ToolBars/TabBars/DialogButtonBoxes laten instellen
* aan inViewport vastgemaakte eigenschap
* kopverschuiving repareren bij touchscreen
* AbstractapplicationHeader met een nieuw "ScrollIntention" concept opnieuw ontwerpen
* op bureaubladen ankers altijd aan ouder vullen
* [controls/BasicListItem]: niet verankeren aan niet-bestaand item
* Geen avatartekst tonen bij smalle grootte
* # en @ verwijderen uit het extractieproces van initieel van avatar
* Eigenschap initialiseren in sizeGroup
* muisinteractie gebruiken bij isMobile voor gemakkelijker testen
* Hotfix voor voorloop/naloop met gebruik van naloopwaarden voor voorloop scheidingstekenmarge
* [controls/BasicListItem]: voorloop/naloop-eigenschappen toevoegen
* positionering van vel bij wijziging van inhoud repareren
* Oud gedrag in brede modus toepassen en geen topMargin in FormLayout toevoegen
* Formulierindeling op een smal scherm repareren
* U kunt geen AbstractListItem in een SwipeListItem gebruiken
* "Kan niet toekennen [ongedefinieerd] aan int" in OverlaySheet repareren
* [overlaysheet]: Geen rare transitie uitvoeren wanneer hoogte van inhoud wijzigt
* [overlaysheet]: wijziging in hoogte animeren
* Positionering van overlay-vel repareren
* index altijd instellen bij klikken op een pagina
* Slepen van FAB's in modus RTL repareren
* Scheidingsteken in lijst verfijnen (bug 428739)
* tekenhandvaten in modus RTL repareren
* Rendering van randen met de juiste grootte met software terugval repareren (bug 427556)
* Software terugvalitem niet plaatsen buiten rechthoekitemgrenzen met schaduw
* fwidth() voor gebruiken voor gladstrijken in lage energiemodus (bug 427553)
* Een achtergrondkleur ook renderen in modus weinig energie
* Transparante rendering inschakelen voor Shadowed(Border)Texture in lage energie
* Geen alfacomponenten annuleren voor rechthoek met schaduw in lage energiemodus
* Geen lagere gladstrijkwaarde gebruiken bij renderen van textuur van ShadowedBorderTexture
* "uitsnij"-stappen uit rechthoek met schaduw en gerelateerde oprollers verwijderen
* icon.name gebruiken in plaats van iconName in de documentatie
* [Avatar] pictogram een pictogramgrootte laten gebruiken dichtbij de tekstgrootte
* [Avatar] de initialen meer ruimte laten gebruiken en verticale uitlijning verbeteren
* [Avatar] brongrootte en gladstrijkeigenschappen blootstellen voor iedereen die de grootte wil animeren
* [Avatar] de brongrootte instellen om te voorkomen dat afbeeldingen wazig worden
* [Avatar] De kleur van de voorgrond eenmalig instellen
* [Avatar] achtergrondverloop wijzigen
* [Avatar] randbreedte wijzigen naar 1px om overeen te laten komen met andere randbreedten
* [Avatar] opvullen, verticalPadding en horizontalPadding altijd laten werken
* [avatar]: verloop van kleur toevoegen

### KItemModels

* KRearrangeColumnsProxyModel: alleen kolom 0 heeft afgesplitsten

### KMediaPlayer

* Bestanden voor player & engine servicetype def. installeren op overeenkomend type bestandsnaam

### KNewStuff

* Installatie ongedaan maken repareren wanneer het item niet in de cache is
* Wanneer we aanroepen voor controle op bijwerken, verwachten we deze (bug 418082)
* QWidgets-dialoog opnieuw gebruiken (bug 429302)
* Wikkel blokkering van compatibiliteit in KNEWSTUFFCORE_BUILD_DEPRECATED_SINCE
* Niet in cache schrijven voor tussenliggende statussen
* enum voor compressie ongedaan maken gebruiken, in plaats van waarden in tekenreeksen
* Te vroeg verdwijnen van item uit bij te werken pagina repareren (bug 427801)
* DetailsLoadedEvent enum aan nieuw signaal toevoegen
* Adoptie-API opnieuw bewerken (bug 417983)
* Een aantal wurgers met oude URL van leveranciers repareren
* Item uit cache verwijderen alvorens nieuw item toe te voegen (bug 424919)

### KNotification

* Geen voorbijgaande tip doorgeven (bug 422042)
* Fout met hoofd-/kleine lettergevoeligheid van AppKit-kop op macOS repareren
* Geen ongeldige meldingacties oproepen
* Behandeling van geheugen voor notifybysnore repareren

### KPackage-framework

* X-KDE-PluginInfo-Depends laten vallen

### KParts

* Methode embed() afkeuren, vanwege gebrek aan gebruik
* KParts KPluginMetaData laten gebruiken in plaats van KAboutData

### KQuickCharts

* Algoritme voor lijn gladstrijken opnieuw bewerken
* Interpolatie toepassen verplaatsen naar stap polijsten
* Puntdelegaties op de juiste manier centreren op lijngrafiek en zet grootte in lijn met breedte
* Een keuzevak "glad" toevoegen aan voorbeeld van lijngrafiek
* Verzeker dat puntdelegaties van lijngrafiek juist worden gewist
* Naam in tekstballon ook tonen op voorbeeldpagina van lijngrafiek
* LineChartAttached documenteren en een typo in LineChart documenten repareren
* Eigenschappen naam en shortName toevoegen aan LineChartAttached
* Eigenschap pointDelegate meer doorwrocht documenteren
* Lid previousValues verwijderen en gestapelde lijngrafieken repareren
* pointDelegate in voorbeeld lijnengrafiek gebruiken om waarden te tonen bij er boven zweven
* Ondersteuning toegevoegd voor "point delegate" aan lijngrafieken
* LineChart: berekening van punt verplaatsen van updatePaintNode naar polish

### KRunner

* Overblijfsels van KDE4 pakketten afkeuren
* Gebruik maken van nieuwe KPluginMetaData plug-in constructorondersteuning van KPluginLoader

### KService

* [kapplicationtrader] API-documenten repareren
* KSycoca: DB opnieuw aanmaken wanneer versie < verwachte versie
* KSycoca: hulpbronbestanden van KMimeAssociation in de gaten houden

### KTextEditor

* KComboBox naar QComboBox overzetten
* KSyntaxHighlighting themeForPalette gebruiken
* i18n aanroep repareren, ontbrekend argument (bug 429096)
* De automatische selectie van thema verbeteren

### KWidgetsAddons

* Geen dubbel signaal passwordChanged verzenden
* KMessageDialog toevoegen, en async-centric variant van KMessageBox
* De oude standaard pop-upmodus van KActionMenu herstellen
* KActionMenu overzetten naar QToolButton::ToolButtonPopupMode

### KWindowSystem

* Statisch gekoppelde integratieplug-ins laden
* Overzetten van pid() naar processId()

### KXMLGUI

* HideLibraries introduceren en HideKdeVersion afkeuren
* KKeySequenceWidget herschrijven om KeySequenceRecorder te gebruiken (bug 407395)

### Plasma Framework

* [Representation] alleen opvulling van boven/onderkant verwijderen wanneer kop/voettekst zichtbaar is
* [PlasmoidHeading] techniek uit Representation gebruiken voor inset/margins
* Een component representatie toevoegen
* [Bureaubladthema] hint-inset-side-margin hernoemen naar hint-side-inset
* [FrameSvg] insetMargin naar inset hernoemen
* [PC3] PC3 schuifbalk in ScrollView gebruiken
* [Breeze] inset-hint rapporteren
* [FrameSvg*] shadowMargins hernoemen naar inset
* [FrameSvg] schaduwmarges in cache en voorvoegsels honoreren
* De animatie beëindigen voor het wijzigen van de lengte van de accentuering van de voortgangsbalk (bug 428955)
* [textfield] opschoonknop met overlappende tekst repareren (bug 429187)
* Afrolmenu op juiste globale positie tonen
* gzip -n gebruiken om ingebedde bouwtijden te voorkomen
* KPluginMetaData gebruiken om containmentActions te tonen
* Laden van packageStructure uit KPluginTrader overzetten (porten)
* KPluginMetaData gebruiken om lijst te maken van DataEngines 
* [TabBar] accentuering op toetsenbordfocus toevoegen
* [FrameSvg*] marges van schaduwen blootstellen
* Waarde van MarginAreasSeparator helderder maken
* [TabButton] centraal pictogram en tekst uitlijnen wanneer tekst naast het pictogram staat
* [SpinBox] logische fout in richting van schuiven repareren
* mobiele schuifbalk in RTL modus repareren
* [PC3 ToolBar] randen niet uitschakelen
* [PC3 ToolBar] juiste svg-marge-eigenschappen gebruiken voor opvulling
* [pc3/scrollview] pixelAligned verwijderen
* Margegebieden toevoegen

### Omschrijving

* [bluetooth] delen van meerdere bestanden repareren (bug 429620)
* Vertaalde actielabel van plug-in lezen (bug 429510)

### QQC2StyleBridge

* Knop: op omlaag vertrouwen, niet ingedrukt voor stijlen
* De grootte van ronde knoppen op mobiel verkleinen
* mobiele schuifbalk in RTL modus repareren
* voortgangsbalk in RTL modus repareren
* RTL weergave voor RangeSlider repareren

### Solid

* errno.h invoegen voor EBUSY/EPERM
* FstabBackend: DeviceBusy teruggeven waar umount is mislukt bij EBUSY (bug 411772)
* Detectie van recente libplist en libimobiledevice repareren

### Accentuering van syntaxis

* afhankelijkheden van de gegenereerde bestanden repareren
* indexer: enige problemen repareren en 2 controleurs (capture-groep en sleutelwoord met scheidingsteken) uitschakelen
* indexer: alle xml-bestanden in geheugen laden voor gemakkelijk controleren
* C++ accentuering: bijwerken tot Qt 5.15
* syntaxisgeneratoren opnieuw starten wanneer het broncodebestand is gewijzigd
* systemd eenheid: bijwerken naar systemd v247
* ILERPG: vereenvoudigen en testen
* Zsh, Bash, Fish, Tcsh: truncate en tsort in sleutelwoorden van unixcommand toevoegen
* Latex: enige math-omgevingen kunnen genest worden (bug 428947)
* Bash: veel reparaties en verbeteringen
* --syntax-trace=stackSize toevoegen
* php.xml: overeenkomst endforeach repareren
* bestThemeForApplicationPalette verplaatsen van KTextEditor hier
* debchangelog: Trixie toevoegen
* alert.xml: `NOQA` toevoegen nog een andere populaire let-op in broncode
* cmake.xml: Upstream heeft besloten `cmake_path` uit te stellen voor de volgende uitgave

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
