---
aliases:
- ../../kde-frameworks-5.63.0
date: 2019-10-14
layout: framework
libCount: 70
---
### Breeze pictogrammen

- KFloppy pictogram verbeteren (bug 412404)
- Actiepictogrammen format-text-underline-squiggle toevoegen (bug 408283)
- Pictogram voor "preferences-desktop-navigation" toevoegen (bug 406900)
- App-pictogrammen toegevoegd voor app Kirogi Drone besturing
- Scripts toevoegen om een weblettertype te maken uit alle breeze actiepictogrammen
- Pictogram lettertype-inschakelen en lettertype-uitschakelen voor kfontinst KCM toevoegen
- Grote pictogrammen systeem-opnieuw-opstarten draaiend in een niet-consistente richting repareren (bug 411671)

### Extra CMake-modules

- nieuwe module ECMSourceVersionControl
- FindEGL repareren bij gebruik van Emscripten
- ECMAddQch: INCLUDE_DIRS argument toevoegen

### Frameworkintegratie

- nagaan dat winId() niet wordt aangeroepen op niet-inheemse widgets (bug 412675)

### kcalendarcore

Nieuwe module, eerder bekend als kcalcore in kdepim

### KCMUtils

- Muisgebeurtenissen in KCM's onderdrukken die vensters verplaatsen
- marges aanpassen van KCMultiDialog (bug 411161)

### KCompletion

- [KComboBox] Qt's ingebouwde aanvulling op de juiste manier uitschakelen [reparatie van regressie]

### KConfig

- Genereren van eigenschappen die beginnen met een hoofdletter repareren

### KConfigWidgets

- KColorScheme compatibel maken met QVariant

### kcontacts

Nieuwe module, eerder onderdeel van KDE PIM

### KCoreAddons

- KListOpenFilesJob toevoegen

### KDeclarative

- QQmlObjectSharedEngine-context verwijderen in sync met QQmlObject
- [KDeclarative] Overzetten van afgekeurde QWheelEvent::delta() naar angleDelta()

### Ondersteuning van KDELibs 4

- NetworkManager 1.20 ondersteunen en compile echt de NM-backend

### KIconThemes

- De globale [Small|Desktop|Bar]Icon() methoden afkeuren

### KImageFormats

- Bestanden voor testen toevoegen bug411327
- xcf: regressie repareren bij lezen van bestanden met "niet-ondersteunde" eigenschappen
- xcf: de resolutie van afbeelding juist lezen
- HDR (Radiance RGBE) afbeeldingslader overzetten naar Qt5

### KIO

- [Places panel] de sectie Recent opgeslagen opknappen
- [DataProtocol] compileren zonder impliciete conversie uit ascii
- Het gebruik van WebDAV methoden als voldoende beschouwen voor aannemen van WebDAV
- REPORT ondersteunt ook de kopregel Depth
- QSslError::SslError &lt;-&gt; KSslError::Error voor conversie herbruikbaar maken
- De KSslError::Error ctor van KSslError afkeuren
- [Windows] lijst maken van de oudermap van C:foo, die C: is en niet C:
- Crash repareren bij verlaten in kio_file (bug 408797)
- == en != operatoren toevoegen aan KIO::UDSEntry
- KSslError::errorString vervangen door QSslError::errorString
- Move/copy job: stat van hulpbronnen overslaan als in de doelmap niet is te schijven (bug 141564)
- Interactie met DOS/Windows uitvoerbare programma's in KRun::runUrl gerepareerd
- [KUrlNavigatorPlacesSelector] teardown-actie juist identificeren (bug 403454)
- KCoreDirLister: crash repareren bij aanmaken van nieuwe mappen uit kfilewidget (bug 401916)
- [kpropertiesdialog] pictogrammen toevoegen voor de sectie grootte
- Pictogrammen voor menu's "Openen met" en "Acties" toevoegen
- Een onnodige variabele initialiseren vermijden
- Meer functionaliteit verplaatsen uit KRun::runCommand/runApplication naar KProcessRunner
- [Advanced Permissions] Pictogramnamen repa  (bug 411915)
- [KUrlNavigatorButton] gebruik van QString repareren om geen [] buiten grenzen te gebruiken
- Zorgen dat KSslError een QSslError intern houd
- KSslErrorUiData afsplitsen van KTcpSocket
- kpac uit QtScript overzetten

### Kirigami

- altijd slechts het laatste item in de cache zetten
- maar z (bug 411832)
- import-versie in PagePoolAction repareren
- PagePool is Kirigami 2.11
- rekening houden met snelheid van slepen wanneer een flick eindigt
- Kopiëren van URL's naar het klembord repareren
- meer controleren of we een actueel item een nieuw ouder geven
- basis ondersteuning voor ListItem-acties
- cachePages introduceren
- compatibiliteit met Qt5.11 repareren
- PagePoolAction introduceren
- nieuwe klasse: PagePool om pagina's te recycleren nadat ze verschenen
- tabbalken er beter uit laten zien
- enige marges rechts (bug 409630)
- "Kleinere pictogramgroottes compenseren op mobiel in de ActionButton" terugdraaien
- lijstitems niet inactief laten lijken (bug 408191)
- "Schaling van eenheid van pictogramgrootte voor isMobile" terugdraaien
- Layout.fillWidth zou gedaan moeten worden door de client (bug 411188)
- Sjabloon voor ontwikkeling van toepassing Kirigami toevoegen
- Een modus om acties te centreren toevoegen en laat de titel weg bij gebruik van een werkbalkstijl (bug 402948)
- Kleinere pictogramgroottes compenseren op mobiel in de ActionButton
- Enige ongedefinieerde fouten tijdens uitvoeren van eigenschappen gerepareerd
- ListSectionHeader achtergrondkleur voor enige kleurenschema's repareren
- Aangepast item voor inhoud verwijderen uit scheidingsteken in ActionMenu

### KItemViews

- [KItemViews] Overzetten naar niet afgekeurde QWheelEvent API

### KJobWidgets

- aan dbus gerelateerde objecten vroeg genoeg opschonen om hangen bij verlaten van programma te vermijden

### KJS

- startsWith(), endsWith() en includes() JS tekenreeksfuncties toegevoegd
- Date.prototype.toJSON() aangeroepen op non-Date objecten gerepareerd

### KNewStuff

- KNewStuffQuick naar functiepariteit brengen met KNewStuff(Widgets)

### KPeople

- Android claimen als een ondersteund platform
- Standaard avatar gebruiken via qrc
- plug-in-bestanden bundelen op Android
- DBus-gedeelten op Android uitschakelen
- Crash repareren bij monitoren van een contact die verwijderd wordt op PersonData (bug 410746)
- Volledig gekwalificeerde typen op signalen gebruiken

### KRunner

- Beschouw UNC-paden als NetworkShare-context

### KService

- Amusement naar Games-map in plaats van Games &gt; Toys (bug 412553)
- [KService] kopieerconstructor toevoegen
- [KService] workingDirectory() toevoegen, keur af path()

### KTextEditor

- artifacts proberen te vermijden in tekstvoorbeeld
- Variabele expansie: std::functie intern gebruiken
- QRectF in plaats van QRect lost problemen met afsnijden op, (bug 390451)
- volgende artifact door rendering verdwijnt als u de clip-rect een beetje aanpast (bug 390451)
- de magie van lettertype kiezen vermijden en zet anti-aliasing af (bug 390451)
- KadeModeMenuList: geheugenlekken en andere zaken repareren
- probeer te zoeken naar bruikbare lettertypen, werkt redelijk goed als u geen domme schaalfactor gebruikt zoals 1,1
- Modus van menu Statusbalk: lege QIcon opnieuw gebruiken die impliciet wordt gedeeld
- KTextEditor::MainWindow::showPluginConfigPage() laten zien
- QSignalMapper vervangen met lambda
- KateModeMenuList: QString() voor lege tekenreeksen gebruiken
- KateModeMenuList: sectie "Beste zoekovereenkomsten" toevoegen en reparaties voor Windows
- Variabele expansie: QTextEdits ondersteunen
- Sneltoets toevoegen voor omschakelen van invoermodi om menu te bewerken (bug 400486)
- Variabele uitvouwdialoog: selectiewijziging en activatie van item juist behandelen
- Uitvouwdialoog van variabele: filter van regelbewerking toevoegen
- Reservekopie maken bij opslaan: vervangen van tijd- en datumtekenreeks ondersteunen (bug 403583)
- Uitvouwen van variabele: teruggeefwaarde heeft voorkeur boven teruggeefargument
- Initiële start van dialoog van variabelen
- nieuw formaat API gebruiken

### KWallet Framework

- Ondersteuning voor HiDPI

### KWayland

- Bestanden alfabetisch sorteren in lijst van cmake

### KWidgetsAddons

- OK-knop te configureren maken in KMessageBox::sorry/detailedSorry
- [KCollapsibleGroupBox] QTimeLine::start waarschuwing bij uitvoeren repareren
- Naamgeving van KTitleWidget pictogrammethoden verbeteren
- QIcon-instellers voor de wachtwoorddialogen toevoegen
- [KWidgetAddons] overzetten naar niet-afgekeurde Qt API

### KWindowSystem

- XCB instellen naar vereist bij bouwen van de X-backend
- Minder gebruik maken van afgekeurde enum alias NET::StaysOnTop

### KXMLGUI

- Item "Modus Volledigscherm" verplaatsen van menu Instellingen naar menu Beeld (bug 106807)

### NetworkManagerQt

- ActiveConnection: stateChanged() signaal verbinden met juiste interface

### Plasma Framework

- Plasma core lib log categorie exporteren, een categorie aan een qWarning toevoegen
- [pluginloader] gecategoriseerde logging gebruiken
- editMode een corona globale eigenschap maken
- Globale animatiesnelheidsfactor honoreren
- gehele plasmacomponent3 juist installeren
- [Dialog] Venstertype toepassen na wijzigen van vlaggen
- Logica van knop Wachtwoord onthullen wijzigen
- Crash bij neerhalen met ConfigLoader van applet repareren (bug 411221)

### QQC2StyleBridge

- Verschillende bouwsysteemfouten repareren
- marges uit qstyle nemen
- [Tab] grootte bepalen repareren (bug 409390)

### Accentuering van syntaxis

- Syntaxisaccentuering toevoegen voor RenPy (.rpy) (bug 381547)
- WordDetect regel: scheidingen detecteren aan de binnenste rand van de tekenreeks
- GeoJSON bestanden accentueren alsof ze platte JSON zijn
- Syntaxisaccentuering voor SubRip Text (SRT) Subtitles toevoegen
- skipOffset met dynamische RegExpr repareren (bug 399388)
- bitbake: ingebedde shell en python behandelen
- Jam: identifier in een SubRule repareren
- Syntaxisdefinitie voor Perl6 toevoegen (bug 392468)
- .inl extensie voor C++ ondersteunen, op dit moment niet gebruikt door andere xml-bestanden (bug 411921)
- *.rej voor accentuering van diff ondersteunen (bug 411857)

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
