---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: KDE stelt de Release Candidate van KDE Applicaties 16.04 beschikbaar
layout: application
release: applications-16.03.90
title: KDE stelt de Release Candidate van KDE Applicaties 16.04 beschikbaar
---
7 april 2016. Vandaag heeft KDE de "release candidate" van de nieuwe versies van KDE Applications vrijgegeven. Met het bevriezen van afhankelijkheden en functies, is het team van KDE nu gefocust op repareren van bugs en verder oppoetsen.

Met de verschillende toepassingen gebaseerd op KDE Frameworks 5, weet de KDE Applications 16.04 uitgave de kwaliteit en gebruikservaring te handhaven en te verbeteren. Echte gebruikers zijn kritisch in het proces om de hoge kwaliteit van KDE te handhaven, omdat ontwikkelaars eenvoudig niet elke mogelijke configuratie kunnen testen. We rekenen op u om in een vroeg stadium bugs te vinden zodat ze gekraakt kunnen worden voor de uiteindelijke vrijgave. Ga na of u met het team mee kunt doen door de uitgavekandidaat te installeren <a href='https://bugs.kde.org/'>en elke bug te rapporteren</a>.

#### Installeren van KDE Applications 16.04 Release Candidate binaire pakketten

<em>Pakketten</em>. Sommige Linux/UNIX OS leveranciers zijn zo vriendelijk binaire pakketten van KDE Applications 16.04 Release Candidate (intern 16.03.90) voor sommige versies van hun distributie en in andere gevallen hebben vrijwilligers uit de gemeenschap dat gedaan. Extra binaire pakketten, evenals updates voor de pakketten zijn nu beschikbaar of zullen beschikbaar komen in de komende weken.

<em>Pakketlocaties</em>. Voor een huidige lijst met beschikbare binaire pakketten waarover het KDE Project is geïnformeerd, bezoekt u de <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki van de gemeenschap</a>.

#### KDE Applications 16.04 Release Candidate compileren

De complete broncode voor KDE Applications 16.04 Release Candidate kan <a href='http://download.kde.org/unstable/applications/15.11.90/src/'>vrij gedownload</a> worden. Instructies over compileren en installeren zijn beschikbaar vanaf de <a href='/info/applications/applications-16.03.90'>Informatiepagina van KDE Applications Release Candidate</a>.
