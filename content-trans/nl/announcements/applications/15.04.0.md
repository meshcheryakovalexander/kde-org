---
aliases:
- ../announce-applications-15.04.0
changelog: true
date: '2015-04-15'
description: KDE stelt Applicaties 15.04 beschikbaar.
layout: application
title: KDE stelt KDE Applicaties 15.04.0 beschikbaar
version: 15.04.0
---
15 april 2015. Vandaag heeft KDE Applications 15.04 uitgegeven. Met deze uitgave zijn een totaal van 72 toepassingen overgezet naar <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. Het team streeft er naar om de beste kwaliteit op uw bureaublad te brengen en deze toepassingen. We rekenen op uw om ons uw terugkoppeling te zenden.

In deze uitgave zijn er nieuwe toevoegingen op de lijst met op KDE Frameworks 5 gebaseerde toepassingen, bevattende <a href='https://www.kde.org/applications/education/khangman/'>KHangMan</a>, <a href='https://www.kde.org/applications/education/rocs/'>Rocs</a>, <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, <a href='https://www.kde.org/applications/development/kompare'>Kompare</a>, <a href='https://kdenlive.org/'>Kdenlive</a>, <a href='https://userbase.kde.org/Telepathy'>KDE Telepathy</a> en <a href='https://games.kde.org/'>enige KDE Games</a>.

Kdenlive is een van de beste softwarepakketten voor video-bewerking. Het heeft recent zijn <a href='https://community.kde.org/Incubator'>incubatieproces</a> beëindigd om een officieel KDE project te worden en is overgebracht naar KDE Frameworks 5. Het team achter dit meesterwerk heeft besloten dat Kdenlive samen uitgegeven zou moeten worden met KDE Applications. Enige nieuwe functies zijn automatisch opslaan van nieuwe projecten en een vaste stabilisatie van clips.

KDE Telepathy is het hulpmiddel voor instant messaging. Het is overgezet naar KDE Frameworks 5 en Qt5 en is een nieuw lid van de uitgave van KDE Applications. Het is grotendeels compleet, behalve het gebruikersinterface voor audio- en video-aanroep dat nog steeds ontbreekt.

Waar mogelijk gebruikt KDE bestaande technologie zoals gedaan is met het nieuwe KAccounts die ook wordt gebruikt in SailfishOS en Unity van Canonical. Binnen KDE Applications, wordt het nu alleen gebruikt door KDE Telepathy. Maar in de toekomst zal het breder worden gebruikt door toepassingen zoals Kontact en Akonadi.

In de <a href='https://edu.kde.org/'>KDE Educatiemodule</a>, kreeg Cantor enige nieuwe functies rond zijn ondersteuning van Python: een nieuwe Python 3 backend en nieuwe categorieën Vers van de pers. Rocs is volledig op zijn kop gezet: de kern graph-theory is herschreven, scheiding in gegevensstructuur is verwijderd en een meer algemeen graph-document als centrale graph-entiteit is geïntroduceerd evenals een grote herziening van de scripting-API voor graph-algorithmen die nu alleen een geünificeerde API biedt. KHangMan is overgezet naar QtQuick en heeft een verse lik verf in het proces ontvangen. En Kanagram ontving een nieuwe modus met twee spelers en de letters zijn nu aan te klikken knoppen en kunnen ingetypt worden zoals eerder.

Naast de gebruikelijke reparaties van bugs kreeg <a href='https://www.kde.org/applications/development/umbrello/'>Umbrello</a> deze keer enige verbeteringen in bruikbaarheid en stabiliteit. Verder kan de de functie Zoeken nu begrenst kan worden door categorie: klasse, interface, pakket, bewerkingen of attributen.
