---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDE stelt Applicaties 19.08 beschikbaar.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE stelt Applicaties 19.08.3 beschikbaar
version: 19.08.3
---
{{% i18n_date %}}

Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../19.08.0'>KDE Applicaties 19.08</a> Deze uitgave bevat alleen reparaties van bugs en bijgewerkte vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan een dozijn aangegeven reparaties van bugs, die verbeteringen omvatten aan Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle, Umbrello, naast andere.

Verbeteringen bevatten:

- In de videobewerker Kdenlive verdwijnen composities niet langer bij heropening van een project met vergrendelde tracks
- De annotatieweergave van Okular toont nu aanmaaktijden in de lokale tijdzone in plaats van UTC
- Besturing met toetsenbord is verbeterd in het hulpmiddel voor schermafdrukken, Spectacle
