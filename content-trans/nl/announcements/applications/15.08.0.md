---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE stelt Applicaties 15.08.0 beschikbaar.
layout: application
release: applications-15.08.0
title: KDE stelt KDE Applicaties 15.08.0 beschikbaar
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphin met het nieuwe uiterlijk - nu gebaseerd op KDE Frameworks 5` class="text-center" width="600px" caption=`Dolphin met het nieuwe uiterlijk - nu gebaseerd op KDE Frameworks 5`>}}

19 augustus 2015. Vandaag heeft KDE KDE Frameworks i5.08 uitgebracht.

Met deze uitgave zijn een totaal van 107 toepassingen overgezet naar <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. Het team streeft er naar om de beste kwaliteit op uw bureaublad te brengen en deze toepassingen. We rekenen op uw om ons uw terugkoppeling te zenden.

In deze uitgave zijn er nieuwe toevoegingen op de lijst met op KDE Frameworks 5 gebaseerde toepassingen, bevattende <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>de Suite Kontact</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Kontact-suite technische vooruitblik

In de laatste paar maanden heeft het team  van KDE PIM veel inspanning gestoken in het overbrengen van Kontact naar Qt 5 en KDE Frameworks 5. Bovendien is de prestatie voor toegang tot de gegevens aanzienlijk verbeterd door een geoptimaliseerde communicatielaag. Het team van KDE PIM werkt hard aan het verder oppoetsen van de Suite Kontact en kijkt uit naar uw terugkoppeling. Voor meer en gedetailleerde informatie over wat er in KDE PIM in gewijzigd zie <a href='http://www.aegiap.eu/kdeblog/'>Laurent Montels blog</a>.

### Kdenlive en Okular

Deze uitgave van Kdenlive bevat veel reparaties in de DVD-assistent, samen met een groot aantal bug-reparaties en andere functies die de integratie omvatten van grotere herschrijvingen. Meer informatie over de wijzigingen in Kdenlive zijn te zien in zijn <a href='https://kdenlive.org/discover/15.08.0'>extensieve wijzigingslog</a>. Okular ondersteunt nu vervagingsovergang in de presentatiemodus.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku met een puzzel met tekens` class="text-center" width="600px" caption=`KSudoku met een puzzel met tekens`>}}

### Dolphin, Edu en Spellen

Dolphin is ook overgebracht naar KDE Frameworks 5. Marble kreeg verbeteringen in ondersteuning van <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a> evenals betere ondersteuning voor annotaties, bewerken en  KML-overlays.

Ark heeft een verbazingwekkend aantal commits gezien inclusief vele kleine reparaties. Kstars ontving een groot aantal commits, inclusief verbetering van het flat ADU-algoritme en controle op buitenissige waarden, opslaan van Meridian Flip, Guide Deviation en Autofocus HFR limiet in het volgordebestand en toevoeging van telescopesnelheid en ondersteuning van unpark. KSudoku is gewoon beter geworden. Commits omvatten: GUI en engine toevoegen voor invoer in Mathdoku en Killer Sudoku puzzels en een nieuwe oplosser gebaseerd op het  algoritme van Donald Knuth's Dancing Links (DLX).

### Andere uitgaven

Samen met deze uitgave zal Plasma 4 worden gepubliceerd in zijn LTS-versie voor de laatste keer als versie 4.11.22.
