---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE stelt KDE Applicaties 14.12.1 beschikbaar.
layout: application
title: KDE stelt KDE Applicaties 14.12.1 beschikbaar
version: 14.12.1
---
11 januari 2018. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../14.12.0'>KDE Applicaties 17.12</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Er zijn meer dan 50 aangegeven bugreparaties inclusief verbeteringen aan het archiveringshulpmiddel Ark, de UML-modeller Umbrello, de documentviewer Okular, de toepassing voor uitspraak leren, en de client voor een bureaublad op afstand KRDC.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van Plasma Workspaces 4.11.15, KDE Development Platform 4.14.4 en de Kontact Suite 4.14.4.
