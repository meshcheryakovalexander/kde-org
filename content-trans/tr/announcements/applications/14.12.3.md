---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE Uygulamalar 14.12.3'ü Gönderdi.
layout: application
title: KDE, KDE Uygulamaları 14.12.3'ü Gönderdi
version: 14.12.3
---
March 3, 2015. Today KDE released the third stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen 19 hata düzeltmesiyle, anagram oyunu Kanagram, Umbrello UML Modeller, belge görüntüleyici Okular ve geometri uygulaması Kig'de iyileştirmeler içerir.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.17, KDE Development Platform 4.14.6 and the Kontact Suite 4.14.6.
