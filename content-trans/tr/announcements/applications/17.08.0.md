---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE, KDE Uygulamaları 17.08.0'ı Gönderdi
layout: application
title: KDE, KDE Uygulamaları 17.08.0'ı Gönderdi
version: 17.08.0
---
August 17, 2017. KDE Applications 17.08 is here. We have worked to make both the applications and the underlying libraries more stable and easier to use. By ironing out wrinkles and listening to your feedback, we have made the KDE Applications suite less prone to glitches and much friendlier. Enjoy your new apps!

### More Porting to KDE Frameworks 5

Kdelibs4'e dayanan aşağıdaki uygulamaların artık KDE Frameworks 5'e dayandığından memnunuz: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat ve umbrello. Zamanlarını gönüllü olarak harcayan ve bunun gerçekleşmesi için çalışan çalışkan geliştiricilere teşekkürler.

### What's new in KDE Applications 17.08

#### Dolphin

Dolphin developers report that Dolphin now shows 'Deletion Time' in the Trash, and shows 'Creation Time' if the OS supports it such as on BSDs.

#### KIO-Extras

Kio-Extras artık Samba paylaşımları için daha iyi destek sağlıyor.

#### KAlgebra

KAlgebra geliştiricileri, Kirigami ön uçlarını masaüstünde geliştirmek için çalıştılar ve kod tamamlamayı uyguladılar.

#### Kontak

- KMailtransport'ta, geliştiriciler akonadi taşıma desteğini yeniden etkinleştirdiler, eklenti desteği oluşturdular ve sendmail posta taşıma desteğini yeniden oluşturdular.
- In SieveEditor, a lot of bugs in autocreate scripts have been fixed and closed. Along with general bug-fixing, a regexp editor line-editor has been added.
- K Posta'da bir dış düzenleyici kullanma yeteneği bir eklenti olarak yeniden oluşturuldu.
- The Akonadi-import-wizard now has the 'convert all converter' as a plugin, so that developers can create new converters easily.
- Applications now depend on Qt 5.7. Developers have fixed a lot of compile errors on Windows. All of kdepim does not compile on Windows yet but the developers have made big progress. To begin, the developers created a craft-recipe for it. Much bug fixing has been done to modernize code (C++11). Wayland support on Qt 5.9. Kdepim-runtime adds a facebook resource.

#### Kdenlive

In Kdenlive, the team fixed the broken 'Freeze effect'. In recent versions, it was impossible to change the frozen frame for the freeze effect. Now a keyboard shortcut for Extract Frame feature is allowed. Now the user can save screenshots of their timeline with a keyboard shortcut, and a name is now suggested based on frame number <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. Fix downloaded transition lumas do not appear in interface: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Fix audio clicks issue (for now, requires building the dependency MLT from git until a MLT release): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>.

#### Krfb

Developers have finished porting the X11 plugin to Qt5, and krfb is working again using a X11 backend that is much faster than the Qt plugin. There's a new Settings page, allowing the user to change preferred framebuffer plugin.

#### Konsole

Konsole now allows unlimited scrollback to extend past 2GB (32bit) limit. Now Konsole allows users to enter any location to store scrollback files. Also, a regression was fixed, Konsole can once again allow KonsolePart to call the Manage Profile dialog.

#### KAppTemplate

In KAppTemplate there is now an option to install new templates from the filesystem. More templates have been removed from KAppTemplate and instead integrated into related products; ktexteditor plugin template and kpartsapp template (ported to Qt5/KF5 now) have become part of KDE Frameworks KTextEditor and KParts since 5.37.0. These changes should simplify creation of templates in KDE applications.

### Bug Stomping

Kontak Takımı, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsole ve daha fazlası dahil olmak üzere uygulamalarda 80'den fazla hata çözüldü!

### Full Changelog
