---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE Uygulamalar 19.04.2.'yi Gönderdi
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE, KDE Uygulamaları 19.04.2'yi Gönderdi
version: 19.04.2
---
{{% i18n_date %}}

Bugün KDE, <a href='../19.04.0'>KDE Uygulamaları 19.04</a> için ikinci kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

Nearly fifty recorded bugfixes include improvements to Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular, Spectacle, among others.

İyileştirmeler şunları içerir:

- A crash with viewing certain EPUB documents in Okular has been fixed
- Gizli anahtarlar yine Kleopatra kriptografi yöneticisinden dışa aktarılabilir
- The KAlarm event reminder no longer fails to start with newest PIM libraries
