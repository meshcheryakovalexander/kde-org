---
aliases:
- ../announce-applications-17.08-rc
date: 2017-08-04
description: KDE Uygulamalar 17.08  Sürüm Adayını Gönderdi.
layout: application
release: applications-17.07.90
title: KDE, KDE Uygulamalar 17.08 Sürüm Adayını Gönderdi
---
4 Ağustos 2017. Bugün KDE, KDE Uygulamalarının yeni sürümlerinin yayın adayını yayınladı. Bağımlılık ve özellik donmalarıyla birlikte, KDE ekibinin odak noktası artık hataları düzeltmek ve daha fazla parlatmaktır.

Check the <a href='https://community.kde.org/Applications/17.08_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

The KDE Applications 17.08 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
