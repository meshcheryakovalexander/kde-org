---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE, KDE Uygulamalar 18.08.0'ı Gönderdi
layout: application
title: KDE, KDE Uygulamalar 18.08.0'ı Gönderdi
version: 18.08.0
---
16 Ağustos 2018. KDE Uygulamaları 18.08.0 çıktı.

We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!

### What's new in KDE Applications 18.08

#### Sistem

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, has received various quality-of-life improvements:

- 'Ayarlar' iletişim kutusu, tasarım yönergelerimize daha iyi uyacak ve daha sezgisel olacak şekilde çağdaşlaştırıldı.
- Bilgisayarınızı yavaşlatabilecek çeşitli bellek sızıntıları giderildi.
- Çöp kutusu görüntülenirken 'Yeni Oluştur' menü öğeleri artık kullanılamaz.
- Uygulama artık yüksek çözünürlüklü ekranlara daha iyi uyum sağlıyor.
- The context menu now includes more useful options, allowing you to sort and change the view mode directly.
- Değişiklik zamanına göre sıralama artık 12 kat daha hızlı. Ayrıca, kök kullanıcı hesabıyla oturum açtığınızda artık Dolphin'i yeniden başlatabilirsiniz. Dolphin'i normal bir kullanıcı olarak çalıştırırken kökün sahip olduğu dosyaları değiştirme desteği halen devam etmektedir.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Multiple enhancements for <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, are available:

- 'Bul' pencere öğesi artık iş akışınızı kesintiye uğratmadan pencerenin üst kısmında görünecektir.
- Support for more escape sequences (DECSCUSR & XTerm Alternate Scroll Mode) has been added.
- Artık herhangi bir karakteri kısayol tuşu olarak da atayabilirsiniz.

#### Grafikler

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 is a major release for <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer. Over the last months contributors worked on a plethora of improvements. Highlights include:

- Gwenview'in durum çubuğu artık bir görüntü sayacı içeriyor ve toplam görüntü sayısını gösteriyor.
- Artık derecelendirmeye göre ve azalan düzende sıralama yapmak mümkün. Tarihe göre sıralama artık dizinleri ve arşivleri ayırıyor ve bazı durumlarda düzeltildi.
- Sürükle ve bırak desteği, dosyaları ve klasörleri görüntülemek için Görünüm moduna sürüklemenize ve görüntülenen öğeleri harici uygulamalara sürüklemenize izin verecek şekilde geliştirilmiştir.
- Pasting copied images from Gwenview now also works for applications which only accept raw image data, but no file path. Copying modified images is now supported as well.
- Görüntü yeniden boyutlandırma iletişim kutusu daha fazla kullanılabilirlik için elden geçirildi ve görüntüleri yüzdeye göre yeniden boyutlandırma seçeneği eklendi.
- Red Eye Reduction tool's size slider and crosshair cursor were fixed.
- Transparent background selection now has an option for 'None' and can be configured for SVGs as well.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

Görüntü yakınlaştırma daha rahat hale geldi:

- Enabled zooming by scrolling or clicking as well as panning also when the Crop or Red Eye Reduction tools are active.
- Middle-clicking once again toggles between Fit zoom and 100% zoom.
- Added Shift-middle-clicking and Shift+F keyboard shortcuts for toggling Fill zoom.
- Ctrl-clicking now zooms faster and more reliably.
- Gwenview now zooms to the cursor's current position for Zoom In/Out, Fill and 100% zoom operations when using the mouse and keyboard shortcuts.

Görüntü karşılaştırma modu birkaç geliştirme aldı:

- Seçim vurgusunun sabit boyutu ve hizalaması.
- SVG'lerin seçim vurgusuyla örtüşmesi düzeltildi.
- Küçük SVG görüntüleri için, seçim vurgusu görüntü boyutuyla eşleşir.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

İş akışınızı daha da eğlenceli hale getirmek için bir dizi küçük iyileştirme getirildi:

- Farklı boyutlardaki görüntüler ve asetatlar arasındaki solma geçişleri geliştirildi.
- Açık renk düzeni kullanıldığında bazı hareketli düğmelerdeki simgelerin görünürlüğü düzeltildi.
- Bir görüntüyü yeni bir adla kaydederken, izleyici daha sonra ilgisiz bir görüntüye atlamaz.
- When the share button is clicked and kipi-plugins are not installed, Gwenview will prompt the user to install them. After the installation they are immediately displayed.
- Kenar çubuğu artık yeniden boyutlandırırken yanlışlıkla gizlenmeyi önlüyor ve genişliğini hatırlıyor.

#### Ofis

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>K Posta</a>, KDE'nin güçlü e-posta istemcisi, yolculuk verisi çıkarma işletkesinde bazı iyileştirmeler aldı. Artık UIC 918.3 ve SNCF tren bileti kare kodlarını ve Wikidata tarafından desteklenen tren istasyonu konumu aramayı destekliyor. Çoklu yolcular için belge desteği eklendi ve K Posta artık KDE Yol Kılavuzu ile tümleşik olarak çalışıyor.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, the personal information management framework, is now faster thanks to notification payloads and features XOAUTH support for SMTP, allowing for native authentication with Gmail.

#### Eğitim

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE's frontend to mathematical software, now saves the status of panels (\"Variables\", \"Help\", etc.) for each session separately. Julia sessions have become much faster to create.

User experience in <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, our graphing calculator, has been significantly improved for touch devices.

#### İzlenceler

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Contributors to <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, focused on improving the Rectangular Region mode:

- Dikdörtgen Bölge modunda, piksel açısından mükemmel bir seçim dikdörtgeni çizmenize yardımcı olacak bir büyüteç var.
- Artık seçim dikdörtgenini klavyeyi kullanarak taşıyabilir ve yeniden boyutlandırabilirsiniz.
- Kullanıcı arayüzü, kullanıcının renk şemasını takip eder ve yardım metninin sunumu iyileştirilmiştir.

Ekran görüntülerinizi başkalarıyla paylaşmayı kolaylaştırmak için, paylaşılan görüntülerin bağlantıları artık otomatik olarak panoya kopyalanmaktadır. Ekran görüntüleri artık otomatik olarak kullanıcı tarafından belirlenen alt dizinlere kaydedilebilir.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, our webcam recorder, was updated to avoid crashes with newer GStreamer versions.

### Bug Stomping

Kontak Suite, Ark, Cantor, Dolphin, gwenview, Kate, Konsole, Okular, Spectacle, Umbrello ve daha fazlasını içeren uygulamalarda 120'den fazla hata çözüldü!

### Full Changelog
