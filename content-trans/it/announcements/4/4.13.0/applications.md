---
date: '2014-04-16'
description: KDE rilascia la versione 4.13 delle applicazioni (Applications) e della
  piattaforma (Platform).
hidden: true
title: KDE Applications 4.13 beneficia della nuova ricerca semantica e introduce nuove
  funzionalità
---
La comunità KDE è orgogliosa di annunciare gli ultimi importanti aggiornamenti alle applicazioni KDE che forniscono nuove funzionalità e correzioni. Kontact (il gestore delle informazioni personali) è stato oggetto di intense attività, beneficiando dei miglioramenti della tecnologia di ricerca semantica di KDE e portando nuove funzionalità. Il visore di documenti Okular e l'editor di testo avanzato Kate hanno ottenuto miglioramenti relativi all'interfaccia e alle funzionalità. Nelle aree educative e di gioco, introduciamo il nuovo allenatore di lingue straniere Artikulate; Marble (il globo per il desktop) ottiene supporto per sole, luna, pianeti, piste ciclabili e miglia nautiche. Palapeli (l'applicazione di rompicapi) è balzata a nuove dimensioni e capacità senza precedenti.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## KDE Kontact introduce nuove funzioni e maggiore velocità

La suite Kontact di KDE introduce una serie di funzioni nei suoi vari componenti. KMail introduce la memorizzazione nel cloud (Cloud Storage) e migliora il supporto sieve per i filtri lato server. KNotes adesso può generare avvisi e introduce funzioni di ricerca, e ci sono stati vari miglioramenti al sistema di cache dei dati in Kontact, velocizzando quasi tutte le operazioni.

### Supporto per il salvataggio nel cloud

KMail introduce il supporto per i servizi di memorizzazione, così grandi allegati possono essere memorizzati in servizi di cloud e inclusi come collegamento nei messaggi di posta elettronica. I servizi supportati includono Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic e c'è un'opzione per un servizio generico WebDAV. Lo strumento <em>storageservicemanager</em> aiuta nella gestione dei file in questi servizi.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Notevole miglioramento al supporto sieve

I filtri sieve, una tecnologia che permette a KMail la gestione dei filtri sul server, può ora gestire il supporto per le ferie per server multipli. Lo strumento KSieveEditor consente agli utenti di modificare i filtri sieve senza dover aggiungere il server in Kontact.

### Altre modifiche

La barra di ricerca veloce mostra piccoli miglioramenti all'interfaccia e beneficia molto dalle funzionalità di ricerca migliorate introdotte nel rilascio di KDE Development Platform 4.13. La ricerca è diventata significativamente più veloce e più affidabile. Il compositore introduce un «abbreviatore» di URL, che si aggiunge ai preesistenti strumenti di traduzione e per i frammenti di testo.

Etichette e annotazioni dei dati di PIM sono attualmente memorizzate in Akonadi. In versioni future saranno anche memorizzate sul server (per IMAP e Kolab), rendendo possibile condividere le etichette tra computer diversi. Akonadi: è stato aggiunto il supporto per l'API di Google Drive. C'è il supporto per cercare con estensioni di terze parti (questo vuol dire che i risultati possono essere ottenuti molto rapidamente) e ricerca lato server (ricerca di elementi non indicizzati da un servizio di indicizzazione locale).

### KNotes, KAddressbook

Lavoro significativo è stato fatto in KNotes, correggendo una serie di bug e piccoli comportamenti fastidiosi. L'abilità di impostare avvisi sulle note è nuova, così come la ricerca nelle note. Maggiori informazioni <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>qui</a>. KAddressBook ha ottenuto il supporto per la stampa: maggiori dettagli <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>qui</a>.

### Miglioramenti alle prestazioni

Le prestazioni di Kontact sono notevolmente migliorate in questa versione. Alcuni miglioramenti sono dovuti all'integrazione con la nuova versione dell'infrastruttura di <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Ricerca semantica</a> e il livello di memorizzazione nella cache dei dati e il caricamento dei dati in KMail hanno visto un lavoro significativo. Un lavoro notevole è stato dedicato al miglioramento del supporto per il database PostgreSQL. Maggiori informazioni e dettagli sulle modifiche relative alle prestazioni sono disponibili in questi collegamenti:

- Ottimizzazioni dell'archiviazione: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>rapporto sprint</a>
- velocizzare e ridurre le dimensioni della banca dati: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>lista di distribuzione</a>
- ottimizzazione dell'accesso alle cartelle: <a href='https://git.reviewboard.kde.org/r/113918/'>review board</a>

### KNotes, KAddressbook

Lavoro significativo è stato fatto in KNotes, correggendo una serie di bug e piccoli comportamenti fastidiosi. L'abilità di impostare avvisi sulle note è nuova, così come la ricerca nelle note. Maggiori informazioni <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>qui</a>. KAddressBook ha ottenuto il supporto per la stampa: maggiori dettagli <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>qui</a>.

## Okular perfeziona l'interfaccia utente

Questo rilascio del visualizzatore di documenti Okular porta un gran numero di miglioramenti. Puoi ora aprire vari file PDF in un'istanza di Okular grazie al supporto per le schede. C'è un nuova modalità «lente d'ingrandimento» del mouse e i DPI dello schermo corrente sono usati per la resa dei PDF, migliorando l'aspetto dei documenti. Un nuovo pulsante «Riproduci» è incluso in modalità presentazione e ci sono miglioramenti alle azioni Trova e Annulla/Rifai.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Kate introduce una barra di stato migliorata, corrispondenza delle parentesi animata, estensioni migliorate

L'ultima versione dell'editor di testo avanzato Kate introduce <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>abbinamento animato delle parentesi</a>, modifiche per rendere <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>le tastiere con AltGr funzionanti in modalità vim</a> e una serie di miglioramenti nelle estensioni di Kate, in particolare nell'area del supporto Python e <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>l'estensione di compilazione</a>. C'è una nuova, molto <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>migliorata barra di stato</a> che abilita azioni dirette come la modifica delle impostazioni di rientro, la codifica e l'evidenziazione, una nuova barra delle schede in ogni vista, supporto per il completamento del codice per <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>il linguaggio di programmazione D</a> e <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>Molto di più</a>. La squadra ha <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>ha chiesto un riscontro su cosa migliorare di Kate</a> e sta spostando parte della sua attenzione su una conversione a Frameworks 5.

## Funzioni varie in giro

Konsole introduce ulteriore flessibilità consentendo l'uso di fogli di stile personalizzati per controllare le barre delle schede. I profili possono memorizzare la dimensione desiderata di righe e colonne. Per maggiori informazioni vedi <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>qui</a>.

Umbrello consente di duplicare i diagrammi e introduce menu contestuali intelligenti che regolano il loro contenuto agli oggetti selezionati. Sono stati migliorati anche il supporto per l'annullamento e le proprietà visive. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'> introduce il supporto di anteprima RAW</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

Il mixer audio KMix ha introdotto il telecomando tramite il protocollo di comunicazione inter-processo DBUS (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>dettagli</a>), aggiunte al menu audio e una nuova finestra di configurazione (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>dettagli</a>) e una serie di correzioni di bug e miglioramenti minori.

L'interfaccia di ricerca di Dolphin è stata modificata per sfruttare la nuova infrastruttura di ricerca e ha ricevuto ulteriori miglioramenti delle prestazioni. Per i dettagli, leggi questa <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>panoramica del lavoro di ottimizzazione durante l'ultimo anno </a>.

KHelpcenter aggiunge l'ordinamento alfabetico per moduli e una riorganizzazione delle categorie per renderlo più semplice da usare.

## Giochi e applicazioni didattiche

Il gioco di KDE e le applicazioni educative hanno ricevuto molti aggiornamenti in questa versione. L'applicazione di puzzle di KDE, Palapeli, ha guadagnato <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>nuove eleganti funzionalità</a> che  consentono di risolvere grandi puzzle (fino a 10.000 Pezzi) molto più facilmente per coloro che sono all'altezza della sfida. KNavalBattle mostra le posizioni delle navi nemiche dopo la fine del gioco in modo da poter vedere dove hai sbagliato.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

Le applicazioni educative di KDE hanno acquisito nuove funzionalità. KStars ottiene un'interfaccia di creazione script tramite D-BUS e può utilizzare l'API dei servizi web astrometry.met per ottimizzare l'utilizzo della memoria. Cantor ha ottenuto un'evidenziazione di sintassi nel suo editor di script e i suoi motori di Scilab e Python 2 sono ora supportati nell'editor. Lo strumento di mappatura educativa e di navigazione Marble ora include le posizioni di <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Sole, Luna</a> e <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>pianeti</a> e consente di <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>catturare filmati durante i tour virtuali</a>. I percorsi per le biciclette sono migliorati con l'aggiunta del supporto cyclestreets.net. Le miglia nautiche sono ora supportate e facendo clic su un <a href='http://en.wikipedia.org/wiki/geo_uri'>Geo URI</a> ora si aprirà Marble.

#### Installare le applicazioni KDE

Il software KDE, incluse tutte le sue librerie e le sue applicazioni, è disponibile liberamente e gratuitamente sotto licenze Open Source. Il software KDE funziona su varie configurazioni hardware e architetture CPU come ARM e x86, su vari sistemi operativi e funziona con ogni tipo di gestore di finestre o ambiente desktop. Oltre a Linux e altri sistemi operativi basati su UNIX puoi trovare versioni per Microsoft Windows di buona parte delle applicazioni KDE sul sito del <a href='http://windows.kde.org'>software KDE per Windows</a> e versioni Apple Mac OS X sul sito del <a href='http://mac.kde.org/'>software KDE per Mac</a>. Versioni sperimentali di applicazioni KDE per varie piattaforme mobili come MeeGo, MS Windows Mobile e Symbian possono essere trovate sul web ma non sono al momento supportate. <a href='http://plasma-active.org'>Plasma Active</a> fornisce l'esperienza utente per una vasta gamma di dispositivi, come tablet e altro hardware mobile.

Il software KDE si può scaricare come sorgente e in vari formati binari da <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a> e può essere anche ottenuto su <a href='/download'>CD-ROM</a> o con qualsiasi <a href='/distributions'>principale distribuzione GNU/Linux o sistema UNIX</a> attualmente disponibile.

##### Pacchetti

Alcuni fornitori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti binari di 4.13.0 per alcune versioni delle rispettive distribuzioni, e in altri casi dei volontari della comunità hanno provveduto.

##### Posizione dei pacchetti

Per l'elenco aggiornato dei pacchetti binari disponibili di cui la squadra di rilascio di KDE è stata informata, visita il <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>wiki Community</a>.

Il codice sorgente completo per 4.13.0 può essere <a href='/info/4/4.13.0'>scaricato liberamente</a>. Le istruzioni su come compilare e installare il software KDE 4.13.0 sono disponibili dalla <a href='/info/4/4.13.0#binary'>pagina di informazioni di 4.13.0</a>.

#### Requisiti di sistema

Per poter ottenere il massimo da questi rilasci, raccomandiamo l'uso di una versione recente di Qt, come 4.8.4. Questo è necessario per poter assicurare un'esperienza stabile e più efficiente, dato che alcuni miglioramenti del software KDE sono stati in realtà effettuati sul framework Qt sottostante.

Per poter usare in pieno le funzionalità del software KDE raccomandiamo inoltre di usare i più recenti driver grafici disponibili per il tuo sistema, perché questo può migliorare notevolmente l'esperienza utente sia nelle funzionalità opzionali sia nelle prestazioni sia nella stabilità globale.
