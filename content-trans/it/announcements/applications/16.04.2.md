---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE rilascia KDE Applications 16.04.2
layout: application
title: KDE rilascia KDE Applications 16.04.2
version: 16.04.2
---
14 giugno 2016. Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../16.04.0'>KDE Applications 16.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 25 errori corretti includono, tra gli altri, miglioramenti ad Akonadi, Ark, Artikulate, Dolphin, Kdenlive e kdepim.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.21.
