---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE에서 KDE 프로그램 17.08.0 출시
layout: application
title: KDE에서 KDE 프로그램 17.08.0 출시
version: 17.08.0
---
2017년 8월 17일. KDE 프로그램 17.08을 출시했습니다. 프로그램과 기반 라이브러리 모두를 보다 안정적이고 사용하기 쉽도록 개선했습니다. 여러 사소한 곳을 수정하고 피드백을 반영하여 KDE 프로그램 제품군의 결함을 줄이고 더 친숙하게 만들었습니다. 새 앱을 즐겁게 사용하십시오!

### KDE 프레임워크 5로 추가 이식

다음 프로그램이 kdelibs4에서 KDE 프레임워크 5로 이식되었습니다: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat 및 umbrello. 이식에 도움을 준 개발자 여러분께 감사드립니다.

### What's new in KDE Applications 17.08

#### Dolphin

Dolphin 개발자에 의하면 휴지통에서 '삭제한 날짜' 및 BSD와 같은 지원하는 운영 체제에서 '생성한 날짜'를 표시합니다.

#### KIO 추가 기능

Kio-Extras에 Samba 공유에 지원을 개선했습니다.

#### KAlgebra

KAlgebra의 데스크톱용 Kirigami 프론트엔드, 코드 자동 완성을 개선했습니다.

#### Kontact

- KMailtransport의 Akonadi 전송 지원을 다시 활성화하고, 플러그인 지원을 작성했고, sendmail 메일 전송을 다시 지원합니다.
- SieveEditor의 autocreate 스크립트의 많은 버그를 수정했습니다. 일반적인 버그 수정 이외에도 정규 표현식 편집기의 라인 편집기를 추가했습니다.
- KMail에서 외부 편집기를 사용하는 기능을 플러그인으로 다시 추가했습니다.
- Akonadi 가져오기 마법사의 '모든 변환기 변환' 기능이 플러그인으로 추가되어 개발자가 새로운 변환기를 쉽게 만들 수 있습니다.
- 프로그램을 사용하려면 Qt 5.7 이상이 필요합니다. Windows 환경에서 많은 컴파일 오류를 수정했습니다. 아직 kdepim의 일부는 Windows에서 컴파일되지 않지만 개선이 진행 중입니다. 작업을 진행하기 위한 준비 중입니다. 코드 현대화(C++11)를 통해서 많은 버그를 수정했습니다. Qt 5.9에서 Wayland를 지원합니다. Kdepim-runtime에 Facebook 자원을 추가했습니다.

#### Kdenlive

Kdenlive 팀은 작동하지 않았던 '정지 효과'를 수정했습니다. 최근 버전에서는 고정 효과에서 고정할 프레임을 변경할 수 없었습니다. 키보드 단축키를 사용하여 프레임 추출 기능을 사용할 수 있습니다. 타임라인 스크린샷을 키보드 단축키로 저장할 수 있으며, 프레임 번호를 기반으로 이름을 제안합니다. <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>(버그 381325)</a> 다운로드한 루마 전환 효과가 인터페이스에 나타나지 않는 문제를 수정했습니다. <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>(버그 382451)</a> 오디오에서 딸깍 소리가 나는 문제를 개선했습니다(MLT가 릴리스될 때까지 git에서 MLT를 빌드해야 합니다). <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>(버그 371849)</a>

#### Krfb

X11 플러그인을 Qt5로 이식하는 작업을 완료했으며, krfb는 Qt 플러그인보다 훨씬 빠른 X11 백엔드를 사용합니다. 설정 페이지를 새로 작업하여 선호하는 선호하는 프레임 버퍼 플러그인을 변경할 수 있습니다.

#### Konsole

Konsole의 무제한 스크롤백이 2GB(32비트) 상한선을 초과할 수 있습니다. Konsole 스크롤백 파일을 저장할 위치를 변경할 수 있습니다. Konsole에서 KonsolePart의 프로필 관리 대화 상자 호출 문제를 수정했습니다.

#### KAppTemplate

In KAppTemplate there is now an option to install new templates from the filesystem. More templates have been removed from KAppTemplate and instead integrated into related products; ktexteditor plugin template and kpartsapp template (ported to Qt5/KF5 now) have become part of KDE Frameworks KTextEditor and KParts since 5.37.0. These changes should simplify creation of templates in KDE applications.

### 버그 수정

Kontact 제품군, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsole 등 프로그램에서 80개 이상의 버그가 수정되었습니다!

### 전체 변경 기록
