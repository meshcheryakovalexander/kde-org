---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE에서 KDE 프로그램 15.08.3 출시
layout: application
title: KDE에서 KDE 프로그램 15.08.3 출시
version: 15.08.3
---
November 10, 2015. Today KDE released the third stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Ark, Dolphin, kdenlive, kdepim, Kig, Lokalize 및 Umbrello 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.14.
