---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE에서 KDE 프로그램 18.08.0 출시
layout: application
title: KDE에서 KDE 프로그램 18.08.0 출시
version: 18.08.0
---
2018년 8월 16일. KDE 프로그램 18.08.0을 출시했습니다.

KDE 프로그램 릴리스에 포함된 소프트웨어를 지속적으로 개선하고 있으며, 새로운 개선 사항과 버그 수정을 즐겁게 사용하십시오!

### What's new in KDE Applications 18.08

#### 시스템

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, has received various quality-of-life improvements:

- '설정' 대화 상자를 디자인 가이드라인을 따르도록, 더 직관적으로 개선했습니다.
- 컴퓨터 속도를 저하시킬 수 있는 다양한 메모리 누수가 제거되었습니다.
- 휴지통을 볼 때 '새로 만들기' 메뉴 항목이 더 이상 표시되지 않습니다.
- 고해상도 화면에서 프로그램 표시가 개선되었습니다.
- 콘텍스트 메뉴에 보기 모드를 직접 정렬하고 변경할 수 있는 옵션을 추가했습니다.
- Sorting by modification time is now 12 times faster. Also, you can now launch Dolphin again when logged in using the root user account. Support for modifying root-owned files when running Dolphin as a normal user is still work in progress.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Multiple enhancements for <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, are available:

- '찾기' 위젯이 작업 흐름을 방해하지 않도록 창 위쪽에 나타납니다.
- 더 많은 탈출 시퀀스 지원(DECSCUSR, XTerm 대체 스크롤 모드)이 추가되었습니다.
- 단축키에 임의의 문자열 키를 지정할 수 있습니다.

#### 그래픽

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 is a major release for <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer. Over the last months contributors worked on a plethora of improvements. Highlights include:

- Gwenview의 상태 표시줄에는 그림 카운터가 있으며 총 그림 파일 개수가 표시됩니다.
- 별점순 및 내림차순으로 정렬할 수 있습니다. 날짜별 정렬 시 디렉터리와 압축 파일을 별도로 정렬하며 일부 문제점을 수정했습니다.
- 드래그 앤 드롭 기능이 향상되어 파일 및 폴더를 보기 모드에 끌어다 놓아서 표시할 수 있으며, 표시하고 있는 항목을 외부 프로그램으로 드래그할 수도 있습니다.
- Gwenview에서 복사한 그림을 그림 파일 경로를 지원하지 않고 원본 그림 데이터만 지원하는 프로그램에도 붙여넣을 수 있습니다. 수정한 그림도 복사할 수 있습니다.
- 그림 크기 조정 대화 상자를 개선하여 사용 편의성을 높이고 백분율 단위로 크기를 조정할 수 있습니다.
- 적목 현상 제거 도구의 크기 슬라이더와 십자형 커서가 수정되었습니다.
- 투명 배경 선택에 '없음' 옵션을 추가했으며 SVG에서도 사용할 수 있습니다.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

그림 크기 조정이 더욱 편리해졌습니다:

- Enabled zooming by scrolling or clicking as well as panning also when the Crop or Red Eye Reduction tools are active.
- Middle-clicking once again toggles between Fit zoom and 100% zoom.
- Added Shift-middle-clicking and Shift+F keyboard shortcuts for toggling Fill zoom.
- Ctrl-clicking now zooms faster and more reliably.
- Gwenview now zooms to the cursor's current position for Zoom In/Out, Fill and 100% zoom operations when using the mouse and keyboard shortcuts.

Image comparison mode received several enhancements:

- Fixed size and alignment of the selection highlight.
- Fixed SVGs overlapping the selection highlight.
- For small SVG images, the selection highlight matches the image size.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

A number of smaller enhancements was introduced to make your workflow even more enjoyable:

- Improved the fade transitions between images of varying sizes and transparencies.
- Fixed the visibility of icons in some floating buttons when a light color scheme is used.
- When saving an image under a new name, the viewer does not jump to an unrelated image afterwards.
- When the share button is clicked and kipi-plugins are not installed, Gwenview will prompt the user to install them. After the installation they are immediately displayed.
- Sidebar now prevents getting hidden accidentally while resizing and remembers its width.

#### 사무용 도구

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client, features some improvements in the travel data extraction engine. It now supports UIC 918.3 and SNCF train ticket barcodes and Wikidata-powered train station location lookup. Support for multi-traveler itineraries was added, and KMail now has integration with the KDE Itinerary app.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, the personal information management framework, is now faster thanks to notification payloads and features XOAUTH support for SMTP, allowing for native authentication with Gmail.

#### 교육

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE's frontend to mathematical software, now saves the status of panels (\"Variables\", \"Help\", etc.) for each session separately. Julia sessions have become much faster to create.

User experience in <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, our graphing calculator, has been significantly improved for touch devices.

#### 유틸리티

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Contributors to <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, focused on improving the Rectangular Region mode:

- 직사각형 영역 모드에서 픽셀 단위로 완벽한 선택 사각형을 그리는 데 도움이 되는 돋보기가 추가되었습니다.
- 키보드를 사용하여 선택 사각형을 이동하고 크기를 조정할 수 있습니다.
- 사용자 인터페이스는 사용자의 색 배열을 따르고 도움말 텍스트의 표시가 개선되었습니다.

다른 사람들과 스크린샷을 쉽게 공유할 수 있도록 그림 공유 링크를 자동으로 클립보드에 복사합니다. 사용자가 지정한 하위 디렉터리에 스크린샷을 자동으로 저장합니다.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, our webcam recorder, was updated to avoid crashes with newer GStreamer versions.

### 버그 수정

Kontact 제품군, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle, Umbrello 등 프로그램에서 120개 이상의 버그가 수정되었습니다!

### 전체 변경 기록
