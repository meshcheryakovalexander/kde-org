---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE에서 KDE 프로그램 18.04.0 출시
layout: application
title: KDE에서 KDE 프로그램 18.04.0 출시
version: 18.04.0
---
2018년 4월 19일. KDE 프로그램 18.04.0을 출시했습니다.

KDE 프로그램 릴리스에 포함된 소프트웨어를 지속적으로 개선하고 있으며, 새로운 개선 사항과 버그 수정을 즐겁게 사용하십시오!

## What's new in KDE Applications 18.04

### 시스템

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

The first major release in 2018 of <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, features many improvements to its panels:

- '위치' 패널의 섹션을 개별적으로 숨길 수 있으며, 원격 위치를 저장할 수 ᄋᅠᆻㅣ는 '네트워크' 섹션이 추가되었습니다.
- '터미널' 패널은 창의 모든 모서리에 도킹할 수 있으며, Konsole을 설치하지 않고 열려고 하면 경고를 표시하고 설치를 안내합니다.
- '정보' 패널의 HiDPI 지원이 개선되었습니다.

폴더 보기와 메뉴도 업데이트되었습니다:

- 휴지통 폴더에 '휴지통 비우기' 단추를 표시합니다.
- 심볼릭 링크 대상을 찾는 데 도움이 되는 '대상 보이기' 메뉴 항목을 추가했었습니다.
- git 폴더의 콘텍스트 메뉴에 'git log' 및 'git 합치기' 동작이 추가되어 Git 통합을 개선했습니다.

추가 개선 사항:

- 슬래시(/) 키를 눌러서 필트 표시줄로 이동하는 새로운 단축키를 추가했습니다.
- 사진을 찍은 날짜별로 정렬하고 정리할 수 있습니다.
- Dolphin에서 작은 파일 여러 개 드래그 앤 드롭 속도가 빨라졌으며, 일괄 이름 바꾸기 작업을 실행 취소할 수 있습니다.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

To make working on the command line even more enjoyable, <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, can now look prettier:

- KNewStuff를 통해 색 배열을 다운로드할 수 있습니다.
- 스크롤바가 활성 색상 구성표와 더 잘 어울립니다.
- 기본값으로 필요한 경우에만 탭 표시줄을 표시합니다.

Konsole의 기능 개선은 이뿐만 아니라 다양한 새로운 기능 도입도 있습니다:

- 새로운 읽기 전용 모드가 추가되었으며 HTML로 텍스트 복사를 전환하는 프로필 속성이 추가되었습니다.
- Wayland에서 Konsole 드래그 앤 드롭 메뉴를 지원합니다.
- ZMODEM 프로토콜 처리를 개선했습니다: 데이터 전송 진행 상태를 표시하는 ZMODEM 업로드 표시기 B01을 처리할 수 있습니다. 대화 상자의 취소 단추가 예상하는 대로 작동합니다. 1MB 단위로 메모리에 불러오는 형태로 더 큰 파일 전송을 개선했습니다.

추가 개선 사항:

- libinput 사용 시 마우스 휠 스크롤이 수정되었으며 마우스 휠을 사용하여 스크롤할 때 셸 과거 기록을 볼 수 있습니다.
- 검색 시 정규 표현식 옵션을 변경하면 새로 검색을 시작하며, 'Ctrl' + 'Backspace'를 눌렀을 때 xterm과 같은 동작을 수행합니다.
- '--background-mode' 단축키를 수정했습니다.

### 멀티미디어

<a href='https://juk.kde.org/'>JuK</a>, KDE's music player, now has Wayland support. New UI features include the ability to hide the menu bar and having a visual indication of the currently playing track. While docking to the system tray is disabled, JuK will no longer crash when attempting to quit via the window 'close' icon and the user interface will remain visible. Bugs regarding JuK autoplaying unexpectedly when resuming from sleep in Plasma 5 and handling the playlist column have also been fixed.

For the 18.04 release, contributors of <a href='https://kdenlive.org/'>Kdenlive</a>, KDE's non-linear video editor, focused on maintenance:

- 클립 크기를 조정할 때 페이드와 키 프레임이 더 이상 손상되지 않습니다.
- HiDPI 모니터에서 디스플레이 크기 조정 사용 시 아이콘이 더 선명해졌습니다.
- 일부 환경에서 시작 시 발생할 수 있는 충돌이 수정되었습니다.
- MLT 버전 6.6.0 이상이 필요하며 호환성을 개선했습니다.

#### 그래픽

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

Over the last months, contributors of <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer, worked on a plethora of improvements. Highlights include:

- Support for MPRIS controllers has been added so you can now control full screen slideshows via KDE Connect, your keyboard's media keys, and the Media Player plasmoid.
- The thumbnail hover buttons can now be turned off.
- The Crop tool received several enhancements, as its settings are now remembered when switching to another image, the shape of the selection box can now be locked by holding down the 'Shift' or 'Ctrl' keys and it can also be locked to the aspect ratio of the currently displayed image.
- In full screen mode, you can now exit by using the 'Escape' key and the color palette will reflect your active color theme. If you quit Gwenview in this mode, this setting will be remembered and the new session will start in full screen as well.

Attention to detail is important, so Gwenview has been polished in the following areas:

- Gwenview will display more human-readable file paths in the 'Recent Folders' list, show the proper context menu for items in the 'Recent Files' list, and correctly forget everything when using the 'Disable History' feature.
- Clicking on a folder in the sidebar allows toggling between Browse and View modes and remembers the last used mode when switching between folders, thus allowing faster navigation in huge image collections.
- Keyboard navigation has been further streamlined by properly indicating keyboard focus in Browse mode.
- The 'Fit Width' feature has been replaced with a more generalized 'Fill' function.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Even smaller enhancements often can make user's workflows more enjoyable:

- To improve consistency, SVG images are now enlarged like all other images when 'Image View → Enlarge Smaller Images' is turned on.
- After editing an image or undoing changes, image view and thumbnail won't get out of sync anymore.
- When renaming images, the filename extension will be unselected by default and the 'Move/Copy/Link' dialog now defaults to showing the current folder.
- Lots of visual papercuts were fixed, e.g. in the URL bar, for the full screen toolbar, and for the thumbnail tooltip animations. Missing icons were added as well.
- Last but not least, the full screen hover button will directly view the image instead of showing the folder's content, and the advanced settings now allow for more control over the ICC color rendering intent.

#### 사무용 도구

In <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE's universal document viewer, PDF rendering and text extraction can now be cancelled if you have poppler version 0.63 or higher, which means that if you have a complex PDF file and you change the zoom while it's rendering it will cancel immediately instead of waiting for the render to finish.

You will find improved PDF JavaScript support for AFSimple_Calculate, and if you have poppler version 0.64 or higher, Okular will support PDF JavaScript changes in read-only state of forms.

Management of booking confirmation emails in <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client, has been significantly enhanced to support train bookings and uses a Wikidata-based airport database for showing flights with correct timezone information. To make things easier for you, a new extractor has been implemented for emails not containing structured booking data.

추가 개선 사항:

- The message structure can again be shown with the new 'Expert' plugin.
- A plugin has been added in the Sieve Editor for selecting emails from the Akonadi database.
- The text search in the editor has been improved with regular expression support.

#### 유틸리티

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

Improving the user interface of <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, was a major focus area:

- The bottom row of buttons has received an overhaul, and now displays a button to open the Settings window and a new 'Tools' button that reveals methods to open the last-used screenshot folder and launch a screen recording program.
- 마지막으로 사용한 저장 모드를 기본값으로 기억합니다.
- 창 크기가 스크린샷의 가로 세로 비율에 맞게 조정되어 보다 왜곡이 적고 공간 효율적인 스크린샷 축소판을 볼 수 있습니다.
- 설정 창을 단순하게 변경했습니다.

다음 새로운 기능을 사용하여 작업 방식을 단순화할 수 있습니다:

- 특정 창을 캡처할 때 화면 캡처 파일의 이름에 창 제목을 추가할 수 있습니다.
- 저장 또는 복사 작업 후에 Spectacle 자동 종료 여부를 선택할 수 있습니다.

중요한 버그 수정 사항은 다음과 같습니다:

- Chromium 창으로 드래그 앤 드롭이 올바르게 작동합니다.
- 키보드 단축키를 연속적으로 눌러서 스크린샷을 저장해도 모호한 단축키 경고 대화 상자가 더 이상 나타나지 않습니다.
- 직사각형 영역 스크린샷을 찍을 때 선택 영역의 아래쪽 가장자리를 보다 정확하게 조정할 수 있습니다.
- 컴포지팅이 꺼져 있을 때 화면 가장자리 주변에 있는 창 캡처 시 안정성을 개선했습니다.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

With <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, KDE's certificate manager and universal crypto GUI, Curve 25519 EdDSA keys can be generated when used with a recent version of GnuPG. A 'Notepad' view has been added for text based crypto actions and you can now sign/encrypt and decrypt/verify directly in the application. Under the 'Certificate details' view you will now find an export action, which you can use to export as text to copy and paste. What's more, you can import the result via the new 'Notepad' view.

In <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE's graphical file compression/decompression tool with support for multiple formats, it is now possible to stop compressions or extractions while using the libzip backend for ZIP archives.

### KDE 프로그램 릴리스 일정에 참여하는 프로그램

KDE's webcam recorder <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> and backup program <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> will now follow the Applications releases. The instant messenger <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> is also being reintroduced after being ported to KDE Frameworks 5.

### 자체 릴리스 일정으로 전환한 프로그램

The hex editor <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> will have its own release schedule after a request from its maintainer.

### 버그 수정

Kontact 제품군, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello 등 프로그램에서 120개 이상의 버그가 수정되었습니다!

### 전체 변경 기록
