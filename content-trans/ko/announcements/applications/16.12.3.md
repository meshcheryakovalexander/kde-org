---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE에서 KDE 프로그램 16.12.3 출시
layout: application
title: KDE에서 KDE 프로그램 16.12.3 출시
version: 16.12.3
---
March 9, 2017. Today KDE released the third stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

kdepim, Ark, Filelight, Gwenview, Kate, Kdenlive, Okular 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.30.
