---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: KDE Ships Applications 18.12.
layout: application
release: applications-18.12.0
title: KDE Ships KDE Applications 18.12.0
version: 18.12.0
---
{{% i18n_date %}}

KDE Applications 18.12 are now released.

{{%youtube id="ALNRQiQnjpo"%}}

Traballamos continuamente en mellorar o software que se inclúe nas aplicacións de KDE, e esperamos que as novas melloras e correccións de fallos lle resulten de utilidade!

## What's new in KDE Applications 18.12

Solucionáronse máis de 140 fallos en aplicacións como, entre outras, a colección de Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle e Umbrello!

### Xestión de ficheiros

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager:

- Nova realización de MTP lista para produción
- Gran mellora de rendemento para ler ficheiros polo protocolo SFTP
- Agora, nas vistas previas das miniaturas, os marcos e as sombras só se debuxan para ficheiros de imaxes sen transparencia, mellorando a visualización das iconas
- Novas vistas previas de miniaturas para documentos de LibreOffice e aplicacións de AppImage
- Agora os vídeos de tamaño superior a 5 MB se mostran nas miniaturas de directorio cando se activan as miniaturas de directorio
- Agora, ao ler os CD de son, Dolphin pode cambiar a taxa de bits de CBR do codificador de MP3 e corrixe as marcas temporais de FLAC
- Agora o menú «Control» de Dolphin mostra os elementos de «Crear un novo…» e ten un novo elemento de menú «Mostrar os lugares ocultos»
- Agora Dolphin péchase cando só ten unha xanela aberta e se preme o atallo de teclado estándar de pechar o separador (Ctrl+W)
- Agora, tras desmontar un volume desde o panel de «Lugares», pode montalo de novo
- A vista de documentos recentes (dispoñíbel ao examinar recentdocuments:/ en Dolphin) agora só mostra documentos reais e retira automaticamente os URL web
- Agora Dolphin mostra un aviso antes de permitirlle cambiar o nome do ficheiro e o directorio de xeito que causaría que se agochase de maneira inmediata
- Xa non se pode intentar desmontar os discos do sistema operativo activo ou o directorio persoal desde o panel de lugares

<a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, KDE's traditional file search, now has a metadata search method based on KFileMetaData.

### Oficina

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client:

- Agora KMail pode mostrar unha caixa de entrada unificada
- Novo complemento: xerar unha mensaxe HTML a partir da linguaxe Markdown
- Usar Purpose para compartir texto (por correo electrónico)
- Agora as mensaxes HTML poden lerse independente do esquema de cores que haxa en uso

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE's versatile document viewer:

- Nova ferramenta de anotacións de escritura que se pode usar para escribir texto en calquera parte
- Agora a vista de índice xerárquico permite expandir e pregar todo ou só unha sección específica
- Mellorouse o comportamento de salto automático de liña en anotacións entre liñas
- Ao cubrir unha ligazón co rato, agora o URL móstrase cada vez que pode premerse, en vez de só cando está no modo de navegar
- Agora os ficheiros ePub que conteñen recursos con espazos nos URL móstranse correctamente

### Desenvolvemento

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, KDE's advanced text editor:

- Ao usar o terminal incrustado, agora sincronízase automaticamente o directorio actual co lugar onde está o documento activo no disco
- Agora o terminal incrustado de Kate pode recibir ou perder o foco usando o atallo de teclado F4
- Agora o cambiador de separadores de Kate mostra as rutas completas de ficheiros con nomes similares
- Agora os números de liña móstranse de maneira predeterminada
- Agora o complemento de filtro de texto, moi útil e potente, está activado de maneira predeterminada e é máis doado atopalo
- Agora abrir un documento xa aberto usando a funcionalidade de apertura rápida volve a ese documento
- A funcionalidade de abrir rapidamente xa non mostra entradas duplicadas
- Ao usar varias actividades, agora os ficheiros ábrense na actividade correcta
- Agora Kate mostra todas as iconas correctas ao executalo en GNOME usando o tema de iconas de GNOME

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator:

- Agora Konsole permite caracteres emoji
- Agora as iconas de separadores inactivos saliéntanse cando reciben un sinal de campá
- Os dous puntos ao final xa non se consideran parte dunha palabra á hora de seleccionar mediante clic duplo, facendo máis fácil seleccionar rutas na saída de «grep»
- Agora, cando se conecta un rato con botóns de diante e atrás, Konsole pode usalos para cambiar entre separadores
- Agora Konsole ten un elemento de menú para restabelecer o tamaño de letra ao predeterminado do perfil se o seu tamaño se ampliou ou reduciu
- Agora é máis difícil separar accidentalmente os separadores, e reorganizalos con precisión é agora máis rápido
- Mellorouse o comportamento de selección de Maiús+clic
- Corrixiuse o clic duplo nunha liña de texto que supera a anchura da xanela
- A barra de busca volve pecharse cando preme a tecla Escape

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, KDE's translation tool:

- Agochar os ficheiros traducidos no separador do proxecto
- Engadiuse integración básica con pology, o sistema de comprobación de sintaxe e glosario
- Simplificouse a navegación coa ordenación dos separadores e a apertura de varios separadores
- Corrixíronse quebras debidas ao acceso simultáneo á base de datos de obxectos
- Corrixiuse o arrastras e soltar inconsistente
- Corrixiuse un fallo nos atallos que eran distintos entre editores
- Mellorouse o comportamento da busca (a busca atopará e mostrará formas plurais)
- Restaurar a compatibilidade con Windows grazas ao sistema de construción craft

### Utilidades

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer:

- A ferramenta de redución de ollos vermellos recibiu varias melloras de facilidade de uso
- Agora Gwenview mostra un diálogo de aviso cando agocha a barra de menú que lle informa de como recuperala

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's screenshot utility:

- Agora Spectacle pode numerar os ficheiros de captura de pantalla de maneira secuencial e usa este esquema de nome de maneira predeterminada se baleira o campo de texto do nome de ficheiro
- Corrixiuse a garda de imaxes en formatos distintos de PNG ao usar «Gardar como…»
- Agora, ao usar Spectacle para abrir unha captura de pantalla nunha aplicación externa, pódese modificar e gardar a imaxe tras rematar con ela
- Agora Spectacle abre o cartafol correcto ao premer «Ferramentas → Abrir o cartafol de capturas de pantalla»
- As marcas de tempo das capturas de pantalla agora reflicten cando se creou a imaxe, non cando se gardou
- Todas as opcións de garda están agora situadas na páxina de «Gardar»

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE's archive manager:

- Engadiuse compatibilidade co formato Zstandard (arquivos tar.zst)
- Corrixiuse que Ark fornecese unha vista previa de certos ficheiros (p. ex. Open Document) como arquivos en vez de abrilos nunha aplicación axeitada

### Matemáticas

<a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, KDE's simple calculator, now has a setting to repeat the last calculation multiple times.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE's mathematical frontend:

- Engadir o tipo de entrada Markdown
- Salientado animado da entrada de orde calculada actual
- Visualización das entradas de ordes pendentes (encoladas pero aínda sen calcularse)
- Permitir formatar as entradas de ordes (cor de fondo, cor principal, propiedades da fonte)
- Permitir inserir novas entradas de ordes en lugares arbitrarios na folla de cálculo colocando o cursor no lugar desexado e comezar a escribir
- Mostrar os resultados como obxectos de resultado independentes na folla de cálculo para expresións que teñan moitas ordes.
- Engadir a posibilidade de abrir follas de cálculo mediante rutas relativas desde a consola
- Engadir a posibilidade de abrir varios ficheiros nun mesmo intérprete de ordes de Cantor
- Cambiar a cor e a fonte ao solicitar información adicional para discriminar de maneira mellor da entrada habitual da entrada de ordes
- Engadíronse atallos para a navegación entre follas de traballo (Ctrl+RePáx, Ctrl+AvPáx)
- Engadir unha acción no submenú de «Vista» para restabelecer a ampliación
- Activar a descarga de proxectos de Cantor desde store.kde.org (de momento só se poden enviar traballos mediante o sitio web)
- Abrir a folla de traballo en modo de só lectura se a infraestrutura non está dispoñíbel no sistema

<a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, KDE's function plotter, fixed many issues:

- Corrixíronse os nomes incorrectos das gráficas de derivadas e integrais na notación convencional
- A exportación de SVG xa funciona ben en Kmplot
- A funcionalidade de primeira derivada non ten notación de primo
- Desmarcar unha función sen o foco xa oculta o seu gráfico:
- Resolveuse unha quebra de Kmplot ao abrir «Editar as constantes» desde o editor de funcións de maneira recursiva
- Solucionouse unha quebra de KmPlot tras eliminar unha función á que sigue o punteiro do rato na gráfica
- Agora pode exportar os datos debuxados en calquera formato de imaxe
