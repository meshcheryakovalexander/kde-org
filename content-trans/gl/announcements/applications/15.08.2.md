---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE publica a versión 15.08.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 15.08.2 das aplicacións de KDE
version: 15.08.2
---
October 13, 2015. Today KDE released the second stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 30 correccións de erros inclúen melloras en, entre outros, Ark, Gwenview, Kate, KBruch, KDE Libs, KDE PIM, Lokalize e Umbrello.

This release also includes Long Term Support version of KDE Development Platform 4.14.13.
