---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Corrixir o filtro de data que timeline:// usa
- BalooCtl: Volver tras as ordes
- Limpar e protexer Baloo::Database::open(), xestionar máis casos de quebra
- Engadir unha comprobación en Database::open(OpenDatabase) para fallar se a base de datos non existe

### Iconas de Breeze

- Engadíronse ou melloráronse moitas iconas
- usar follas de estilos nas iconas de Breeze (fallo 126166)
- Corrección do fallo 355902 e cambiouse system-lock-screen (corrección do fallo 355902 e cambiouse system-lock-screen)
- Engadir dialog-information de 24 px para aplicacións de GTK (fallo 355204)

### Módulos adicionais de CMake

- Non avisar cando as iconas SVG(Z) se fornecen con varios tamaños e niveis de detalle
- Asegurarse de que cargamos as traducións no fío principal (fallo 346188).
- Reescribir o sistema de construción de ECM.
- Permitir activar Clazy en calquera proxecto de KDE
- Non atopar a biblioteca XINPUT de XCB de maneira predeterminada.
- Limpar o directorio de exportación antes de xerar de novo o APK
- Usar quickgit para os URL de repositorios de Git

### Integración de infraestruturas

- Engadir «a instalación do plasmoide fallou» a plasma_workspace.notifyrc

### KActivities

- Corrixir un bloqueo no primeiro inicio do servizo
- Mover a creación de QAction ao fío principal. (fallo 351485)
- Ás veces clang-format toma unha mala decisión (fallo 355495)
- Solución para problemas potenciais de sincronización
- Usar org.qtproject en vez de com.trolltech
- Retirar o uso de libkactivities nos complementos
- Retirouse a configuración de KAStats da API
- Engadíronse linking e unlinking a ResultModel

### Ferramentas de Doxygen de KDE

- Facer kgenframeworksapidox máis robusto.

### KArchive

- Corrixir KCompressionDevice::seek(), ao que se chama ao crear un KTar sobre un KCompressionDevice.

### KCoreAddons

- KAboutData: permitir https:// e outros esquemas de URL no sitio web. (fallo 355508)
- Reparar a propiedade MimeType ao usar kcoreaddons_desktop_to_json()

### KDeclarative

- Migrar KDeclarative ao uso directo de KI18n
- Agora delegateImage, de DragArea, pode ser unha cadea a partir da cal se crea automaticamente unha icona
- Engadir a nova biblioteca CalendarEvents

### KDED

- Retirar a definición da variábel de contorno SESSION_MANAGER en vez de definila cun valor baleiro

### Compatibilidade coa versión 4 de KDELibs

- Corrixir algunhas chamadas de internacionalización.

### KFileMetaData

- Marcar m4a como lexíbel por taglib

### KIO

- Diálogo de cookies: facer que funcione como se quería
- Corrixir que a suxestión de nome de ficheiro cambiase a algo aleatorio ao cambiar o tipo MIME de «Gardar como».
- Rexistrar un nome de D-Bus para kioexec (fallo 353037)
- Actualizar KProtocolManager tras cambios de configuración.

### KItemModels

- Corrixir o uso de KSelectionProxyModel en QTableView (fallo 352369)
- Corrixir restabelecer ou cambiar o modelo de orixe dun KRecursiveFilterProxyModel.

### KNewStuff

- registerServicesByGroupingNames pode definir máis elementos de maneira predeterminada
- Facer que KMoreToolsMenuFactory::createMenuFromGroupingNames só se cargue no primeiro uso

### KTextEditor

- Engadir salientado de sintaxe para TaskJuggler e PL/I
- Permitir desactivar o completado de palabras clave mediante a interface de configuración.
- Cambiar de tamaño da árbore cando se restabeleza o modelo de completado.

### Infraestrutura de KWallet

- Xestionar correctamente o caso no que o usuario nos desactivou

### KWidgetsAddons

- Corrixir un pequeno erro visual de KRatingWidget en HiDPI.
- Reorganizar o código e corrixir a funcionalidade introducida no fallo 171343 (fallo 171343)

### KXMLGUI

- Non chamar a QCoreApplication::setQuitLockEnabled(true) durante a inicialización.

### Infraestrutura de Plasma

- Engadir un plasmoide básico como exemplo a guía de desenvolvedores
- Engadir un par de modelos de plasmoide para kapptemplate/kdevelop
- [calendario] Atrasar o restablecemento do modelo ata que a vista estea lista (fallo 355943)
- Non cambiar de posición mentres se agocha (fallo 354352)
- [IconItem] Non quebrar cando o tema de KIconLoader sexa nulo (fallo 355577)
- Soltar ficheiros de imaxe nun panel xa non ofrecerá usalos como fondo do panel
- Soltar un ficheiro .plasmoid nun panel u non escritorio o instalará e engadirá
- retirar o módulo de kded platformstatus que xa non se usa (fallo 348840)
- permitir pegar nos campos de contrasinal
- corrixir a colocación do menú de editar, engadir un botón para seleccionar
- [calendario] Usar o idioma da interface de usuario para obter o nome do mes (fallo 353715)
- [calendar] Ordenar os eventos tamén por tipo
- [Calendario] Migrar a biblioteca de complementos a KDeclarative
- [calendario] qmlRegisterUncreatableType necesita algúns argumentos máis
- Permitir engadir categorías de configuración de maneira dinámica
- [calendario] Mover a xestión dos complementos a unha clase aparte
- Permitir aos complementos fornecer os datos de evento ao miniaplicativo de calendario (fallo 349676)
- comprobar se a rañura existe antes de conectar ou desconectar (fallo 354751)
- [plasmaquick] Non ligar con OpenGL de maneira explícita
- [plasmaquick] Retirar XCB::COMPOSITE e a dependencia de DAMAGE

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
