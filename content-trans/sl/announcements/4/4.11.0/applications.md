---
date: 2013-08-14
hidden: true
title: Aplikacije KDE 4.11 prinašajo ogromen korak naprej pri upravljanju osebnih
  podatkov in vsesplošne izboljšave
---
Upravitelj datotek Dolphin v tej izdaji prinaša številne majhne popravke in optimizacije. Nalaganje velikih map je bilo pospešeno in zahteva do 30 &#37; manj pomnilnika. Dejavnost trdega diska in procesorja se zmanjšuje z nalaganjem predogledov zgolj okoli vidnih elementov. Bilo je še veliko izboljšav: na primer, veliko napak, ki so vplivale na razširjene mape v pogledu podrobnosti, je bilo odpravljenih, brez »neznanih«; ikon nadomestnih mest ne bodo prikazane več ob vstopu v mapo, s srednjim klikom na arhiv pa se zdaj odpre nov zavihek z vsebino arhiva, kar ustvarja bolj dosledno izkušnjo nasploh.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Nov potek dela »pošlji pozneje« v Kontactu` width="600px">}}

## Izboljšave zbirke Kontact Suite

Kontact Suite se je znova osredotočila na stabilnost, zmogljivost in uporabo pomnilnika. Uvažanje map, preklapljanje med zemljevidi, pridobivanje pošte, označevanje ali premikanje velikega števila sporočil in zagonski čas so bili v zadnjih 6 mesecih izboljšani. Za podrobnosti glejte <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>ta spletni dnevnik</a>. <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>Funkcija arhiviranja je doživela številne popravke napak</a >, izboljšave pa so tudi v ImportWizardu, ki omogoča uvoz nastavitev iz poštnega odjemalca Trojitá in boljši uvoz iz različnih drugih aplikacij. Več informacij najdete <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>tukaj</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`Arhivski agent upravlja shranjevanje e-pošte v stisnjeni obliki` width="600px">}}

Ta izdaja vsebuje tudi nekaj pomembnih novih zmožnosti. Obstaja <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>nov urejevalnik tem za glave e-poštnih sporočil</a> in e-poštne slike lahko sprotno spremenite. Zmožnost <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Pošlji pozneje</a> omogoča razporejanje pošiljanja e-pošte na določen datum in uro, z dodano možnostjo ponovnega pošiljanja v določenem intervalu. Podpora za filter KMail (funkcija IMAP, ki omogoča filtriranje na strežniku) je bila izboljšana, uporabniki lahko ustvarijo skripte za filtriranje sit <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim -4-11-improve-sieve-support-22/'>z vmesnikom, ki je enostaven za uporabo</a>. KMail na varnostnem področju uvaja samodejno »odkrivanje prevar«, tako da <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>prikazuje opozorilo</a>, ko e-poštna sporočila vsebujejo tipične trike z lažnim predstavljanjem. Zdaj prejmete <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>informativno obvestilo</a>, ko prihaja nova pošta, in nenazadnje urejevalnik blogov Blogilo ima precej izboljšan urejevalnik HTML, ki temelji na QtWebKitu.

## Razširjena jezikovna podpora za Kate

Napredni urejevalnik besedil Kate predstavlja nove vtičnike: Python (2 in 3), JavaScript & JQuery, Django in XML. Uvajajo funkcije, kot so statično in dinamično samodokončanje, preverjanje skladnje, vstavljanje izrezkov kode in možnost samodejnega zamika XML s tipkami za bližnjico. Toda za prijatelje Pythona je tu še več: konzola python, ki zagotavlja poglobljene informacije o odprti izvorni datoteki. Opravljenih je bilo tudi nekaj manjših izboljšav uporabniškega vmesnika, vključno z <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>novimi pasivnimi obvestili za funkcijo iskanja</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimizacijo načina VIM</a> in <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>novo funkcijo preloma besedila</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars prikazuje zanimive prihajajoče dogodke, ki so vidni z vaše lokacije` width="600px">}}

## Druge izboljšave aplikacij

Na področju iger in izobraževanja je prispelo več manjših in večjih novosti in optimizacij. Bodoči tipkarji na dotik lahko uživajo v podpori od desne proti levi v KTouchu, medtem ko ima prijatelj opazovalca zvezd KStars zdaj orodje, ki prikazuje zanimive dogodke, ki prihajajo na vašem območju. Pozornost so pritegnila matematična orodja Rocs, Kig, Cantor in KAlgebra, ki podpirajo več ozadij in izračunov. In igra KJumpingCube ima zdaj funkcije večje velikosti plošč, nove ravni spretnosti, hitrejše odzive in izboljšan uporabniški vmesnik.

Preprosta aplikacija za slikanje Kolourpaint se lahko ukvarja s slikovnim formatom WebP, univerzalni pregledovalnik dokumentov Okular pa ima nastavljiva orodja za pregledovanje in uvaja podporo za razveljavitev/ponovitev v obrazcih in opombah. Avdio označevalnik/predvajalnik JuK podpira predvajanje in urejanje metapodatkov novega avdio formata Ogg Opus (vendar to zahteva, da tudi avdio gonilnik in TagLib podpirata Ogg Opus).

#### Nameščanje aplikacij KDE

Programska oprema KDE, vključno z vsemi knjižnicami in aplikacijami, je prosto na voljo pod odprto-kodnimi licencami. Programska oprema KDE deluje na različnih konfiguracijah strojne opreme in arhitekturah CPE, kot sta ARM in x86, operacijskih sistemih in deluje s kakršnim koli upravljalnikom oken ali namiznim okoljem. Poleg Linuxa in drugih operacijskih sistemov, ki temeljijo na sistemu UNIX, lahko najdete različice večine aplikacij KDE za Microsoft Windows na spletnem mestu <a href='http://windows.kde.org'>Programska oprema KDE za Windows</a> in Apple Mac OS X različice na <a href='http://mac.kde.org/'>Programska oprema KDE na spletnem mestu Mac</a>. Eksperimentalne različice aplikacij KDE za različne mobilne platforme, kot so MeeGo, MS Windows Mobile in Symbian, je mogoče najti na spletu, vendar trenutno niso podprte. <a href='http://plasma-active.org'>Plasma Active</a> je uporabniška izkušnja za širši spekter naprav, kot so tablični računalniki in druga mobilna strojna oprema.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paketi

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Mesta paketov

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Sistemske zahteve

Da bi kar najbolje izkoristili te izdaje, priporočamo uporabo novejše različice Qt, kot je 4.8.4. To je potrebno za zagotovitev stabilne in učinkovite izkušnje, saj so bile nekatere izboljšave programske opreme KDE dejansko narejene v osnovnem okviru Qt.<br /> Da bi v celoti izkoristili zmogljivosti programske opreme KDE, priporočamo tudi za uporabo najnovejših grafičnih gonilnikov za vaš sistem, saj lahko s tem bistveno izboljšate uporabniško izkušnjo, tako v izbirni funkcionalnosti kot pri splošni zmogljivosti in stabilnosti.

## Danes objavljeno tudi:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" />Plasma Workspaces 4.11 še naprej izboljšuje uporabniško izkušnjo</a>

Kot priprava za dolgoročno vzdrževanje Plasma Workspaces prinaša nadaljnje izboljšave osnovne funkcionalnosti z bolj gladko opravilno vrstico, pametnejšim pripomočkom za baterijo in izboljšanim mešalnikom zvoka. Uvedba KScreen prinaša inteligentno upravljanje z več monitorji v delovne prostore, obsežne izboljšave zmogljivosti v kombinaciji z majhnimi popravki uporabnosti pa zagotavljajo na splošno lepšo izkušnjo.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/>Platforma KDE 4.11 zagotavlja boljšo zmogljivost</a>

Ta izdaja platforme KDE 4.11 se še naprej osredotoča na stabilnost. Nove zmožnosti se izdelujejo za našo prihodnjo izdajo KDE Frameworks 5.0, toda za stabilno izdajo smo uspeli stisniti optimizacije za naš okvir Nepomuk.
