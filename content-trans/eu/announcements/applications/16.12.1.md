---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDEk, KDE Aplikazioak 16.12.1 kaleratzen du
layout: application
title: KDEk, KDE Aplikazioak 16.12.1 kaleratzen du
version: 16.12.1
---
January 12, 2017. Today KDE released the first stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

This release fixes a DATA LOSS bug in the iCal resource which failed to create std.ics if it didn't exist.

More than 40 recorded bugfixes include improvements to kdepim, ark, gwenview, kajongg, okular, kate, kdenlive, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.28.
