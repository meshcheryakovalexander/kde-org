---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDEk, Aplikazioak 18.12.1 kaleratu du.
layout: application
major_version: '18.12'
title: KDEk, KDE Aplikazioak 18.12.1 kaleratu du
version: 18.12.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About 20 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole, Okular, among others.

Improvements include:

- Akregator now works with WebEngine from Qt 5.11 or newer
- Sorting columns in the JuK music player has been fixed
- Konsole renders box-drawing characters correctly again
