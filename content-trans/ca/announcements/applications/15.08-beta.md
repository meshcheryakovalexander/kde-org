---
aliases:
- ../announce-applications-15.08-beta
date: 2015-07-28
description: Es distribueixen les aplicacions 15.08 beta del KDE.
layout: application
release: applications-15.07.80
title: KDE distribueix la beta 15.08 de les aplicacions del KDE
---
28 de juliol de 2015. Avui KDE distribueix la beta de les noves versions de les aplicacions del KDE. S'han congelat les dependències i les funcionalitats, i ara l'equip del KDE se centra a corregir els errors i acabar de polir-la.

Amb diverses aplicacions basades en els Frameworks 5 del KDE, la distribució 15.08 de les aplicacions del KDE necessita una prova exhaustiva per tal de mantenir i millorar la qualitat i l'experiència d'usuari. Els usuaris reals són imprescindibles per mantenir l'alta qualitat del KDE, perquè els desenvolupadors no poden provar totes les configuracions possibles. Comptem amb vós per ajudar a trobar errors amb anticipació, a fi que es puguin solucionar abans de la publicació final. Considereu unir-vos a l'equip instal·lant la beta <a href='https://bugs.kde.org/'>i informant de qualsevol error</a>.
