---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE distribueix les aplicacions 16.04.0 del KDE
layout: application
title: KDE distribueix les aplicacions 16.04.0 del KDE
version: 16.04.0
---
20 d'abril de 2016. Avui KDE presenta les Aplicacions del KDE 16.04 amb una gamma impressionant d'actualitzacions que proporcionen una facilitat d'accés, la introducció de funcionalitats molt útils i l'eliminació de problemes petits que porten les Aplicacions del KDE un pas endavant per oferir-vos la configuració perfecta per al vostre dispositiu.

El <a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, el <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, el <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> i el <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> ara s'han adaptat als Frameworks 5 del KDE i estem a l'espera dels vostres comentaris i opinions sobre les funcionalitats noves introduïdes en aquest llançament. També ens agradaria fomentar el vostre suport per al <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a>, ja que li donem la benvinguda a les Aplicacions del KDE i als vostres comentaris quant al que us agradaria trobar-hi addicionalment.

### L'addició més nova al KDE

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

S'ha afegit una aplicació nova al conjunt d'aplicacions educatives del KDE. El <a href='https://minuet.kde.org'>Minuet</a> és un programa d'educació musical que funciona amb MIDI, amb control del tempo, to i volum, que el fa adequat tant per músics novells com experimentats.

El Minuet inclou 44 exercicis d'entrenament de l'oïda amb escales, acords, intervals i ritmes, habilita la visualització del contingut musical en el teclat del piano i permet la integració dels vostres propis exercicis.

### Més ajuda

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

El KHelpCenter, que anteriorment es distribuïa amb el Plasma, ara es distribueix com a part de les Aplicacions del KDE.

L'equip del KHelpCenter ha portat a terme una campanya descomunal de triatge i resolució d'errors, que ha donat com a resultat 49 errors solucionats, molts d'ells relacionats amb la millora i restauració de la funcionalitat prèvia de cerca.

La cerca interna de documents basada en el programari ht::/dig obsolet s'ha substituït per un sistema d'indexació i cerca basada en Xapian que aporta la restauració de funcionalitats com la cerca a les pàgines «man», pàgines «info» i documentació subministrada pel programari KDE, amb la funció afegida de punts per a les pàgines de documentació.

Amb l'eliminació del suport per les KDELibs4 i una neteja addicional del codi i altres errors petits, el manteniment del codi s'ha preparat a fons per proporcionar-vos el KHelpCenter d'una forma nova més destacable.

### Control enèrgic de plagues

El paquet Kontact ha rebut la solució per a 55 errors, ni més ni menys; alguns dels quals estaven relacionats amb problemes en establir alarmes, la importació de correus del Thunderbird, la reducció de les icones de l'Skype i el Google talk en la vista del plafó dels contactes, les alternatives relacionades amb el KMail com la importació de carpetes, la importació de vCard, l'obertura d'adjunts de correu ODF, les insercions d'URL des del Chromium, l'eina del menú que diferencia entre l'aplicació iniciada com a part del Kontact en oposició al seu ús autònom, la manca de l'element del menú «Envia» i altres. S'ha afegit la implementació per les fonts RSS del portuguès del Brasil, així com l'esmena de les adreces per les fonts hongareses i espanyoles.

Les funcionalitats noves inclouen un redisseny de l'editor de contactes del KAddressbook, un tema predeterminat nou de capçalera del KMail, millores a la configuració de l'exportador i una esmena a la implementació de les icones de web a l'Akregator. S'ha netejat la interfície de composició del KMail, junt amb la introducció d'un tema predeterminat nou de capçalera del KMail, i el tema Grantlee usat a la pàgina «Quant a» del KMail i també al Kontact i l'Akregator. Ara, l'Akregator usa el QtWebKit, un dels motors destacats per a representar pàgines web i executar codi javascript com a motor de representació web, i el procés per a implementar el QtWebEngine a l'Akregator i a altres aplicacions del paquet Kontact ja ha començat. Diverses funcionalitats s'han convertit com a connectors en els kdepim-addons, i les biblioteques «pim» s'han dividit en nombrosos paquets.

### L'arxivat en detall

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

L'<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, el gestor d'arxius, ha solucionat dos errors importants: ara avisa a l'usuari si l'extracció falla per manca d'espai suficient a la carpeta de destí, i no omple tota la RAM durant l'extracció de fitxers enormes via arrossegar i deixar anar.

L'Ark ara també inclou un diàleg de propietats que mostra informació de l'arxiu actualment obert com el tipus d'arxiu, la mida comprimida i descomprimida, o els resums criptogràfics MD5/SHA-1/SHA-256.

Ara l'Ark també pot obrir i extreure arxius RAR sense utilitzar les utilitats «rar» no lliures, i pot obrir, extreure i crear arxius TAR comprimits amb els formats lzop/lzip/lrzip.

La interfície d'usuari de l'Ark s'ha polit reorganitzant la barra de menús i la barra d'eines per millorar la usabilitat i eliminar ambigüitats, també per estalviar espai vertical gràcies al fet que ara la barra d'estat està oculta de manera predeterminada.

### Més precisió a l'edició de vídeo

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

El <a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, l'editor de vídeo no lineal ha implementat nombroses funcionalitats noves. El titulador ara té una funcionalitat de quadrícula, degradats, l'addició de text ombrejat i l'ajustament de l'espai entre lletres i l'interlineat.

La finestra integrada dels nivells d'àudio permet controlar fàcilment l'àudio del projecte, i junt amb el nou monitor de vídeo superposa la velocitat de reproducció dels fotogrames, zones segures i les formes d'ona de l'àudio i una barra d'eines per cercar els marcadors i un monitor de zoom.

També hi ha una funcionalitat de biblioteca nova que permet copiar/enganxar seqüències entre projectes i una vista dividida en la línia de temps per comparar i visualitzar els efectes aplicats al clip i sense aplicar.

S'ha reescrit el diàleg de representació amb una opció afegida per fer una codificació més ràpida, produint fitxers grans i l'efecte de velocitat també s'ha fet per funcionar amb so.

S'han introduït corbes en els fotogrames clau per diversos efectes i ara els efectes més usats es poden accedir ràpidament via el giny d'efectes preferits. En usar l'eina de navalla, ara la línia vertical de la línia de temps mostrarà el marc exacte a on s'efectuarà el tall i els nous generadors de clips permeten crear clips de barres de colors i comptadors. A banda, s'ha tornat a introduir els comptadors d'ús de clips a la safata del projecte i les miniatures d'àudio s'han reescrit per a fer-les molt més ràpides.

Les esmenes d'errors més importants inclouen la fallada en usar títols (que requereix la darrera versió de MLT), la solució de corrupcions quan els fotogrames per segon del projecte eren diferents de 25, les fallades en la supressió de pistes, el mode de sobreescriptura problemàtic en la línia de temps, i la corrupció/pèrdua d'efectes en usar una configuració regional amb una coma de separador. A banda d'aquests, l'equip ha treballat coherentment per fer millores importants en l'estabilitat, el flux de treball i petites funcionalitats d'usabilitat.

### I més!

L'<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, el visualitzador de documents, aporta noves funcionalitats com l'amplada que es pot personalitzar a les anotacions en línia, la possibilitat d'obrir directament fitxers incrustats en lloc de només desar-los, i també la visualització dels marcadors de les taules de continguts quan els enllaços fills estan col·lapsats.

El <a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> ha introduït una implementació millorada per al GnuPG 2.1 amb l'esmena d'errors de l'autocomprovació i les actualitzacions incòmodes de claus provocats per la nova disposició de directoris del GnuPG (GNU Privacy Guard). S'ha afegit la generació de claus ECC amb la visualització dels detalls de les corbes per als certificats ECC. El Kleopatra ara s'ha publicat com un paquet separat i s'ha inclòs la implementació de fitxers .pfx i .crt. Per resoldre la diferència en el comportament de les claus importades i les claus generades, el Kleopatra ara permet definir la confiança del propietari a màxima per a les claus secretes importades.
