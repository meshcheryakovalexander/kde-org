---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE distribueix les aplicacions 15.08.1 del KDE
layout: application
title: KDE distribueix les aplicacions 15.08.1 del KDE
version: 15.08.1
---
15 de setembre de 2015. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../15.08.0'>aplicacions 15.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 40 esmenes registrades d'errors que inclouen millores a les Kdelibs, Kdepim, Kdenlive, Dolphin, Marble, Kompare, Konsole, Ark i Umbrello.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.12.
