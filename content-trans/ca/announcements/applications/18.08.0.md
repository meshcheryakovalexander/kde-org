---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE distribueix les aplicacions 18.08.0 del KDE
layout: application
title: KDE distribueix les aplicacions 18.08.0 del KDE
version: 18.08.0
---
16 d'agost de 2018. S'han publicat les aplicacions 18.08.0 de KDE.

Treballem contínuament per a millorar el programari inclòs a les sèries de les aplicacions del KDE, i esperem que trobeu útils totes les millores i esmenes d'errors!

### Novetats a les Aplicacions 18.08 del KDE

#### Sistema

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

El <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, és el potent gestor de fitxers del KDE, ha rebut diverses millores de qualitat de vida:

- El diàleg «Arranjament» s'ha modernitzat perquè segueixi millor les nostres pautes de disseny i que aquest sigui més intuïtiu.
- S'han eliminat diverses pèrdues de memòria que poden alentir l'ordinador.
- Els elements del menú «Crea nou» ja no estan disponibles quan es visualitza la paperera.
- L'aplicació ara s'adapta millor a les pantalles amb alta resolució.
- El menú contextual inclou més opcions útils, les quals permeten ordenar i canviar directament el mode de vista.
- L'ordenació per hora de modificació ara és 12 vegades més ràpida. A més, ara podeu llançar el Dolphin de nou quan hàgiu iniciat la sessió amb el compte d'usuari «root». Encara s'està treballant en la implementació per a modificar el propietari «root» dels fitxers quan s'executa el Dolphin com a usuari normal.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Múltiples millores per al <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, l'aplicació d'emulació de terminal del KDE, estan disponibles:

- El giny de «Cerca» ara apareixerà a la part superior de la finestra sense interrompre el flux de treball.
- S'ha afegit la implementació per a més seqüències d'escapada (modes DECSCUSR i desplaçament alternatiu de l'XTerm).
- També podeu assignar qualsevol caràcter com a tecla per a una drecera.

#### Gràfics

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

La 18.08 és una versió major per al <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, el visualitzador i organitzador d'imatges del KDE. Durant els darrers mesos, els col·laboradors han treballat en una gran quantitat de millores. Els aspectes destacats inclouen:

- La barra d'estat del Gwenview ara inclou un comptador d'imatges i mostra el nombre total d'imatges.
- Ara és possible ordenar per puntuació i en ordre descendent. L'ordenació per data ara separa els directoris i els arxius, i s'ha esmenat en algunes situacions.
- S'ha millorat la implementació per arrossegar i deixar anar, per a permetre arrossegar fitxers i carpetes al mode de vista per a mostrar-los, així com arrossegar elements per a visualitzar-los en aplicacions externes.
- Enganxar les imatges copiades des del Gwenview ara també funciona per a les aplicacions que només accepten dades d'imatge RAW, però sense cap camí de fitxer. També es poden copiar les imatges modificades.
- S'ha posat a punt el diàleg per a redimensionar la imatge per a una major usabilitat i per afegir una opció per a redimensionar les imatges en funció d'un percentatge.
- S'ha esmenat el control lliscant de mida de l'eina «Reducció dels ulls vermells» i el cursor de creu.
- La selecció del fons transparent ara té una opció per a «Sense» i també es pot configurar per a les SVG.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

S'ha fet que el zoom de la imatge sigui més pràctic:

- S'habilita el zoom desplaçant o fent clic, així com la panoràmica quan les eines «Escapça» o «Reducció dels ulls vermells» estan actives.
- Fer clic del mig alterna entre el zoom ajustat i el zoom al 100%.
- S'han afegit les dreceres de teclat «Majúscules-Clic_mig» i «Majúscules+F» per alternar el zoom ajustat.
- «Ctrl-fer_clic» ara fa el zoom més ràpid i fiable.
- El Gwenview ara fa zoom a la posició actual del cursor per a les operacions de zoom Amplia/Redueix, Ajusta i al 100% quan s'utilitza el ratolí i les dreceres de teclat.

El mode de comparació de les imatges ha rebut diverses millores:

- S'han solucionat la mida i alineació del ressaltat de la selecció.
- S'ha solucionat el superposat dels SVG en el ressaltat de la selecció.
- Per a imatges SVG petites, el ressaltat de la selecció coincideix amb la mida de la imatge.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

S'han introduït una sèrie de petites millores per a fer que el flux de treball de l'usuari encara sigui més agradable:

- S'ha millorat la transició de l'esvaïment entre les imatges amb mides i transparències diferents.
- S'ha solucionat la visibilitat de les icones en alguns botons flotants quan s'utilitza un esquema de color clar.
- Quan es desa una imatge amb un nom nou, el visualitzador no saltarà després a una imatge sense relació.
- Quan es fa clic al botó per a compartir i els connectors de Kipi no estan instal·lats, el Gwenview demanarà a l'usuari que els instal·li. Després de la instal·lació, es mostraran immediatament.
- Ara s'evita que s'oculti accidentalment la barra lateral mentre es canvia la mida i aquesta recorda la seva amplada.

#### Oficina

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

El <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, el potent client de correu electrònic del KDE, presenta algunes millores en el motor per a l'extracció de les dades del viatge. Ara admet els codis de barres UIC 918.3 i SNCF dels bitllets de tren, i la cerca de la ubicació de l'estació de tren a la Wikidata. S'ha afegit una implementació per a itineraris de viatgers múltiples, i el KMail ara està integrat amb l'aplicació Itinerari del KDE.

L'<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, el marc de treball per a la gestió de la informació personal, ara és més ràpid gràcies al contingut de la notificació i que admet XOAUTH per SMTP, el qual permet l'autenticació autòctona amb el Gmail.

#### Educació

El <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, un frontal de KDE per al programari matemàtic, ara desa l'estat dels plafons («Variables», «Ajuda», etc.) separadament per a cada sessió. Les sessions del Julia seran molt més ràpides en crear.

S'ha millorat significativament l'experiència d'usuari en els dispositius tàctils amb el <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, la nostra calculadora gràfica.

#### Utilitats

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Col·laboracions a l'<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, una eina versàtil per a les captures de pantalla al KDE, s'han centrat a millorar el mode «Regió rectangular»:

- En el mode «Regió rectangular», ara hi ha una lupa que us ajudarà a dibuixar un rectangle de selecció perfecte per píxels.
- Ara podeu moure i canviar la mida del rectangle de selecció emprant el teclat.
- La interfície d'usuari segueix l'esquema de color de l'usuari i s'ha millorat la presentació del text d'ajuda.

Per a fer que les captures de pantalla es puguin compartir fàcilment amb altres persones, els enllaços per a les imatges compartides ara es copien automàticament al porta-retalls. Les captures de pantalla es poden desar automàticament en subdirectors especificats per l'usuari.

El <a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, la nostra gravadora de càmera web, s'ha actualitzat per evitar fallades amb les versions més noves de GStreamer.

### Cacera d'errors

S'han resolt més de 120 errors a les aplicacions, incloses el paquet Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle, Umbrello i més!

### Registre complet de canvis
