---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE distribueix les aplicacions 16.12.3 del KDE
layout: application
title: KDE distribueix les aplicacions 16.12.3 del KDE
version: 16.12.3
---
9 de març de 2017. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../16.12.0'>aplicacions 16.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 20 esmenes registrades d'errors que inclouen millores al Kdepim, Ark, Filelight, Gwenview, Kate, Kdenlive i Okular entre d'altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.30.
