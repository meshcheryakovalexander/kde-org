---
aliases:
- ../../kde-frameworks-5.79.0
date: 2021-02-13
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Adaptació des de QNetworkRequest::FollowRedirectsAttribute a QNetworkRequest::RedirectPolicyAttribute

### Baloo

* [SearchStore] Elimina les dependències del sistema de fitxers de la propietat «includeFolder»
* [FileIndexerConfig] Corregeix la verificació d'ocultació per als directoris explícitament inclosos
* [FilteredDirIterator] Sol·licita els fitxers ocults del QDirIterator

### Icones Brisa

* Icones noves de «telegram-panel»
* Usa l'estil correcte per a les icones «align-horizontal-left-out» (error 432273)
* Afegeix icones «kickoff» noves (error 431883)
* Afegeix «rating-half», 100% opacitat a «rating-unrated», color de text de «rating» en el tema fosc
* Elimina les icones KeePassXC (error 431593)
* Corregeix les icones @3x (error 431475)
* Afegeix la icona «neochat»

### Mòduls extres del CMake

* Només activa GNU_TAR_FOUND si està disponible «--sort=name»
* Elimina la generació de metadades «fastlane» d'una APK donada
* KDEFrameworksCompilerSettings: defineix -DQT_NO_KEYWORDS i -DQT_NO_FOREACH com a valors predeterminats
* [KDEGitCommitHooks] Crea una còpia dels scripts al directori origen
* També admet la nova extensió de fitxer d'Appstream
* fetch-translations: resol l'URL abans de passar-lo a «fetchpo.rb»
* Considera els URL de donació d'Appstream per a crear les metadades del F-Droid
* Corregeix els permisos dels scripts (error 431768)
* ECMQtDeclareLoggingCategory: crea els fitxers «.categories» en la construcció, no en la configuració
* Afegeix la funció del CMake per a configurar els «hooks» «pre-commit» del Git

### Integració del marc de treball

* Corregeix les decoracions de les finestres que no són desinstal·lables (error 414570)

### Eines de Doxygen del KDE

* Assegura que no s'usa el tipus de lletra predeterminada del «doxygen»
* Millora la renderització de QDoc i corregeix un error en el tema fosc
* Actualitza la informació del mantenidor
* Tema nou de trinca coherent amb develop.kde.org/docs

### KCalendarCore

* Suprimeix el registre de MemoryCalendar com a observador en eliminar una incidència
* Usa la «recurrenceId» per a suprimir l'ocurrència correcta
* També neteja les associacions amb la llibreta de notes en tancar un MemoryCalendar

### KCMUtils

* Assegura el mode de columna única

### KCodecs

* Elimina l'ús de literals de cadena no UTF-8

### KCompletion

* Corregeix una regressió causada per l'adaptació de l'operador + a l'operador |

### KConfig

* Refactoritza el codi per a desar/restaurar la geometria de les finestres perquè sigui menys fràgil
* Corregeix la mida de la finestra en tancar mentre està maximitzada (error 430521)
* KConfig: manté el component dels mil·lisegons de QDateTime

### KCoreAddons

* Afegeix KFuzzyMatcher per a un filtratge aproximat de cadenes
* KJob::infoMessage: documenta que l'argument «richtext» s'eliminarà, sense ús
* KJobUiDelegate::showErrorMessage: implementa amb «qWarning()»
* Fa obsolets els mètodes relacionats amb X-KDE-PluginInfo-Depends
* Elimina les claus X-KDE-PluginInfo-Depends

### KDeclarative

* Permet elements de columna única
* KeySequenceItem: assigna una cadena buida en netejar en lloc d'indefinida (error 432106)
* Desambigua els estats seleccionats enfront dels passats per sobre per a GridDelegate (error 406914)
* Activa de manera predeterminada el mode «Single»

### KFileMetaData

* ffmpegextractor: usa «av_find_default_stream_index» per a trobar el flux de vídeo

### KHolidays

* Actualitza els festius de Maurici per al 2021
* Actualitza els festius taiwanesos

### KI18n

* No estableix el còdec per a «textstream» en construir amb les Qt6

### KImageFormats

* Simplifica una porció del codi de perfil de color NCLX
* [imagedump] Afegeix l'opció «list MIME type» (-m)
* Soluciona una fallada amb els fitxers malformats
* ani: assegura que «riffSizeData» és de la mida correcta abans de fer la dansa del «cast» «quint32_le»
* Afegeix un connector per als cursors animats de Windows (ANI)

### KIO

* Usa la macro Q_LOGGING_CATEGORY en lloc de la QLoggingCategory explícita (error 432406)
* Corregeix el còdec predeterminat que s'estableix a «US-ASCII» a les aplicacions del KIO (error 432406)
* CopyJob: corregeix una fallada en ometre/reintentar (error 431731)
* KCoreDirLister: elimina la sobrecàrrega dels senyals «canceled()» i «completed()»
* KFilePreviewGenerator: modernitza el codi base
* KCoreDirLister: elimina la sobrecàrrega del senyal «clear()»
* MultiGetJob: elimina la sobrecàrrega de senyals
* FileJob: elimina la sobrecàrrega del senyal «close()»
* SkipDialog: fa obsolet el senyal «result(SkipDialog *_this, int _button)»
* Corregeix un interbloqueig en reanomenar un fitxer des del diàleg de propietats (error 431902)
* Fa obsolets «addServiceActionsTo» i «addPluginActionsTo»
* [KFilePlacesView] Baixa l'opacitat dels elements ocults a «mostra-ho tot»
* No canvia els directoris en obrir URL no llistables
* Ajusta la lògica de KFileWidget::slotOk en el mode fitxers+directori
* FileUndoManager: corregeix el desfet de la còpia de directoris buits
* FileUndoManager: no sobreescriu fitxers en desfer
* FileUndoManager: fa obsolet el mètode «undoAvailable()»
* ExecutableFileOpenDialog: fa més genèrica l'etiqueta de text
* KProcessRunner: només emet una vegada el senyal «processStarted()»
* Reverteix «kio_trash: corregeix la lògica quan no s'ha definit cap límit de mida»

### Kirigami

* Usa una icona no simbòlica per a l'acció de sortida
* Corregeix els botons del menú de la barra d'eines en deixar-los anar correctament
* Usa una subsecció en lloc d'una secció
* [controls/BasicListItem]: Afegeix la propietat «reserveSpaceForSubtitle»
* Fa explícita l'alçada implícita del botó de navegació
* Corregeix l'alineació vertical de «BasicListItem»
* [basiclistitem] Assegura que les icones són quadrades
* [controls/ListItem]: Elimina el separador sagnat per als elements inicials
* Torna a afegir el marge dret del separador d'elements de llista quan hi ha un element inicial
* No crida manualment «reverseTwinsChanged» en destruir FormLayout (error 428461)
* [org.kde.desktop/Units] Fa que les durades coincideixin amb «controls/Units»
* [Units] Redueix «veryLongDuration» a 400 ms
* [Units] Redueix «shortDuration» i «longDuration» en 50 ms
* No considera els esdeveniments de ratolí «Synthetized» com a Mouse (error 431542)
* Ús més enèrgic d'«implicitHeight» en lloc de «preferredHeight»
* Usa les textures «atlas» per a les icones
* controls/AbstractApplicationHeader: centra verticalment els fills
* [actiontextfield] Corregeix els marges i la mida de les accions en línia
* Actualitza correctament la mida de la capçalera (error 429235)
* [controls/OverlaySheet]: Respecta Layout.maximumWidth (error 431089)
* [controls/PageRouter]: Exposa els paràmetres definits en bolcar «currentRoutes»
* [controls/PageRouter]: Exposa els paràmetres de nivell superior en el mapa de propietats «params»
* Mou els exemples relacionats amb «pagerouter» a una subcarpeta
* Possibilita arrossegar la finestra en àrees no interactives
* AbstractApplicationWindow: usa una finestra ampla en sistemes d'escriptori per a tots els estils
* No oculta el separador d'elements de llista en passar per sobre quan el fons és transparent
* [controls/ListItemDragHandle] Corregeix un arranjament erroni en el cas de no desplaçament (error 431214)
* [controls/applicationWindow]: Té en compte les amplades del calaix en calcular «wideScreen»

### KNewStuff

* Refactoritza la capçalera i el peu de pàgina KNSQuick per al Kirigami
* Afegeix una etiqueta de número al component Ratings per a una intel·ligibilitat més fàcil
* Redueix la mida mínima de la finestra de diàleg QML GHNS
* Coincideix amb una aparença més clara en passar-hi per sobre per als delegats de quadrícula dels KCM
* Assegura l'amplada mínima per al diàleg QML si és d'amplada de pantalla o menys
* Corregeix la disposició del delegat BigPreview
* Torna a validar les entrades de la memòria cau abans de mostrar el diàleg
* Afegeix la implementació per als URL «kns:/» a l'eina «knewstuff-dialog» (error 430812)
* filecopyworker: Obre els fitxers abans de llegir/escriure
* Reinicia l'entrada a actualitzable quan no s'ha identificat cap contingut a actualitzar (error 430812)
* Corregeix una fallada fortuïta deguda a mantenir incorrectament un apuntador
* Fa obsoleta la classe DownloadManager
* Fa obsoleta la propietat AcceptHtmlDownloads
* Fa obsoletes les propietats ChecksumPolicy i SignaturePolicy
* Fa obsoleta la propietat Scope
* Fa obsoleta la propietat CustomName

### KNotification

* Emet NewMenu quan s'estableix un menú contextual nou (error 383202)
* Fa obsolet KPassivePopup
* Assegura que tots els dorsals referencien la notificació abans de fer la feina
* Fa que l'aplicació d'exemple de notificació es construeixi i funcioni a l'Android
* Mou la gestió de la ID de notificació dins la classe KNotification
* Corregeix l'eliminació de la notificació pendent de la cua (error 423757)

### Framework del KPackage

* Documenta la propietat de PackageStructure en usar PackageLoader

### KPty

* Corregeix la generació del camí complet a «kgrantpty» en el codi per a ! HAVE_OPENPTY

### KQuickCharts

* Afegeix un mètode «primer» a ChartDataSource i l'usa a Legend (error 432426)

### KRunner

* Comprova si hi ha una acció seleccionada en cas de coincidència d'informació
* Corregeix una cadena de resultat buida per a l'activitat actual
* Fa obsoletes les sobrecàrregues per als «ids» de QueryMatch
* [DBus Runner] Prova de RemoteImage

### KService

* Fa obsolet KPluginInfo::dependencies()
* CMake: especifica les dependències d'«add_custom_command()»
* Fa explícitament obsoleta la sobrecàrrega de KToolInvocation::invokeTerminal
* Afegeix un mètode per a obtenir KServicePtr de l'aplicació de terminal predeterminada
* KService: afegeix un mètode per a establir «workingDirectory»

### KTextEditor

* [Vimode] No commutar la vista en canviar a majúscules/minúscules (ordre ~) (error 432056)
* Augmenta l'amplada màxima del sagnat a 200 (error 432283)
* Assegura que s'actualitza el mapatge de l'interval p. ex. a la invalidació dels intervals buits nous
* Només mostra l'error de caràcters dels punts en el «vimode» (error 424172)
* [vimode] Corregeix el moviment de l'element coincident per un menys
* Reté el text de substitució mentre no es tanqui la barra de cerca potent (error 338111)
* KateBookMarks: modernitza el codi base
* No ignora el canal alfa quan hi ha una marca
* Corregeix el canal alfa que s'ignorava en llegir des de la interfície de configuració
* Evita que la vista prèvia de concordança de parèntesis s'estengui fora de la vista
* Evita que la vista prèvia de concordança de parèntesis a vegades romangui després de canviar a una pestanya diferent
* Fa més compacta la vista prèvia de concordança de parèntesis
* No mostra la vista prèvia de concordança de parèntesis si cobrirà el cursor
* Maximitza l'amplada de la vista prèvia de concordança de parèntesis
* Oculta la vista prèvia de concordança de parèntesis en desplaçar-se
* Evita intervals duplicats de ressaltat que mataran la renderització ARGB
* Exposa el KSyntaxHighlighting::Repository global només de lectura
* Corregeix un error de sagnat quan la línia conté «for» o «else»
* Corregeix un error de sagnat
* Elimina el cas especial «tagLine», ha provocat fallades aleatòries d'actualització, p. ex. al ressaltat d'«iconborder», números de línia i línia actual
* Corregeix la renderització dels marcadors d'ajust de paraula + selecció
* Corregeix el sagnat en prémer retorn i el paràmetre de la funció té una coma al final
* Pinta el petit buit en el color de la selecció, si el final de la línia anterior és a la selecció
* Simplifica el codi + esmena els comentaris
* Reverteix l'error retallat, ha suprimit massa codi per a renderitzacions extres
* Evita el pintat complet de la selecció de línia, més semblant a altres editors
* Adapta el sagnador als fitxers de ressaltat canviats
* [Vimode] Adapta Command a QRegularExpression
* [Vimode] Adapta «findPrevWordEnd» i «findSurroundingBrackets» a QRegularExpression
* [Vimode] Adapta QRegExp::lastIndexIn a QRegularExpression i QString::lastIndexOf
* [Vimode] Adapta ModeBase::addToNumberUnderCursor a QRegularExpression
* [Vimode] Adapta els usos senzills de QRegExp::indexIn a QRegularExpression i QString::indexOf
* Usa «rgba(r,g,b,aF)» per a gestionar el canal alfa a l'exportació HTML
* Respecta els colors de l'alfa en exportar HTML
* Presenta un mètode «helper» per a obtenir correctament el nom del color
* Implementació dels colors de l'alfa: capa de configuració
* Evita que els marcadors de línia canviada matin el ressaltat de la línia actual
* Pinta també el ressaltat de la línia actual sobre l'«iconborder»
* Activa el canal alfa per als colors de l'editor
* Corregeix el ressaltat de la línia actual quan hi ha un buit petit al començament de les línies ajustades dinàmicament
* No fa càlculs sense necessitat, compara directament els valors
* Corregeix el ressaltat de la línia actual quan hi ha un buit petit al començament
* [Vimode] No omet l'interval plegat quan el moviment aterra a dins
* Usa un bucle «for» d'interval sobre «m_matchingItems» en lloc d'usar QHash::keys()
* Adapta «normalvimode» a QRegularExpression
* Exporta adequadament les dependències correctes
* Exposa el tema de KSyntaxHighlighting
* Afegeix «configChanged» a KTextEditor::Editor, també per a canvis globals de configuració
* Reordena una mica la configuració
* Corregeix la gestió de tecles
* Afegeix els paràmetres «doc»/«view» als senyals nous, corregeix les connexions antigues
* Permet accedir i alterar el tema actual via la interfície de configuració
* Elimina el qualificador «const» en passar-hi LineRanges
* Usa «.toString()», ja que a QStringView li manca «.toInt()» a les versions antigues de les Qt
* Declara «toLineRange()» «constexpr» en línia sempre que sigui possible
* Reutilitza la versió de QStringView des de QStringRev, elimina el qualificador «const»
* Mou el comentari «TODO KF6» fora del comentari del «doxygen»
* Cursor, Range: afegeix la sobrecàrrega de «fromString(QStringView)»
* Permet que es pugui desactivar «Sagnat alineat a ajust dinàmic de les paraules» (error 430987)
* LineRange::toString(): evita el signe «-», ja que és confús per a nombres negatius
* Usa KTextEditor::LineRange en el mecanisme de «notifyAboutRangeChange()»
* Adapta «tagLines()», «checkValidity()» i «fixLookup()» a LineRanges
* Afegeix KTextEditor::LineRange
* Mou la implementació de KateTextBuffer::rangesForLine() a KateTextBlock i evita construccions innecessàries de contenidors
* [Vimode] Corregeix les cerques dins d'intervals plegats (error 376934)
* [Vimode] Corregeix la reproducció de compleció de macro (error 334032)

### KTextWidgets

* Té més classes privades heretades d'aquelles dels pares

### KUnitConversion

* Defineix la variable abans d'usar-la

### KWidgetsAddons

* Fa ús d'AUTORCC
* Té més classes privades heretades d'aquelles dels pares
* Inclou explícitament QStringList

### KWindowSystem

* Afegeix «helpers» d'utilitat d'opacitat fraccionària
* Corregeix efectivament les inclusions
* Corregeix les inclusions
* xcb: treballa amb la pantalla activa com ha informat QX11Info::appScreen()

### KXMLGUI

* Corregeix les inclusions
* Afegeix el senyal KXMLGUIFactory::shortcutsSaved
* Usa l'URL correcte de «kde get involved» (error 430796)

### Frameworks del Plasma

* [plasmoidheading] Usa el color destinat per als peus de pàgina
* [plasmacomponents3/spinbox] Corregeix el color seleccionat de text
* widgets>lineedit.svg: corregeix els problemes de pèrdua d'alineació a píxel (error 432422)
* Corregeix el farciment esquerre i dret a PlasmoidHeading
* Actualitza els colors de «breeze-dark»/«breeze-light»
* Reverteix «[SpinBox] Corregeix un error lògic a la direccionalitat del desplaçament»
* [calendar] Corregeix els noms d'importació que manquen
* [ExpandableListItem] Fa que la vista expandida de llista d'accions respecti el tipus de lletra
* DaysCalendar: adapta a PC3/QQC2 a on sigui possible
* Elimina l'animació de passar per sobre dels botons plans, calendari, elements de llista, botons
* Bolca els errors del plasmoide a la consola
* Resol la dependència cíclica d'Units.qml
* [PlasmaComponents MenuItem] Crea una acció fictícia quan l'acció es destrueix
* No fa mapes de píxels més grans del que es necessitin
* Afegeix els fitxers de projecte dels IDE JetBrains per a ignorar
* Corregeix els avisos de Connections
* Afegeix RESET a la propietat «globalShortcut» (error 431006)

### Purpose

* [nextcloud] Refà la IU de configuració
* Avalua la configuració inicial
* Talla les ListViews a la configuració del KDEConnect i del Bluetooth
* Elimina les propietats adjuntes innecessàries de Layout
* [plugins/nextcloud] Usa la icona del Nextcloud
* [cmake] Mou «find_package» al nivell principal de CMakeLists.txt

### QQC2StyleBridge

* [combobox] Corregeix la velocitat de desplaçament del ratolí tàctil (error 400258)
* «qw» pot ser nul
* Implementa QQuickWidget (error 428737)
* Permet arrossegar la finestra des d'àrees buides

### Solid

* CMake: usa «configure_file()» per a assegurar les construccions «noop» incrementals
* [Fstab] Ignora els muntatges «overlay» del Docker (error 422385)

### Sonnet

* No fa cerques múltiples quan una és suficient

### Ressaltat de la sintaxi

* Afegeix un increment de la versió de «context.xml» que mancava
* Admet els finals de fitxer oficials, vegeu https://mailman.ntg.nl/pipermail/ntg-context/2020/096906.html
* Actualitza els resultats de referència
* Color d'operador menys vibrant per als temes Brisa
* Corregeix la sintaxi de l'«hugo» nou
* Corregeix errors de sintaxi
* Millora la intel·ligibilitat del tema Solarized fosc
* Elimina captures innecessàries amb una regla dinàmica
* Fusiona el ressaltat d'operadors => actualitza les referències
* Fusiona el ressaltat d'operadors
* Afegeix sobrecàrregues de «const» per a diversos accessors
* Bash, Zsh: corregeix cmd;; en un cas (error 430668)
* Atom Light, Breeze Dark/Light: color nou per a l'Operator; Breeze Light: color nou per al ControlFlow
* email.xml: detecta comentaris imbricats i caràcters escapats (error 425345)
* Actualitza «atom light» per a usar els colors de l'alfa
* Reassigna diversos estils de Symbol i Operator a «dsOperator»
* Bash: corregeix } a ${!xy*} i més operadors d'expansió de paràmetres (# a ${#xy} ; !,*,@,[*],[@] a ${!xy*}) (error 430668)
* Actualitza les revisions de tema
* Actualitza el ressaltat de sintaxi del «kconfig» al Linux 5.9
* Usa «rgba(r,g,b,aF)» per a gestionar el canal alfa correctament per als Qt/Browsers
* No usa «rgba» a «themeForPalette»
* Assegura que «rgba» es respecta a HTML i a Format
* Correccions per a «atom-one-dark»
* Diverses actualitzacions addicionals per a l'Atom One fosc
* No comprova «rgba» en comprovar una coincidència millor
* Permet usar el canal alfa als colors de tema
* Actualitza els colors del Monokai i corregeix el blau incorrecte
* C++: corregeix el sufix «us»
* Bash: correcció núm. 5: $ al final d'una cadena amb cometa doble
* VHDL: corregeix una funció, un procediment, els tipus de «range/units» i altres millores
* Actualitza «breeze-dark.theme»
* Raku: núm. 7: corregeix els símbols que comencen per Q
* Correcció ràpida de contrast que manca per a Extension
* Afegeix l'esquema de color Oblivion des de GtkSourceView/Pluma/gEdit

### ThreadWeaver

* Corregeix els iteradors de mapa en construir amb les Qt6
* No inicia explícitament els «mutexes» com a NonRecursive

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
