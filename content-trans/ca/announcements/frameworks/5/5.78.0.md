---
aliases:
- ../../kde-frameworks-5.78.0
date: 2021-01-09
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Respecta que el treball s'interrompi immediatament (error 429939)

### Baloo

* [ExtractorProcess] Mou el senyal del DBus del «helper» al procés principal
* [timeline] Consolida el codi per a «stat» i «list» de la carpeta arrel
* Fa que les entrades UDS dels «ioslave» del nivell superior siguin només de lectura
* Evita els errors d'inici d'aplicació si no s'ha creat mai l'índex del Baloo
* [BasicIndexingJob] Treu les barres del final de les carpetes (error 430273)

### Icones Brisa

* Afegeix la icona nova d'acció «compass»
* Afegeix la icona «image-missing» al tema
* Afegeix una icona per a les imatges VIM

### Mòduls extres del CMake

* Indica al MSVC que aquests fitxers de codi font estan codificats amb UTF-8
* Afegeix Findepoxy.cmake
* Considera els actius d'imatge «fastlane» locals
* Arxius tar reproduïbles només amb el «tar» de GNU
* Conserva el subconjunt de text enriquit admès pel F-Droid
* Actualitza la versió requerida del CMake per l'Android.cmake (error 424392)
* Detecta automàticament les dependències de biblioteques dels connectors a l'Android
* Comprova si existeix el fitxer abans d'eliminar l'arxiu «fastlane»
* Neteja la carpeta d'imatges i el fitxer d'arxiu abans de baixar-los/generar-los
* Reté l'ordre de les captures de pantalla del fitxer «appstream»
* Windows: corregeix QT_PLUGIN_PATH per a les proves
* No falla si no s'ha trobat cap categoria
* Fa que KDEPackageAppTemplates creï arxius tar reproduïbles

### KActivitiesStats

* Elimina la funcionalitat «lastQuery» trencada, corregeix les fallades del KRunner per a mi

### KCalendarCore

* CMakeLists.txt - augmenta la versió mínima de la «libical» a 3.0

### KCMUtils

* El KPluginSelector implementa l'indicador predeterminat de ressaltat
* kcmoduleqml: no vincula l'amplada de les columnes a l'amplada de la vista (error 428727)

### KCompletion

* [KComboBox] Corregeix una fallada en cridar «setEditable(false)» amb el menú contextual obert

### KConfig

* Corregeix les finestres que es maximitzen inadequadament en iniciar (erro 426813)
* Format correcte de la cadena maximitzada de finestra
* Corregeix la mida i la posició de la finestra en el Windows (error 429943)

### KConfigWidgets

* KCodecAction: afegeix els senyals no sobrecarregats «codecTriggered» i «encodingProberTriggered»

### KCoreAddons

* Adapta KJobTrackerInterface a la sintaxi de connexió de les Qt5
* KTextToHtml: corregeix un «assert» per una crida «at()» fora de límits
* Usa una jerarquia plana per als camins dels connectors a l'Android
* Conversió de «desktop» a JSON: ignora les entrades «Actions=»
* Fa obsolet KProcess::pid()
* ktexttohtml: corregeix l'ús de KTextToHTMLHelper

### KCrash

* Usa «std::unique_ptr<char[]>» per a evitar fuites de memòria

### KDeclarative

* Canvia a Findepoxy proporcionat pels ECM
* KCMShell: Afegeix la implementació per a passar arguments
* Solució temporal d'una fallada amb la detecció de GL i «kwin_wayland»
* [KQuickAddons] QtQuickSettings::checkBackend() com a alternativa de reserva per al dorsal de programari (error 346519)
* [abstractkcm] Corregeix la versió d'importació a l'exemple de codi
* Evita establir QSG_RENDER_LOOP si ja s'ha establert
* ConfigPropertyMap: carrega el valor predeterminat de la propietat en el mapa

### KDocTools

* Afegeix una entitat per a l'acrònim del MathML
* Canvia «Naval Battle» a «KNavalBattle» per complir amb els aspectes legals

### KGlobalAccel

* Evita l'inici automàtic de «kglobalaccel» en apagar (error 429415)

### KHolidays

* Actualitza els festius japonesos

### KIconThemes

* Omet l'avís per a diverses icones Adwaita per a compatibilitat cap enrere
* QSvgRenderer::setAspectRatioMode() es va introduir a les Qt 5.15

### KImageFormats

* Afegeix AVIF a la llista de formats acceptats
* Afegeix un connector per al Format de fitxer d'imatge AV1 (AVIF)

### KIO

* [KFileItemDelegate] No malgasta espai amb icones inexistents en columnes diferents de la primera
* KFilePlacesView, KDirOperator: adapta a «askUserDelete()» asíncron
* Refà la manera que CopyJob cerca les extensions de JobUiDelegate
* Presenta AskUserActionInterface, una API asíncrona per als diàlegs Reanomena/Omet
* RenameDialog: només crida «compareFiles()» per fitxers
* kcm/webshortcuts: corregeix el botó Reinicia
* KUrlNavigatorMenu: corregeix la gestió del clic del mig
* Elimina l'element «knetattach» de la vista de l'«ioslave» «remote://» (error 430211)
* CopyJob: adapta a AskUserActionInterface
* Jobs: afegeix el senyal «mimeTypeFound» no sobrecarregat per a fer obsolet «mimetype»
* RenameDialog: afegeix la inicialització que mancava d'un «nullptr» (error 430374)
* KShortUriFilter: no filtra les cadenes «../» i similars
* No fa un «assert» si KIO::rawErrorDetail() rep un URL sense esquema (error 393496)
* KFileItemActions: corregeix una condició, es vol excloure només els directoris remots (error 430293)
* KUrlNavigator: elimina l'ús de «kurisearchfilter»
* KUrlNavigator: fa complecions de feina de camins relatius (error 319700)
* KUrlNavigator: resol camins relatius de directori (error 319700)
* Silencia avisos relatius a problemes de configuració del Samba quan no s'usa explícitament el Samba
* KFileWidget: permet seleccionar fitxers que comencin per «:» (error 322837)
* [KFileWidget] Corregeix la posició del botó d'adreces d'interès a la barra d'eines
* KDirOperator: fa obsolet «mkdir(const QString &, bool)»
* KFilePlacesView: permet definir una mida estàtica d'icona (error 182089)
* KFileItemActions: afegeix un mètode nou per a inserir accions «openwith» (error 423765)

### Kirigami

* [controls/SwipeListItem]: Mostra sempre les accions a l'escriptori de manera predeterminada
* [overlaysheet] Usa un posicionament més condicional per al botó de tancar (error 430581)
* [controls/avatar]: Obre un AvatarPrivate intern com a API pública de NameUtils
* [controls/avatar]: Exposa el color generat
* Afegeix el component Hero
* [controls/Card]: Elimina l'animació de passar per sobre
* [controls/ListItem]: Elimina l'animació de passar per sobre
* Mou ListItems per a usar «veryShortDuration» per a passar-hi per sobre en lloc de «longDuration»
* [controls/Units]: Afegeix «veryShortDuration»
* Dona color a la icona ActionButton
* Usa només mapes de píxels d'icona tan grans com es necessitin
* [controls/avatar]: Aparença predeterminada millor
* [controls/avatar]: Corregeix errors visuals
* Crea el component CheckableListItem
* [controls/avatar]: Escala la vora segons la mida de l'avatar
* Reverteix «[Avatar] Canvia el degradat del fons»
* Reverteix «[Avatar] Canvia l'amplada de la vora a 1px per a coincidir amb altres amplades molestes»
* [controls/avatar]: Fa que Avatar sigui accessible
* [controls/avatar]: Augmenta el farciment de l'alternativa de reserva d'icona
* [controls/avatar]: Fa que el mode d'imatge defineixi «sourceSize»
* [controls/avatar]: Ajusta la mida del text
* [controls/avatar]: Ajusta les tècniques usades per la forma circular
* [controls/avatar]: Afegeix una acció primària/secundària a Avatar
* Escriu al codi font el farciment de l'element de capçalera d'OverlaySheet
* QMake build: afegeix el codi font/capçalera de SizeGroup que mancava
* Icones de color, no botons (error 429972)
* Corregeix els botons d'avançar i retrocedir de la capçalera que no tenien amplada
* [BannerImage]: Corregeix el títol de capçalera que no estava centrat verticalment en els temes no Plasma

### KItemModels

* Afegeix la propietat «count», que permet la vinculació de «rowCount» en el QML

### KItemViews

* El KWidgetItemDelegate permet activar un «resetModel» des del KPluginSelector

### KNewStuff

* Fa obsolets els mètodes «standardAction» i «standardActionUpload»
* Corregeix el model del QtQuick si només hi ha un contingut, però no enllaços de baixada
* Afegeix un «dptr» a Cache, i mou el temporitzador del regulador allí per a corregir una fallada (error 429442)
* Refactoritza KNS3::Button per a usar internament el diàleg nou
* Crea una classe contenidora per al diàleg QML
* Verifica si la versió és buida abans de concatenar-la

### KNotification

* Millora la documentació de l'API del KNotification

### KParts

* Fa obsolet BrowserHostExtension

### KQuickCharts

* Usa una macro personalitzada per als missatges d'obsolescència en el QML
* Usa ECMGenerateExportHeader per a fer obsoletes les macros i usar-les
* El canvi d'interval no necessita netejar l'historial
* Canvia l'exemple de diagrama de línies contínues a la font d'intermediari d'historial
* Fa obsolet Model/ValueHistorySource
* Presenta HistoryProxySource com a substitut de Model/ValueHistorySource
* Afegeix categories d'enregistrament per a diagrames i les usa per a avisos existents

### KRunner

* [DBus Runner] Afegir la implementació per a icones personalitzades de mapes de píxels per als resultats
* Afegeix una clau per verificar si es va migrar la configuració
* Separa els fitxers de configuració i de dades
* API nova per a executar cerques de coincidències i per a l'historial
* No construeix RunnerContextTest en el Windows

### KService

* KSycoca: evita la reconstrucció de la base de dades si XDG_CONFIG_DIRS conté duplicats
* KSycoca: assegura que els fitxers extres estan ordenats per a les comparacions (error 429593)

### KTextEditor

* Reanomena «Variable:» a «Document:Variable:»
* Expansió de variables: corregeix el prefix de cerca de coincidències amb dos punts múltiples
* Mou el pintat des de KateTextPreview a KateRenderer
* S'assegura que només s'usen les línies en la vista per a pintar el mapa de píxels
* Usa KateTextPreview per a renderitzar el mapa de píxels
* Expansió de variables: afegeix la implementació per a %{Document:Variable:<name>}
* Mostra el text arrossegat en arrossegar (error 398719)
* Corregeix el desacoblament a TextRange::fixLookup()
* No pinta el fons de «currentLine» si hi ha una selecció superposada
* KateRegExpSearch: corregeix la lògica en afegir «\n» entre intervals de línies
* Reanomena l'acció «Commuta amb el contingut del porta-retalls»
* Afegeix una acció per a activar la còpia i l'enganxament com a una acció
* Fet: afegeix la icona d'acció «text-wrap» per a l'Ajust dinàmic de les paraules
* Desfà el sagnat en un pas (error 373009)

### KWidgetsAddons

* KSelectAction: afegeix els senyals no sobrecarregats «indexTriggered» i «textTriggered»
* KFontChooserDialog: gestiona el diàleg que suprimeix el pare durant l'«exec()»
* KMessageDialog: crida «setFocus()» en el botó predeterminat
* Adapta des de QLocale::Norwegian a QLocale::NorwegianBokmal
* Adapta KToolBarPopupActionTest a QToolButton::ToolButtonPopupMode

### KXMLGUI

* KXmlGui: en actualitzar un fitxer «.rc» local, manté les barres d'eines noves de l'aplicació
* Corregeix l'enregistrament de les tecles del «setWindow» abans que s'iniciï la captura (error 430388)
* Elimina la dependència sense ús del KWindowSystem
* Neteja el KXMLGUIClient al document XML en memòria després de desar les dreceres al disc

### Icones de l'Oxygen

* Afegeix «upindicator»

### Frameworks del Plasma

* Exposa la informació de l'error al plasmoide d'error d'una manera més estructurada
* [components] Connecta els mnemònics
* [svg] Inicia sempre el temporitzador SvgRectsCache des del fil correcte
* [PC3 ProgressBar] Estableix la vinculació per a l'amplada (error 430544)
* Corregeix la construcció al Windows + inversió de variables
* [PlasmaComponents/TabGroup] Corregeix la comprovació si l'element hereta des de la Page
* Adapta diversos components a «veryShortDuration» en passar per sobre
* Mou ListItems per a usar «veryShortDuration» per a passar-hi per sobre en lloc de «longDuration»
* Afegeix «veryShortDuration»
* No permet anys negatius de calendari (error 430320)
* Corregeix un fons trencat (error 430390)
* Substitueix les ID de la memòria cau de QString per una versió basada en una estructura
* [TabGroup] Inverteix les animacions en mode RTL
* Només elimina les dreceres en eliminar la miniaplicació, no la destrucció
* Oculta les accions contextuals desactivades d'ExpandableListItem

### Purpose

* KFileItemActions: afegeix «windowflag» del menú
* Share fileitemplugin: usa el Widget pare com a pare del menú (error 425997)

### QQC2StyleBridge

* Actualitza org.kde.desktop/Dialog.qml
* Dibuixa ScrollView usant Frame en lloc d'Edit (error 429601)

### Sonnet

* Millora el rendiment de «createOrderedModel» usant QVector
* Evita un avís en temps d'execució si no existeix cap resultat estimat

### Ressaltat de la sintaxi

* Ressaltat del C++: QOverload i similars
* Corregeix les etiquetes que comencen per un punt que no estan ressaltades en el GAS
* Ressaltat del C++: afegeix la macro «qGuiApp»
* Millora el tema Dracula
* Correcció núm. 5: Bash, Zsh: ! amb «if», «while», «until»; Bash: estil de patró per a «${var,patt}» i «${var^patt}»
* Correcció núm. 5: Bash, Zsh: comentaris en una matriu
* Sintaxi de la funcionalitat Cucumber
* Zsh: incrementa la versió de la sintaxi
* Zsh: corregeix l'expansió de claus en una ordre
* Afegeix «weakDeliminator» i «additionalDeliminator» amb «keyword», WordDetect, Int, Float, HlCOct i HlCHex
* Indexador: reinicia «currentKeywords» i «currentContext» en obrir una definició nova
* Zsh: moltes correccions i millores
* Bash: corregeix els comentaris a «case», expressió de seqüència i ) després de ]
* Gestiona correctament el color d'importació a la base i l'especialitza per al C/C++
* Actualitza el tema Monokai
* Verifica la correcció/presència d'estils personalitzats en els temes
* Afegeix els temes fosc/clar de GitHub
* Incrementa la versió, no canvia la versió del Kate fins que s'entengui perquè és necessari
* Afegeix llicències
* Afegeix els temes fosc/clar d'Atom One
* Mancava incrementar la versió en canviar
* Corregeix l'atribut i el color dels operadors en el Monokai
* Afegeix el tema Monokai
* CMake: Afegeix variables 3.19 que manquen i diverses noves afegides al 3.19.2
* Kotlin: corregeix diversos problemes i altres millores
* Groovy: corregeix diversos problemes i altres millores
* Scala: corregeix diversos problemes i altres millores
* Java: corregeix diversos problemes i altres millores
* Corregeix && i || en un subcontext i corregeix el patró del nom de funció
* Afegeix QRegularExpression::DontCaptureOption quan no hi ha cap regla dinàmica
* Bash: afegeix (...), ||, && a [[ ... ]]; afegeix l'accent obert a [ ... ] i [[ ... ]]

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
