---
aliases:
- ../../kde-frameworks-5.58.0
date: 2019-05-13
layout: framework
libCount: 70
---
### Baloo

- [baloo_file] Espera el procés de l'extractor abans d'iniciar
- [balooctl] Afegeix l'ordre per mostrar els fitxers que han fallat a la indexació (error 406116)
- Afegeix el QML als tipus de codi font
- [balooctl] Captura la «totalSize» constant al «lambda»
- [balooctl] Canvia la sortida multilínia a un ajudant nou
- [balooctl] Usa un ajudant nou a la sortida JSON
- [balooctl] Usa l'ajudant nou per al format de sortida senzill
- [balooctl] Desfactoritza la recopilació d'estats d'índex dels fitxers de la sortida
- Manté buits els documents de metadades JSON de la BBDD DocumentData
- [balooshow] Permet referenciar fitxers per l'URL del disc dur
- [balooshow] Suprimeix l'avís quan l'URL es refereix a un fitxer no indexat
- [MTimeDB] Permet un segell de temps més nou que el document més nou a l'interval de coincidència
- [MTimeDB] Usa una coincidència exacta quan es requereix una coincidència exacta
- [balooctl] Neteja la gestió dels arguments posicionals diferents
- [balooctl] Amplia el text d'ajuda de les opcions, millora la verificació d'errors
- [balooctl] Usa noms més intel·ligibles per a la mida a la sortida de l'estat
- [balooctl] Ordre «clear»: elimina una verificació errònia de «documentData», neteja
- [kio_search] Corregeix un avís, usa UDSEntry per a «.» al «listDir»
- Usa la notació hexadecimal per a l'enumeració d'indicador de DocumentOperation
- Calcula correctament la mida total de la BBDD
- Posposa l'anàlisi del terme fins que calgui, no defineix el terme i la cadena de cerca simultàniament
- No afegeix valors predeterminats als filtres de dates al JSON
- Usa un format compacte del JSON en convertir els URL de consulta
- [balooshow] No imprimeix un avís fals per als fitxers no indexats

### Icones Brisa

- Afegeix versions de 16px no simbòliques de «find-location» i «mark-location»
- Enllaç simbòlic de «preferences-system-windows-effect-flipswitch» a «preferences-system-tabbox»
- Afegeix un enllaç simbòlic de la icona «edit-delete-remove» i afegeix la versió 22px de «paint-none» i «edit-none»
- Usa una icona predeterminada coherent d'usuari del Kickoff
- Afegeix una icona per al KCM de Thunderbolt
- Aguditza les Z a les icones «system-suspend*»
- Millora la icona «widget-alternatives»
- Afegeix «go-up/down/next/previous-skip»
- Actualitza el logo del KDE per ser més fidel a l'original
- Afegeix una icona d'alternatives

### Mòduls extres del CMake

- Correcció d'error: cerca «stl» del C++ usant les expressions regulars
- Activa incondicionalment -DQT_STRICT_ITERATORS, no només en el mode depuració

### KArchive

- KTar: protegeix contra mides negatives de longitud d'enllaç
- Corregeix una escriptura no vàlida a memòria en fitxers «tar» mal formats
- Soluciona una fuita de memòria en llegir diversos fitxers «tar»
- Corregeix l'ús de memòria sense inicialitzar en llegir fitxers «tar» mal formats
- Corregeix un desbordament de memòria intermèdia en llegir fitxers mal formats
- Corregeix una desreferència nul·la en fitxers «tar» mal formats
- Instal·la la capçalera «krcc.h»
- Corregeix una supressió doble en fitxers trencats
- Inhabilita la còpia de KArchiveDirectoryPrivate i de KArchivePrivate
- Soluciona que KArchive::findOrCreate exhaureixi la pila en els camins molt llargs
- Presenta i usa KArchiveDirectory::addEntryV2
- «removeEntry» pot fallar, per tant, és bo conèixer si ho fa
- KZip: corregeix un ús de memòria en monticles després d'alliberar en fitxers trencats

### KAuth

- Força als ajudants del KAuth a permetre UTF-8 (error 384294)

### KBookmarks

- Afegeix la implementació al KBookmarkOwner per comunicar si té pestanyes obertes

### KCMUtils

- Usa indicacions de mida des del mateix ApplicationItem
- Corregeix el degradat de fons de l'Oxygen als mòduls QML

### KConfig

- Afegeix la capacitat Notify al KConfigXT

### KCoreAddons

- Corregeix els avisos erronis de «No s'ha pogut trobar el tipus de servei»
- Classe nova KOSRelease - un analitzador de fitxers «os-release»

### KDeclarative

- [KeySequenceItem] Fa que el botó de neteja tingui la mateixa alçada que el botó de drecera
- Traçador: Ajusta l'àmbit del programa GL a la vida útil del node de l'escenògraf (error 403453)
- KeySequenceHelperPrivate::updateShortcutDisplay: no mostra el text anglès a l'usuari
- [ConfigModule] Passa les propietats inicials al «push()»
- Activa de manera predeterminada la implementació de «glGetGraphicsResetStatus» a les Qt &gt;= 5.13 (error 364766)

### KDED

- Instal·la el fitxer «.desktop» per al «kded5» (error 387556)

### KFileMetaData

- [TagLibExtractor] Corregeix una fallada als fitxers Speex no vàlids (error 403902)
- Corregeix una fallada del «exivextractor» amb fitxers mal formats (error 405210)
- Declara les propietats com a tipus «meta»
- Canvia els atributs de les propietats per coherència
- Gestiona la llista de variants en funcions de format
- Esmena el tipus «LARGE_INTEGER» per al Windows
- Corregeix errors (compilació) de la implementació «UserMetaData» per al Windows
- Afegeix un tipus MIME que manca a l'escriptor de la «taglib»
- [UserMetaData] Gestiona correctament els canvis a la mida de les dades de l'atribut
- [UserMetaData] Desenreda el codi «stub» al Windows i Linux/BSD/Mac

### KGlobalAccel

- Copia el contenidor a Component::cleanUp abans d'iterar
- No usa «qAsConst» en una variable temporal (error 406426)

### KHolidays

- holidays/plan2/holiday_zm_en-gb - afegeix els festius de Zambia
- holidays/plan2/holiday_lv_lv - corregeix el Dia del mig de l'estiu
- holiday_mu_en - festius 2019 a Mauritius
- holiday_th_en-gb - actualització per al 2019 (error 402277)
- Actualitza els festius japonesos
- Afegeix els festius oficials per a la Baixa Saxònia (Alemanya)

### KImageFormats

- TGA: no intenta llegir més que «max_palette_size» de la paleta
- TGA: fa «memset» «dst» si falla la lectura
- TGA: fa «memset» de tota la matriu de la paleta, no només de la «palette_size»
- Inicialitza els bits no llegits de «_starttab»
- xcf: soluciona l'ús de memòria sense inicialitzar en documents trencats
- RAS: no excedeix la lectura de l'entrada en fitxers mal formats
- XCF: la capa és «const» en copiar i fusionar, la marca com a tal

### KIO

- [FileWidget] Substitueix «Filter:» per «File type:» en desar amb una llista limitada de tipus MIME (error 79903)
- Els fitxers «Enllaç a aplicació» creats de nou tenen una icona genèrica
- [Properties dialog] Usa la cadena «Free space» en lloc de «Disk usage» (error 406630)
- Omple UDSEntry::UDS_CREATION_TIME sota Linux quan la «glibc» &gt;= 2.28
- [KUrlNavigator] Corregeix la navegació de l'URL en sortir de l'arxiu amb «krarc» i el Dolphin (error 386448)
- [KDynamicJobTracker] Quan el «kuiserver» no estigui disponible, també pren com a alternativa el diàleg del giny (error 379887)

### Kirigami

- [aboutpage] Oculta la capçalera Authors si no hi ha cap autor
- Actualitza «qrc.in» perquè coincideixi amb «.qrc» (manca ActionMenuItem)
- Assegura que no es comprimeix l'ActionButton (error 406678)
- Pàgines: exporta els modes correctes de «contentHeight/implicit»
- [ColumnView] També verifica l'índex al filtre fill.
- [ColumnView] No permet que el botó enrere del ratolí torni més enllà de la primera pàgina
- La capçalera té immediatament la mida adequada

### KJobWidgets

- [KUiServerJobTracker] Segueix el temps de vida del servei «kuiserver» i torna a registrar els treballs si cal

### KNewStuff

- Elimina la vora pixelada (error 391108)

### KNotification

- [Notify by Portal] Permet una acció predeterminada i consells prioritaris
- [KNotification] Afegeix HighUrgency
- [KNotifications] Actualitza quan canviïn els indicadors, els URL o la urgència
- Permet definir la urgència de les notificacions

### Framework del KPackage

- Afegeix les propietats que manquen a «kpackage-generic.desktop»
- kpackagetool: llegeix «kpackage-generic.desktop» des de «qrc»
- Generació de l'AppStream: assegura que se cerquen dins l'estructura dels paquets que també tenen «metadata.desktop/json»

### KTextEditor

- Revisió de les pàgines de configuració del Kate per millorar la facilitat del manteniment
- Permet canviar el mode, després de canviar el ressaltat
- ViewConfig: usa una interfície de configuració genèrica nova
- Corregeix el pintat del mapa de píxels dels punts a la barra d'icones
- S'assegura que la manca de la vora esquerra no canvia els dígits de número del comptador de línia
- Corregeix la vista prèvia del plegat en moure el ratolí des de baix cap a dalt
- Revisió de l'IconBorder
- Afegeix mètodes d'entrada al botó de la barra d'estat dels mètodes d'entrada
- Pinta el marcador de plegat en el color adequat i el fa més visible
- Elimina la drecera predeterminada F6 per mostrar la vora de la icona
- Afegeix una acció per commutar el plegat dels intervals fills (error 344414)
- Retitula el botó «Tanca» a «Tanca el fitxer» quan un fitxer s'ha eliminat del disc (error 406305)
- Indica els drets d'autor, potser això també hauria de ser un «define»
- Evita les dreceres conflictives per commutar entre pestanyes
- KateIconBorder: Corregeix l'amplada i l'alçada del missatge emergent de plegat
- Evita el salt de vista a la part inferior en els canvis del plegat
- DocumentPrivate: respecta el mode de sagnat en seleccionar un bloc (error 395430)
- ViewInternal: Corregeix «makeVisible(...)» (error 306745)
- DocumentPrivate: Fa més intel·ligent la gestió dels parèntesis (error 368580)
- ViewInternal: Revisa l'esdeveniment de deixar anar
- Permet tancar un document que el seu fitxer s'ha suprimit del disc
- KateIconBorder: Usa un caràcter UTF-8 en lloc d'un mapa de píxels especial com a indicador d'ajust dinàmic
- KateIconBorder: Assegura que es mostra el marcador d'ajust dinàmic
- KateIconBorder: Embelliment del codi
- DocumentPrivate: Accepta el parèntesi automàtic en seleccionar un bloc (error 382213)

### KUnitConversion

- Corregeix la conversió de l/100 km a MPG (error 378967)

### Framework del KWallet

- Defineix correctament «kwalletd_bin_path»
- Exporta el camí de l'executable «kwalletd» per al «kwallet_pam»

### KWayland

- Afegeix el tipus de finestra CriticalNotification al protocol PlasmaShellSurface
- Implementa la interfície de servidor «wl_eglstream_controller»

### KWidgetsAddons

- Actualitza les «kcharselect-data» a Unicode 12.1
- Model intern del KCharSelect: assegura que «rowCount()» és 0 per als índexs vàlids

### KWindowSystem

- Presenta CriticalNotificationType
- Implementa NET_WM_STATE_FOCUSED
- Documenta que «modToStringUser» i «stringUserToMod» només tracten cadenes de l'anglès

### KXMLGUI

- KKeySequenceWidget: no mostra cadenes en anglès a l'usuari

### NetworkManagerQt

- WireGuard: no requereix que la «private-key» sigui no buida per a les «private-key-flags»
- WireGuard: solució temporal del tipus d'indicador incorrecte
- WireGuard: «private-key» i «preshared-keys» es poden requerir juntes

### Frameworks del Plasma

- PlatformComponentsPlugin: corregeix l'IID del connector a QQmlExtensionInterface
- IconItem: elimina els fragments romanents i sense ús de la propietat «smooth»
- [Dialog] Afegeix el tipus CriticalNotification
- Corregeix els noms de grups incorrectes per 22, 32 px a «audio.svg»
- Fa que la barra d'eines de text mòbil només aparegui en prémer
- Usa el Kirigami.WheelHandler nou
- Afegeix més mides d'icones per a «audio», «configure», «distribute»
- [FrameSvgItem] Actualitza el filtratge en canvis suaus
- Air/Oxygen desktoptheme: corregeix l'alçada de la barra de progrés usant «hint-bar-size»
- Corregeix el funcionament del full d'estil per a «audio-volume-medium»
- Actualitza les icones «audio», «drive», «edit», «go», «list», «media», «plasmavault» per a concordar amb les icones del Brisa
- Alinea les «z» a la quadrícula dels píxels a «system.svg»
- Usa el «mobiletextcursor» de l'espai de noms adequat
- [FrameSvgItem] Respecta la propietat «smooth»
- Oxygen desktoptheme: afegeix farciment a les mans, contra el contorn dentat en la rotació
- SvgItem, IconItem: descarta la sobreescriptura de la propietat «smooth», actualitza el node en canviar
- Admet la compressió «gzip» dels SVGZ també al Windows, usant «7z»
- Air/Oxygen desktoptheme: corregeix els desplaçaments de les mans amb «hint-*-rotation-center-offset»
- Afegeix una API pública invocable per emetre «contextualActionsAboutToShow»
- Breeze desktoptheme clock: admet la indicació del desplaçament de l'ombra de les manetes del Plasma 5.16
- Manté els fitxers SVG del «desktoptheme» sense comprimir al repositori, instal·la SVGZ
- Selecció de text mòbil separada per evitar importacions recursives
- Usa la icona i el text «Alternatives» més adequats
- FrameSvgItem: afegeix la propietat «mask»

### Prison

- Aztec: esmena el farciment si l'última «codeword» parcial és tota bits d'u

### QQC2StyleBridge

- Evita Controls imbricats a TextField (error 406851)
- Fa que la barra d'eines de text mòbil només aparegui en prémer
- [TabBar] Actualitza l'alçada quan s'afegeix TabButtons dinàmicament
- Usa el Kirigami.WheelHandler nou
- Admet una mida d'icona personalitzada per a ToolButton
- Compila bé sense «foreach»

### Solid

- [Fstab] Afegeix la implementació per a sistemes de fitxers sense xarxa
- [FsTab] Afegeix memòria intermèdia per al tipus de sistema de fitxers del dispositiu
- [Fstab] Treball preparatori per activar sistemes de fitxers més enllà de NFS/SMB
- Corregeix l'error quan no hi ha cap membre anomenat «setTime_t» a «QDateTime» en construir (error 405554)

### Ressaltat de la sintaxi

- Afegeix el ressaltat de sintaxi per a l'intèrpret del «fish»
- AppArmor: no ressalta les assignacions de variables ni regles d'àlies als perfils

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
