---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE levererar Program 14.12.3.
layout: application
title: KDE levererar KDE-program 14.12.3
version: 14.12.3
---
3:e mars, 2015. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../14.12.0'>KDE-program 14.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Med 19 registrerade felrättningar omfattar förbättringar av anagramspelet Kanagram, UML-modelleringsverktyget Umbrello, dokumentvisaren Okular och geometriprogrammet Kig.

Utgåvan inkluderar också versioner för långtidsunderhåll av Plasma arbetsrymder 4.11.17, KDE:s utvecklingsplattform 4.14.6 och Kontakt-sviten 4.14.6.
