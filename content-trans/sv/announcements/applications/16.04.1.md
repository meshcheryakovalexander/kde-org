---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE levererar KDE-program 16.04.1
layout: application
title: KDE levererar KDE-program 16.04.1
version: 16.04.1
---
10:e maj, 2016. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../16.04.0'>KDE-program 16.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 25 registrerade felrättningar omfattar förbättringar av bland annat kdepim, ark, kate, dolphin, kdenlive, lokalize och spectacle.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.20.
