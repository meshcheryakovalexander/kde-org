---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### I många ramverk

- Byt namn på privata klasser för att undvika att de exporteras av misstag

### Baloo

- Lägg till org.kde.baloo gränssnitt för rotobjekt för bakåtkompatibilitet
- Installera en falsk org.kde.baloo.file.indexer.xml för att rätta kompilering av plasma-desktop 5.4
- Organisera om D-Bus gränssnitten
- Använd json-metadata i kded-insticksprogram och rätta insticksprogrammets namn
- Skapa en databasinstans per process (fel 350247)
- Förhindra att baloo_file_extractor dödas vid registrering
- Skapa XML-gränssnittsfil med qt5_generate_dbus_interface
- Rättning av övervakning i Baloo
- Flytta export av filwebbadress till huvudtråd
- Säkerställ att konfigurationer i kaskad tas hänsyn till
- Installera inte namnlänk för privat bibliotek
- Installera översättningar, upptäckt av Hrvoje Senjan.

### BluezQt

- Skicka inte vidare signalen deviceChanged efter att enheten har tagits bort (fel 351051)
- Respektera -DBUILD_TESTING=OFF

### Extra CMake-moduler

- Lägg till makron för att skapa deklarationer av loggningskategori för Qt5.
- ecm_generate_headers: Lägg till alternativet COMMON_HEADER och funktionalitet för flera deklarationer
- Lägg till -pedantic för KF5-kod (när gcc eller clang används)
- KDEFrameworkCompilerSettings: Aktivera bara strikta iteratorer i felsökningsläge
- Ställ också in normal synlighet för C-kod till dold.

### Integrering med ramverk

- Propagera också fönsterrubriker för fildialogrutor enbart för kataloger.

### KActivities

- Starta bara en åtgärdsinläsare (tråd) när åtgärderna i FileItemLinkingPlugin inte är initierade (fel 351585)
- Rättade byggproblem som introducerades genom att byta namn på privata klasser (11030ffc0)
- Lägg till saknade deklarationssökväg till boost för att bytta på OS X
- Inställning av genvägar flyttade till aktivitetsinställningar
- Att ställa in privat aktivitetsläge fungerar
- Omstrukturering av användargränssnittet för inställning
- Grundläggande aktivitetsmetoder är funktionella
- Användargränssnitt för inställning av aktiviteter och borttagning av meddelanderutor
- Grundläggande användargränssnitt för att skapa, ta bort, ställa in aktiviteter i IM-sektion
- Ökade styckesstorleken för inläsning av resultat
- Lägg till saknad inkludering av std::set

### KDE Doxygen-verktyg

- Rättning för Windows: Ta bort befintliga filer innan de ersätts med os.rename.
- Använd specifika sökvägar när python anropas för att rätta byggning på Windows

### KCompletion

- Rätta dåligt beteende / slut på minne på Windows (fel 345860)

### KConfig

- Optimera readEntryGui
- Undvik QString::fromLatin1() i genererad kod
- Minimera anrop till kostsam QStandardPaths::locateAll()
- Avsluta konverteringen till QCommandLineParser (nu har den addPositionalArgument)

### Stöd för KDELibs 4

- Konvertera solid-networkstatus kded-insticksprogram till json-metadata
- KPixmapCache: Skapa katalog om den inte finns

### KDocTools

- Synkronisera katalanska user.entities med den engelska versionen (en).
- Lägg till entiteter för sebas och plasma-pa

### KEmoticons

- Prestanda: Lagra en instans av KEmoticons här, inte KEmoticonsTheme.

### KFileMetaData

- PlainTextExtractor: Aktivera grenen O_NOATIME på GNU libc-plattformar
- PlainTextExtractor: Gör så att Linux-grenen också fungerar utan O_NOATIME
- PlainTextExtractor: Rätta felkontroll vid misslyckad open(O_NOATIME)

### KGlobalAccel

- Starta bara kglobalaccel5 om det behövs.

### KI18n

- Hantera avsaknad av nyrad sist i pmap-fil snyggt

### KIconThemes

- KIconLoader: Rätta att reconfigure() glömmer bort ärvda teman och programkataloger
- Följ specifikationen för ikoninläsning bättre

### KImageFormats

- eps: Rätta inkluderingar relaterade till QT:s katerogiserade loggning

### KIO

- Använd Q_OS_WIN istället för Q_OS_WINDOWS
- Gör så att KDE_FORK_SLAVES fungerar på Windows
- Inaktivera installation av skrivbordsfiler för kded-modulen ProxyScout
- Tillhandahåll deterministisk sorteringsordning för KDirSortFilterProxyModelPrivate::compare
- Visa egna katalogikoner igen (fel 350612)
- Flytta kpasswdserver från kded till kiod
- Rätta konverteringsfel i kpasswdserver
- Ta bort föråldrad kod för att kommunicera med mycket gamla versioner av kpasswdserver.
- KDirListerTest: Använd QTRY_COMPARE för båda satserna, för att rätta kapplöpning påvisad av CI
- KFilePlacesModel: Implementera gammal ATT GÖRA om att använda trashrc istället för en fullskalig KDirLister.

### KItemModels

- Ny proxymodell: KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: Rätta hantering av layoutChanged.
- Mer kontroll av markeringen efter sortering.
- KExtraColumnsProxyModel: Rätta fel i sibling() som t.ex. fick markeringar att sluta fungera

### Paketet Framework

- kpackagetool kan avinstallera ett paket från en paketfil
- kpackagetool är nu smartare när det gäller att hitta rätt tjänsttyp

### KService

- KSycoca: Kontrollera tidsstämplar och kör kbuildsycoca om det behövs. Inga beroenden på kded längre.
- Stäng inte ksycoca direkt efter att den har öppnats.
- KPluginInfo hanterar nu FormFactor-metadata riktigt

### KTextEditor

- Sammanfoga allokering av TextLineData och block för referensräkning.
- Ändra tangentbordets standardgenväg för "gå till föregående redigeringsrad"
- Rättningar av syntaxfärgläggning för Haskell-kommentarer
- Snabba upp visning av meddelanderuta för kodkomplettering
- minimap: Försök att förbättra utseende och känsla (fel 309553)
- Syntaxfärgläggning för nästlade kommentarar i Haskell
- Rätta problem med felaktig avindentering för python (fel 351190)

### KWidgetsAddons

- KPasswordDialog: Låt användaren ändra lösenordets synlighet (fel 224686)

### KXMLGUI

- Rätta att KSwitchLanguageDialog inte visar de flesta språk

### KXmlRpcClient

- Undvik QLatin1String ifall den allokerar heap-minne

### ModemManagerQt

- Rätta metatypkonflikt med senaste ändring av nm-qt

### NetworkManagerQt

- Lägg till nya egenskaper från senaste NM-utgåvor

### Plasma ramverk

- Ändra föräldraskap till bläddringsbar om möjligt
- Rätta paketlistning
- plasma: Rätta att miniprogramåtgärder kan vara nollpekare (fel 351777)
- Signalen onClicked i PlasmaComponents.ModelContextMenu fungerar nu som den ska
- PlasmaComponents ModelContextMenu kan nu skapa menysektioner
- Konvertera kded-insticksprogrammet platformstatus till json-metadata...
- Hantera ogiltig metadata i PluginLoader
- Låt RowLayout räkna ut etikettens storlek
- Visa alltid redigeringsmenyn när markören är synlig.
- Rätta snurra med ButtonStyle
- Ändra inte en knapps flathet när den klickas
- På pekskärm och mobil är rullningslister tillfälliga
- Justera blädderhastighet och retardation till punkter per tum
- Egen markördelegat bara för mobil
- Pekvänlig textmarkör
- Rättade princip för föräldraskap och uppdykning
- Deklarera __editMenu
- Lägg till saknad hanteringsdelegater för markör
- Skriv om implementering av EditMenu
- Använd bara mobilmenyn villkorligt
- Ändra menyns föräldraskap till rot

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
