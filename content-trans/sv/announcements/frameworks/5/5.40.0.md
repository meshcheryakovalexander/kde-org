---
aliases:
- ../../kde-frameworks-5.40.0
date: 2017-11-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Anse att DjVu-filer är dokument (fel 369195)
- Rätta stavning så att WPS Office-presentationer känns igen på ett riktigt sätt

### Breeze-ikoner

- Lägg till folder-stash för Dolphins verktygsradikon stash

### KArchive

- Rätta möjlig minnesläcka. Rätta logik

### KCMUtils

- Inga marginaler för QML-moduler från qwidget-sidan
- Initiera variabler (hittades av Coverity)

### KConfigWidgets

- Rätta ikon för KStandardAction::MoveToTrash

### KCoreAddons

- Rätta detektering av webbadress för dubbla webbadresser som "http://www.exempel.com&lt;http://exempel.com/&gt;"
- Använd HTTPS för KDE-webbadresser

### Stöd för KDELibs 4

- Fullständig dokumentation för ersättning av disableSessionManagement()
- Gör så att kssl kompilerar med OpenSSL 1.1.0 (fel 370223)

### KFileMetaData

- Rätta visat namn för Generator-egenskap

### KGlobalAccel

- KGlobalAccel: Rätta stöd för numeriska tangenter (igen)

### KInit

- Rätt installation av start_kdeinit när DESTDIR och libcap används tillsammans

### KIO

- Rätta visning av remote:/ i qfiledialog
- Implementera stöd för kategorier i KfilesPlacesView
- HTTP: Rätta felsträng för fallet 207 Multi-Status
- KNewFileMenu: Städa död kod, upptäckt av Coverity
- IKWS: Rätta möjlig oändlig snurra, upptäckt av Coverity
- KIO::PreviewJob::defaultPlugins() funktion

### Kirigami

- Syntax fungerar på äldre Qt 5.7 (fel 385785)
- Lagra overlaysheet annorlunda (fel 386470)
- Visa delegaten färglagd på ett riktigt sätt också när det inte finns något fokus
- Tips om önskad storlek för avskiljaren
- Korrigera användning av Settings.isMobile
- Tillåt att program är något konvergerande på ett desktop-y system
- Säkerställ att innehållet i SwipeListItem inte överlappar med greppet (fel 385974)
- scrollview för Overlaysheet  är alltid ointeractive
- Lägg till kategorier i skrivbordsfilen för galleri (fel 385430)
- Uppdatering filen kirigami.pri
- Använd det icke installerade insticksprogrammet för att göra testerna
- Avråd från användning av Kirigami.Label
- Konvertera galleri-exemplets användning av Labels för att vara konsekvent QQC2
- Konvertera användningar av Kirigami.Label i Kirigami.Controls
- Gör rullningsområdet interaktivt vid beröringshändelser
- Flytta git-anropet find_package till där det används
- Använd normalt genomskinliga listvyobjekt

### KNewStuff

- Ta bort PreferCache från nätverksbegäran
- Koppla inte loss delade pekare till privat data när förhandsgranskningar ställs in
- KMoreTools: Uppdatera och rätt skrivbordsfiler (fel 369646)

### KNotification

- Ta bort kontroll av SNI-värddatorer vid val av användning av äldre läge (fel 385867)
- Kontrollera bara äldre ikoner för systembrickan om någon ska skapas (fel 385371)

### Ramverket KPackage

- Använd de icke installerade tjänstfilerna

### KService

- Initiera värden
- Initiera vissa pekare

### KTextEditor

- Dokumentation av programmeringsgränssnitt: Rätta felaktiga namn på metoder och argument, lägg till saknad \\since
- Undvik (vissa) krascher när QML-skript körs (fel 385413)
- Undvik en QML-krasch triggad av C-stil indenteringsskript
- Öka storlek på efterföljande markering
- Förhindra viss indentering från att indentera vid slumpmässiga tecken
- Rätta varning som avråder från användning

### KTextWidgets

- Initiera värde

### Kwayland

- [klient] Ta bort kontroller av att platformName är "wayland"
- Duplicera inte anslutning till wl_display_flush
- Wayland främmande protokoll

### KWidgetsAddons

- Rätta inkonsekvent createKMessageBox fokuskomponent
- Kompaktare lösenordsdialogruta (fel 381231)
- Ställ in bredd av KPageListView riktigt

### KWindowSystem

- KKeyServer: Rätta hantering av Meta+Skift+Print, Alt+Skift+piltangent etc.
- Stöd flatpak-plattform
- Använd det egna programmeringsgränssnittet för detektering av plattform i KWindowSystem istället för duplicerad kod

### KXMLGUI

- Använd HTTPS för KDE-webbadresser

### NetworkManagerQt

- 8021xSetting: domain-suffix-match är definierad i NM 1.2.0 och senare
- Stöd "domain-suffix-match" i Security8021xSetting

### Plasma ramverk

- Rita cirkelbågen manuellt
- [PlasmaComponents Menu] Lägg till ungrabMouseHack
- [FrameSvg] Optimera updateSizes
- Positionera inte en dialogruta om den är av typen skärmvisning (OSD)

### QQC2StyleBridge

- Förbättra kompilering som ett statiskt insticksprogram
- Gör alternativknappen till en alternativknapp
- Använd qstyle för att rita upp visaren
- Använd en ColumnLayout för menyer
- Rätta dialogruta
- Ta bort ogiltig gruppegenskap
- Rätta formatering av md-filen så att den motsvarar övriga moduler
- Beteende hos kombinationsruta närmare QQC1
- provisorisk lösning för QQuickWidgets

### Sonnet

- Lägg till metoden assignByDictionnary
- Signalera om vi just ska tilldela ordlista

### Syntaxfärgläggning

- Makefile: Rätta matchning av reguljära uttryck i "CXXFLAGS+"

### ThreadWeaver

- CMake-städning: Hårdkoda inte -std=c++0x

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
